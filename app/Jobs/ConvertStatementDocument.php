<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Events\StatementCreated;

use \ConvertApi\ConvertApi as ConvertApi;

class ConvertStatementDocument implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $src;

    public function __construct($src)
    {
        $this->src = $src;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ConvertApi::setApiSecret('JsO4EZ0arrq7th9c');

        $result = ConvertApi::convert('pdf', [
            'File' => '/var/www/html/projects/bilimger-backend/public/storage/'.$this->src.'.docx'
        ], 'doc');

        $result->saveFiles('/var/www/html/projects/bilimger-backend/public/storage/'.$this->src.'.pdf');

        $statement = Statement::query()
            ->where('src', 'storage/'.$this->src.'.pdf')
            ->first();

        StatementCreated::dispatch($statement);
    }
}
