<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use App\Observers\CatoLangObserver;
use App\Observers\CatoObserver;

use App\Listeners\SendStatementRouteNotification;
use App\Listeners\SendStatementNotification;
use App\Listeners\SendTimetableNotification;
use App\Listeners\ConvertStatementDocument;
use App\Listeners\SendTidingNotification;
use App\Listeners\SendEventNotification;

use App\Events\StatementRouteProcessed;
use App\Events\StatementCompleted;
use App\Events\StatementCreated;
use App\Events\TimetableСhanged;
use App\Events\TidingCreated;
use App\Events\EventCreated;

use App\Models\CatoLang;
use App\Models\Cato;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        StatementRouteProcessed::class => [
            SendStatementRouteNotification::class
        ],
        StatementCompleted::class => [
            SendStatementNotification::class
        ],
        TimetableСhanged::class => [
            SendTimetableNotification::class
        ],
        TidingCreated::class => [
            SendTidingNotification::class
        ],
        EventCreated::class => [
            SendEventNotification::class
        ],
        StatementCreated::class => [
            ConvertStatementDocument::class
        ],
        StatementCommentCreated::class => [
            SendStatementCommentNotification::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //  Cato::observe(CatoObserver::class);
        //  CatoLang::observe(CatoLangObserver::class);
    }
}
