<?php

namespace App\Traits;

use App\Models\ActivityLog;
use App\Models\Visit;

trait Observer
{
	protected static function booted() {
		static::created(function ($model) {
			$primary_key = null;

			if ($model->getTable() == 'id_types_countries') {
				$primary_key = implode('-', [
					$model->id_type_id,
					$model->country_id
				]);
			}

			if ($model->getTable() == 'professions_academic_degrees') {
				$primary_key = implode('-', [
					$model->profession_id,
					$model->academic_degree_id
				]);
			}

			if ($model->getTable() == 'professions_departments') {
				$primary_key = implode('-', [
					$model->profession_id,
					$model->department_id
				]);
			}

			if ($model->getTable() == 'notification_views_users_roles') {
				$primary_key = implode('-', [
					$model->view_id,
					$model->user_id,
					$model->role_id
				]);
			}

			if ($model->getTable() == 'students_study_groups') {
				$primary_key = implode('-', [
					$model->student_id,
					$model->study_group_id
				]);
			}

			if ($model->getTable() == 'statement_student') {
				$primary_key = implode('-', [
					$model->statement_id,
					$model->student_id
				]);
			}

			ActivityLog::create([
				'table' => $model->getTable(),
				'action' => 'insert',
				'visit_id' => request()->bearerToken() ? Visit::where('token', request()->bearerToken())->first()->id : null,
				'key' => $primary_key ? $primary_key : $model->id,
				'new' => $model->getAttributes()
			]);
		});

		static::updated(function ($model) {
			$olds = [];
			$news = [];

			foreach ($model->getChanges() as $key => $value) {
				$olds[$key] = $model->getOriginal()[$key];
				$news[$key] = $model->getAttributes()[$key];
			}

			$primary_key = null;

			if ($model->getTable() == 'id_types_countries') {
				$primary_key = implode('-', [
					$model->id_type_id,
					$model->country_id
				]);
			}

			if ($model->getTable() == 'professions_academic_degrees') {
				$primary_key = implode('-', [
					$model->profession_id,
					$model->academic_degree_id
				]);
			}

			if ($model->getTable() == 'professions_departments') {
				$primary_key = implode('-', [
					$model->profession_id,
					$model->department_id
				]);
			}

			if ($model->getTable() == 'notification_views_users_roles') {
				$primary_key = implode('-', [
					$model->view_id,
					$model->user_id,
					$model->role_id
				]);
			}

			if ($model->getTable() == 'students_study_groups') {
				$primary_key = implode('-', [
					$model->student_id,
					$model->study_group_id
				]);
			}

			if ($model->getTable() == 'statement_student') {
				$primary_key = implode('-', [
					$model->statement_id,
					$model->student_id
				]);
			}

			ActivityLog::create([
				'table' => $model->getTable(),
				'action' => 'update',
				'visit_id' => request()->bearerToken() ? Visit::where('token', request()->bearerToken())->first()->id : null,
				'key' => $primary_key ? $primary_key : $model->id,
				'old' => $olds,
				'new' => $news
			]);
		});

		static::deleted(function ($model) {
			ActivityLog::create([
				'table' => $model->getTable(),
				'action' => 'delete',
				'visit_id' => request()->bearerToken() ? Visit::where('token', request()->bearerToken())->first()->id : null,
				'key' => $model->id
			]);
		});
	}
}
