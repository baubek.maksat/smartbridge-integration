<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\UserEmailConfirm;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user_email_confirm;

    public function __construct(UserEmailConfirm $user_email_confirm)
    {
        $this->user_email_confirm = $user_email_confirm;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user.welcome')
            ->subject('Добро пожаловать')
            ->with([
                'user' => $this->user_email_confirm->user
            ]);
    }
}
