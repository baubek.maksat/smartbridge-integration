<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\UserEmailRestore;

class Restore extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user_email_restore;

    public function __construct(UserEmailRestore $user_email_restore)
    {
        $this->user_email_restore = $user_email_restore;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user.restore')
            ->subject('Восстановление пароля')
            ->with([
                'user' => $this->user_email_restore->user,
                'hash' => $this->user_email_restore->hash
            ]);
    }
}
