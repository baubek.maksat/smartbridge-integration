<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Models\University;
use App\Models\User;
use App\Models\Role;

class RoleMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
	public $role;
	public $university;

    public function __construct(User $user, Role $role, University $university)
    {
        $this->user = $user;
		$this->role = $role;
		$this->university = $university;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user.role')
			->subject('Был предоставлен доступ к «'.$this->university->name.'»')
			->with([
				'user' => $this->user,
				'role' => $this->role,
				'university' => $this->university
			]);
    }
}
