<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Models\StatementStatus;
use App\Models\StatementStage;
use App\Models\Statement;

use Carbon\Carbon;

use DB;

class Kernel extends ConsoleKernel
{
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */

	public function addWorkDay($schedule, $date, $count)
	{
		$dates = collect();
				
		$i = 1;
				
		while (true) {
			$new_date = Carbon::parse($date)->addDay($i);
				
			if (!$schedule->where('week_id', $new_date->format('N'))->where('time_start', '00:00:00')->where('time_end', '00:00:00')->first()) {
				$dates->push($new_date->format('Y-m-d H:i:s'));
			}
					
			if ($dates->count() == $count) break;
					
			$i++;
		}
				
		return $dates->last();
	}

	function addWorkHour($schedule, $datetime, $minutes)
	{
		$datetime = Carbon::parse($datetime);

		$i = 0;

		while (true) {
			if (!$schedule->where('week_id', $datetime->format('N'))->where('time_start', '00:00:00')->where('time_end', '00:00:00')->first()) {
				$new_datetime = Carbon::parse($datetime)->addMinutes($minutes);

				if ($new_datetime->format('Y-m-d H:i:s') <= $datetime->format('Y-m-d').' '.$schedule->where('week_id', $datetime->format('N'))->first()->time_end) {
					return $new_datetime->format('Y-m-d H:i:s');
				} else {
					$minutes -= Carbon::parse($datetime)->diffInMinutes($datetime->format('Y-m-d').' '.$schedule->where('week_id', $datetime->format('N'))->first()->time_end, false);

					$datetime = Carbon::parse($datetime->format('Y-m-d').' '.$schedule->where('week_id', $datetime->format('N'))->first()->time_start);
				}

				$i++;
			}

			$datetime->addDay();



			if ($i == 100) break;
		}
	}

    protected function schedule(Schedule $schedule)
	{
		$schedule->call(function() {
			$datetime_now = Carbon::now();

			$statements = Statement::query()
				->whereHas('status', function($query) {
					$query->whereIn('slug', ['waiting', 'processing']);
				})
				//	->whereNotNull('responsible_user_id')
				->whereNull('expired_at')
				->get();

			if ($statements) {
				foreach ($statements as $statement) {
					DB::transaction(function() use ($statement, $datetime_now) {
						$current_operating_mode = [
							'time_start' => $statement->view->category->ssc->currentOperatingMode->time_start,
							'time_end' => $statement->view->category->ssc->currentOperatingMode->time_end
						];

						if ($current_operating_mode['time_start'] == '00:00:00' && $current_operating_mode['time_end'] == '00:00:00') {
							return true;
						}

						if ($current_operating_mode['time_start'] >= $datetime_now->format('H:i:s') || $current_operating_mode['time_end'] <= $datetime_now->format('H:i:s')) {
							return true;
						}

						if ($statement->responsible_user_id) {
							$status = StatementStatus::where('slug', 'processing')->first();
						}

						if (!$statement->responsible_user_id) {
							$status = StatementStatus::where('slug', 'waiting')->first();
						}

						$statement->update([
							'status_id' => $status->id,
							'started_at' => $statement->started_at ? $statement->started_at : $datetime_now->format('Y-m-d H:i:s'),
							'expired_at' => $this->addWorkDay($statement->view->category->ssc->operating_modes, $datetime_now->format('Y-m-d H:i:s'), $statement->view->term_of_consideration)
						]);

						if ($statement->stages) {
							foreach ($statement->stages as $stage) {	
								foreach ($stage->routes as $route) {
									$route->update([
										'expired_at' => $route->term ? $this->addWorkHour($statement->view->category->ssc->operating_modes, $datetime_now->format('Y-m-d H:i:s'), $route->term * 60) : null
									]);
								}

								break;
							}
						}
					});
				}
			}
		})->everyMinute();
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands()
	{
		$this->load(__DIR__.'/Commands');

		require base_path('routes/console.php');
	}
}
