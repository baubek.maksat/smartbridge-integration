<?php

namespace App\Console\Commands\Integration\Platonus\Import;

use Illuminate\Console\Command;

use App\Models\Faculty;
use Illuminate\Support\Facades\Http;

class SearchServiceForAdministrativeFine extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'kpsisu-s-3338:run';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		/*
		$response = Http::post('http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso', [
			'name' => 'Steve',
			'role' => 'Network Administrator',
		])->withHeaders([
			'Content-Type' => 'text/xml',
		])->withBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Body><CapitalCity xmlns="http://www.oorsprong.org/websamples.countryinfo"><sCountryISOCode>IN</sCountryISOCode></CapitalCity></Body></Envelope>', 'xml');

		dd($response->body());

		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => 'http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>
				'<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
					<Body>
						<CapitalCity xmlns="http://www.oorsprong.org/websamples.countryinfo">
							<sCountryISOCode>IN</sCountryISOCode>
						</CapitalCity>
					</Body>
				</Envelope>',
			CURLOPT_HTTPHEADER => [
				'Content-Type: text/xml'
			],
		]);

		$response = curl_exec($curl);

		echo $response;

		curl_close($curl);

		*/

		/**$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => 'http://192.168.39.30/bip-sync-wss-gost',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>
				'
				',
			CURLOPT_HTTPHEADER => [
				'Content-Type: text/xml'
			],
		]);*/

        /*

		$xml = '<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<SOAP-ENV:Header xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
			<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soap:mustUnderstand="1">
				<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="SIG-BEFF7CB55C69AB1BB5147624829678710">
					<ds:SignedInfo>
						<ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
							<ec:InclusiveNamespaces xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#" PrefixList="soap"/>
						</ds:CanonicalizationMethod>
						<ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gost34310-gost34311"/>
						<ds:Reference URI="#id-BEFF7CB55C69AB1BB514762482966309">
							<ds:Transforms>
								<ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">
									<ec:InclusiveNamespaces xmlns:ec="http://www.w3.org/2001/10/xml-exc-c14n#" PrefixList=""/>
								</ds:Transform>
							</ds:Transforms>
							<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gost34311"/>
							<ds:DigestValue>vUl211ZIPO+aw7vuM+WQf/OSmrRSSwXzbVysKyuhOEQ=</ds:DigestValue>
						</ds:Reference>
					</ds:SignedInfo>
					<ds:SignatureValue>KOOGKOHg+JwbbYOVrE1escS/RB6ndFRa4NhSwYAqBddlVTj7gqA72XuCx99CuuQ+/7k4a/ZMsG35kA5wQ25vyA==</ds:SignatureValue>
					<ds:KeyInfo Id="KI-BEFF7CB55C69AB1BB514762482966307">
						<wsse:SecurityTokenReference wsu:Id="STR-BEFF7CB55C69AB1BB514762482966308">
							<wsse:KeyIdentifier EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary" ValueType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3">MIIE/jCCBKigAwIBAgIUIot+st4VFxykmapdDov4vudlWlAwDQYJKoMOAwoBAQECBQAwUzELMAkGA1UEBhMCS1oxRDBCBgNVBAMMO9Kw0JvQotCi0KvSmiDQmtCj05jQm9CQ0J3QlNCr0KDQo9Co0Ksg0J7QoNCi0JDQm9Cr0pogKEdPU1QpMB4XDTIyMDQxMjEyMzMzMVoXDTIzMDQxMjEyMzMzMVowggGdMRswGQYDVQQEDBLQntCg0JDQl9CQ0JvQmNCV0JIxKDAmBgNVBAMMH9Ce0KDQkNCX0JDQm9CY0JXQkiDQotCQ0KPQmtCV0J0xGDAWBgNVBAUTD0lJTjk0MTEwNjMwMDY2OTELMAkGA1UEBhMCS1oxGDAWBgNVBAsMD0JJTjE5MDc0MDAwOTY1NDEbMBkGA1UEKgwS0JHQldCT0JDQm9Cr0rDQm9CrMYHLMIHIBgNVBAoMgcDQk9Ce0KHQo9CU0JDQoNCh0KLQktCV0J3QndCe0JUg0KPQp9Cg0JXQltCU0JXQndCY0JUgItCc0JjQndCY0KHQotCV0KDQodCi0JLQniDQrdCa0J7Qm9Ce0JPQmNCYLCDQk9CV0J7Qm9Ce0JPQmNCYINCYINCf0KDQmNCg0J7QlNCd0KvQpSDQoNCV0KHQo9Cg0KHQntCSINCg0JXQodCf0KPQkdCb0JjQmtCYINCa0JDQl9CQ0KXQodCi0JDQnSIxKDAmBgkqhkiG9w0BCQEWGVQuT1JBWkFMSUVWQEVDT0dFTy5HT1YuS1owbDAlBgkqgw4DCgEBAQEwGAYKKoMOAwoBAQEBAQYKKoMOAwoBAwEBAANDAARAawHQQr4ktkf6LK34Tu/MOtehKW6JOy4WF8TgK/Cc1d1L1pGocMh0uPvaESSovA9EIgjKda6Nsy/cO24js+3uCqOCAfYwggHyMA4GA1UdDwEB/wQEAwIGwDAzBgNVHSUELDAqBggrBgEFBQcDBAYIKoMOAwMEAQIGCSqDDgMDBAECBQYJKoMOAwMEAwIBMA8GA1UdIwQIMAaABFtqc+kwHQYDVR0OBBYEFHIWUiJt+p4qH8C/KNG62NRCPihvMF4GA1UdIARXMFUwUwYHKoMOAwMCATBIMCEGCCsGAQUFBwIBFhVodHRwOi8vcGtpLmdvdi5rei9jcHMwIwYIKwYBBQUHAgIwFwwVaHR0cDovL3BraS5nb3Yua3ovY3BzMFgGA1UdHwRRME8wTaBLoEmGImh0dHA6Ly9jcmwucGtpLmdvdi5rei9uY2FfZ29zdC5jcmyGI2h0dHA6Ly9jcmwxLnBraS5nb3Yua3ovbmNhX2dvc3QuY3JsMFwGA1UdLgRVMFMwUaBPoE2GJGh0dHA6Ly9jcmwucGtpLmdvdi5rei9uY2FfZF9nb3N0LmNybIYlaHR0cDovL2NybDEucGtpLmdvdi5rei9uY2FfZF9nb3N0LmNybDBjBggrBgEFBQcBAQRXMFUwLwYIKwYBBQUHMAKGI2h0dHA6Ly9wa2kuZ292Lmt6L2NlcnQvbmNhX2dvc3QuY2VyMCIGCCsGAQUFBzABhhZodHRwOi8vb2NzcC5wa2kuZ292Lmt6MA0GCSqDDgMKAQEBAgUAA0EACm7hsvtWZVw25YiRap6Z395hv5Lw7UKpPbc/KT+djDoAoNOT7Sprg/3XzgOYHp1OnECCLQeFEa+xzwEqv9K8pQ==</wsse:KeyIdentifier>
						</wsse:SecurityTokenReference>
					</ds:KeyInfo>
				</ds:Signature>
			</wsse:Security>
		</SOAP-ENV:Header>
		<soap:Body xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="id-BEFF7CB55C69AB1BB514762482966309">
			<ns2:SendMessage xmlns:ns2="http://bip.bee.kz/SyncChannel/v10/Types">
				<request>
					<requestInfo>
						<messageId>1544911984184</messageId>
						<serviceId>search_violation</serviceId>
						<messageDate>2022-05-30T17:09:31.169+06:00</messageDate>
						<sender>
							<senderId>efish</senderId>
							<password>q9B&lt;wSC7</password>
						</sender>
						<sessionId>9b569176-73a2-4168-aab2-0bc0baee0314</sessionId>
					</requestInfo>
					<requestData>
						<data xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Hello</data>
					</requestData>
				</request>
			</ns2:SendMessage>
		</soap:Body>
	</soap:Envelope>';

    */

        $xml = '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"><SOAP-ENV:Header xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"/><S:Body><ns3:SendMessage xmlns:ns3="http://bip.bee.kz/SyncChannel/v10/Types" xmlns:ns2="http://bip.bee.kz/SyncChannel/v10/Interfaces"><request><requestInfo><messageId>9fa84970-d99a-4d54-862d-b0558ddf934a</messageId><serviceId>GbdulInfoByBin_v2</serviceId><messageDate>2022-06-07T19:20:20.952+06:00</messageDate><sender><senderId></senderId><password></password></sender></requestInfo><requestData><data xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string"><?xml version="1.0" encoding="UTF-8" standalone="no"?><data xmlns:ns3="http://gbdulinfobybin_v2.egp.gbdul.tamur.kz" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns3:RequestDataType">
    <RequestorBIN>131140022032</RequestorBIN>
    <BIN>080340019421</BIN>
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
<ds:SignedInfo>
<ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>
<ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gost34310-gost34311"/>
<ds:Reference URI="">
<ds:Transforms>
<ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
<ds:Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments"/>
</ds:Transforms>
<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gost34311"/>
<ds:DigestValue>Mjh9UjAQqMjLvVVo6LBRaKgJZSzAPlBKfLaaU3y0OD4=</ds:DigestValue>
</ds:Reference>
</ds:SignedInfo>
<ds:SignatureValue>
+pKz+92JMWb9MYIbSun0Ijy/BgpWCcTMKQ1digDqTNQ/6HpgAgBYUPi6sbSsvC3uJR1SDmHEpJnT
+TshtlKqKQ==
</ds:SignatureValue>
<ds:KeyInfo>
<ds:X509Data>
<ds:X509Certificate>
MIIEnTCCBEegAwIBAgIUNUmv6wqrLNA55aIDP1X84ExA/F0wDQYJKoMOAwoBAQECBQAwUzELMAkG
A1UEBhMCS1oxRDBCBgNVBAMMO9Kw0JvQotCi0KvSmiDQmtCj05jQm9CQ0J3QlNCr0KDQo9Co0Ksg
</ds:X509Certificate>
</ds:X509Data>
</ds:KeyInfo>
</ds:Signature></data></data></requestData></request></ns3:SendMessage></S:Body></S:Envelope>';

		$url = "http://192.168.39.30/bip-sync-wss-gost/"; // http://192.168.39.30/bip-sync-wss-gost

		$response = Http::send('POST', $url, [
            'body' => $xml,
		]);
		echo $response->status();
		echo $response->body();

		exit;
	}
}
