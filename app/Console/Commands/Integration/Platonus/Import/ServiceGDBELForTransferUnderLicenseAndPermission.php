<?php

namespace App\Console\Commands\Integration\Platonus\Import;

use Illuminate\Console\Command;

use App\Models\Faculty;
use Illuminate\Support\Facades\Http;

class ServiceGDBELForTransferUnderLicenseAndPermission extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'mind-s-0062:run';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
		<SOAP-ENV:Header xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"/><soap:Body>
				<ns2:SendMessage xmlns:ns2="http://bip.bee.kz/SyncChannel/v10/Types">
					<request>
						<requestInfo>
							<messageId>fa967f95-40b6-4ead-b7c4-ebcb3f1786c0</messageId>
							<serviceId>GbdulInfoByBin_v2</serviceId>
							<messageDate>2022-05-27T10:32:09.199Z</messageDate>
							<sender>
								<senderId>efish</senderId>
							    <password>q9B&lt;wSC7</password>
							</sender>
							<sessionId>502ea64c-450a-4123-bf2d-b57be5d1559f</sessionId>
						</requestInfo>
						<requestData>
							<data xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string"><?xml version="1.0" encoding="UTF-8" standalone="no"?>
		<ns3:Request xmlns:ns3="http://gbdulinfobybin_v2.egp.gbdul.tamur.kz" xmlns:ns2="http://www.w3.org/2000/09/xmldsig#" id="sign"><RequestorBIN>190740009654</RequestorBIN><BIN>190740009654</BIN><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="1">
		<ds:SignedInfo>
		<ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>
		<ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gost34310-gost34311"/>
		<ds:Reference URI="">
		<ds:Transforms>
		<ds:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
		<ds:Transform Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments"/>
		</ds:Transforms>
		<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#gost34311"/>
		<ds:DigestValue>+gY1rQApas3DL3NhSJrueZrIvcWFH9xS8jQ9iDjtdNI=</ds:DigestValue>
		</ds:Reference>
		</ds:SignedInfo>
		<ds:SignatureValue>HAz6lI8pOFKPMxt7op8qxQRSXLXdUXCOZxazzLIderLq6Uq3SxJ2m+I76jXQB9lR
		gQxfUGzqhDM7Zt91kkRY1A==</ds:SignatureValue>
		<ds:KeyInfo>
		<ds:X509Data>
		<ds:X509Certificate>MIIE4jCCBIygAwIBAgIUWVquqg2Hq23ObZ0DYg/Bvlav2/wwDQYJKoMOAwoBAQEC
		BQAwUzELMAkGA1UEBhMCS1oxRDBCBgNVBAMMO9Kw0JvQotCi0KvSmiDQmtCj05jQ
		m9CQ0J3QlNCr0KDQo9Co0Ksg0J7QoNCi0JDQm9Cr0pogKEdPU1QpMB4XDTIxMDkx
		ODEwMjA1NloXDTIyMDkxODEwMjA1NlowggGMMSIwIAYDVQQDDBnQotCQ0KHQotCQ
		0J0g0J3SsNCg0JHQntCbMRUwEwYDVQQEDAzQotCQ0KHQotCQ0J0xGDAWBgNVBAUT
		D0lJTjkwMDUwNTMwMjY4NDELMAkGA1UEBhMCS1oxgcswgcgGA1UECgyBwNCT0J7Q
		odCj0JTQkNCg0KHQotCS0JXQndCd0J7QlSDQo9Cn0KDQldCW0JTQldCd0JjQlSAi
		0JzQmNCd0JjQodCi0JXQoNCh0KLQktCeINCt0JrQntCb0J7Qk9CY0JgsINCT0JXQ
		ntCb0J7Qk9CY0Jgg0Jgg0J/QoNCY0KDQntCU0J3Qq9ClINCg0JXQodCj0KDQodCe
		0JIg0KDQldCh0J/Qo9CR0JvQmNCa0Jgg0JrQkNCX0JDQpdCh0KLQkNCdIjEYMBYG
		A1UECwwPQklOMTkwNzQwMDA5NjU0MRkwFwYDVQQqDBDQldCg0JHQntCb0rDQm9Cr
		MSUwIwYJKoZIhvcNAQkBFhZOLlRBU1RBTkBFQ09HRU8uR09WLktaMGwwJQYJKoMO
		AwoBAQEBMBgGCiqDDgMKAQEBAQEGCiqDDgMKAQMBAQADQwAEQPmCWIJyf5ikb0I/
		Bck/OxIH2XBurIskUx1mmD8Qjo0OBxEBAiVQYoRdF8t8PGySg1JUAGJ4RTTKEPXo
		fLn/oQ2jggHrMIIB5zAOBgNVHQ8BAf8EBAMCBsAwKAYDVR0lBCEwHwYIKwYBBQUH
		AwQGCCqDDgMDBAECBgkqgw4DAwQBAgIwDwYDVR0jBAgwBoAEW2pz6TAdBgNVHQ4E
		FgQUqxpVvLPf+4J3xah9NDR+9M3a6y0wXgYDVR0gBFcwVTBTBgcqgw4DAwIBMEgw
		IQYIKwYBBQUHAgEWFWh0dHA6Ly9wa2kuZ292Lmt6L2NwczAjBggrBgEFBQcCAjAX
		DBVodHRwOi8vcGtpLmdvdi5rei9jcHMwWAYDVR0fBFEwTzBNoEugSYYiaHR0cDov
		L2NybC5wa2kuZ292Lmt6L25jYV9nb3N0LmNybIYjaHR0cDovL2NybDEucGtpLmdv
		di5rei9uY2FfZ29zdC5jcmwwXAYDVR0uBFUwUzBRoE+gTYYkaHR0cDovL2NybC5w
		a2kuZ292Lmt6L25jYV9kX2dvc3QuY3JshiVodHRwOi8vY3JsMS5wa2kuZ292Lmt6
		L25jYV9kX2dvc3QuY3JsMGMGCCsGAQUFBwEBBFcwVTAvBggrBgEFBQcwAoYjaHR0
		cDovL3BraS5nb3Yua3ovY2VydC9uY2FfZ29zdC5jZXIwIgYIKwYBBQUHMAGGFmh0
		dHA6Ly9vY3NwLnBraS5nb3Yua3owDQYJKoMOAwoBAQECBQADQQCrvgz6YnIeWNF+
		8RAXJOvM+JkcfqEjNqd+iWgPZOSAepxCv6eEQXURwhoGiMLrmZWOyoBql0PoTOWS
		b5cbhbfK</ds:X509Certificate>
		</ds:X509Data>
		</ds:KeyInfo>
		</ds:Signature></ns3:Request>
		</data>
						</requestData>
					</request>
				</ns2:SendMessage>
			</soap:Body>
		</soap:Envelope>';
		/*
		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => 'http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>
				'<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
					<Body>
						<CapitalCity xmlns="http://www.oorsprong.org/websamples.countryinfo">
							<sCountryISOCode>IN</sCountryISOCode>
						</CapitalCity>
					</Body>
				</Envelope>',
			CURLOPT_HTTPHEADER => [
				'Content-Type: text/xml'
			],
		]);

		$response = curl_exec($curl);

		echo $response;

		curl_close($curl);

		*/



		$url = "http://192.168.39.30/bip-sync-wss-gost/"; // http://192.168.39.30/bip-sync-wss-gost


		$response = Http::send('POST', $url, [
            'body' => $xml,
		]);
		echo $response->status();
		echo $response->body();

		exit;










		/*

		$client = new \SoapClient(NULL, [
			'location' => 'http://192.168.39.30',
			'uri'      => 'urn:bip-sync-wss-gost',
			'style'    => SOAP_RPC,
			'use'      => SOAP_ENCODED
		]);

		dd($client);

		*/

		/*

		$client = new \SoapClient('http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL');
		dd($client->__getFunctions());

		*/

	}
}



