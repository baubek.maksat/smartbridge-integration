<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

use App\Models\StatementRoute;

class StatementRouteProcessed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
	
	public $statement_status;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(StatementRoute $statement_route)
    {
    	$this->statement_route = $statement_route;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
