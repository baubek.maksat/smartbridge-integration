<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

use App\Models\StatementComment;

class StatementCommentCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
	
    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $statement_comment;

    public function __construct(StatementComment $statement_comment)
    {
    	$this->statement_comment = $statement_comment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('statement-comments');
    }
}
