<?php

namespace App\Listeners;

use App\Events\TimetableСhanged;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Models\NotificationViewUserRole;
use App\Models\NotificationView;
use App\Models\Notification;
use App\Models\Timetable;
use App\Models\User;
use App\Models\Role;

use Http;
use DB;

class SendTimetableNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatementStatusProcessed  $event
     * @return void
     */
    public function handle(TimetableСhanged $event)
    {
    	$messages = [
			'kz' => [
				'schedule' => 'Кесте',
				'dear student' => 'Құрметті',
				'changed' => 'Кестеге өзгерістер'
			],
			'ru' => [
				'schedule' => 'Расписание',
				'dear student' => 'Уважаемый',
				'changed' => 'Внесен изменений в расписание'
			],
			'en' => [
				'schedule' => 'Schedule',
				'dear student' => 'Dear student',
				'changed' => 'Changes to the schedule'
			],
		];

		$langs = [
			'kz', 'ru', 'en'
		];

    	$students = $event->timetable->studyGroup->students;

    	$timetable = $event->timetable;

    	if ($students) {
    		foreach ($students as $student) {
    			$notification = NotificationViewUserRole::query()
					->where('user_id', $student->user->id)
					->where('role_id', Role::where('slug', 'student')->first()->id)
					->where('view_id', NotificationView::where('slug', 'schedule')->first()->id)
					->first();

				DB::transaction(function() use ($student, $timetable, $messages, $langs) {
					$notification = Notification::create([
						'view_id' => NotificationView::where('slug', 'schedule')->first()->id
					]);
						
					foreach ($langs as $lang) {
						$text = implode(PHP_EOL, [
							$messages[$lang]['changed']
						]);
						
						$notification->translations()->create([
							'lang' => $lang,
							'text' => $text
						]);
					}
					
					$notification->recipients()->create([
						'user_id' => $student->user->id,
						'role_id' => Role::where('slug', 'student')->first()->id
					]);
				});
    		}
    	}

    	//	

    	$key = env('FIREBASE_API_KEY');
		$url = env('FIREBASE_API_URL');

		if ($students) {
    		foreach ($students as $student) {
    			if ($student->user->apps()->wherePivotNull('deleted_at')->count() > 0 && $notification->state == '1') {
					foreach ($student->user->apps()->wherePivotNull('deleted_at')->get() as $app) {
						$fields = [
							'to' => $app->registration_id,
							'notification' => [
								'title' => $messages[$student->user->defaultLang->slug]['schedule'],
								'body' => implode(PHP_EOL, [
									$messages[$student->user->defaultLang->slug]['changed']
								])
							]
						];

						$headers = [
							'Authorization: key='.$key, 
							'Content-Type: application/json'
						];

						$response = Http::withHeaders($headers)->post($url, $fields);
					}
				}	
			}
		}	
    }
}
