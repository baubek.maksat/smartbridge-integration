<?php

namespace App\Listeners;

use App\Events\StatementRouteProcessed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Models\NotificationViewUserRole;
use App\Models\NotificationView;
use App\Models\Notification;
use App\Models\Statement;
use App\Models\Role;

use DB;

class SendStatementRouteNotification// implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatementStatusProcessed  $event
     * @return void
     */
    public function handle(StatementRouteProcessed $event)
    {
		$user = $event->statement_route->stage->statement->student->user;
		
		$notification = NotificationViewUserRole::query()
			->where('user_id', $user->id)
			->where('role_id', Role::where('slug', 'student')->first()->id)
			->where('view_id', NotificationView::where('slug', 'statement')->first()->id)
			->first();
		
		if ($notification->state == 0) {
			return;
		}
		
		$statement = Statement::find($event->statement_route->stage->statement->id);
		
		$messages = [
			'kz' => [
				'statement' => 'Өтініш',
				'dear student' => 'Құрметті',
				'status' => 'Статус',
				'specialist appointed' => 'Маман тағайындалды'
			],
			'ru' => [
				'statement' => 'Заявление',
				'dear student' => 'Уважаемый',
				'specialist appointed' => 'Назначен специалист',
				'status' => 'Статус'
			],
			'en' => [
				'statement' => 'Application',
				'dear student' => 'Dear student',
				'status' => 'Status',
				'specialist appointed' => 'Specialist appointed'
			],
		];
		
		DB::transaction(function() use($user, $statement, $messages, $event) {
			$ssc = auth()->user()->sscs[0];
			
			$notification = Notification::create([
				'view_id' => NotificationView::where('slug', 'statement')->first()->id,
				'params' => [
					'resource' => 'statement',
					'id' => $statement->id
				]
			]);
			
			foreach ($ssc->langs as $lang) {
				$text = implode(PHP_EOL, [
					$messages[$lang->slug]['dear student'].', '.$user->full_name,
					$statement->view->translations()->where('lang', $lang->slug)->first()->name.' №'.$statement->id.' '.$messages[$lang->slug]['specialist appointed'],
						$messages[$lang->slug]['status'].' '.mb_strtolower($statement->status->translations()->where('lang', $lang->slug)->first()->name)
					]);
				
				if ($event->statement_route->status == 'success' || $event->statement_route->status == 'fail') {
					$text = implode(PHP_EOL, [
						$messages[$lang->slug]['dear student'].', '.$user->full_name,
						$statement->view->translations()->where('lang', $lang->slug)->first()->name.' №'.$statement->id.' '.($event->statement_route->status == 'success' ? $event->statement_route->action->success : $event->statement_route->action->fail),
						$messages[$lang->slug]['status'].' '.mb_strtolower($statement->status->translations()->where('lang', $lang->slug)->first()->name)
					]);
				}

				if ($event->statement_route->status == 'processing') {
					$text = implode(PHP_EOL, [
						$messages[$lang->slug]['dear student'].', '.$user->full_name,
						$statement->view->translations()->where('lang', $lang->slug)->first()->name.' №'.$statement->id.' '.$messages[$user->defaultLang->slug]['specialist appointed'],
						$messages[$lang->slug]['status'].' '.mb_strtolower($statement->status->translations()->where('lang', $lang->slug)->first()->name)
					]);
				}
				
				$notification->translations()->create([
					'lang' => $lang->slug,
					'text' => $text
				]);
			}

			$notification->recipients()->create([
				'user_id' => $user->id,
				'role_id' => Role::where('slug', 'student')->first()->id
			]);
		});
		
		$key = env('FIREBASE_API_KEY');
		$url = env('FIREBASE_API_URL');
		
		if ($user->apps()->wherePivotNull('deleted_at')->count() == 0 || $notification->state == 0) {
			return;
		}
		
		foreach ($user->apps()->wherePivotNull('deleted_at')->get() as $app) {
			if ($event->statement_route->status == 'success' || $event->statement_route->status == 'fail') {
				$fields = [
					'to' => $app->registration_id,
					'notification' => [
						'title' => $messages[$user->defaultLang->slug]['statement'],
						'body' => implode(PHP_EOL, [
							$messages[$user->defaultLang->slug]['dear student'].', '.$user->full_name,
							$statement->view->translations()->where('lang', $user->defaultLang->slug)->first()->name.' №'.$statement->id.' '.($event->statement_route->status == 'success' ? $event->statement_route->action->success : $event->statement_route->action->fail),
							$messages[$user->defaultLang->slug]['status'].' '.mb_strtolower($statement->status->translations()->where('lang', $user->defaultLang->slug)->first()->name)
						])
					]
				];
			}

			if ($event->statement_route->status == 'processing') {
				$fields = [
					'to' => $app->registration_id,
					'notification' => [
						'title' => $messages[$user->defaultLang->slug]['statement'],
						'body' => implode(PHP_EOL, [
							$messages[$user->defaultLang->slug]['dear student'].', '.$user->full_name,
							$statement->view->translations()->where('lang', $user->defaultLang->slug)->first()->name.' №'.$statement->id.' '.$messages[$user->defaultLang->slug]['specialist appointed'],
							$messages[$user->defaultLang->slug]['status'].' '.mb_strtolower($statement->status->translations()->where('lang', $user->defaultLang->slug)->first()->name)
						])
					]
				];
			}

			$headers = [
				'Authorization: key='.$key, 
				'Content-Type: application/json'
			];

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

			$result = curl_exec($ch);

			curl_close($ch);
		}	 	
    }
}
