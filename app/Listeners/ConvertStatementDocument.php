<?php

namespace App\Listeners;

use App\Events\StatementCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Http\Resources\StatementResource;

use App\Models\NotificationViewUserRole;
use App\Models\NotificationView;
use App\Models\Notification;
use App\Models\Event;
use App\Models\User;
use App\Models\Role;

use Pusher\Pusher as Pusher;

use Http;
use DB;

class ConvertStatementDocument//	 implements ShouldQueue
{
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  StatementStatusProcessed  $event
	 * @return void
	 */
	public function handle(StatementCreated $event)
	{
		$options = [
			'cluster' => 'ap2',
			'useTLS' => true
		];

		$pusher = new Pusher(
			'3ca4cf8be0971f59e092',
			'29f892205faa2ca88608',
			'1295256',
			$options
		);

		$data['statement'] = [
			'id' => $event->statement->id,
			'view' => [
				'id' => $event->statement->id,
				'name' => $event->statement->view->name,
				'translations' => $event->statement->view->translations
			],
			'responsible' => [
				'role' => [
					'id' => $event->statement->responsibleRole->id,
					'name' => $event->statement->responsibleRole->name
				],
				'user' => $event->statement->responsibleUser ? [
					'id' => $event->statement->responsibleUser->id,
					'surname' => $event->statement->responsibleUser->surname,
					'name' => $event->statement->responsibleUser->name,
					'patronymic' => $event->statement->responsibleUser->patronymic,
					'image' => $event->statement->responsibleUser->image
				] : null
			],
		];

		$pusher->trigger('statements.'.$event->statement->id, 'show', $data);
		$pusher->trigger('statements', 'store', $data);
	}
}
