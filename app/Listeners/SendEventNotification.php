<?php

namespace App\Listeners;

use App\Events\EventCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Models\NotificationViewUserRole;
use App\Models\NotificationView;
use App\Models\Notification;
use App\Models\Event;
use App\Models\User;
use App\Models\Role;

use Http;
use DB;

class SendEventNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatementStatusProcessed  $event
     * @return void
     */
    public function handle(EventCreated $event)
    {
		$event = $event->event;

		if (Notification::where('params->id', $event->id)->where('params->resource', 'event')->first()) {
    		return;
    	}

		$users = User::query()
			->whereHas('student.card.group.speciality.department.faculty', function ($query) use ($event) {
				$query->where('university_id', $event->university_id);
			})
			->get();

		if (!$users) return;
		
		$messages = [
			'kz' => [
				'events' => 'Іс шаралар'
			],
			'ru' => [
				'events' => 'События'
			],
			'en' => [
				'events' => 'Events'
			]
		];
		
		DB::transaction(function() use ($users, $event, $messages) {
			$notification = Notification::create([
				'view_id' => NotificationView::where('slug', 'event')->first()->id,
				'params' => [
					'resource' => 'event',
					'id' => $event->id
				]
			]);

			foreach ($event->translations as $translation) {
				$notification->translation()->create([
					'lang' => $translation->lang,
					'text' => $event->translations()->where('lang', $translation->lang)->first()->heading
				]);
			}

			foreach($users as $user) {
				$notification->recipients()->create([
					'user_id' => $user->id,
					'role_id' => Role::where('slug', 'student')->first()->id
				]);
			}
		});

		//	Send push notifications

		$key = env('FIREBASE_API_KEY');
		$url = env('FIREBASE_API_URL');

		foreach($users as $user) {
			$notification = NotificationViewUserRole::query()
				->where('user_id', $user->id)
				->where('role_id', Role::where('slug', 'student')->first()->id)
				->where('view_id', NotificationView::where('slug', 'event')->first()->id)
				->first();
			
			if ($user->apps()->wherePivotNull('deleted_at')->count() > 0 && $notification->state == 1 && $event->translations()->where('lang', $user->defaultLang->slug)->first()) {
				foreach ($user->apps()->wherePivotNull('deleted_at')->get() as $app) {
					$fields = [
						'to' => $app->registration_id,
						'notification' => [
							'title' => $messages[$user->defaultLang->slug]['events'],
							'body' => $event->translations()->where('lang', $user->defaultLang->slug)->first()->heading
						]
					];

					$headers = [
						'Authorization' => 'key='.$key,
					    'Content-Type' => 'application/json'
					];

					$response = Http::withHeaders($headers)->post($url, $fields);
				}
			}
		}
    }
}
