<?php

namespace App\Listeners;

use App\Events\TidingCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Models\NotificationViewUserRole;
use App\Models\NotificationView;
use App\Models\Notification;
use App\Models\Tiding;
use App\Models\User;
use App\Models\Role;

use Http;
use DB;

class SendTidingNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatementStatusProcessed  $event
     * @return void
     */
    public function handle(TidingCreated $event)
    {
    	$tiding = $event->tiding;

    	if (Notification::where('params->id', $tiding->id)->where('params->resource', 'tiding')->first()) {
    		return;
    	}

		$users = User::query()
			->whereHas('student.card.group.speciality.department.faculty', function ($query) use ($tiding) {
				$query->where('university_id', $tiding->university_id);
			})
			->get();

		if (!$users) return;
		
		$messages = [
			'kz' => [
				'tidings' => 'Жаңалықтар'
			],
			'ru' => [
				'tidings' => 'Новости'
			],
			'en' => [
				'tidings' => 'News'
			]
		];
		
		DB::transaction(function() use ($users, $tiding, $messages) {
			$notification = Notification::create([
				'view_id' => NotificationView::where('slug', 'tiding')->first()->id,
				'params' => [
					'resource' => 'tiding',
					'id' => $tiding->id
				]
			]);

			foreach ($tiding->translations as $translation) {
				$notification->translation()->create([
					'lang' => $translation->lang,
					'text' => $tiding->translations()->where('lang', $translation->lang)->first()->heading
				]);
			}

			foreach($users as $user) {
				$notification->recipients()->create([
					'user_id' => $user->id,
					'role_id' => Role::where('slug', 'student')->first()->id
				]);
			}
		});

		//	Send push notifications

		$key = env('FIREBASE_API_KEY');
		$url = env('FIREBASE_API_URL');

		foreach($users as $user) {
			$notification = NotificationViewUserRole::query()
				->where('user_id', $user->id)
				->where('role_id', Role::where('slug', 'student')->first()->id)
				->where('view_id', NotificationView::where('slug', 'tiding')->first()->id)
				->first();
			
			if ($user->apps()->wherePivotNull('deleted_at')->count() > 0 && $notification->state == 1 && $tiding->translations()->where('lang', $user->defaultLang->slug)->first()) {
				foreach ($user->apps()->wherePivotNull('deleted_at')->get() as $app) {
					$fields = [
						'to' => $app->registration_id,
						'notification' => [
							'title' => $messages[$user->defaultLang->slug]['tidings'],
							'body' => $tiding->translations()->where('lang', $user->defaultLang->slug)->first()->heading
						]
					];

					$headers = [
						'Authorization' => 'key='.$key,
					    'Content-Type' => 'application/json'
					];

					$response = Http::withHeaders($headers)->post($url, $fields);
				}
			}
		}
    }
}
