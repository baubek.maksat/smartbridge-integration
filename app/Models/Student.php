<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Student extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $fillable = [
        'id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'id');
    }

    public function card()
    {
        return $this->hasOne(StudentCard::class, 'user_id', 'id')->whereNull('date_end');
    }

    public function cards()
    {
        return $this->hasMany(StudentCard::class, 'user_id', 'id');
    }
}
