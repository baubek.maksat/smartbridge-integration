<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentSubjectCost extends Model
{
    use HasFactory, Observer;

    protected $table = 'student_subject_costs';

    protected $fillable = [
    	'id',
    	'platonus_key',
    	'student_id',
        'subject_id',
        'value'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function student()
    {
        return $this->hasOne(StudentCard::class, 'id', 'student_id');
    }

    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }
}
