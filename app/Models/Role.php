<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Role extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id', 
    	'slug'
    ];

    public function translation()
    {
        return $this->hasOne(RoleLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(RoleLang::class, 'id', 'id');
    }

    public function visit()
    {
        return $this->hasOne(Visit::class, 'role_id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
