<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Group extends Model
{
	use HasFactory, Observer;

	protected $fillable = [
		'id',
		'platonus_key',
		'speciality_id',
		'name',
		'state_id',
        'integration_fields'
	];

    protected $casts = [
        'integration_fields' => 'array'
    ];

	public function speciality()
	{
		return $this->hasOne(Speciality::class, 'id', 'speciality_id');
	}
}
