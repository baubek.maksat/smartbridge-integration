<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class SeeType extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
        'id',
        'name_kz',
        'name_ru',
        'name_en',
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(SeeTypeLang::class, 'id', 'id')->where('lang', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(SeeTypeLang::class, 'id', 'id');
    }
}
