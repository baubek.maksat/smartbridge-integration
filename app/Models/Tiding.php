<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class Tiding extends Model
{
    use HasFactory, SoftDeletes, Observer;
	
	protected $table = 'tidings';

    protected $fillable = [
    	'id',
		'state_id',
        'university_id',
        'date_of_publication',
		'image'
    ];

    public function translation()
    {
        return $this->hasOne(TidingLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(TidingLang::class, 'id', 'id');
    }
	
	public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function ips()
    {
        return $this->hasMany(TidingIp::class, 'id', 'id');
    }
	
	public function getHeadingAttribute()
    {
        return $this->translation ? $this->translation->heading : null;
    }
	
	public function getDiscriptionAttribute()
    {
        return $this->translation ? $this->translation->discription : null;
    }
}
