<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class TransferViewLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'transfer_view_langs';

    protected $fillable = [
    	'id',
    	'lang',
    	'name'
    ];
}
