<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class FacultyAdministrator extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'faculty_administrators';

    protected $fillable = [
    	'id'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'id');
    }

    public function card()
    {
        return $this->belongsToMany(FacultyAdministratorCard::class, 'faculty_administrators_cards', 'user_id', 'card_id');
    }

    public function cards()
    {
        return $this->belongsToMany(FacultyAdministratorCard::class, 'faculty_administrators_cards', 'user_id', 'card_id');
    }
}
