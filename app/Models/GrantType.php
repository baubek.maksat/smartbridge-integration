<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class GrantType extends Model
{
    use HasFactory, Observer;

    protected $table = 'grant_types';

    protected $fillable = [
    	'id',
        'platonus_key',
        'payment_form_id',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(GrantTypeLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(GrantTypeLang::class, 'id', 'id');
    }

    public function paymentForm()
    {
        return $this->hasOne(PaymentForm::class, 'id', 'payment_form_id');
    }
	
	public function getNameAttribute()
	{
		return $this->translation->name;
	}
}
