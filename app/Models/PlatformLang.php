<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class PlatformLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'platform_langs';

    protected $fillable = [
    	'id',
        'lang',
        'name'
    ];
}
