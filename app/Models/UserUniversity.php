<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserUniversity extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
        'id',
        'user_id', 
        'citizenship_id',
        'cato_id',
        'cato_name_kz',
        'cato_name_ru',
        'cato_name_en', 
        'name_kz',
        'name_ru',
        'name_en',
        'academic_degree_name_kz',
        'academic_degree_name_ru',
        'academic_degree_name_en',
        'study_form_name_kz',
        'study_form_name_ru',
        'study_form_name_en',
        'faculty_name_kz',
        'faculty_name_ru',
        'faculty_name_en',
        'speciality_name_kz',
        'speciality_name_ru',
        'speciality_name_en',
        'year_start',
        'year_end'
    ];
    
    public function citizenship()
    {
        return $this->hasOne(Citizenship::class, 'id', 'citizenship_id');
    }

    public function cato()
    {
        return $this->hasOne(Cato::class, 'id', 'cato_id');
    }
}
