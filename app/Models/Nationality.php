<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Nationality extends Model
{
    use HasFactory, Observer;

    protected $table = 'nationalities';

    protected $fillable = [
    	'id',
        'platonus_key'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function genders()
    {
        return $this->hasMany(NationalityGender::class, 'nationality_id', 'id');
    }

    public function translation()
    {
        return $this->hasOne(NationalityLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(NationalityLang::class, 'id', 'id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
