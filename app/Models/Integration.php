<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    use HasFactory;

	protected $fillable = [
		'id',
		'university_id',
		'system',
		'table',
		'batch',
		'created',
		'updated',
		'deleted',
		'time'
	];

    public function university()
    {
        return $this->hasOne(University::class, 'id', 'university_id');
    }
}
