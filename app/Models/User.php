<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class User extends Authenticatable
{
	use HasApiTokens, HasFactory, Notifiable, SoftDeletes, Observer;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id',
		'platonus_key',
		'username', 
		'password', 
		'surname',
		'name',
		'patronymic',
		'surname_en',
		'name_en',
		'patronymic_en',
		'email', 
		'phone',
		'birth_date', 
		'birth_country_id',
		'birth_cato_id',
		'arrived_country_id',
		'arrived_cato_id',
		'image',
		'registration_cato_id',
		'living_cato_id',
		'gender_id',
		'marital_status_id',
		'nationality_id',
		'default_role_id',
		'default_lang_id',
		'access',
		'email_verified',
        'integration_fields'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
		'integration_fields' => 'array'
	];

	public function preferredLocale()
	{
		return $this->locale;
	}
	
	public function roles()
	{
		return $this->belongsToMany(Role::class, 'users_roles')
			->wherePivotNull('deleted_at');
			//	->withTrashed();
	}

	public function rolesWithTrashed()
	{
		return $this->belongsToMany(Role::class, 'users_roles');
			//	->withTrashed();
	}

	public function visits()
	{
		return $this->hasMany(Visit::class, 'user_id', 'id');
	}

	public function emailConfirms()
	{
		return $this->hasMany(UserEmailConfirm::class, 'user_id', 'id');
	}

	public function phoneConfirms()
	{
		return $this->hasMany(UserPhoneConfirm::class, 'user_id', 'id');
	}

	public function emailRestores()
	{
		return $this->hasMany(UserEmailRestore::class, 'user_id', 'id');
	}

	public function gender()
	{
		return $this->hasOne(Gender::class, 'id', 'gender_id');
	}

	public function identityDocument()
	{
		return $this->hasOne(Id::class, 'user_id', 'id');
	}

	public function identityDocuments()
	{
		return $this->hasMany(Id::class, 'user_id', 'id');
	}

	public function father()
	{
		return $this->hasOne(UserLink::class, 'user_id_a', 'id')->where('type_id', 101);
	}

	public function mother()
	{
		return $this->hasOne(UserLink::class, 'user_id_a', 'id')->where('type_id', 102);
	}

	public function registrationCato()
	{
		return $this->hasOne(Cato::class, 'id', 'registration_cato_id');
	}

	public function livingCato()
	{
		return $this->hasOne(Cato::class, 'id', 'living_cato_id');
	}

	public function maritalStatus()
	{
		return $this->hasOne(MaritalStatusGender::class, 'marital_status_id', 'marital_status_id')
			->where('gender_id', $this->gender_id);
	}

	public function nationality()
	{
		return $this->hasOne(Nationality::class, 'id', 'nationality_id');
	}

	public function birthCountry()
	{
		return $this->hasOne(Country::class, 'id', 'birth_country_id');
	}

	public function birthCato()
	{
		return $this->hasOne(Cato::class, 'id', 'birth_cato_id');
	}

	public function defaultRole()
	{
		return $this->hasOne(Role::class, 'id', 'default_role_id');
	}

	public function defaultLang()
	{
		return $this->hasOne(Lang::class, 'id', 'default_lang_id');
	}

	public function defaultLangSlug()
	{
		return $this->default_lang_id
			? Lang::find($this->default_lang_id)->slug
			: null;
	}

	public function citizenships()
	{
		return $this->belongsToMany(Citizenship::class, 'users_citizenships');
	}
	
	public function apps()
	{
		return $this->belongsToMany(App::class, 'users_apps', 'user_id', 'app_id');
	}

	public function sscs()
	{
		return $this->belongsToMany(StudentServiceCenter::class, 'student_service_centers_users', 'user_id', 'ssc_id')
			->wherePivotNull('deleted_at')
			->withTrashed();
	}

	public function sscsWithTrashed()
	{
		return $this->belongsToMany(StudentServiceCenter::class, 'student_service_centers_users', 'user_id', 'ssc_id')
			->withTrashed();
	}

	public function hasSsc($id)
	{
		if ($this->sscs->contains('id', $id)) {
			return true;
		} else {
			return false;
		}
	}

	public function hasSscWithTrashed($id)
	{
		if ($this->sscsWithTrashed->contains('id', $id)) {
			return true;
		} else {
			return false;
		}
	}

	public function studentServiceCenterRoles()
	{
		return $this->belongsToMany(StudentServiceCenterRole::class, 'student_service_center_users_roles', 'user_id', 'role_id')
			->wherePivotNull('deleted_at')
			->withTrashed();
	}

	public function studentServiceCenterRolesWithTrashed()
	{
		return $this->belongsToMany(StudentServiceCenterRole::class, 'student_service_center_users_roles', 'user_id', 'role_id')
			->withTrashed();
	}

	public function sec()
	{
		return $this->hasOne(Sec::class, 'user_id', 'id');
	}

	public function hpeds()
	{
		return $this->hasMany(Hped::class, 'user_id', 'id');
	}

	public function see()
	{
		return $this->hasOne(See::class, 'user_id', 'id');
	}

	public function sees()
	{
		return $this->hasMany(See::class, 'user_id', 'id');
	}

	public function identificationDocument()
	{
		return $this->hasOne(Id::class, 'user_id', 'id');
	}

	public function identificationDocuments()
	{
		return $this->hasMany(Id::class, 'user_id', 'id');
	}

	public function translation()
	{
		return $this->hasOne(UserLang::class, 'id', 'id')->where('lang', app()->getLocale());
	}

	public function translations()
	{
		return $this->hasMany(UserLang::class, 'id', 'id');
	}

	public function notificationViews()
	{
		return $this->hasMany(NotificationViewUserRole::class, 'user_id', 'id');
	}

	public function student()
	{
		return $this->hasOne(Student::class, 'id', 'id');
	}
	
	public function teacher()
	{
		return $this->hasOne(Teacher::class, 'id', 'id');
	}

	public function content_manager()
	{
		return $this->hasOne(ContentManager::class, 'id', 'id');
	}
	
	public function departmentAdministrator()
	{
		return $this->hasOne(DepartmentAdministrator::class, 'id', 'id');
	}
	
	public function facultyAdministrator()
	{
		return $this->hasOne(FacultyAdministrator::class, 'id', 'id');
	}
	
	public function universityAdministrator()
	{
		return $this->hasOne(UniversityAdministrator::class, 'id', 'id');
	}

	public function account()
	{
		return $this->hasOne(Account::class, 'user_id', 'id')->where('view_id', 100001);
	}

	public function accounts()
	{
		return $this->hasMany(Account::class, 'user_id', 'id');
	}

	public function currentRole()
	{
		$role = Role::query()
			->whereHas('visit', function($query){
				$token = request()->bearerToken() ? request()->bearerToken() : null;
				
				$query->where('token', $token);
			})
			->first();

		return $role;
	}

	public function hasRole($role) {
		if ($this->roles->contains('slug', $role)) {
			return true;
		} else {
			return false;
		}
	}

	public function hasRoleWithTrashed($role) {
		if ($this->rolesWithTrashed->contains('slug', $role)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function hasStudentServiceCenterRole($id) {
		if ($this->studentServiceCenterRoles->contains('id', $id)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function hasStudentServiceCenterRoleWithTrashed($id) {
		if ($this->studentServiceCenterRolesWithTrashed->contains('id', $id)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function hasApp($id) {
		if ($this->apps->contains('id', $id)) {
			return true;
		} else {
			return false;
		}
	}

	public function hasEmail($email) {
		if ($this->where('email', $email)->first()) {
			return true;
		} else {
			return false;
		}
	}

	public function hasPhone($phone) {
		if ($this->where('phone', $phone)->first()) {
			return true;
		} else {
			return false;
		}
	}

	public function getBirthCatoNameAttribute()
	{
    	return $this->translation ? $this->translation->birth_cato_name : null;
	}
	
	public function getFullnameAttribute()
	{
    	return "{$this->surname} {$this->name} {$this->patronymic}";
	}
	
	public function getLivingAddressAttribute()
	{
    	return $this->translation ? $this->translation->living_address : null;
	}
	
	public function getRegistrationAddressAttribute()
	{
    	return $this->translation ? $this->translation->registration_address : null;
	}
}
