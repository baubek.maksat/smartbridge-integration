<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ApplicationComponentType extends Model
{
    use HasFactory, Observer;

    protected $table = 'application_component_types';

    protected $fillable = [
    	'id',
        'slug',
        'rules'
    ];

    protected $casts = [
        'rules' => 'array'
    ];

    public function translation()
    {
        return $this->hasOne(ApplicationComponentTypeLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(ApplicationComponentTypeLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
