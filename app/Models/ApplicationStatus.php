<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ApplicationStatus extends Model
{
	use HasFactory, Observer;

	protected $table = 'application_statuses';

	protected $fillable = [
		'id',
		'slug',
		'color'
	];

	public function translation()
	{
		return $this->hasOne(ApplicationStatusLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLang ? auth()->user()->defaultLang->slug : app()->getLocale());
	}

	public function translations()
	{
		return $this->hasMany(StatementStatusLang::class, 'id', 'id');
	}
	
	public function getIdBySlug($slug)
	{
		return $this->where('slug', $slug)->first();
	}
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
