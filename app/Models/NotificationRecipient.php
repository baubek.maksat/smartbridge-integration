<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class NotificationRecipient extends Model
{
    use HasFactory, Observer;

    protected $table = 'notification_recipients';

    protected $fillable = [
    	'id',
    	'user_id',
    	'role_id',
    	'reviewed_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'id');
    }
}
