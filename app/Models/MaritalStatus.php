<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class MaritalStatus extends Model
{
	use HasFactory, Observer;

	protected $fillable = [
		'id',
		'platonus_key'
	];

	public function getRouteKeyName()
	{
		return 'id';
	}

	public function genders()
	{
		return $this->hasMany(MaritalStatusGender::class, 'marital_status_id', 'id');
	}

	public function translation()
	{
		return $this->hasOne(MaritalStatusLang::class, 'id', 'id')
			->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
				? auth()->user()->defaultLangSlug()
				: app()->getLocale()
			);
	}

	public function translations()
	{
		return $this->hasMany(MaritalStatusLang::class, 'id', 'id');
	}
	
	public function getNameAttribute()
	{
		return $this->translation->name;
	}
}
