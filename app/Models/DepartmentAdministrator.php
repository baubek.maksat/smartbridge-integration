<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class DepartmentAdministrator extends Model
{
	use HasFactory, Observer;

    public $incrementing = false;

	protected $table = 'department_administrators';

	protected $fillable = [
		'id'
	];
}
