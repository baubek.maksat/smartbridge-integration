<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class PaymentCategory extends Model
{
    use HasFactory, Observer;

    protected $table = 'payment_categories';

    protected $fillable = [
    	'id',
    	'icon'
    ];

    public function translation()
    {
        return $this->hasOne(PaymentCategoryLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(PaymentCategoryLang::class, 'id', 'id');
    }
}
