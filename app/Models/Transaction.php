<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Transaction extends Model
{
	use HasFactory, Observer;

	protected $fillable = [
		'id',
		'account_id',
		'view_id',
		'datetime',
		'additional_fields',
		'sum'
	];

	protected $casts = [
		'additional_fields' => 'array'
	];

	public function payment()
	{
		return $this->hasOne(Payment::class, 'id', 'id');
	}

	public function replenishment()
	{
		return $this->hasOne(Replenishment::class, 'id', 'id');
	}

	public function transfer()
	{
		return $this->hasOne(Transfer::class, 'id', 'id');
	}

	public function refund()
	{
		return $this->hasOne(Refund::class, 'id', 'id');
	}

	public function accrual()
	{
		return $this->hasOne(Accrual::class, 'id', 'id');
	}

	public function account()
	{
		return $this->hasOne(Account::class, 'id', 'account_id');
	}

	public function view()
	{
		return $this->hasOne(TransactionView::class, 'id', 'view_id');
	}

}
