<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementStage extends Model
{
    use HasFactory, Observer;

    protected $table = 'statement_stages';

    protected $fillable = [
    	'id',
		'slug',
		'statement_id',
		'conditions',
		'processes'
    ];
	
	protected $casts = [
		'conditions' => 'array',
		'processes' => 'array'
	];

    public function routes()
    {
        return $this->hasMany(StatementRoute::class, 'stage_id', 'id');
    }
	
	public function statement()
    {
        return $this->hasOne(Statement::class, 'id', 'statement_id');
    }
}
