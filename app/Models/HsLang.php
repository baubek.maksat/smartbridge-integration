<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class HsLang extends Model
{
    use HasFactory, Observer;

    protected $table = 'hs_langs';

    protected $fillable = [
    	'id',
		'lang',
    	'question',
        'answer'
    ];
}
