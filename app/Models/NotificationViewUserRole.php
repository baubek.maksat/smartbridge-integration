<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class NotificationViewUserRole extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'notification_views_users_roles';

    protected $fillable = [
    	'view_id',
    	'user_id',
    	'role_id',
    	'state'
    ];

    public function view()
    {
        return $this->hasOne(NotificationView::class, 'id', 'view_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }
}
