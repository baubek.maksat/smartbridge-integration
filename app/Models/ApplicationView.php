<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class StatementView extends Model
{
    use HasFactory, SoftDeletes, Observer;

    protected $fillable = [
    	'category_id',
		'responsible_role_id',
		'responsible_user_id',
		'state_id',
        'term_of_consideration'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
		return $this->hasOne(StatementViewLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StatementViewLang::class, 'id', 'id');
    }

    public function category()
    {
        return $this->hasOne(StatementCategory::class, 'id', 'category_id')->withTrashed();
    }
	
	public function statements()
    {
        return $this->hasMany(Statement::class, 'view_id', 'id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function role()
    {
        return $this->hasOne(StudentServiceCenterRole::class, 'id', 'responsible_role_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'responsible_user_id');
    }

    public function components()
    {
        return $this->hasMany(StatementComponent::class, 'view_id', 'id');
    }

   	public function stages()
    {
        return $this->hasMany(StatementViewStage::class, 'statement_view_id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation ? $this->translation->name : null;
    }
	
	public function getInstructionAttribute()
    {
        return $this->translation ? $this->translation->instruction : null;
    }
	
	public function getTemplateAttribute()
    {
        return $this->translation ? $this->translation->template : null;
    }
}
