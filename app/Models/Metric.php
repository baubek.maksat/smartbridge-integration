<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
    use HasFactory;

    protected $table = 'metrics';

    protected $fillable = [
    	'id',
        'visit_id',
        'type_id',
        'app_id',
        'params',
		'route'
    ];

    protected $casts = [
        'params' => 'array'
    ];

    public function visit()
    {
        return $this->hasOne(Visit::class, 'id', 'visit_id');
    }

    public function type()
    {
        return $this->hasOne(Type::class, 'id', 'type_id');
    }

    public function app()
    {
        return $this->hasOne(App::class, 'id', 'app_id');
    }
}
