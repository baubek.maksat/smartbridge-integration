<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class TidingLang extends Model
{
    use HasFactory, SoftDeletes, Observer;
	
	public $incrementing = false;

    protected $table = 'tiding_langs';

    protected $fillable = [
    	'id',
		'lang',
        'heading',
		'discription'
    ];
}
