<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class University extends Model
{
	use HasFactory, Observer;

	protected $fillable = [
		'id', 
		'type_id',
		'cato_id',
		'state_id',
		'country_id',
		'faculty_type_id',
		'administrator_id',
		'department_type_id',
		'speciality_type_id',
		'administrator_worker_position_id',
		'faculty_administrator_worker_position_id',
		'department_administrator_worker_position_id',
		'bin',
		'logo',
		'website',
		'begin_of_work'
	];

	public function getRouteKeyName()
	{
		return 'id';
	}

	public function translation()
	{
		return $this->hasOne(UniversityLang::class, 'id', 'id')
			->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
				? auth()->user()->defaultLangSlug()
				: app()->getLocale()
			);
	}

	public function translations()
	{
		return $this->hasMany(UniversityLang::class, 'id', 'id');
	}

	public function administrator()
	{
		return $this->hasOne(User::class, 'id', 'administrator_id');
	}
	
	public function administratorWorkerPosition()
	{
		return $this->hasOne(WorkerPosition::class, 'id', 'administrator_worker_position_id');
	}
	
	public function administrator_worker_position()
	{
		return $this->hasOne(WorkerPosition::class, 'id', 'administrator_worker_position_id');
	}

	public function emails()
	{
		return $this->hasMany(UniversityEmail::class, 'id', 'id');
	}

	public function country()
	{
		return $this->hasOne(Country::class, 'id', 'country_id');
	}

	public function cato()
	{
		return $this->hasOne(Cato::class, 'id', 'cato_id');
	}

	public function phones()
	{
		return $this->hasMany(UniversityPhone::class, 'id', 'id');
	}

	public function images()
	{
		return $this->hasMany(UniversityImage::class, 'id', 'id');
	}

	public function facultyType()
	{
		return $this->hasOne(FacultyType::class, 'id', 'faculty_type_id');
	}

	public function faculty_type()
	{
		return $this->hasOne(FacultyType::class, 'id', 'faculty_type_id');
	}

	public function department_type()
	{
		return $this->hasOne(DepartmentType::class, 'id', 'department_type_id');
	}

	public function speciality_type()
	{
		return $this->hasOne(SpecialityType::class, 'id', 'speciality_type_id');
	}
	
	public function langs()
	{
		return $this->belongsToMany(Lang::class, 'universities_langs', 'university_id', 'lang_id');
	}

	public function administratorCards()
	{
		return $this->hasMany(UniversityAdministratorCard::class, 'university_id', 'id');
	}

	public function studyLanguages()
	{
		return $this->belongsToMany(StudyLanguage::class, 'universities_study_languages');
	}

	public function facultyAdministratorWorkerPosition()
	{
		return $this->hasOne(WorkerPosition::class, 'id', 'faculty_administrator_worker_position_id');
	}

	public function faculty_administrator_worker_position()
	{
		return $this->hasOne(WorkerPosition::class, 'id', 'faculty_administrator_worker_position_id');
	}

	public function department_administrator_worker_position()
	{
		return $this->hasOne(WorkerPosition::class, 'id', 'department_administrator_worker_position_id');
	}

	public function studentServiceCenter()
	{
		return $this->hasOne(StudentServiceCenter::class, 'university_id', 'id');
	}
	
	public function tidings()
	{
		return $this->hasMany(Tiding::class, 'university_id', 'id');
	}
	
	public function events()
	{
		return $this->hasMany(Event::class, 'university_id', 'id');
	}
	
	public function hsCategories()
	{
		return $this->hasOne(HsCategory::class, 'university_id', 'id');
	}
	
	public function state()
	{
		return $this->hasOne(State::class, 'id', 'state_id');
	}

	public function chronology()
	{
		return $this->hasMany(ActivityLog::class, 'key', 'id')->whereIn('table', ['universities', 'university_langs']);
	}
	
	public function getNameAttribute()
	{
		return $this->translation ? $this->translation->name : null;
	}
	
	public function getAddressAttribute()
	{
		return $this->translation ? $this->translation->address : null;
	}

	public function hasImage($src) {
		if ($this->images()->where('src', $src)->first()) {
			return true;
		} else {
			return false;
		}
	}

	public function hasEmail($value) {
		if ($this->emails()->where('value', $value)->first()) {
			return true;
		} else {
			return false;
		}
	}

	public function hasPhone($value) {
		if ($this->phones()->where('value', $value)->first()) {
			return true;
		} else {
			return false;
		}
	}
}
