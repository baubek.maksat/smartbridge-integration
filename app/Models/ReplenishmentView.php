<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ReplenishmentView extends Model
{
    use HasFactory, Observer;

    protected $table = 'replenishment_views';

    protected $fillable = [
    	'id',
        'slug'
    ];

    public function translation()
    {
        return $this->hasOne(ReplenishmentViewLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(ReplenishmentViewLang::class, 'id', 'id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation ? $this->translation->name : null;
    }
}
