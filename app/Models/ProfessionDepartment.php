<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ProfessionDepartment extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'professions_departments';

    protected $fillable = [
    	'profession_id',
    	'department_id'
    ];
}
