<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserRole extends Model
{
	use HasFactory, Observer;

    public $incrementing = false;

	protected $table = 'users_roles';

	protected $fillable = [
		'user_id', 
		'role_id'
	];
}
