<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudyGroup extends Model
{
    use HasFactory, Observer;

    protected $table = 'study_groups';

    protected $fillable = [
    	'id',
        'platonus_key',
        'type_id',
        'year',
        'term',
        'code',
        'name',
        'hours',
        'subject_id',
		'teacher_id',
        'study_form_id',
        'study_language_id',
        'state_id',
        'student_count',
        'student_count_min',
        'student_count_max'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function type()
    {
        return $this->hasOne(StudyGroupType::class, 'id', 'type_id');
    }

    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }

    public function study_language()
    {
        return $this->hasOne(StudyLanguage::class, 'id', 'study_language_id');
    }

    public function study_form()
    {
        return $this->hasOne(StudyForm::class, 'id', 'study_form_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
	
	public function teacher()
    {
        return $this->hasOne(TeacherCard::class, 'id', 'teacher_id');
    }

    public function students()
    {
        return $this->belongsToMany(StudentCard::class, 'students_study_groups', 'study_group_id', 'student_id');
    }
	
	public function journal()
    {
        return $this->hasOne(Journal::class, 'study_group_id', 'id');
    }
}
