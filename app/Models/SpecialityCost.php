<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class SpecialityCost extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
        'platonus_key',
        'speciality_id',
        'study_form_id',
        'year_of_admission',
        'study_year',
        'state_id',
        'value'
    ];

    public function studyForm()
    {
        return $this->hasOne(StudyForm::class, 'id', 'study_form_id');
    }

    public function study_form()
    {
        return $this->hasOne(StudyForm::class, 'id', 'study_form_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function speciality()
    {
        return $this->hasOne(Speciality::class, 'id', 'speciality_id');
    }
}
