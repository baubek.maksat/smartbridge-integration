<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class StudentServiceCenterUserRole extends Model
{
    use HasFactory, SoftDeletes, Observer;

    protected $table = 'student_service_center_users_roles';

    protected $fillable = [
    	'user_id',
		'role_id'
    ];
}
