<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class AcademicCalendarTypeLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'academic_calendar_type_langs';

    protected $fillable = [
    	'id',
        'lang', 
    	'name'
    ];
}
