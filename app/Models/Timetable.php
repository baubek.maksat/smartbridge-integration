<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Timetable extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id', 
        'platonus_key',
        'week_number',
        'week_id',
        'classroom_id',
        'lesson_hour_id',
        'study_group_id',
        'state_id',
        'online'
    ];

    public function week()
    {
        return $this->hasOne(Week::class, 'id', 'week_id');
    }

    public function classroom()
    {
        return $this->hasOne(Classroom::class, 'id', 'classroom_id');
    }

    public function lessonHour()
    {
        return $this->hasOne(LessonHour::class, 'id', 'lesson_hour_id');
    }

    public function studyGroup()
    {
        return $this->hasOne(StudyGroup::class, 'id', 'study_group_id');
    }
}
