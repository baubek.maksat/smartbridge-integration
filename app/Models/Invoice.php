<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Invoice extends Model
{
	use HasFactory, Observer;

	protected $table = 'invoices';

	protected $fillable = [
		'id',
		'debt',
		'paid',
		'status_id',
		'payment_view_id'
	];

	public function payments()
	{
		return $this->belongsToMany(Payment::class, 'invoices_payments');
	}

	public function paymentView()
	{
		return $this->hasOne(PaymentView::class, 'id', 'payment_view_id');
	}

	public function status()
	{
		return $this->hasOne(InvoiceStatus::class, 'id', 'status_id');
	}

	public function account()
	{
		return $this->hasOne(Account::class, 'id', 'account_id');
	}
}
