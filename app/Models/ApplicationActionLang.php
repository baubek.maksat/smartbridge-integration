<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ApplicationActionLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'application_action_langs';

    protected $fillable = [
    	'id',
    	'name',
    	'make',
		'process',
		'success',
		'fail'
    ];
}
