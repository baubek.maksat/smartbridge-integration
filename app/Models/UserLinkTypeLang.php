<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserLinkTypeLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'user_link_type_langs';

    protected $fillable = [
    	'id',
        'lang',
        'name'
    ];
}
