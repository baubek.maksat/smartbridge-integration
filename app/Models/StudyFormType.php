<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudyFormType extends Model
{
    use HasFactory, Observer;

    protected $table = 'study_form_types';

    protected $fillable = [
    	'id',
        'platonus_key',
        'university_id',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(StudyFormTypeLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudyFormTypeLang::class, 'id', 'id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
