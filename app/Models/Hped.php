<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Hped extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
        'id',
        'user_id',
        'country_id',
        'cato_id',
        'cato_name_kz',
        'cato_name_ru',
        'cato_name_en',
        'university_name_kz',
        'university_name_ru',
        'university_name_en',
        'series',
        'number',
        'date_of_issue',
        'average_score',
        'with_distinction'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function country()
    {
        return $this->hasOne(Citizenship::class, 'id', 'country_id');
    }

    public function cato()
    {
        return $this->hasOne(Cato::class, 'id', 'cato_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
