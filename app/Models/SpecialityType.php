<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class SpecialityType extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(SpecialityTypeLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(SpecialityTypeLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
