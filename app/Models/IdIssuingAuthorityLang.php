<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class IdIssuingAuthorityLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'id_issuing_authority_langs';

    protected $fillable = [
    	'id',
        'lang', 
        'name'
    ];

    public function citizenship()
    {
        return $this->hasOne(Citizenship::class, 'id', 'citizenship_id');
    }

    public function citizenships()
    {
        return $this->hasMany(UserIdTypeCitizenship::class, 'type_id', 'id');
    }
}
