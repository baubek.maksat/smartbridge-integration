<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementStudent extends Model
{
	use HasFactory, Observer;

    public $incrementing = false;

	protected $table = 'statement_student';

	protected $fillable = [
		'statement_id',
		'student_id'
	];
}
