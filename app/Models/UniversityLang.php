<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UniversityLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'university_langs';

    protected $fillable = [
    	'id', 
    	'lang',
    	'name',
    	'cato_name',
    	'address'
    ];
}
