<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class StatementRoute extends Model
{
    use HasFactory, SoftDeletes, Observer;

    protected $table = 'statement_routes';

    protected $fillable = [
        'id',
    	'stage_id',
    	'action_id',
    	'responsible_role_id',
    	'responsible_user_id',
		'responsible_type',
		'delivered_at',
		'started_at',
		'expired_at',
		'ended_at',
    	'message',
		'status',
    	'files',
		'term'
    ];

    protected $casts = [
    	'files' => 'array'
    ];
	
	public function stage()
    {
        return $this->hasOne(StatementStage::class, 'id', 'stage_id');
    }

    public function action()
    {
        return $this->hasOne(StatementAction::class, 'id', 'action_id');
    }
	
	public function role()
    {
        return $this->hasOne(StudentServiceCenterRole::class, 'id', 'responsible_role_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'responsible_user_id')->withTrashed();
    }
}
