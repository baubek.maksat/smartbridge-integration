<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserLinkType extends Model
{
    use HasFactory, Observer;

    protected $table = 'user_link_types';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(UserLinkTypeLang::class, 'id', 'id')->where('lang', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(UserLinkTypeLang::class, 'id', 'id');
    }
}
