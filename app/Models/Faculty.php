<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Faculty extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
        'integration_fields',
        'university_id',
        'administrator_id',
        'classroom_id',
        'state_id'
    ];

    protected $casts = [
        'integration_fields' => 'array'
    ];

    public function translation()
    {
        return $this->hasOne(FacultyLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(FacultyLang::class, 'id', 'id');
    }

    public function university()
    {
        return $this->hasOne(University::class, 'id', 'university_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function administrator()
    {
        return $this->hasOne(User::class, 'id', 'administrator_id');
    }
	
	public function getNameAttribute()
    {
        //	return $this->translation->name;
		return $this->translation ? $this->translation->name : null;
    }
}
