<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Subject extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
    	'platonus_key',
    	'academic_degree_id',
    	'department_id',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(SubjectLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(SubjectLang::class, 'id', 'id');
    }

    public function academicDegree()
    {
        return $this->hasOne(AcademicDegree::class, 'id', 'academic_degree_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function academic_degree()
    {
        return $this->hasOne(AcademicDegree::class, 'id', 'academic_degree_id');
    }

    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }
	
	public function studyGroup()
    {
        return $this->hasOne(StudyGroup::class, 'subject_id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
