<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class Application extends Model
{
    use HasFactory, SoftDeletes, Observer;

    protected $table = 'applications';

    protected $fillable = [
    	'id',
		'responsible_role_id',
		'responsible_user_id',
        'role_id',
    	'status_id',
        'message',
        'view_id',
        'files',
        'hash',
        'lang',
        'src',
        'qr',
		'delivered_at',
		'started_at',
		'ended_at',
		'expired_at'
    ];

    protected $casts = [
        'files' => 'array'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function language()
    {
        return $this->hasOne(Lang::class, 'slug', 'lang');
    }

    /*

    public function student()
    {
        return $this->hasOne(StudentCard::class, 'id', 'student_id');
    }

    */
	
	public function responsibleRole()
    {
        return $this->hasOne(StudentServiceCenterRole::class, 'id', 'responsible_role_id');
    }
	
	public function responsibleUser()
    {
        return $this->hasOne(User::class, 'id', 'responsible_user_id');
    }

    public function view()
    {
        return $this->hasOne(StatementView::class, 'id', 'view_id')->withTrashed();
    }

    public function status()
    {
        return $this->hasOne(StatementStatus::class, 'id', 'status_id');
    }

    public function stages()
	{
		return $this->hasMany(StatementStage::class, 'statement_id', 'id');
	}

    public function comments()
    {
        return $this->hasMany(StatementComment::class, 'statement_id', 'id');
    }

    public function student()
    {
        return $this->hasOneThrough(StudentCard::class, StatementStudent::class, 'statement_id', 'id', 'id', 'student_id');
    }

    public function details()
    {
        return $this->hasMany(StatementDetail::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLang
                ? auth()->user()->defaultLang->slug
                : app()->getLocale()
            );
    }
}