<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TidingIp extends Model
{
    use HasFactory;

    protected $table = 'tidings_ips';

    protected $fillable = [
    	'id',
		'type_id',
        'ip'
    ];
}
