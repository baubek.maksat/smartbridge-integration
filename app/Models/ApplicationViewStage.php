<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementViewStage extends Model
{
    use HasFactory, Observer;

    protected $table = 'statement_view_stages';

    protected $fillable = [
    	'id',
		'slug',
		'statement_view_id',
		'conditions',
		'processes'
    ];

    protected $casts = [
		'conditions' => 'array',
		'processes' => 'array'
	];

    public function routes()
    {
        return $this->hasMany(StatementViewRoute::class, 'stage_id', 'id');
    }

    public function statementView()
    {
        return $this->hasMany(StatementView::class, 'id', 'statement_view_id');
    }
}
