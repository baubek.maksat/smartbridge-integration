<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Speciality extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
        'platonus_key',
        'department_id',
        'profession_id',
        'state_id',
        'code'
    ];

    public function translation()
    {
        return $this->hasOne(SpecialityLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(SpecialityLang::class, 'id', 'id');
    }

    public function department()
    {
        return $this->hasOne(Department::class, 'id', 'department_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function profession()
    {
        return $this->hasOne(Profession::class, 'id', 'profession_id');
    }
	
	public function getNameAttribute()
    {
        //	return $this->translation->name;
		return $this->translation ? $this->translation->name : null;
    }
}
