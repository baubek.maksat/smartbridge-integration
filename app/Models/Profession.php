<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Profession extends Model
{
    use HasFactory, Observer;

    protected $table = 'professions';

    protected $fillable = [
    	'id',
        'platonus_key',
        'university_id',
        'code',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(ProfessionLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(ProfessionLang::class, 'id', 'id');
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'professions_departments', 'profession_id', 'department_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function academicDegress()
    {
        return $this->belongsToMany(AcademicDegree::class, 'professions_academic_degrees', 'profession_id', 'academic_degree_id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation ? $this->translation->name : null;
    }
}
