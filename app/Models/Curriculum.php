<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Curriculum extends Model
{
    use HasFactory, Observer;

    protected $table = 'curriculums';

    protected $fillable = [
    	'id',
        'platonus_key',
        'study_form_id',
        'speciality_id',
        'state_id'
    ];

    public function studyForm()
    {
        return $this->hasOne(StudyForm::class, 'id', 'study_form_id');
    }

    public function speciality()
    {
        return $this->hasOne(Speciality::class, 'id', 'speciality_id');
    }

    public function translation()
    {
        return $this->hasOne(CurriculumLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(CurriculumLang::class, 'id', 'id');
    }
}
