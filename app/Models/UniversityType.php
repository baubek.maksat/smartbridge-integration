<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UniversityType extends Model
{
    use HasFactory, Observer;

    protected $table = 'university_types';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(UniversityTypeLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(UniversityTypeLang::class, 'id', 'id');
    }
}
