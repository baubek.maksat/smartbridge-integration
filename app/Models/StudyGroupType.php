<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudyGroupType extends Model
{
    use HasFactory, Observer;

    protected $table = 'study_group_types';

    protected $fillable = [
    	'id',
        'platonus_key',
        'academic_degree_id',
        'suffix',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(StudyGroupTypeLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudyGroupTypeLang::class, 'id', 'id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
