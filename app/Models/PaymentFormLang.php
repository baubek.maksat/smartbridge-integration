<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class PaymentFormLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'payment_form_langs';

    protected $fillable = [
    	'id',
        'lang',
        'name'
    ];
}
