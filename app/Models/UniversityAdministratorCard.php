<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UniversityAdministratorCard extends Model
{
    use HasFactory, Observer;

    protected $table = 'university_administrator_cards';

    protected $fillable = [
    	'id',
    	'university_id',
    	'user_id',
    	'date_start',
    	'date_end'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
