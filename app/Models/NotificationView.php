<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class NotificationView extends Model
{
    use HasFactory, Observer;

    protected $table = 'notification_views';

    protected $fillable = [
    	'id',
    	'icon'
    ];

    public function translation()
    {
        return $this->hasOne(NotificationViewLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(NotificationViewLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
