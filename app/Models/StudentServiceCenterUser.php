<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class StudentServiceCenterUser extends Model
{
    use HasFactory, SoftDeletes, Observer;

    protected $table = 'student_service_centers_users';

    protected $fillable = [
    	'student_service_center_id',
		'user_id'
    ];

	public function translation()
    {
        return $this->hasOne(StudentServiceCenterRoleLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudentServiceCenterRoleLang::class, 'id', 'id');
    }

	public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
	
	public function access()
    {
        return $this->hasOne(StudentServiceCenterRoleAccess::class, 'id', 'access_id');
    }
	
	public function users()
	{
		return $this->belongsToMany(User::class, 'student_service_center_users_roles', 'role_id', 'user_id');
	}
	
	public function studentServiceCenter()
	{
		return $this->hasOne(StudentServiceCenter::class, 'id', 'student_service_center_id');
	}
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
	
}
