<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Classroom extends Model
{
    use HasFactory, Observer;

    protected $table = 'classrooms';

    protected $fillable = [
        'id',
    	'platonus_key',
    	'type_id',
    	'campus_id',
    	'capacity',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function campus()
    {
        return $this->hasOne(Campus::class, 'id', 'campus_id');
    }

    public function type()
    {
        return $this->hasOne(ClassroomType::class, 'id', 'type_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function translation()
    {
        return $this->hasOne(ClassroomLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(ClassroomLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
