<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Feedback extends Model
{
    use HasFactory, Observer;

    protected $table = 'feedbacks';

    protected $fillable = [
    	'id',
        'university_id',
        'visit_id',
        'essence',
        'more'
    ];
}
