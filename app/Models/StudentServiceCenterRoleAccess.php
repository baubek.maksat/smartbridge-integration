<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentServiceCenterRoleAccess extends Model
{
    use HasFactory, Observer;

    protected $table = 'student_service_center_role_accesses';

    protected $fillable = [
    	'slug'
    ];

    public function translation()
    {
        return $this->hasOne(StudentServiceCenterRoleAccessLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudentServiceCenterRoleAccessLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation ? $this->translation->name : null;
    }
}
