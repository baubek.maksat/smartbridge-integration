<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Device extends Model
{
    use HasFactory, Observer;

    protected $table = 'devices';

    protected $fillable = [
        'platform_id',
        'brand',
        'model',
        'imei',
        'width',
        'height',
        'signature',
        'ip'
    ];

    public function platform()
    {
        return $this->hasOne(Platform::class, 'id', 'platform_id');
    }
}
