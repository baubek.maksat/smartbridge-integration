<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Catolang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'cato_langs';

    protected $fillable = [
    	'id',
        'lang', 
    	'name'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function extend()
    {
        return $this->hasOne(Cato::class, 'id', 'id');
    }

    public function citizenship()
    {
        return $this->hasOne(Citizenship::class, 'id', 'citizenship_id');
    }

    public function cato()
    {
        return $this->hasOne(Cato::class, 'id', 'parent_id');
    }

    public function parent()
    {
        return $this->hasOne(Cato::class, 'id', 'parent_id')->with('cato');
    }

    public function catos()
    {
        return $this->hasMany(Cato::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(Cato::class, 'parent_id', 'id')->with('catos');
    }
}
