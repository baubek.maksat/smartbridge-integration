<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class Event extends Model
{
    use HasFactory, SoftDeletes, Observer;

    protected $fillable = [
    	'id',
		'image',
		'state_id',
		'university_id',
		'holding_date',
		'holding_time',
        'date_of_publication'
    ];

    public function translation()
    {
        return $this->hasOne(EventLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(EventLang::class, 'id', 'id');
    }

    public function ips()
    {
        return $this->hasMany(EventIp::class, 'id', 'id');
    }
	
	public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
	
	public function getHeadingAttribute()
    {
        return $this->translation->heading;
    }
	
	public function getDiscriptionAttribute()
    {
        return $this->translation->discription;
    }
}
