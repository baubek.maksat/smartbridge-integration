<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class ApplicationComponent extends Model
{
    use HasFactory, SoftDeletes, Observer;

    protected $table = 'statement_components';

    protected $fillable = [
    	'id',
        'type_id',
        'view_id',
        'rules',
		'resource'
    ];

    protected $casts = [
        'rules' => 'array'
    ];

    public function translation()
    {
        return $this->hasOne(ApplicationComponentLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(ApplicationComponentLang::class, 'id', 'id');
    }

    public function type()
    {
        return $this->hasOne(ApplicationComponentType::class, 'id', 'type_id');
    }

    public function view()
    {
        return $this->hasOne(ApplicationView::class, 'id', 'view_id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation ? $this->translation->name : null;
    }
	
	public function getDataAttribute()
    {
        return $this->translation ? $this->translation->data : null;
    }
}
