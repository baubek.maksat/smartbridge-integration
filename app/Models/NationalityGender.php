<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class NationalityGender extends Model
{
    use HasFactory, Observer;

    protected $table = 'nationalities_genders';

    protected $fillable = [
    	'id',
    	'nationality_id',
    	'gender_id'
    ];

    public function translation()
    {
        return $this->hasOne(NationalityGenderLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(NationalityGenderLang::class, 'id', 'id');
    }
}
