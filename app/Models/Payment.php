<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Payment extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
    	'view_id'
    ];

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class, 'invoices_payments');
    }

    public function view()
    {
        return $this->hasOne(PaymentView::class, 'id', 'view_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id', 'id');
    }
}
