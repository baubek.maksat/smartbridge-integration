<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Account extends Model
{
    use HasFactory, Observer;

    protected $table = 'accounts';

    protected $fillable = [
    	'id',
        'user_id',
        'view_id',
        'balance',
        'state_id'
    ];

	public function view()
    {
        return $this->hasOne(AccountView::class, 'id', 'view_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'account_id', 'id');
    }
}
