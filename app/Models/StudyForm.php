<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudyForm extends Model
{
    use HasFactory, Observer;

    protected $table = 'study_forms';

    protected $fillable = [
    	'id',
        'platonus_key',
        'type_id',
        'academic_degree_id',
        'state_id',
        'course_count'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(StudyFormLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudyFormLang::class, 'id', 'id');
    }

    public function academicDegree()
    {
        return $this->hasOne(AcademicDegree::class, 'id', 'academic_degree_id');
    }

    public function academic_degree()
    {
        return $this->hasOne(AcademicDegree::class, 'id', 'academic_degree_id');
    }

    public function type()
    {
        return $this->hasOne(StudyFormType::class, 'id', 'type_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
