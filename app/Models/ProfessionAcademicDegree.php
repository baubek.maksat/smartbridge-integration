<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ProfessionAcademicDegree extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'professions_academic_degrees';

    protected $fillable = [
    	'profession_id',
    	'academic_degree_id'
    ];
}
