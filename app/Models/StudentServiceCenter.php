<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

use Carbon\Carbon;

class StudentServiceCenter extends Model
{
    use HasFactory, SoftDeletes, Observer;

    protected $fillable = [
    	'id',
		'head_id',
		'state_id',
    	'university_id',
		'accepting_applications_24_7'
    ];
	
	public function currentOperatingMode()
	{
		return $this->hasOne(StudentServiceCenterOperatingMode::class, 'id', 'id')->where('week_id', Carbon::now()->dayOfWeekIso);
	}

    public function operating_modes()
    {
    	return $this->hasMany(StudentServiceCenterOperatingMode::class, 'id', 'id');
    }

    public function langs()
    {
        return $this->belongsToMany(Lang::class, 'student_service_centers_langs', 'ssc_id', 'lang_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'student_service_centers_users', 'ssc_id', 'user_id')->wherePivotNull('deleted_at');
    }

    public function university()
    {
    	return $this->hasOne(University::class, 'id', 'university_id');
    }
	
	public function roles()
    {
        return $this->hasMany(StudentServiceCenterRole::class, 'student_service_center_id', 'id');
    }
	
	public function categories()
    {
        return $this->hasMany(StatementCategory::class, 'ssc_id', 'id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
	
	public function head()
    {
        return $this->hasOne(User::class, 'id', 'head_id');
    }

    public function translation()
    {
        return $this->hasOne(StudentServiceCenterLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudentServiceCenterLang::class, 'id', 'id');
    }

    public function getNameAttribute()
    {
        return $this->translation ? $this->translation->name : null;
    }
	
	public function hasLang($lang) {
		if ($this->langs->contains('slug', $lang)) {
			return true;
		} else {
			return false;
		}
	}
}
