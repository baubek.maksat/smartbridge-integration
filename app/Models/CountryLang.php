<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class CountryLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'country_langs';

    protected $fillable = [
    	'id', 
    	'lang',
    	'name'
    ];
}
