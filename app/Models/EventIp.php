<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventIp extends Model
{
    use HasFactory;

    protected $table = 'events_ips';

    protected $fillable = [
    	'id',
		'type_id',
        'ip'
    ];
}
