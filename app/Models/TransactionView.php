<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class TransactionView extends Model
{
    use HasFactory, Observer;

    protected $table = 'transaction_views';

    protected $fillable = [
    	'id',
        'icon'
    ];

    public function translation()
    {
        return $this->hasOne(TransactionViewLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(TransactionViewLang::class, 'id', 'id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
