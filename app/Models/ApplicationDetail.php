<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementDetail extends Model
{
    use HasFactory, Observer;

    protected $table = 'statement_details';

    protected $fillable = [
    	'id',
        'type_id',
        'lang',
        'title',
        'value'
    ];

    protected $casts = [
        'value' => 'array'
    ];

    public function type()
    {
        return $this->hasOne(StatementComponentType::class, 'id', 'type_id');
    }
}