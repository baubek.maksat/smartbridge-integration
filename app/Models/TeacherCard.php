<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class TeacherCard extends Model
{
    use HasFactory, Observer;

    protected $table = 'teacher_cards';

    protected $fillable = [
        'id',
        'platonus_key',
		'user_id',
        'department_id',
        'date_start',
        'date_end'
    ];
	
	public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function translation()
    {
        return $this->hasOne(StudentCardLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudentCardLang::class, 'id', 'id');
    }

    public function studyGroups()
    {
        return $this->hasMany(StudyGroup::class, 'teacher_id', 'id');
    }
}
