<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class BenefitLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'benefit_langs';

    protected $fillable = [
    	'id',
        'lang', 
    	'name'
    ];
}
