<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Benefit extends Model
{
    use HasFactory, Observer;

    protected $table = 'benefits';

    protected $fillable = [
    	'id',
        'platonus_key',
        'university_id',
        'inclusive_education',
        'state_id'
    ];

    public function translation()
    {
        return $this->hasOne(BenefitLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(BenefitLang::class, 'id', 'id');
    }
}
