<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementViewLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'statement_view_langs';

    protected $fillable = [
    	'id',
    	'lang', 
    	'name',
        'instruction',
        'template'
    ];

    protected $casts = [
    	'instruction' => 'array'
    ];
}
