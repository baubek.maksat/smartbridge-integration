<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentCardStatus extends Model
{
    use HasFactory, Observer;

    protected $table = 'student_card_statuses';

    protected $fillable = [
    	'id',
        'integration_fields'
    ];

    protected $casts = [
        'integration_fields' => 'array'
    ];

    public function translation()
    {
        return $this->hasOne(StudentCardStatusLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudentCardStatusLang::class, 'id', 'id');
    }
}
