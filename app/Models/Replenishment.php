<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Replenishment extends Model
{
    use HasFactory, Observer;

    protected $table = 'replenishments';

    protected $fillable = [
    	'id',
    	'view_id',
        'account_id'
    ];

    public function view()
    {
        return $this->hasOne(ReplenishmentView::class, 'id', 'view_id');
    }

    public function account()
    {
        return $this->hasOne(Account::class, 'id', 'account_id');
    }
}
