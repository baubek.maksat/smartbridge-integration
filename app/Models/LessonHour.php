<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class LessonHour extends Model
{
    use HasFactory, Observer;

    protected $table = 'lesson_hours';

    protected $fillable = [
    	'id',
        'platonus_key',
        'shift_id',
        'state_id',
        'start',
        'end'
    ];

    public function shift()
    {
        return $this->hasOne(Shift::class, 'shift_id', 'id');
    }
}
