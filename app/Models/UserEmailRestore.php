<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserEmailRestore extends Model
{
    use HasFactory, Observer;

    protected $table = 'user_email_restores';

    protected $fillable = [
    	'user_id',
    	'lang_id',
        'type_id',
    	'valid_until', 
        'ip',
        'ua',
        'ua_device_brand',
        'ua_device_model',
        'ua_os',
        'ua_os_major',
        'ua_os_minor',
        'ua_browser',
        'ua_browser_major',
        'ua_browser_minor',
    	'fit',
    	'hash',
    	'src'
    ];

   	public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
