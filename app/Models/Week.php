<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Week extends Model
{
    use HasFactory, Observer;

    protected $table = 'weeks';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(WeekLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(WeekLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
	
	public function getShortNameAttribute()
    {
        return $this->translation->short_name;
    }
}
