<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Notification extends Model
{
    use HasFactory, Observer;

    protected $table = 'notifications';

    protected $fillable = [
    	'view_id',
		'params'
    ];
	
	protected $casts = [
    	'params' => 'array'
    ];

    public function translation()
    {
        return $this->hasOne(NotificationLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(NotificationLang::class, 'id', 'id');
    }

    public function view()
    {
        return $this->hasOne(NotificationView::class, 'id', 'view_id');
    }
	
	public function recipient()
    {
        return $this->hasOne(NotificationRecipient::class, 'id', 'id')->where('user_id', auth()->user()->id);
    }
	
	public function recipients()
    {
        return $this->hasMany(NotificationRecipient::class, 'id', 'id');
    }
	
	public function getTextAttribute()
    {
        //  return $this->translation ? $this->translation->text : null;
        return $this->translation->text;
    }
}
