<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Sec extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
        'id',
        'user_id',
        'country_id',
        'cato_id',
        'cato_name_kz',
        'cato_name_ru',
        'cato_name_en',
        'school_name_kz',
        'school_name_ru',
        'school_name_en',
        'series',
        'number',
        'date_of_issue',
        'average_score',
        'with_distinction',
        'gold_mark'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function cato()
    {
        return $this->hasOne(Cato::class, 'id', 'cato_id');
    }
}
