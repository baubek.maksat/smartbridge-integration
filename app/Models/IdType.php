<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class IdType extends Model
{
    use HasFactory, Observer;

    protected $table = 'id_types';

    protected $fillable = [
    	'id',
        'platonus_key'
    ];

    public function citizenships()
    {
        return $this->hasMany(UserIdTypeCitizenship::class, 'type_id', 'id');
    }

    public function translation()
    {
        return $this->hasOne(IdTypeLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLang ? auth()->user()->defaultLang->slug : app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(IdTypeLang::class, 'id', 'id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
