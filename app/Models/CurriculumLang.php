<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class CurriculumLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'curriculum_langs';

    protected $fillable = [
    	'id',
        'lang', 
    	'name'
    ];
}
