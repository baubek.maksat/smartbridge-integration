<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Transcript extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
        'platonus_key',
		'course_number',
		'student_id',
		'term',
		'credits',
		'traditional_mark',
		'alpha_mark',
		'numeral_mark',
		'total_mark',
		'exam_mark',
    	'student_id',
    	'state_id'
    ];

    public function translation()
    {
        return $this->hasOne(TranscriptLang::class, 'id', 'id')
        	->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
				? auth()->user()->defaultLangSlug()
				: app()->getLocale()
			);
    }

    public function translations()
    {
        return $this->hasMany(TranscriptLang::class, 'id', 'id');
    }
	
	public function getSubjectCodeAttribute()
	{
		return $this->translation->subject_code;
	}
	
	public function getSubjectNameAttribute()
	{
		return $this->translation->subject_name;
	}
}
