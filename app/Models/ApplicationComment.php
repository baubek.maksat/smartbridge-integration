<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class StatementComment extends Model
{
    use HasFactory, SoftDeletes, Observer;

	protected $fillable = [
		'id',
		'statement_id',
		'user_id',
		'reply_id',
		'message',
		'files'
	];

    protected $casts = [
        'files' => 'array'
    ];
	
	public function statement()
    {
        return $this->hasOne(Statement::class, 'id', 'statement_id');
    }
	
	public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
	
	public function reply()
    {
        return $this->hasOne(StatementComment::class, 'id', 'reply_id');
    }
}
