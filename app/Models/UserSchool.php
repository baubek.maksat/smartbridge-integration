<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserSchool extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
        'id',
        'user_id', 
        'citizenship_id',
        'cato_id',
        'cato_name_kz',
        'cato_name_ru',
        'cato_name_en', 
        'name_kz',
        'name_ru',
        'name_en',
        'year_start',
        'year_end'
    ];

    public function citizenship()
    {
        return $this->hasOne(Citizenship::class, 'id', 'citizenship_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function cato()
    {
        return $this->hasOne(Cato::class, 'id', 'cato_id');
    }

    public function certificate()
    {
        return $this->hasOne(UserSchoolCertificate::class, 'user_school_id', 'id');
    }
}
