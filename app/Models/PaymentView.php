<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class PaymentView extends Model
{
    use HasFactory, Observer;

    protected $table = 'payment_views';

    protected $fillable = [
    	'id',
        'category_id'
    ];

    public function translation()
    {
        return $this->hasOne(PaymentViewLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(PaymentViewLang::class, 'id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'payment_view_id', 'id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation->name;
    }

}
