<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class InvoiceStatus extends Model
{
    use HasFactory, Observer;

    protected $table = 'invoice_statuses';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(InvoiceStatusLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(InvoiceStatusLang::class, 'id', 'id');
    }
}
