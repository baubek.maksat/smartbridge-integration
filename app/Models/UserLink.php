<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserLink extends Model
{
    use HasFactory, Observer;

    protected $table = 'user_links';

    protected $fillable = [
    	'id', 
    	'lang',
    	'name'
    ];
}
