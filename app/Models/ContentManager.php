<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ContentManager extends Model
{
	use HasFactory, Observer;

	protected $fillable = [
		'id',
	];

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	public function card()
    {
        return $this->hasOne(ContentManagerCard::class, 'user_id', 'id')->whereNull('date_end');
    }
}
