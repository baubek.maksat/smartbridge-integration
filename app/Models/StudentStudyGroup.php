<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentStudyGroup extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'students_study_groups';

    protected $fillable = [
    	'study_group_id',
        'student_id',
        'cicle_id'
    ];

    public function studyGroup()
    {
        return $this->hasOne(StudyGroup::class, 'id', 'study_group_id');
    }

    public function student()
    {
        return $this->hasOne(StudentCard::class, 'id', 'student_id');
    }

    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }
}
