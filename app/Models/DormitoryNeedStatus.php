<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class DormitoryNeedStatus extends Model
{
    use HasFactory, Observer;

    protected $table = 'dormitory_need_statuses';

    protected $fillable = [
    	'id',
    	'platonus_key',
        'university_id',
        'state_id'
    ];

    public function translation()
    {
        return $this->hasOne(DormitoryNeedStatusLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(DormitoryNeedStatusLang::class, 'id', 'id');
    }
}
