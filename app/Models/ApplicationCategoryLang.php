<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementCategoryLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'statement_category_langs';

    protected $fillable = [
    	'id',
    	'lang', 
    	'name'
    ];
}
