<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class App extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'apps';

    protected $fillable = [
    	'id',
    	'release_id',
        'device_id',
        'registration_id'
    ];

    public function release()
    {
        return $this->hasOne(AppRelease::class, 'id', 'release_id');
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }
}