<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class See extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
        'id',
        'type_id',
        'user_id',
        'series',
        'number',
        'individual_code_of_the_tested',
        'date_of_issue',
        'src'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }
}
