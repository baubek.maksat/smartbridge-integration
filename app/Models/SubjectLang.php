<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class SubjectLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'subject_langs';

    protected $fillable = [
    	'id',
    	'lang', 
    	'code',
    	'name',
    	'info',
        'syllabus'
    ];

    protected $casts = [
        'syllabus' => 'array'
    ];
}
