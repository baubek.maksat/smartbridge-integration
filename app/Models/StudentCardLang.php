<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentCardLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'student_card_langs';

    protected $fillable = [
        'id',
        'lang', 
        'arrived_cato_name'
    ];
}
