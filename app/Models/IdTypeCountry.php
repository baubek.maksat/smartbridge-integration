<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class IdTypeCountry extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'id_types_countries';

    protected $fillable = [
    	'id_type_id',
        'country_id'
    ];
}
