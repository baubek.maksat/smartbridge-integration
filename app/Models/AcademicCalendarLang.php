<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class AcademicCalendarLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'academic_calendar_langs';

    protected $fillable = [
    	'id',
        'lang', 
    	'name'
    ];
}
