<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class ApplicationCategory extends Model
{
	use HasFactory, SoftDeletes, Observer;

	protected $fillable = [
		'id',
		'parent_id',
		'ssc_id',
		'state_id'
	];

	public function translation()
	{
		return $this->hasOne(StatementCategoryLang::class, 'id', 'id')
			->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
				? auth()->user()->defaultLangSlug()
				: app()->getLocale()
			);
	}

	public function translations()
	{
		return $this->hasMany(StatementCategoryLang::class, 'id', 'id');
	}

	public function ssc()
	{
		return $this->hasOne(StudentServiceCenter::class, 'id', 'ssc_id');
	}

	public function parent()
	{
		return $this->hasOne(StatementCategory::class, 'id', 'parent_id');
	}

	public function state()
	{
		return $this->hasOne(State::class, 'id', 'state_id');
	}

	public function childrens()
	{
		return $this->hasMany(StatementCategory::class, 'parent_id', 'id');
	}

	public function views()
	{
		return $this->hasMany(StatementView::class, 'category_id', 'id');
	}

	public function getNameAttribute()
	{
		return $this->translation ? $this->translation->name : null;
	}
}
