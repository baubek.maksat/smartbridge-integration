<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class TranscriptLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'transcript_langs';

    protected $fillable = [
    	'id',
    	'lang', 
    	'subject_code',
    	'subject_name'
    ];
}
