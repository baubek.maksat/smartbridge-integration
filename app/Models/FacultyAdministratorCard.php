<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class FacultyAdministratorCard extends Model
{
    use HasFactory, Observer;

    protected $table = 'faculty_administrator_cards';

    protected $fillable = [
    	'id',
		'platonus_key',
        'faculty_id',
		'user_id',
    	'faculty_id',
    	'date_start',
    	'date_end'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
