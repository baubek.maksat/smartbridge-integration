<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Journal extends Model
{
    use HasFactory, Observer;

    protected $table = 'journals';

    protected $fillable = [
    	'id',
		'platonus_key',
		'student_id',
		'study_group_id',
		'mark',
		'state_id'
    ];
	
	public function studyGroup()
	{
		return $this->hasOne(StudyGroup::class, 'id', 'study_group_id');
	}
}
