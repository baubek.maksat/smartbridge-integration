<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentCard extends Model
{
    use HasFactory, Observer;

    protected $table = 'student_cards';

    protected $fillable = [
        'id',
        'platonus_key',
        'user_id',
        'group_id',
        'payment_form_id',
        'study_form_id',
        'course_number',
        'date_start',
        'date_end',
		'gpa',
		'study_language_id',
        'grant_number',
		'grant_type_id',
        'transcript_number',
        'benefit_id',
        'quota_id',
        'state_id',
        'speciality_id',
        'verified_at',
        'integration_fields',
        'status_id'
    ];

    protected $casts = [
        'date_start' => 'date',
        'date_end' => 'date',
        'integration_fields' => 'array'
    ];

    public function translation()
    {
        return $this->hasOne(StudentCardLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudentCardLang::class, 'id', 'id');
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'student_id', 'id');
    }

    public function paymentForm()
    {
        return $this->hasOne(PaymentForm::class, 'id', 'payment_form_id');
    }

    public function studyForm()
    {
        return $this->hasOne(StudyForm::class, 'id', 'study_form_id');
    }

    public function studyLanguage()
    {
        return $this->hasOne(StudyLanguage::class, 'id', 'study_language_id');
    }

    public function studyGroups()
    {
        return $this->belongsToMany(StudyGroup::class, 'students_study_groups', 'student_id', 'study_group_id');
    }

    public function group()
    {
        return $this->hasOne(Group::class, 'id', 'group_id');
    }

    public function benefit()
    {
        return $this->hasOne(Benefit::class, 'id', 'benefit_id');
    }

    public function quota()
    {
        return $this->hasOne(Quota::class, 'id', 'quota_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function grantType()
    {
        return $this->hasOne(GrantType::class, 'id', 'grant_type_id');
    }
}
