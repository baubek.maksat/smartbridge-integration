<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    use HasFactory;

    protected $fillable = [
    	'user_id', 
    	'role_id', 
        'lang_id',
        'type_id',
        'app_id',
    	'ip',
        'ua',
        'ua_device_brand',
        'ua_device_model',
        'ua_os',
        'ua_os_major',
        'ua_os_minor',
        'ua_browser',
        'ua_browser_major',
        'ua_browser_minor',
        'token'
    ];

    public function role()
    {
    	return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function lang()
    {
        return $this->hasOne(Lang::class, 'id', 'lang_id');
    }

    public function type()
    {
        return $this->hasOne(Type::class, 'id', 'type_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function app()
    {
        return $this->hasOne(App::class, 'id', 'app_id');
    }
}
