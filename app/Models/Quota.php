<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Quota extends Model
{
    use HasFactory, Observer;

    protected $table = 'quotas';

    protected $fillable = [
    	'id',
        'platonus_key',
        'benefit_id',
        'state_id'
    ];

    public function benefit()
    {
        return $this->hasOne(Benefit::class, 'id', 'benefit_id');
    }

    public function translation()
    {
        return $this->hasOne(QuotaLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(QuotaLang::class, 'id', 'id');
    }
}
