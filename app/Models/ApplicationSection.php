<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementSection extends Model
{
    use HasFactory, Observer;

    protected $table = 'statement_sections';

    protected $fillable = [
    	'id',
    ];

    public function translation()
    {
        return $this->hasOne(StatementSectionLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StatementSectionLang::class, 'id', 'id');
    }

    public function views()
    {
        return $this->belongsToMany(StatementView::class, 'statement_sections_views', 'section_id', 'view_id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
