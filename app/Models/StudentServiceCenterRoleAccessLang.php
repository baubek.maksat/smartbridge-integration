<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentServiceCenterRoleAccessLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'student_service_center_role_access_langs';

    protected $fillable = [
    	'id', 
    	'lang',
    	'name'
    ];
}
