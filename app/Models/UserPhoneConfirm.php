<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserPhoneConfirm extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'user_id',
    	'lang_id',
    	'valid_until', 
    	'fit',
    	'code',
    	'src'
    ];
}
