<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class PaymentForm extends Model
{
    use HasFactory, Observer;

    protected $table = 'payment_forms';

    protected $fillable = [
    	'id',
        'platonus_key',
        'university_id',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(PaymentFormLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(PaymentFormLang::class, 'id', 'id');
    }

    public function university()
    {
        return $this->hasOne(University::class, 'id', 'university_id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
