<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Gender extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
        'platonus_key'
    ];

    public function getForSelect()
    {
        return $this->select('id');
    }

    public function translation()
    {
        return $this->hasOne(GenderLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(GenderLang::class, 'id', 'id');
    }
	
	public function getNameAttribute($value)
	{
		return $this->translation->name;
	}
}
