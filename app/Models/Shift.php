<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Shift extends Model
{
    use HasFactory, Observer;

    protected $table = 'shifts';

    protected $fillable = [
        'id',
        'platonus_key',
        'university_id',
        'state_id',
        'start',
        'end'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(ShiftLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(ShiftLang::class, 'id', 'id');
    }
}
