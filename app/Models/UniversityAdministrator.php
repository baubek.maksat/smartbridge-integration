<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UniversityAdministrator extends Model
{
	use HasFactory, Observer;

    public $incrementing = false;

	protected $table = 'university_administrators';

	protected $fillable = [
		'id'
	];

	public function user()
	{
		return $this->hasOne(User::class, 'id', 'id');
	}
}
