<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class NotificationLang extends Model
{
    use HasFactory, Observer;

    protected $table = 'notification_langs';

    public $incrementing = false;

    protected $fillable = [
    	'id',
    	'lang',
		'text'
    ];
}
