<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Integration;
use App\Models\Benefit;

use Validator;
use DB;

class BenefitController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.inclusive_education' => 'required|string|in:0,1',
			'data.*.name_kz' => 'nullable|string|max:511',
			'data.*.name_ru' => 'nullable|string|max:511',
			'data.*.name_en' => 'nullable|string|max:511'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$benefit = Benefit::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['id']
						]))
						->first();

					if ($benefit) {
						$benefit->update([
							'inclusive_education' => $data['inclusive_education']
						]);

						foreach ($langs as $lang) {
							if (isset($data['name_'.$lang])) {
								$benefit->translations()->where('lang', $lang)->update([
									'name' => $data['name_'.$lang]
								]);
							}
						}
					} else {
						$benefit = Benefit::create([
							'university_id' => $request->university_id,
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'inclusive_education' => $data['inclusive_education'],
							'state_id' => 2
						]);

						foreach ($langs as $lang) {
							if (isset($data['name_'.$lang])) {
								$benefit->translation()->create([
									'lang' => $lang,
									'name' => $data['name_'.$lang]
								]);
							}
						}

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'benefits',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
