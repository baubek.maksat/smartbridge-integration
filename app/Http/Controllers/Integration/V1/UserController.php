<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Integration;
use App\Models\Country;
use App\Models\User;
use App\Models\Cato;
use App\Models\Lang;

use Validator;
use DB;

class UserController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.role' => [
				'required',
				'in:teacher,student,university-adminstrator,faculty-Administrator,department-administrator'
			],
			'data.*.id' => 'required|integer',
			'data.*.username' => 'required|string|max:255',
			'data.*.password' => 'required|string|max:255',
			'data.*.surname' => 'required|string|max:255',
			'data.*.name' => 'required|string|max:255',
			'data.*.patronymic' => 'nullable|string|max:255',
			'data.*.surname_en' => 'nullable|string|max:255',
			'data.*.name_en' => 'nullable|string|max:255',
			'data.*.patronymic_en' => 'nullable|string|max:255',
			'data.*.birth_date' => 'required|date_format:Y-m-d',
			'data.*.gender_id' => 'required|integer',
			'data.*.marital_status_id' => 'nullable|string|max:255',
			'data.*.email' => 'nullable|email|max:255',
			'data.*.phone' => 'nullable|string|max:255',
			'data.*.nationality_id' => 'nullable|integer',
			'data.*.birth_country_id' => 'nullable|integer',
			'data.*.birth_cato_id' => 'nullable|integer',
			'data.*.birth_cato_name' => 'nullable|string|max:255',
			'data.*.registration_address' => 'nullable|string|max:512',
			'data.*.living_address' => 'nullable|string|max:512',
			'data.*.registration_cato_id' => 'nullable|integer',
			'data.*.living_cato_id' => 'nullable|integer'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$user = $data['role'] == 'student' ? User::query()
						->whereHas('student.cards', function($query) use ($request, $data) {
							$query
								->where('integration_fields->platonus->university_id', $request->university_id)
								->where('integration_fields->platonus->id', $data['id']);
						})
						->first() : null;

					$user = !$user ? User::query()
						->where('surname', $data['surname'])
						->where('name', $data['name'])
						->where('birth_date', $data['birth_date'])
						->first() : $user;

					$birth_country = isset($data['birth_country_id']) ? Country::query()
						->where('id', $data['birth_country_id'])
						->first() : null;

					$birth_cato = isset($data['birth_cato_code']) ? Cato::query()
						->where('code', $data['birth_cato_code'])
						->first() : null;

					$registration_cato = isset($data['registration_cato_code']) ? Cato::query()
						->where('code', $data['registration_cato_code'])
						->first() : null;

					$living_cato = isset($data['living_cato_code']) ? Cato::query()
						->where('code', $data['living_cato_code'])
						->first() : null;

					$validator = Validator::make($data, [
						'email' => 'nullable|email|unique:users,email'
					]);

					if ($validator->fails()) {
						$data['email'] = null;
					}

					$validator = Validator::make($data, [
						'phone' => 'nullable|string|unique:users,phone'
					]);

					if ($validator->fails()) {
						$data['phone'] = null;
					}

					if ($user) {
						$user->update([
							'password' => bcrypt($data['password']),
							'surname_en' => isset($data['surname_en']) ? $data['surname_en'] : null,
							'name_en' => isset($data['name_en']) ? $data['name_en'] : null,
							'patronymic_en' => isset($data['patronymic_en']) ? $data['patronymic_en'] : null,
							'email' => isset($data['email']) ? $data['email'] : null,
							'phone' => isset($data['phone']) ? $data['phone'] : null,
							'nationality_id' => isset($data['nationality_id']) ? $data['nationality_id'] : null,
							'marital_status_id' => isset($data['marital_status_id']) ? $data['marital_status_id'] : null,
							'birth_country_id' => $birth_country ? $birth_country->id : null,
							'birth_cato_id' => $birth_cato ? $birth_cato->id : null,
							'email_verified' => '1'
						]);

						foreach ($langs as $lang) {
							if ($user->translations()->where('lang', $lang)->first()) {
								$user->translations()->where('lang', $lang)->update([
									'birth_cato_name' => isset($data['birth_cato_name_'.$lang]) ? $data['birth_cato_name_'.$lang] : null,
									'registration_address' => isset($data['registration_address_'.$lang]) ? $data['registration_address_'.$lang] : null,
									'living_address' => isset($data['living_address_'.$lang]) ? $data['living_address_'.$lang] : null
								]);
							} else {
								$user->translations()->create([
									'lang' => $lang,
									'birth_cato_name' => isset($data['birth_cato_name_'.$lang]) ? $data['birth_cato_name_'.$lang] : null,
									'registration_address' => isset($data['registration_address_'.$lang]) ? $data['registration_address_'.$lang] : null,
									'living_address' => isset($data['living_address_'.$lang]) ? $data['living_address_'.$lang] : null
								]);
							} 
						}

						if ($user->wasChanged()) {
							$updated = 1;
						}
					 } else {
						$user = User::create([
							'username' => 'un'.date('ymdhisu').rand(100000, 999999),
							'password' => bcrypt($data['password']),
							'surname' => $data['surname'],
							'name' => $data['name'],
							'patronymic' => isset($data['patronymic']) ? $data['patronymic'] : null,
							'surname_en' => isset($data['surname_en']) ? $data['surname_en'] : null,
							'name_en' => isset($data['name_en']) ? $data['name_en'] : null,
							'patronymic_en' => isset($data['patronymic_en']) ? $data['patronymic_en'] : null,
							'birth_date' => $data['birth_date'],
							'birth_country_id' => $birth_country ? $birth_country->id : null,
							'birth_cato_id' => $birth_cato ? $birth_cato->id : null,
							'gender_id' => $data['gender_id'],
							'marital_status_id' => isset($data['marital_status_id']) ? $data['marital_status_id'] : null,
							'registration_cato_id' => $registration_cato ? $registration_cato->id : null,
							'living_cato_id' => $living_cato ? $living_cato->id : null,
							'email' => isset($data['email']) ? $data['email'] : null,
							'phone' => isset($data['phone']) ? $data['phone'] : null,
							'nationality_id' => isset($data['nationality_id']) ? $data['nationality_id'] : null,
							'email_verified' => '1',
							'access' => '1',
							'integration_fields' => [
								'platonus' => [
									'username' => $data['username']
								]
							]
						]);

						foreach ($langs as $lang) {
							$user->translation()->create([
								'lang' => $lang,
								'birth_cato_name' => isset($data['birth_cato_name_'.$lang]) ? $data['birth_cato_name_'.$lang] : null,
								'registration_address' => isset($data['registration_address_'.$lang]) ? $data['registration_address_'.$lang] : null,
								'living_address' => isset($data['living_address_'.$lang]) ? $data['living_address_'.$lang] : null
							]);
						}

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'users',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
