<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\StudentSubjectCost;
use App\Models\Integration;
use App\Models\StudentCard;
use App\Models\Subject;

use Validator;
use DB;

class StudentSubjectCostController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.student_id' => 'required|integer',
			'data.*.subject_id' => 'required|integer',
			'data.*.value' => 'required|integer'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$student = StudentCard::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['student_id']
						]))
						->first();

					$subject = Subject::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['subject_id']
						]))
						->first();

					$student_subject_cost = StudentSubjectCost::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['id']
						]))
						->first();

					if ($student_subject_cost) {
						$student_subject_cost->update([
							'student_id' => $student->id,
							'subject_id' => $subject->id,
							'value' => $data['value']
						]);

						if ($student_subject_cost->wasChanged()) {
							$updated = 1;
						}
					} else {
						StudentSubjectCost::create([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'student_id' => $student->id,
							'subject_id' => $subject->id,
							'value' => $data['value']
						]);

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'student_subject_costs',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
