<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AcademicDegree;
use App\Models\Integration;
use App\Models\Department;
use App\Models\Subject;

use Validator;
use DB;

class SubjectController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.academic_degree_id' => 'required|integer',
			'data.*.department_id' => 'required|integer',
			'data.*.code_kz' => 'nullable|string|max:255',
			'data.*.name_kz' => 'nullable|string|max:255',
			'data.*.info_kz' => 'nullable|string|max:10000',
			'data.*.code_ru' => 'nullable|string|max:255',
			'data.*.name_ru' => 'nullable|string|max:255',
			'data.*.info_ru' => 'nullable|string|max:10000',
			'data.*.code_en' => 'nullable|string|max:255',
			'data.*.name_en' => 'nullable|string|max:255',
			'data.*.info_en' => 'nullable|string|max:10000',
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$academic_degree = AcademicDegree::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['academic_degree_id']
						]))
						->first();

					$department = Department::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['department_id']
						]))
						->first();

					$subject = Subject::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['id']
						]))
						->first();

					if ($subject) {
						$subject->update([
							'department_id' => $department->id,
							'academic_degree_id' => $academic_degree->id
						]);

						foreach ($langs as $lang) {
							if (isset($data['name_'.$lang])) {
								$subject->translations()->where('lang', $lang)->update([
									'code' => isset($data['code_'.$lang]) ? $data['code_'.$lang] : null,
									'name' => $data['name_'.$lang],
									'info' => isset($data['info_'.$lang]) ? $data['info_'.$lang] : null
								]);
							}
						}
					} else {
						$subject = Subject::create([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'department_id' => $department->id,
							'academic_degree_id' => $academic_degree->id,
							'state_id' => 2
						]);

						foreach ($langs as $lang) {
							if (isset($data['name_'.$lang])) {
								$subject->translation()->create([
									'lang' => $lang,
									'code' => isset($data['code_'.$lang]) ? $data['code_'.$lang] : null,
									'name' => $data['name_'.$lang],
									'info' => isset($data['info_'.$lang]) ? $data['info_'.$lang] : null
								]);
							}
						}

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'subjects',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
