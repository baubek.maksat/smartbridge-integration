<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\AccountViewResource;

use Illuminate\Validation\Rule;

use App\Models\ReplenishmentView;
use App\Models\TransactionView;
use App\Models\AccountView;
use App\Models\Transaction;
use App\Models\Account;
use App\Models\User;

use Validator;
use DB;

class ReplenishmentController extends Controller
{
	public function index(Request $request)
	{
		$account_views = AccountView::query();

		$account_views = $account_views->paginate();

		return AccountViewResource::collection($account_views);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:5000',
			'data.*' => 'required|array',
			'data.*.view_id' => 'required|exists:replenishment_views,id',
			'data.*.account_view_id' => 'required|exists:account_views,id',
			'data.*.user' => 'required|array',
			'data.*.user.id' => 'nullable|array',
			'data.*.user.id.tin' => 'nullable|string|max:255',
			'data.*.user.student_card' => 'nullable|array',
			'data.*.user.student_card.id' => 'nullable|string|max:255',
			'data.*.sum' => 'required|between:0,999999999',
			'data.*.datetime' => 'required'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		foreach ($request->data as $data) {
			DB::transaction(function() use ($data) {
				$user = User::query()
					->whereHas('identityDocuments', function($query) use ($data) {
						$query->where('tin', $data['user']['id']['tin']);
					})
					->first();

				$account = Account::query()
					->where('user_id', $user->id)
					->where('view_id', $data['account_view_id'])
					->where('state_id', 2)
					->first();

				if (!$account) {
					$account = Account::create([
						'user_id' => $user->id,
						'view_id' => $data['account_view_id'],
						'state_id' => 2
					]);
				}

				$replenishment_view = ReplenishmentView::query()
					->where('id', $data['view_id'])
					->first();

				$transaction = Transaction::query()
					->where('additional_fields->document', $data['document'])
					->where('account_id', $account->id)
					->where('datetime', $data['datetime'])
					->first();

				if ($transaction) {
					$account->update([
						'balance' => $account->balance - $transaction->sum + $data['sum']
					]);

					$transaction->update([
						'sum' => $data['sum']
					]);
				} else {
					$transaction = Transaction::create([
						'account_id' => $account->id,
						'view_id' => TransactionView::where('slug', 'replenishment')->first()->id,
						'additional_fields' => [
							'document' => $data['document']
						],
						'sum' => $data['sum'],
						'datetime' => $data['datetime']
					]);

					$account->update([
						'balance' => $account->balance + $data['sum']
					]);

					if ($replenishment_view->slug == '1c-accounting') {
						$transaction->replenishment()->create([
							'view_id' => $data['view_id']
						]);
					}
				}
			});
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
