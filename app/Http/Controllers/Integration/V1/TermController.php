<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AcademicCalendar;
use App\Models\Integration;
use App\Models\TermType;
use App\Models\Term;

use Validator;
use DB;

class TermController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.academic_calendar_id' => 'required|integer',
			'data.*.type_id' => 'required|integer',
			'data.*.date_start' => 'nullable|date_format:Y-m-d',
			'data.*.date_end' => 'nullable|date_format:Y-m-d'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$term = Term::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['id']
						]))
						->first();
					
					$type = TermType::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['type_id']
						]))
						->first();
					
					$academic_calendar = AcademicCalendar::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['academic_calendar_id']
						]))
						->first();

					if ($term) {
						$term->update([
							'academic_calendar_id' => $academic_calendar->id,
							'type_id' => $type->id,
							'date_start' => isset($data['date_start']) ? $data['date_start'] : null,
							'date_end' => isset($data['date_end']) ? $data['date_end'] : null
						]);

						if ($term->wasChanged()) {
							$updated = 1;
						}
					} else {
						Term::create([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'academic_calendar_id' => $academic_calendar->id,
							'type_id' => $type->id,
							'date_start' => isset($data['date_start']) ? $data['date_start'] : null,
							'date_end' => isset($data['date_end']) ? $data['date_end'] : null,
							'state_id' => 2
						]);

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'terms',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
