<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\SpecialityCost;
use App\Models\Integration;
use App\Models\Speciality;
use App\Models\StudyForm;

use Validator;
use DB;

class SpecialityCostController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.speciality_id' => 'required|integer',
			'data.*.study_form_id' => 'required|integer',
			'data.*.year_of_admission' => 'required|date_format:Y',
			'data.*.study_year' => 'required|date_format:Y',
			'data.*.cost' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$speciality = Speciality::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['speciality_id']
						]))
						->first();

					$study_form = StudyForm::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['study_form_id']
						]))
						->first();

					$speciality_cost = SpecialityCost::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['id']
						]))
						->first();

					if ($speciality_cost) {
						$speciality_cost->update([
							'speciality_id' => $speciality->id,
							'study_form_id' => $study_form->id,
							'year_of_admission' => $data['year_of_admission'],
							'study_year' => $data['study_year'],
							'value' => $data['cost']
						]);

						if ($speciality_cost->wasChanged()) {
							$updated = 1;
						}
					} else {
						$speciality = SpecialityCost::create([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'speciality_id' => $speciality->id,
							'study_form_id' => $study_form->id,
							'year_of_admission' => $data['year_of_admission'],
							'study_year' => $data['study_year'],
							'value' => $data['cost'],
							'state_id' => 2
						]);

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'speciality_costs',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
