<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Integration;
use App\Models\LessonHour;
use App\Models\StudyGroup;
use App\Models\Timetable;
use App\Models\Classroom;
use App\Models\Week;

use Validator;
use DB;

class TimetableController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.week_number' => 'required|integer',
			'data.*.week_id' => 'required|integer',
			'data.*.lesson_hour_id' => 'required|integer',
			'data.*.auditory_id' => 'nullable|integer',
			'data.*.study_group_id' => 'required|integer',
			'data.*.online' => 'required|integer|in:0,1'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages()
			], 200);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$timetable = Timetable::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['id']
						]))
						->where('week_number', $data['week_number'])
						->first();

					$classroom = isset($data['auditory_id']) ? Classroom::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['auditory_id']
						]))
						->first() : null;

					$week = Week::query()
						->where('id', $data['week_id'])
						->first();

					$study_group = StudyGroup::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['study_group_id']
						]))
						->first();

					$lesson_hour = LessonHour::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['lesson_hour_id']
						]))
						->first();

					if ($timetable) {
						$timetable->update([
							'week_id' => $week->id,
							'week_number' => $data['week_number'],
							'lesson_hour_id' => $lesson_hour->id,
							'classroom_id' => $classroom ? $classroom->id : null,
							'study_group_id' => $study_group->id,
							'online' => $data['online']
						]);

						if ($timetable->wasChanged()) {
							$updated++;
						}
					} else {
						$timetable = Timetable::create([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'week_number' => $data['week_number'],
							'week_id' => $week->id,
							'lesson_hour_id' => $lesson_hour->id,
							'classroom_id' => $classroom ? $classroom->id : null,
							'study_group_id' => $study_group->id,
							'online' => $data['online'],
							'state_id' => 2
						]);

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'timetables',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
