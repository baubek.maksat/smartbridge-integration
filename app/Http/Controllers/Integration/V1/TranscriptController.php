<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Integration;
use App\Models\StudentCard;
use App\Models\Transcript;

use Validator;
use DB;

class TranscriptController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.term' => 'required|integer'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$student = StudentCard::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['student_id']
						]))
						->first();

					$transcript = Transcript::query()
	                    ->where('platonus_key', implode('-', [
							$request->university_id,
							$data['id']
						]))
	                    ->first();

					if ($transcript) {
						$transcript->update([
							'student_id' => $student->id,
							'course_number' => $data['course_number'],
							'term' => $data['term'],
							'credits' => isset($data['credits']) ? $data['credits'] : null,
							'traditional_mark' => isset($data['traditional_mark']) ? $data['traditional_mark'] : null,
							'alpha_mark' => isset($data['alpha_mark']) ? $data['alpha_mark'] : null,
							'numeral_mark' => isset($data['numeral_mark']) ? $data['numeral_mark'] : null,
							'total_mark' => isset($data['total_mark']) ? $data['total_mark'] : null,
							'exam_mark' => isset($data['exam_mark']) ? $data['exam_mark'] : null
						]);

						foreach ($langs as $lang) {
							if (isset($data['subject_name_'.$lang])) {
								$transcript->translations()->where('lang', $lang)->update([
									'subject_code' => isset($data['subject_code_'.$lang]) ? $data['subject_code_'.$lang] : null,
									'subject_name' => $data['subject_name_'.$lang]
								]);
							}
						}

						if ($transcript->wasChanged()) {
							$updated = 1;
						}
					} else {
						$transcript = Transcript::create([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'student_id' => $student->id,
							'course_number' => $data['course_number'],
							'term' => $data['term'],
							'credits' => isset($data['credits']) ? $data['credits'] : null,
							'traditional_mark' => isset($data['traditional_mark']) ? $data['traditional_mark'] : null,
							'alpha_mark' => isset($data['alpha_mark']) ? $data['alpha_mark'] : null,
							'numeral_mark' => isset($data['numeral_mark']) ? $data['numeral_mark'] : null,
							'total_mark' => isset($data['total_mark']) ? $data['total_mark'] : null,
							'exam_mark' => isset($data['exam_mark']) ? $data['exam_mark'] : null,
							'state_id' => 2
						]);

						foreach ($langs as $lang) {
							if (isset($data['subject_name_'.$lang])) {
								$transcript->translation()->create([
									'lang' => $lang,
									'subject_code' => isset($data['subject_code_'.$lang]) ? $data['subject_code_'.$lang] : null,
									'subject_name' => $data['subject_name_'.$lang]
								]);
							}
						}

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'transcripts',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
