<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Integration;
use App\Models\Speciality;
use App\Models\Curriculum;
use App\Models\StudyForm;

use Validator;
use DB;

class CurriculumController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.speciality_id' => 'required|integer',
			'data.*.study_form_id' => 'required|integer',
			'data.*.name_kz' => 'nullable|string|max:1023',
			'data.*.name_ru' => 'nullable|string|max:1023',
			'data.*.name_en' => 'nullable|string|max:1023'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$curriculum = Curriculum::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['id']
						]))
						->first();

					$speciality = Speciality::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['speciality_id']
						]))
						->first();

					$study_form = StudyForm::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['study_form_id']
						]))
						->first();

					if ($curriculum) {
						$curriculum->update([
							'speciality_id' => $speciality->id,
							'study_form_id' => $study_form->id,
						]);

						foreach ($langs as $lang) {
							if (isset($data['name_'.$lang])) {
								$curriculum->translations()->where('lang', $lang)->update([
									'name' => $data['name_'.$lang]
								]);
							}
						}
					} else {
						$curriculum = Curriculum::create([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'speciality_id' => $speciality->id,
							'study_form_id' => $study_form->id,
							'state_id' => 2
						]);

						foreach ($langs as $lang) {
							if (isset($data['name_'.$lang])) {
								$curriculum->translation()->create([
									'lang' => $lang,
									'name' => $data['name_'.$lang]
								]);
							}
						}

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'curriculums',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
