<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\AcademicDegree;
use App\Models\StudyGroupType;
use App\Models\StudyLanguage;
use App\Models\TeacherCard;
use App\Models\Integration;
use App\Models\StudyGroup;
use App\Models\StudyForm;
use App\Models\Subject;

use Validator;
use DB;

class StudyGroupController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|string|max:255',
			'data.*.type_id' => 'required|string|max:255|exists:study_group_types,platonus_key',
			'data.*.study_form_id' => 'required|string|max:255|exists:study_forms,platonus_key'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$study_group_type = StudyGroupType::query()
						->where('platonus_key', $data['type_id'])
						->first();

					$study_form = StudyForm::query()
						->where('platonus_key', $data['study_form_id'])
						->first();

					$subject = Subject::query()
						->where('platonus_key', $data['subject_id'])
						->first();

					$teacher = $data['teacher_id'] ? TeacherCard::query()
						->where('platonus_key', $data['teacher_id'])
						->first() : null;

					$study_group = StudyGroup::query()
						->where('platonus_key', $data['id'])
						->first();

					$study_language = StudyLanguage::query()
						->where('id', $data['study_language_id'])
						->first();

					if (!$study_language) {
						echo $data['id'].'-'.$data['study_language_id'].PHP_EOL;
					}

					if ($study_group) {
						$study_group->update([
							'subject_id' => $subject->id,
							'teacher_id' => $teacher ? $teacher->id : null,
							'type_id' => $study_group_type->id,
							'year' => $data['year'],
							'term' => $data['term'],
							'code' => $data['code'],
							'name' => $data['name'],
							'hours' => $data['hours'],
							'study_form_id' => $study_form->id,
							'study_language_id' => $study_language->id,
							'student_count' => $data['student_count'],
							'student_count_min' => $data['student_count_min'],
							'student_count_max' => $data['student_count_max']
						]);

						if ($study_group->wasChanged()) {
							$updated = 1;
						}
					} else {
						StudyGroup::create([
							'platonus_key' => $data['id'],
							'subject_id' => $subject->id,
							'teacher_id' => $teacher ? $teacher->id : null,
							'type_id' => $study_group_type->id,
							'year' => $data['year'],
							'term' => $data['term'],
							'code' => $data['code'],
							'name' => $data['name'],
							'hours' => $data['hours'],
							'study_form_id' => $study_form->id,
							'study_language_id' => $study_language->id,
							'student_count' => $data['student_count'],
							'student_count_min' => $data['student_count_min'],
							'student_count_max' => $data['student_count_max'],
							'state_id' => 2
						]);

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'study_groups',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
