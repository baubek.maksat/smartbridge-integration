<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\TransactionResource;

use Illuminate\Validation\Rule;

use App\Models\ReplenishmentView;
use App\Models\TransactionView;
use App\Models\TransferView;
use App\Models\AccountView;
use App\Models\PaymentView;
use App\Models\Transaction;
use App\Models\Account;
use App\Models\User;

use Validator;
use DB;

class TransactionController extends Controller
{
	public function index(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'filter.datetime_start' => 'nullable|date_format:Y-m-d H:i:s',
			'filter.datetime_end' => 'nullable|date_format:Y-m-d H:i:s'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$transactions = Transaction::query();

		if ($request->has('filter.datetime_start')) {
			$transactions->where('datetime', '>=', $request->filter['datetime_start']);
		}

		if ($request->has('filter.datetime_end')) {
			$transactions->where('datetime', '<=', $request->filter['datetime_end']);
		}

		$transactions = $transactions->paginate($request->has('paginate') ? $request->paginate : 15);

		return TransactionResource::collection($transactions);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'user' => 'required|array',
			'user.id' => 'nullable|array',
			'user.id.tin' => [
				Rule::requiredIf(function () use ($request) {
					return $request->has('user.id');
				}),
				'string',
				'max:255',
				'exists:ids,tin'
			],
			'user.student_card' => 'nullable|array',
			'user.student_card.id' => [
				Rule::requiredIf(function () use ($request) {
					return $request->has('user.student_card');
				}),
				'integer',
				'exists:student_cards,integration_fields->platonus->id'
			],
			'view_id' => 'required|exists:transaction_views,id',
			'replenishment_view_id' => [
				Rule::requiredIf(function () use ($request) {
					return TransactionView::query()
						->where('id', $request->view_id)
						->where('slug', 'replenishment')
						->first();
				}),
				'exists:replenishment_views,id'
			],
			'payment_view_id' => [
				Rule::requiredIf(function () use ($request) {
					return TransactionView::query()
						->where('id', $request->view_id)
						->where('slug', 'payment')
						->first();
				}),
				'exists:payment_views,id'
			],
			'transfer_view_id' => [
				Rule::requiredIf(function () use ($request) {
					return TransactionView::query()
						->where('id', $request->view_id)
						->where('slug', 'transfer')
						->first();
				}),
				'exists:transfer_views,id'
			],
			'sender_user' => [
				Rule::requiredIf(function () use ($request) {
					return TransferView::query()
						->where('id', $request->transfer_view_id)
						->where('slug', 'between-clients')
						->first() || ReplenishmentView::query()
						->where('id', $request->replenishment_view_id)
						->where('slug', 'transfer')
						->first();
				}),
				'array'
			],
			'sender_user.id' => 'nullable|array',
			'sender_user.id.tin' => [
				Rule::requiredIf(function () use ($request) {
					return $request->has('sender_user.id');
				}),
				'exists:ids,tin',
				'string',
				'max:255'
			],
			'sender_user.student_card' => 'nullable|array',
			'sender_user.student_card.id' => [
				Rule::requiredIf(function () use ($request) {
					return $request->has('sender_user.student_card');
				}),
				Rule::exists('student_cards', 'integration_fields->platonus->id')->where(function ($query) use ($request) {
					return $query->where('integration_fields->platonus->university_id', $request->university_id);
				})
			],
			'sender_account_view_id' => [
				Rule::requiredIf(function () use ($request) {
					return TransactionView::query()
						->where('id', $request->view_id)
						->where('slug', 'transfer')
						->first() || ReplenishmentView::query()
						->where('id', $request->replenishment_view_id)
						->where('slug', 'transfer')
						->first();
				}),
				'exists:account_views,id'
			],
			'account_view_id' => 'required|exists:account_views,id',
			'document' => 'required|string|max:255',
			'hash' => 'required|string|max:255',
			'sum' => 'required|regex:/^[-]{0,1}\d*(\.\d{1,2})?$/',
			'datetime' => 'required|date_format:Y-m-d H:i:s'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		DB::transaction(function() use ($request) {
			$user = $request->has('user.id.tin') ? User::query()
				->whereHas('identityDocuments', function($query) use ($request) {
					$query->where('tin', $request->user['id']['tin']);
				})
				->first() : null;

			$user = !$user && $request->has('user.student_card.id') ? User::query()
				->whereHas('student.cards', function($query) use ($request) {
					$query
						->where('integration_fields->platonus->university_id', $request->university_id)
						->where('integration_fields->platonus->id', $request->user['student_card']['student_card']['id']);
					})
				->first() : $user;

			if (!$user) {
				return false;
			}

			$account = Account::query()
				->where('user_id', $user->id)
				->where('view_id', $request->account_view_id)
				->first();

			if (!$account) {
				$account = Account::create([
					'user_id' => $user->id,
					'view_id' => $request->account_view_id,
					'balance' => 0,
					'state_id' => 2
				]);
			}

			$transaction_view = TransactionView::query()
				->where('id', $request->view_id)
				->first();

			if ($transaction_view->slug == 'replenishment') {
				$replenishment_view = ReplenishmentView::query()
					->where('id', $request->replenishment_view_id)
					->first();

				if ($replenishment_view->slug == 'transfer') {
					$transfer_view = TransferView::query()
						->where('id', $request->transfer_view_id)
						->first();
				}
			}

			if ($transaction_view->slug == 'payment') {
				$payment_view = PaymentView::query()
					->where('id', $request->payment_view_id)
					->first();
			}

			if ($transaction_view->slug == 'transfer') {
				$payment_view = PaymentView::query()
					->where('id', $request->payment_view_id)
					->first();
			}

			$transaction = Transaction::query()
				->where('additional_fields->document', $request->document)
				->where('additional_fields->hash', $request->hash)
				->where('account_id', $account->id)
				->where('datetime', $request->datetime)
				->first();

			if ($transaction) {
				$account->update([
					'balance' => $account->balance - $transaction->sum + $request->sum
				]);

				if ($transaction->view_id != $transaction_view->id) {
					if ($transaction->payment) {
						$transaction->payment()->delete();
					}

					if ($transaction->replenishment) {
						$transaction->replenishment()->delete();
					}

					if ($transaction_view->slug == 'replenishment') {
						$transaction->replenishment()->create([
							'view_id' => $request->replenishment_view_id
						]);
					}

					if ($transaction_view->slug == 'payment') {
						$transaction->payment()->create([
							'view_id' => $request->payment_view_id
						]);
					}
				}

				$transaction->update([
					'additional_fields' => [
						'document' => $request->document,
						'hash' => $request->hash
					],
					'view_id' => $transaction_view->id,
					'sum' => $request->sum
				]);
			} else {
				$transaction = Transaction::create([
					'account_id' => $account->id,
					'view_id' => $transaction_view->id,
					'additional_fields' => [
						'document' => $request->document,
						'hash' => $request->hash
					],
					'sum' => $request->sum,
					'datetime' => $request->datetime
				]);

				//	Пополнение

				if ($transaction_view->slug == 'replenishment') {
					if ($replenishment_view->slug == '1c-accounting') {
						$transaction->replenishment()->create([
							'view_id' => $request->replenishment_view_id
						]);
					}

					if ($replenishment_view->slug == 'online-banking') {
						$transaction->replenishment()->create([
							'view_id' => $request->replenishment_view_id
						]);
					}

					if ($replenishment_view->slug == 'transfer') {
						$sender_user = $request->has('sender_user.id.tin') ? User::query()
							->whereHas('identityDocuments', function($query) use ($request) {
								$query->where('tin', $request->sender_user['id']['tin']);
							})
							->first() : null;

						$sender_user = !$sender_user && $request->has('sender_user.student_card.id') ? User::query()
							->whereHas('identityDocuments', function($query) use ($request) {
								$query->where('tin', $request->sender_user['id']['tin']);
							})
							->first() : $sender_user;

						if (!$sender_user) {
							return false;
						}

						$sender_account = Account::query()
							->where('user_id', $sender_user->id)
							->where('view_id', $request->sender_account_view_id)
							->first();

						if (!$sender_account) {
							$sender_account = Account::create([
								'user_id' => $user->id,
								'view_id' => $request->sender_account_view_id,
								'state_id' => 2
							]);
						}

						$transaction->replenishment()->create([
							'view_id' => $request->replenishment_view_id,
							'account_id' => $sender_account->id
						]);
					}
				}

				//	Перевод

				if ($transaction_view->slug == 'transfer') {
					if ($transfer_view->slug == 'between-your-accounts') {
						$sender_account = Account::query()
							->where('user_id', $user->id)
							->where('view_id', $request->sender_account_view_id)
							->first();

						if (!$sender_account) {
							$sender_account = Account::create([
								'user_id' => $user->id,
								'view_id' => $request->sender_account_view_id,
								'state_id' => 2
							]);
						}
					}

					if ($transfer_view->slug == 'between-clients') {
						$sender_user = $request->has('sender_user.id.tin') ? User::query()
							->whereHas('identityDocuments', function($query) use ($request) {
								$query->where('tin', $request->sender_user['id']['tin']);
							})
							->first() : null;

						$sender_account = Account::query()
							->where('user_id', $sender_user->id)
							->where('view_id', $request->sender_account_view_id)
							->first();

						if (!$sender_account) {
							$sender_account = Account::create([
								'user_id' => $user->id,
								'view_id' => $request->sender_account_view_id,
								'state_id' => 2
							]);
						}
					}

					$transaction->transfer()->create([
						'account_id' => $sender_account->id
					]);
				}

				//	Начисление

				if ($transaction_view->slug == 'accrual') {
					$transaction->accrual()->create();
				}

				$account->update([
					'balance' => $account->balance + $request->sum
				]);
			}	
		});

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}

	public function destroy(Request $request, Transaction $transaction)
	{
		DB::transaction(function() use ($transaction) {
			$transaction->account()->update([
				'balance' => $transaction->account->balance - $transaction->sum
			]);

			if ($transaction->accrual) {
				$transaction->accrual()->delete();
			}

			if ($transaction->replenishment) {
				$transaction->replenishment()->delete();
			}

			$transaction->delete();
		});

		return response()->json([
			'code' => 204
		], 200);
	}
}
