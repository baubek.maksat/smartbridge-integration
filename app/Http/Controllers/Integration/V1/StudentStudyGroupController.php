<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\StudentStudyGroup;
use App\Models\Integration;
use App\Models\StudentCard;
use App\Models\StudyGroup;
use App\Models\Cicle;

use Validator;
use DB;

class StudentStudyGroupController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.student_id' => 'required|string|max:255|exists:student_cards,platonus_key',
			'data.*.study_group_id' => 'required|string|max:255|exists:study_groups,platonus_key',
			'data.*.cicle_id' => 'required|string|max:255|exists:cicles,platonus_key'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$student = StudentCard::query()
						->where('platonus_key', $data['student_id'])
						->first();

					$study_group = StudyGroup::query()
						->where('platonus_key', $data['study_group_id'])
						->first();

					$cicle = Cicle::query()
						->where('platonus_key', $data['cicle_id'])
						->first();

					$student_study_group = StudentStudyGroup::query()
						->where('study_group_id', $study_group->id)
						->where('student_id', $student->id)
						->first();

					if ($student_study_group) {
						
					} else {
						StudentStudyGroup::create([
							'study_group_id' => $study_group->id,
							'student_id' => $student->id,
							'cicle_id' => $cicle->id
						]);

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'students_study_groups',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
