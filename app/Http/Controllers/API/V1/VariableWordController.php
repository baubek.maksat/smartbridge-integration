<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\VariableWordResource;

use App\Models\VariableWord;

use Validator;

class VariableWordController extends Controller
{
    public function index(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'per_page' => 'integer|between:1,50'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }
		
		$variable_words = VariableWord::query();
		
		//	filter
		
		if ($request->has('filter.title')) {
			$variable_words->where('title', 'LIKE', $request->filter['title']);
		}
		
		if ($request->has('filter.value')) {
			$variable_words->where('value', 'LIKE', $request->filter['value']);
		}
		
		if ($request->has('filter.key')) {
			$variable_words->where('key', 'LIKE', $request->filter['key']);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$variable_words->where('university_id', auth()->user()->sscs[0]->university->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$variable_words->where('university_id', auth()->user()->sscs[0]->university->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$variable_words->where('university_id', auth()->user()->sscs[0]->university->id);
		}
		
		$variable_words = $variable_words->paginate();
		
		return VariableWordResource::collection($variable_words);
    }

    public function show(VariableWord $variable_word)
    {
    	return new VariableWordResource($variable_word);
    }

    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'key' => [
				'required',
				'string',
				'max:1024',
				Rule::unique('variable_words', 'key')->where(function ($query) use ($request){
					$query->where('title', $request->title)
						->where('value', $request->value);

					if (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee') {
						$ssc = auth()->user()->sscs[0];

						$query->where('university_id', $ssc->university->id);
					}
				})
			],
            'title' => [
				'required',
				'string',
				'max:1024'
			],
			'value' => [
				'required',
				'string',
				'max:1024'
			],
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        if (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee') {
			$university = auth()->user()->sscs[0]->university;
		}
		
		$variable_word = VariableWord::create([
			'university_id' => $university ? $university->id : null,
			'key' => $request->key,
            'title' => $request->title,
			'value' => $request->value
		]);
		
    	return response()->json([
    		'code' => 201,
			'data' => $variable_word
		], 200);
    }

    public function update(Request $request, VariableWord $variable_word)
    {
    	$validator = Validator::make($request->all(), [
			'key' => [
				'required',
				'string',
				'max:1024',
				Rule::unique('variable_words', 'key')->where(function ($query) use ($request){
					$query->where('title', $request->title)
						->where('value', $request->value);

						if (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee') {
							$ssc = auth()->user()->sscs[0];

							$query->where('university_id', $ssc->university->id);
						}
				})->ignore($variable_word->id ,'id')
			],
            'title' => 'required|string|max:1024',
			'value' => 'required|string|max:1024'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        if (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee') {
			$ssc = auth()->user()->sscs[0];
		}
		
		$variable_word->update([
			'university_id' => isset($ssc) ? $ssc->university_id : null,
			'key' => $request->key,
            'title' => $request->title,
			'value' => $request->value
		]);
		
    	return response()->json([
			'code' => 200,
			'data' => new VariableWordResource($variable_word)
		], 200);
    }
	
	public function destroy(Request $request, VariableWord $variable_word)
    {
		$variable_word->delete();
		
    	return response()->json([
			'code' => 204
		], 200);
    }
}
