<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StatementComponentTypeResource;

use App\Models\StatementComponentType;

class StatementComponentTypeController extends Controller
{
    public function index(Request $request)
    {
    	$statement_component_types = StatementComponentType::query()
            ->paginate();

        return StatementComponentTypeResource::collection($statement_component_types);
    }
}
