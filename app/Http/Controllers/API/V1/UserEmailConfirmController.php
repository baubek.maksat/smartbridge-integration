<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\AppResource;

use Illuminate\Validation\Rule;

use App\Models\UserEmailConfirm;
use App\Models\University;
use App\Models\User;
use App\Models\Type;
use App\Models\Lang;

use App\Mail\Confirm;

use Validator;
use Mail;
use DB;
use UA;

class UserEmailConfirmController extends Controller
{
	public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'user_id' => [
				'required',
				'string',
				'max:500',
				'exists:users,id'
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }

        $type = Type::query()
			->where('slug', $request->header('type'))
			->first();
		
		$user = User::find($request->user_id);

		$role = $user->roles()->first();

		$university = University::query()
			->first();

		$confirm = $user->emailConfirms()->create([
			'type_id' => Type::where('slug', $request->header('type'))->first()->id,
			'lang_id' => Lang::where('slug', app()->getLocale())->first()->id,
			'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
			'ip' => $request->server('REMOTE_ADDR'),
			'ua' => $request->server('HTTP_USER_AGENT'),
			'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
			'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
			'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
			'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
			'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
			'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
			'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
			'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
			'hash' => sha1(rand(1000, 9999)),
			'src' => $user->email
		]);

		Mail::to($user->email)
			->queue(new Confirm($confirm));
		
		return response()->json([
            'code' => 201,
			'data' => $confirm
        ], 200);
    }
}
