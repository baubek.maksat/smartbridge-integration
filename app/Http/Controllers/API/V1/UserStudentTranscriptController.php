<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\TranscriptResource;

use Illuminate\Validation\Rule;

use App\Models\Transcript;

use Validator;
use DB;
use UA;

class UserStudentTranscriptController extends Controller
{
    public function index(Request $request)
    {
    	$transcripts = Transcript::query()
			->where('student_id', auth()->user()->student->card->id);
		
		if ($request->has('filter.subject_name')) {
			$transcripts->whereHas('translations', function($query) use ($request){
				$query->where('subject_name', 'LIKE', $request->filter['subject_name']);
			});
		}
		
		$transcripts = $transcripts->get();
		
		return TranscriptResource::collection($transcripts);
    }
}
