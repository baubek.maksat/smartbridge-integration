<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\UserEmailRestore;
use App\Models\User;
use App\Models\Lang;
use App\Models\Type;

use Validator;
use Mail;
use DB;
use UA;

class ResetController extends Controller
{
	public function check(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hash' => [
                'required',
                Rule::exists('user_email_restores')->where(function ($query) use ($request) {
                    $query->where('fit', '1')->where('hash', $request->hash);
                })
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }
        
        return response()->json([
			'code' => 200
		], 200);
    }
	
    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|max:255',
			'hash' => [
                'required',
                Rule::exists('user_email_restores')->where(function ($query) use ($request) {
                    $query->where('fit', '1')->where('hash', $request->hash);
                })
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }
        
        DB::transaction(function() use ($request) {
			$restore = UserEmailRestore::query()
				->where('hash', $request->hash)
				->first();
			
			$restore->user()->update([
				'password' => bcrypt($request->password)
			]);

            $restore->update([
                'fit' => '0'
            ]);
        });

        return response()->json([
			'code' => 200
		], 200);
    }
}
