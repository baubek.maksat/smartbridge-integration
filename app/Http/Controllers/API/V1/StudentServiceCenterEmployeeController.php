<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

use App\Http\Resources\StudentServiceCenterEmployeeResource;

use App\Models\Type;
use App\Models\Lang;
use App\Models\Role;
use App\Models\User;
use App\Models\StudentServiceCenter;
use App\Models\StudentServiceCenterUser;
use App\Models\StudentServiceCenterRole;
use App\Models\StudentServiceCenterRoleAccess;

use App\Mail\Confirm;
use App\Mail\RoleMail;

use Validator;
use Mail;
use DB;
use UA;

class StudentServiceCenterEmployeeController extends Controller
{
    public function index(Request $request)
    {
		$employees = User::query();

		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$employees->whereHas('sscs', function($query) {
				$query->where('id', auth()->user()->sscs[0]->id);
			});
		}

		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$employees->whereHas('sscs', function($query) {
				$query->where('id', auth()->user()->sscs[0]->id);
			});
		}

		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$employees->whereHas('sscs', function($query) {
				$query->where('id', auth()->user()->sscs[0]->id);
			});
		}

		$employees->where('id', '<>', auth()->user()->id);
		
		//	filter
		
		if ($request->has('filter.fullname')) {
			$employees->where(function($query) use ($request) {
				$query->where(DB::raw("CONCAT(`surname`, ' ', `name`, ' ', `patronymic`)"), 'LIKE', $request->filter['fullname']);
			});
		}

		if ($request->has('filter.role_id')) {
			$employees->whereHas('studentServiceCenterRoles', function($query) use ($request) {
				$query->where('id', $request->filter['role_id']);
			});
		}

		//	

		$employees = $employees->paginate($request->has('per_page') ? $request->per_page : 15);
		
		return StudentServiceCenterEmployeeResource::collection($employees);
    }
	
	public function store(Request $request)
	{
		$user = null;
		
		if ($request->has('surname') && $request->has('name') && $request->has('birth_date')) {
			$user = User::query()
				->where('surname', $request->surname)
				->where('name', $request->name)
				->where('birth_date', $request->birth_date)
				->first();
		}

		$validator = Validator::make($request->all(), [
			'surname' => 'required|string|max:255',
			'name' => 'required|string|max:255',
			'patronymic' => 'nullable|string|max:255',
			'email' => [
				'required',
				'email',
				'max:255',
				Rule::unique('users')->ignore($user)
			],
			'gender_id' => 'required|integer|exists:genders,id',
			'birth_date' => 'required|date',
			'role_id' => 'required|exists:student_service_center_roles,id'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		$data = DB::transaction(function() use ($request) {
			$ssc = auth()->user()->sscs[0];
			
			$user = User::query()
				->where('surname', $request->surname)
				->where('name', $request->name)
				->where('birth_date', $request->birth_date)
				->first();
			
			if (!$user) {
				$user = User::create([
					'surname' => $request->surname,
					'name' => $request->name,
					'patronymic' => $request->patronymic,
					'email' => $request->email,
					'username' => Str::slug($request->surname.'_'.$request->name.'_'.rand(1000, 9999), '_'),
					'password' => bcrypt(sha1($request->email)),
					'gender_id' => $request->gender_id,
					'birth_date' => $request->birth_date,
					'default_lang_id' => Lang::where('slug', 'ru')->first()->id
				]);
			}
			
			$ssc_role = StudentServiceCenterRole::query()
				->where('id', $request->role_id)
				->first();
			
			$role = Role::query()
				->where('slug', $ssc_role->access->slug)
				->first();
			
			if (!$user->hasRoleWithTrashed($role->slug)) {
				$user->roles()->attach($role, [
					'created_at' => date('Y-m-d H:i:s')
				]);
			} else {
				$user->rolesWithTrashed()->updateExistingPivot($role->id, [
					'deleted_at' => null,
					'updated_at' => date('Y-m-d H:i:s')
				]);
			}

			if (!$user->hasSscWithTrashed($ssc->id)) {
				$user->sscs()->attach($ssc, [
					'created_at' => date('Y-m-d H:i:s')
				]);
			} else {
				$user->sscsWithTrashed()->updateExistingPivot($ssc->id, [
					'deleted_at' => null,
					'updated_at' => date('Y-m-d H:i:s')
				]);
			}

			if (!$user->hasStudentServiceCenterRoleWithTrashed($ssc_role->id)) {
				$user->studentServiceCenterRoles()->attach($ssc_role, [
					'created_at' => date('Y-m-d H:i:s')
				]);
			} else {
				$user->studentServiceCenterRolesWithTrashed()->updateExistingPivot($ssc_role->id, [
					'deleted_at' => null,
					'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			
			if ($user->email_verified == 0) {
				$confirm = $user->emailConfirms()->create([
					'type_id' => Type::where('slug', $request->header('type'))->first()->id,
					'lang_id' => Lang::where('slug', app()->getLocale())->first()->id,
					'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
					'ip' => $request->server('REMOTE_ADDR'),
					'ua' => $request->server('HTTP_USER_AGENT'),
					'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
					'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
					'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
					'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
					'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
					'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
					'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
					'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
					'hash' => sha1(rand(1000, 9999)),
					'src' => mb_strtolower($request->email)
				]);
			}
			
			return [
				'confirm' => isset($confirm) ? $confirm : null,
				'university' => $ssc->university,
				'user' => $user,
				'role' => $role
			];
		});
		
		if ($data['user']->email_verified == 0) {
			Mail::to($data['user']->email)
				->queue(new Confirm($data['confirm']));
		}
		
		Mail::to($data['user']->email)
			->queue(new RoleMail($data['user'], $data['role'], $data['university']));

		return response()->json([
			'code' => 201,
			'data' => $data
		], 200);
	}
	
	public function update(Request $request, User $employee)
	{
		$validator = Validator::make($request->all(), [
			'surname' => 'required|string|max:255',
			'name' => 'required|string|max:255',
			'patronymic' => 'nullable|string|max:255',
			'email' => [
				'required',
				'email',
				'max:255',
				Rule::unique('users')->ignore($employee)
			],
			'gender_id' => 'required|exists:genders,id',
			'birth_date' => 'required|date_format:Y-m-d',
			'role_id' => [
				'required',
				Rule::exists('student_service_center_roles', 'id')->where(function ($query) {
					return $query->where('student_service_center_id', auth()->user()->sscs[0]->id);
				})
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}

		$data = DB::transaction(function() use ($request, $employee) {
			if ($request->email != $employee->email) {
				$confirm = $employee->emailConfirms()->create([
					'type_id' => Type::where('slug', $request->header('type'))->first()->id,
					'lang_id' => $employee->defaultLang->id,
					'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
					'ip' => $request->server('REMOTE_ADDR'),
					'ua' => $request->server('HTTP_USER_AGENT'),
					'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
					'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
					'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
					'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
					'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
					'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
					'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
					'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
					'hash' => sha1(rand(1000, 9999)),
					'src' => mb_strtolower($request->email)
				]);
			}

			$employee->update([
				'surname' => $request->surname,
				'name' => $request->name,
				'patronymic' => $request->patronymic ? $request->patronymic : null,
				'email' => $request->email,
				'gender_id' => $request->gender_id,
				'email_verified' => $request->email == $employee->email ? $employee->email_verified : '0'
			]);

			if ($employee->studentServiceCenterRoles()->wherePivotNull('deleted_at')->first()->id != $request->role_id) {
				$old_ssc_role = StudentServiceCenterRole::query()
					->where('id', $employee->studentServiceCenterRoles()->wherePivotNull('deleted_at')->first()->id)
					->first();

				$old_role = Role::query()
					->where('slug', $old_ssc_role->access->slug)
					->first();

				$new_ssc_role = StudentServiceCenterRole::query()
					->where('id', $request->role_id)
					->first();
			
				$new_role = Role::query()
					->where('slug', $new_ssc_role->access->slug)
					->first();

				if ($employee->hasRoleWithTrashed($new_role->slug)) {
					$employee->rolesWithTrashed()->updateExistingPivot($new_role->id, [
						'deleted_at' => null,
						'updated_at' => date('Y-m-d H:i:s')
					]);
				}

				if (!$employee->hasRoleWithTrashed($new_role->slug)) {
					$employee->roles()->attach($new_role, [
						'created_at' => date('Y-m-d H:i:s')
					]);
				}

				if ($old_role->id != $new_role->id) {
					$employee->rolesWithTrashed()->updateExistingPivot($old_role->id, [
						'deleted_at' => date('Y-m-d H:i:s')
					]);

					if ($employee->default_role_id == $old_role->id) {
						$employee->update([
							'default_role_id' => $new_role->id
						]); 
					}
				}

				//

				$employee->studentServiceCenterRolesWithTrashed()->updateExistingPivot($employee->studentServiceCenterRoles()->wherePivotNull('deleted_at')->first()->id, [
					'deleted_at' => date('Y-m-d H:i:s')
				]);

				if ($employee->hasStudentServiceCenterRoleWithTrashed($request->role_id)) {
					$employee->studentServiceCenterRolesWithTrashed()->updateExistingPivot($request->role_id, [
						'deleted_at' => null
					]);
				}
				
				if (!$employee->hasStudentServiceCenterRoleWithTrashed($request->role_id)) {
					$employee->studentServiceCenterRolesWithTrashed()->attach($request->role_id, [
						'created_at' => date('Y-m-d H:i:s')
					]);
				}
			}

			return [
				'confirm' => isset($confirm) ? $confirm : null
			];
		});

		if ($request->email != $employee->email) {
			Mail::to($request->email)
				->queue(new Confirm($data['confirm']));
		}
		
		return response()->json([
			'code' => 200,
			'data' => new StudentServiceCenterEmployeeResource($employee)
		], 200);
	}

    public function show(User $employee)
    {
		return new StudentServiceCenterEmployeeResource($employee);
    }
	
	public function destroy(User $employee)
    {
		DB::transaction(function() use ($employee) {
			$employee->sscs()->updateExistingPivot(auth()->user()->sscs[0], [
				'deleted_at' => date('Y-m-d H:i:s')
			]);

			$employee->roles()->whereIn('slug', ['ssc-head', 'ssc-administrator', 'ssc-employee'])->updateExistingPivot($employee->id, [
				'deleted_at' => date('Y-m-d H:i:s')
			]);

			$roles = Role::query()
				->whereIn('slug', ['ssc-head', 'ssc-administrator', 'ssc-employee'])
				->where('id', $employee->default_lang_id)
				->first();

			if ($roles) {
				$employee->update([
					'default_lang_id' => null
				]);
			}
		});
		
		return response()->json([
			'code' => 204
		], 200);
    }
}
