<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Validation\Rule;

use App\Models\Cato;
use App\Models\See;

use Validator;
use DB;

class AuthSeeController extends Controller
{
    public function update(Request $request, See $see)
    {
    	$validator = Validator::make($request->all(), [
    		'type_id' => 'required|integer|exists:see_types,id',
            'series' => 'required|max:255',
            'number' => 'required|max:255',
            'individual_code_of_the_tested' => 'required|string|max:255',
            'date_of_issue' => 'required|date_format:Y-m-d',
            'src' => 'nullable|file|mimes:pdf|max:5120'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages(),
            ], 422);
        }

        $see->update([
     		'type_id' => $request->type_id,
     		'series' => $request->series,
     		'number' => $request->number,
     		'individual_code_of_the_tested' => $request->individual_code_of_the_tested,
     		'date_of_issue' => $request->date_of_issue,
     		'src' => $request->has('src') ? Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->src) : $sec->src,
     	]);

     	return response()->json([], 200);
    }
}
