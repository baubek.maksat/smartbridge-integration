<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserHpedController extends Controller
{
    public function store(Request $request, User $user)
    {
    	$validator = Validator::make($request->all(), [
            'country_id' => 'required|integer|exists:citizenships',
	        'cato_id' => ,
	        'cato_name_kz',
	        'cato_name_ru',
	        'cato_name_en',
	        'university_id' => '',
	        'university_name' => '',
	        'series',
	        'number',
	        'date_of_issue',
	        'average_score',
	        'with_distinction'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $user->hpeds()->create([
        	'type_id' => $request->type_id,
	        'series' => $request->series,
	        'number' => $request->number,
	        'individual_code_of_the_tested' => $request->individual_code_of_the_tested,
	        'date_of_issue' => $request->date_of_issue,
	        'src' => Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->src)
        ]);

        return response()->json(null, 201);
    }
}
