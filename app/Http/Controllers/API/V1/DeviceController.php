<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\DeviceResource;

use App\Models\Platform;
use App\Models\Device;

use Validator;

class DeviceController extends Controller
{
    public function index(Request $request)
    {
        $devices = Device::query();

        
        
        $devices = $devices->paginate();

        return DeviceResource::collection($devices);
    }
    
    public function show(Device $device)
    {
        return response()->json([
            'data' => $device
        ], 200);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
			'platform' => 'required|string|in:ios,android',
            'brand' => 'required|string|max:255',
            'model' => 'required|string|max:255',
            'imei' => 'required|string|max:255',
            'width' => 'required|integer',
            'height' => 'required|integer',
			'signature' => 'required|string|max:256'
        ]);
		
		if ($validator->fails()) {
            return response()->json([
                'code' => 422,
				'messages' => $validator->messages()
            ], 200);
        }
		
		$device = Device::query()
			->where('imei', $request->imei)
			->first();
		
		if ($device) {
			return response()->json([
				'code' => 200,
				'data' => $device
			], 200);
		}
		
		$platform = Platform::query()
            ->where('slug', $request->platform)
            ->first();

        $device = Device::create([
            'platform_id' => $platform->id,
            'brand' => $request->brand,
            'model' => $request->model,
            'imei' => $request->imei,
            'width' => $request->width,
            'height' => $request->height,
            'signature' => $request->signature,
            'ip' => $request->server('REMOTE_ADDR')
        ]);

    	return response()->json([
            'code' => 201,
			'data' => $device
        ], 200);
    }
}
