<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\PlatformResource;

use App\Models\Platform;

class PlatformController extends Controller
{
	public function index(Request $request)
    {
    	$platforms = Platform::query();

    	$platforms = $platforms->paginate();

    	return PlatformResource::collection($platforms);
    }
}
