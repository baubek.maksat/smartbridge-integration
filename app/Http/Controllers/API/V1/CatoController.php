<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\CatoResource;

use App\Models\Cato;

use Validator;

class CatoController extends Controller
{
    public function index(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'per_page' => 'integer|between:1,50'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $catos = Cato::query();

        if ($request->has('filter.parent_id') && $request->filter['parent_id'] == 'null') {
            $catos->whereNull('parent_id');
        }
		
        $catos = $catos->paginate(20);

    	return CatoResource::collection($catos);
    }

    public function show(Cato $cato)
    {
    	$result = $cato->with([
    		'citizenship'
    	])->first();

    	return response()->json([
            'result' => $result
        ], 200);
    }
}
