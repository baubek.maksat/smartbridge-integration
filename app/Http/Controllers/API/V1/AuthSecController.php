<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Validation\Rule;

use App\Models\Cato;
use App\Models\Sec;

use Validator;
use DB;

class AuthSecController extends Controller
{
    public function update(Request $request, Sec $sec)
    {
    	$validator = Validator::make($request->all(), [
    		'citizenship_id' => 'required|exists:citizenships,id',
	        'cato_id' => [
	        	Rule::requiredIf(function () use ($request) {
                    return Cato::where('citizenship_id', $request->citizenship_id)->count() > 0;
                }),
                'exists:catos,id'
	        ],
	        'cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('citizenship_id', $request->citizenship_id)->count() == 0;
                }),
                'string',
                'max:255'
            ],
	        'school_name' => 'required|string|max:255',
	        'series' => 'required|string|max:255',
	        'number' => 'required|string|max:255',
	        'date_of_issue' => 'required|date_format:Y-m-d',
	        'average_score' => 'nullable|integer|1024',
	        'with_distinction' => 'required|boolean',
	        'gold_mark' => 'required|boolean',
	        'src' => 'nullable|file|mimes:pdf|max:5120',
	        'application_src' => 'nullable|file|mimes:pdf|max:5120'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages(),
            ], 422);
        }

        $sec = auth()->user()->sec()->first();

        auth()->user()->sec()->update([
     		'citizenship_id' => $request->citizenship_id,
     		'cato_id' => Cato::where('citizenship_id', $request->citizenship_id)->count() > 0 ? $request->cato_id : null,
     		'cato_name_'.app()->getLocale() => Cato::where('citizenship_id', $request->citizenship_id)->count() == 0 ? $request->cato_name : null,
     		'school_name_kz' => app()->getLocale() == 'kz' ? $request->school_name : $sec->school_name_kz,
     		'school_name_ru' => app()->getLocale() == 'ru' ? $request->school_name : $sec->school_name_ru,
     		'school_name_en' => app()->getLocale() == 'en' ? $request->school_name : $sec->school_name_en,
     		'series' => $request->series,
     		'number' => $request->number,
     		'date_of_issue' => $request->date_of_issue,
     		'average_score' => $request->has('average_score') ? $request->average_score : $sec->average_score,
     		'with_distinction' => $request->with_distinction,
     		'gold_mark' => $request->gold_mark,
     		'src' => $request->has('src') ? Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->src) : $sec->src,
     		'application_src' => $request->has('application_src') ? Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->application_src) : $sec->application_src
     	]);

     	return response()->json([], 200);
    }
}
