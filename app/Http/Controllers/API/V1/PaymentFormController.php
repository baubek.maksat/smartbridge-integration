<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\PaymentFormResource;

use App\Models\PaymentForm;

use Validator;
use DB;

class PaymentFormController extends Controller
{
    public function index(Request $request)
    {
    	$payment_forms = PaymentForm::query();
        
        $payment_forms = $payment_forms->paginate();

    	return PaymentFormResource::collection($payment_forms);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'university_id' => 'required|integer|exists:universities,id',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request) {
	        $payment_form = PaymentForm::create([
	        	'university_id' => $request->university_id
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                    $payment_form->translations()->create([
                        'lang' => $key,
                        'name' => $value['name']
                    ]);
                }
            }
	    });

        return response()->json([
            'messages' => 'Created'
        ], 201);
    }

    public function show(PaymentForm $payment_form)
    {
    	$data = PaymentForm::query()
            ->with('translation')
            ->with('translations')
            ->with('university.translation')
            ->with('university.translations')
            ->where('id', $payment_form->id)
            ->first();

        return response()->json([
            'data' => $data
        ], 201);
    }

    public function update(Request $request, PaymentForm $payment_form)
    {
        $validator = Validator::make($request->all(), [
            'university_id' => 'required|integer|exists:universities,id',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request, $payment_form) {
	        $payment_form->update([
	        	'university_id' => $request->university_id
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                	$payment_form->translations()->where('lang', $key)->update([
                        'name' => $value['name']
                    ]);
                }
            }
	    });
        
    	return response()->json([
            'messages' => 'Ok'
        ], 200);
    }

    public function destroy(PaymentForm $payment_form)
    {
        $payment_form->delete();

        return response()->json(null, 204);
    }
}
