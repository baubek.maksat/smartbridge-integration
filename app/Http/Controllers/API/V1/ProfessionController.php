<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\ProfessionResource;

use Illuminate\Validation\Rule;

use App\Models\Profession;

use Validator;

class ProfessionController extends Controller
{
	public function index(Request $request)
    {
    	$professions = Profession::query();

    	$professions = $professions->paginate();

    	return ProfessionResource::collection($professions);
    }

    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'view_id' => 'exists:replenishment_views,id',
			'state_id' => 'required|exists:states,id',
			'sum' => 'required|numeric|'
		]);

		dd($request->all());

        $data = DB::transaction(function() use ($request, $ssc) {
			$account_view = AccountView::create([
				'ssc_id' => $ssc->id,
				'parent_id' => $request->parent_id,
				'state_id' => $request->state_id
			]);

			foreach ($request->translations as $lang => $translation) {
				$account_view->translation()->create([
					'lang' => $lang,
					'name' => $translation['name']
				]);
			}

			return $account_view;
		});

		return response()->json([
			'code' => 201,
			'data' => new AccountViewResource($data)
		], 200);
    }
}
