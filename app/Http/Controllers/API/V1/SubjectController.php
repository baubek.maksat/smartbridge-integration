<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\SubjectResource;

use Illuminate\Validation\Rule;

use App\Models\User;
use App\Models\Subject;

use Validator;

class SubjectController extends Controller
{
	public function index(Request $request)
    {
    	$account_views = Subject::query();

    	$account_views = $account_views->paginate();

    	return SubjectResource::collection($account_views);
    }

    public function show(Subject $account_view)
    {
    	return new SubjectResource($account_view);
    }

    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'university_id' => 'exists:universities,id',
			'state_id' => 'required|exists:states,id',
			'translations' => 'required',
			'translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz');
				}),
				'string',
				'max:255',
				Rule::unique('account_view_langs', 'name')->where(function ($query) use ($ssc) {
					return true;
					//	return $query->where('lang', 'kz')->whereIn('id', $ssc->categories()->pluck('id'));
				})
			],
			'translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return true;
					//	return $ssc->hasLang('ru');
				}),
				'string',
				'max:255',
				Rule::unique('account_view_langs', 'name')->where(function ($query) use ($ssc) {
					return true;
					//	return $query->where('lang', 'ru')->whereIn('id', $ssc->categories()->pluck('id'));
				})
			],
			'translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en');
				}),
				'string',
				'max:255',
				Rule::unique('account_view_langs', 'name')->where(function ($query) use ($ssc) {
					return true;
					//	return $query->where('lang', 'en')->whereIn('id', $ssc->categories()->pluck('id'));
				})
			],
		]);

        $data = DB::transaction(function() use ($request, $ssc) {
			$account_view = Subject::create([
				'ssc_id' => $ssc->id,
				'parent_id' => $request->parent_id,
				'state_id' => $request->state_id
			]);

			foreach ($request->translations as $lang => $translation) {
				$account_view->translation()->create([
					'lang' => $lang,
					'name' => $translation['name']
				]);
			}

			return $account_view;
		});

		return response()->json([
			'code' => 201,
			'data' => new SubjectResource($data)
		], 200);
    }
}
