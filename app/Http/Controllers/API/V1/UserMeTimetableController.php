<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\TimetableResource;

use App\Models\Timetable;

class UserMeTimetableController extends Controller
{
	public function index()
	{
		$today = getdate();

		$timetables = Timetable::query();

		$timetables->whereHas('studyGroup.students', function($query) {
			$query->where('student_id', auth()->user()->student->card->id);
		});

		$timetables->where('week_id', $today['wday']);
		$timetables->where('week_number', 1);
		$timetables->orderBy('lesson_hour_id', 'asc');

		$timetables = $timetables->paginate();

		//  dates

		$dates = collect([
			[
				'short_name' => 'ПН',
				'number' => date('d', strtotime('monday this week')),
			],[
				'short_name' => 'ВТ',
				'number' => date('d', strtotime('tuesday this week')),
			],[
				'short_name' => 'СР',
				'number' => date('d', strtotime('wednesday this week')),
			],[
				'short_name' => 'ЧТ',
				'number' => date('d', strtotime('thursday this week')),
			],[
				'short_name' => 'ПТ',
				'number' => date('d', strtotime('friday this week')),
			],[
				'short_name' => 'СБ',
				'number' => date('d', strtotime('saturday this week')),
			],[
				'short_name' => 'ВС',
				'number' => date('d', strtotime('sunday this week'))
			]
		]);

		return TimetableResource::collection($timetables)->additional([
			'dates' => $dates
		]);
	}
}
