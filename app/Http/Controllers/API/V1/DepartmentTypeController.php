<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\DepartmentTypeResource;

use App\Models\DepartmentType;

use Validator;

class DepartmentTypeController extends Controller
{
    public function index(Request $request)
    {
		$department_types = DepartmentType::query();
		
		$department_types = $department_types->paginate();

    	return DepartmentTypeResource::collection($department_types);
    }
}
