<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\SpecialityCostResource;

use App\Models\SpecialityCost;

use Validator;

class SpecialityCostController extends Controller
{
    public function index(Request $request)
    {
		$speciality_costs = SpecialityCost::query();
		
		$speciality_costs = $speciality_costs->paginate();

    	return SpecialityCostResource::collection($speciality_costs);
    }
}
