<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\DepartmentResource;

use Illuminate\Validation\Rule;

use App\Models\Department;

use Validator;
use DB;

class DepartmentController extends Controller
{
    public function index(Request $request)
    {
    	$departments = Department::query();

        if ($request->has('filter.faculty_id')) {
            $departments->where('faculty_id', $request->filter['faculty_id']);
        }

        if ($request->has('filter.state_id')) {
            $departments->where('state_id', $request->filter['state_id']);
        }

        $departments = $departments->paginate();

    	return DepartmentResource::collection($departments);
    }

    public function show(Department $department)
    {
        return new DepartmentResource($department);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'faculty_id' => 'required|integer|exists:faculties,id',
            'administrator_id' => 'required|integer|exists:users,id',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request) {
	        if ($request->has('administrator_id')) {
                DepartmentAdministrator::firstOrCreate([
					'id' => $request->administrator_id
				]);
            }

            $department = Department::create([
	        	'faculty_id' => $request->faculty_id,
	        	'administrator_id' => $request->administrator_id,
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                    $academic_degree->translations()->create([
                        'lang' => $key,
                        'name' => $value['name']
                    ]);
                }
            }

            if ($request->has('administrator_id')) {
            	$card = $faculty->administratorCards()->create([
                    'worker_position_id' => $faculty->university->faculty_type_id,
                    'date_start' => date('Y-m-d')
                ]);

                $card->administrator()->attach($request->administrator_id);
            }
	    });

        return response()->json([
            'code' => 201
        ], 201);
    }
}
