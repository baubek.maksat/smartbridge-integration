<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Validation\Rule;

use App\Http\Resources\HsCategoryResource;

use App\Models\HsCategory;

use Validator;
use DB;

class HsCategoryController extends Controller
{
    public function index(Request $request)
    {
    	$hs_categories = HsCategory::query()
			->has('translation');
		
		if (auth()->user()->currentRole()->slug == 'student') {
			$hs_categories->where('university_id', auth()->user()->student->card->group->speciality->department->faculty->university_id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$hs_categories->where('university_id', auth()->user()->sscs[0]->university->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$hs_categories->where('university_id', auth()->user()->sscs[0]->university->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$hs_categories->where('university_id', auth()->user()->sscs[0]->university->id);
		}
		
		
		
		if ($request->has('filter.name')) {
			$hs_categories->whereHas('translations', function($query) use ($request) {
				$query->where('name', 'LIKE', $request->filter['name']);
			});
		}
			
		$hs_categories = $hs_categories->paginate();
			
    	return HsCategoryResource::collection($hs_categories);
    }

    public function show(Request $request, HsCategory $hs_category)
    {
		$hs_category = HsCategory::query()
			->where('id', $hs_category->id);
			
		if ($request->has('filter.query')) {
			$hs_category->whereHas('hss.translations', function ($query) use ($request) {
				if ($request->has('filter.query')) {
					$query->where('question', 'LIKE', $request->filter['query'])
						->orWhere('answer', 'LIKE', $request->filter['query']);
				}
			});
		}
		
		$hs_category = $hs_category->first();
		
    	return new HsCategoryResource($hs_category);
    }

    public function store(Request $request)
    {
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'state_id' => 'required|exists:states,id',
            'icon' => 'nullable|image|max:1024|dimensions:ratio=1/1',
			'translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:255',
				Rule::unique('hs_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})
			],
			'translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:255',
				Rule::unique('hs_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})
			],
			'translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('hs_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})
			],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$data = DB::transaction(function() use ($request, $ssc) {
			$hs_category = HsCategory::create([
				'state_id' => $request->state_id,
				'university_id' => $ssc->university->id,
				'icon' => $request->icon ? 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $request->icon, rand(1000, 9999).'.'.$request->icon->extension()) : null
			]);

			foreach ($request->translations as $lang => $translation) {
				$hs_category->translations()->create([
					'lang' => $lang,
					'name' => $translation['name']
				]);
			}

			return $hs_category;
		});

		return response()->json([
			'code' => 201,
			'data' => new HsCategoryResource($data)
		], 200);
    }

    public function update(Request $request, HsCategory $hs_category)
    {
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'state_id' => 'required|exists:states,id',
            'icon' => 'nullable|image|dimensions:ratio=1/1',
			'icon_link' => 'nullable|active_url',
			'translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:255',
				Rule::unique('hs_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs_category->id, 'id')
			],
			'translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:255',
				Rule::unique('hs_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs_category->id, 'id')
			],
			'translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('hs_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs_category->id, 'id')
			],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$data = DB::transaction(function() use ($request, $hs_category, $ssc) {
			if ($request->has('icon_link')) {
				$icon = $hs_category->icon;
			}
			
			if ($request->has('icon')) {
				$icon = 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $request->icon, rand(1000, 9999).'.'.$request->icon->extension());
			}
			
			$hs_category->update([
				'state_id' => $request->state_id,
				'university_id' => $ssc->university->id,
				'icon' => isset($icon) ? $icon : null
			]);

			$hs_category->translations()->whereNotIn('lang', $request->langs)->delete();

			foreach ($request->translations as $lang => $translation) {
				if ($hs_category->translations()->where('lang', $lang)->first()) {
					$hs_category->translations()->where('lang', $lang)->update([
						'name' => $translation['name']
					]);
				} else {
					$hs_category->translations()->create([
						'lang' => $lang,
						'name' => $translation['name']
					]);
				}
			}

			return $hs_category;
		});

		return response()->json([
			'code' => 200,
			'data' => new HsCategoryResource($data)
		], 200);
    }

    public function destroy(HsCategory $hs_category)
	{
		$hs_category->delete();
		
		return response()->json([
			'code' => 204
		], 200);
	}
}