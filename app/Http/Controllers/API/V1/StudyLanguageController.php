<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StudyLanguageResource;

use App\Models\StudyLanguage;

use Validator;
use DB;

class StudyLanguageController extends Controller
{
	public function index(Request $request)
	{
		$study_languages = StudyLanguage::query();

		$study_languages = $study_languages->paginate();

		return StudyLanguageResource::collection($study_languages);
	}

	public function show(Request $request, StatementView $statement_view)
	{
		return new StudyLanguageResource($statement_view);
	}

	public function store(Request $request)
	{
		$ssc = auth()->user()->sscs[0];
		
		//	Validator start

		$validator = Validator::make($request->all(), [
			'category_id' => 'required|exists:statement_categories,id',
			'state_id' => 'required|exists:states,id',
			'responsible_role_id' => [
				'required',
				Rule::exists('student_service_center_roles', 'id')->where(function ($query) use ($ssc) {
					return $query->where('student_service_center_id', $ssc->id);
				})
			],
			'responsible_user_id' => [
				'nullable',
				Rule::exists('student_service_center_users_roles', 'user_id')->where(function ($query) use ($request) {
					return $query->where('role_id', $request->responsible_role_id);
				})
			],
			'term_of_consideration' => 'required|integer|max:255',
			'translations' => 'required',
			'translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz');
				}),
				'string',
				'max:255',
				Rule::unique('statement_view_langs', 'name')->where(function ($query) use ($ssc) {
					$statement_views = StatementView::query()
						->whereIn('category_id', $ssc->categories()->pluck('id'))
						->get();
					
					return $query->where('lang', 'kz')->whereIn('id', $statement_views->pluck('id'));
				})
			],
			'translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('ru');
				}),
				'string',
				'max:255',
				Rule::unique('statement_view_langs', 'name')->where(function ($query) use ($ssc) {
					$statement_views = StatementView::query()
						->whereIn('category_id', $ssc->categories()->pluck('id'))
						->get();
					
					return $query->where('lang', 'ru')->whereIn('id', $statement_views->pluck('id'));
				})
			],
			'translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en');
				}),
				'string',
				'max:255',
				Rule::unique('statement_view_langs', 'name')->where(function ($query) use ($ssc) {
					$statement_views = StatementView::query()
						->whereIn('category_id', $ssc->categories()->pluck('id'))
						->get();
					
					return $query->where('lang', 'en')->whereIn('id', $statement_views->pluck('id'));
				})
			],
			
			'templates' => 'array',
			'templates.kz' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz') && $request->has('templates');
				}),
				'file',
				'mimetypes:application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'max:5120'
			],
			'templates.ru' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('ru') && $request->has('templates');
				}),
				'file',
				'mimetypes:application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'max:5120'
			],
			'templates.en' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en') && $request->has('templates');
				}),
				'file',
				'mimetypes:application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'max:5120'
			],
			
			'instructions' => 'array',
			'instructions.kz' => [
				'array',
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz') && $request->has('instructions');
				})
			],
			'instructions.ru' => [
				'array',
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('ru') && $request->has('instructions');
				})
			],
			'instructions.en' => [
				'array',
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en') && $request->has('instructions');
				})
			],

			'components' => 'array',
			'components.*.type_id' => 'required|exists:statement_component_types,id',
			'components.*.rules' => 'array',
			'components.*.rules.min' => 'integer|min:1|max:255',
			'components.*.rules.max' => 'integer|min:1|max:255',
			'components.*.translations' => 'required',
			'components.*.translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz');
				}),
				'string',
				'max:511'
			],
			'components.*.translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('ru');
				}),
				'string',
				'max:511'
			],
			'components.*.translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en');
				}),
				'string',
				'max:511'
			],
			'components.*.translations.kz.data' => 'array',
			'components.*.translations.ru.data' => 'array',
			'components.*.translations.en.data' => 'array',
			'components.*.translations.kz.data.*' => 'required|string',
			'components.*.translations.ru.data.*' => 'required|string',
			'components.*.translations.en.data.*' => 'required|string',

			'stages' => 'array',
			'stages.*' => 'array',
			'stages.*.slug' => 'required|string|max:256',
			'stages.*.conditions' => 'nullable|array',
			'stages.*.conditions.*' => 'required|string|max:1024',
			'stages.*.processes' => 'nullable|array',
			'stages.*.processes.*' => 'required|string|max:1024',
			/*
			'stages.*.processes.*.if' => 'required|array',
			'stages.*.processes.*.if.left' => 'required|string|max:1024',
			'stages.*.processes.*.if.operator' => 'required|string|in:==,!=',
			'stages.*.processes.*.if.right' => 'required|string|max:1024',
			'stages.*.processes.*.then' => 'required|string|max:1024',
			'stages.*.processes.*.else' => 'nullable|string|max:1024',
			*/
			'stages.*.routes' => 'required',
			'stages.*.routes.*.responsible_type' => 'required|string|in:role,employee,author',
			'stages.*.routes.*.responsible_role_id' => [
				'nullable',
				'integer',
				Rule::exists('student_service_center_roles', 'id')->where(function ($query) use ($ssc) {
					return $query->where('student_service_center_id', $ssc->id);
				})
			],
			'stages.*.routes.*.responsible_employee_id' => [
				'integer',
				'exists:student_service_centers_users,user_id'
			],
			'stages.*.routes.*.author' => [
				'in:1'
			],
			'stages.*.routes.*.action_id' => 'required|integer|exists:statement_actions,id',
			'stages.*.routes.*.term' => 'nullable|integer'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		//	Validator end

		$data = DB::transaction(function() use ($request) {
			$statement_view = StatementView::create([
				'category_id' => $request->category_id,
				'responsible_role_id' => $request->responsible_role_id,
				'responsible_user_id' => $request->has('responsible_user_id') ? $request->responsible_user_id : null,
				'state_id' => $request->state_id,
				'term_of_consideration' => $request->term_of_consideration,
			]);

			foreach ($request->translations as $lang => $translation) {
				if ($request->has('templates')) {
					$template = asset('storage/'.Storage::putFileAs('uploads/'.date('y/m/d/h/i/s'), $request->templates[$lang], rand(1000, 9999).'.'.$request->templates[$lang]->extension()));
				}
				
				$statement_view->translation()->create([
					'lang' => $lang,
					'name' => $translation['name'],
					'template' => $request->has('templates') ? $template : null,
					'instruction' => $request->has('instructions') ? $request->instructions[app()->getLocale()] : null
				]);
			}

			if ($request->has('components')) {
				foreach ($request->components as $component) {
					$rules = collect();

					//	If slug request component type file or files then rules to indicate file 

					$statement_component_type = StatementComponentType::query()
						->where('id', $component['type_id'])
						->first();

					if ($statement_component_type->slug == 'file') $rules->push('file');
					if ($statement_component_type->slug == 'files') $rules->push('file');
					if ($statement_component_type->slug == 'select-multiple') $rules->push('array');

					if (isset($component['rules'])) {
						if (count($component['rules']) > 0) {
							foreach ($component['rules'] as $rule_key => $rule_value) {
								if ($rule_key == 'required' && $rule_value == 1) $rules->push('required');
							}
						}
					}

					$statement_component = $statement_view->components()->create([
						'type_id' => $component['type_id'],
						'rules' => $rules,
						'resource' => ($statement_component_type->slug == 'resource' || $statement_component_type->slug == 'resource-multiple') ? $component['resource'] : null
					]);

					foreach ($component['translations'] as $lang => $translation) {
						$data = collect();
						
						if (isset($translation['data']) && count($translation['data']) > 0) {
							foreach ($translation['data'] as $option) {
								$data->push($option);
							}
						}
						
						$statement_component->translation()->create([
							'lang' => $lang,
							'name' => $translation['name'],
							'data' => $data->count() > 0 ? $data : null
						]);
					}
				}
			}

			if ($request->has('stages')) {
				foreach ($request->stages as $stage) {
					
					//	conditions start
					
					$conditions = null;
						
					if (isset($stage['conditions'])) {
						foreach ($stage['conditions'] as $condition) {
							if (collect($request->stages)->where('slug', $condition)->count() > 0) {
								$conditions[] = '\App\Models\StatementRoute::whereHas("stage",function($query){$query->where("slug","'.$condition.'");})->where("status","success")->count() == \App\Models\StatementRoute::whereHas("stage",function($query){$query->where("slug","'.$condition.'");})->count()';
							}
						}
					}
					
					//	conditions end
					
					//	processes start
					
					$processes = null;
						
					if (isset($stage['processes'])) {
						foreach ($stage['processes'] as $process) {
							if ($process == 'pass') {
								$processes[] = [
									'if' => [
										'left' => '$statement_route->stage->routes->where("status","success")->count()',
										'operator' => '==',
										'right' => '$statement_route->stage->routes->count()'
									],
									'then' => '$statement_route->stage->statement->update(["status_id"=>\App\Models\StatementStatus::where("slug","ready")->first()->id])'
								];
							}
							
							if ($process == 'fail') {
								$processes[] = [
									'if' => [
										'left' => '$statement_route->stage->routes->where("status","fail")->count()',
										'operator' => '==',
										'right' => '$statement_route->stage->routes->count()'
									],
									'then' => '$statement_route->stage->statement->update(["status_id"=>\App\Models\StatementStatus::where("slug","renouncement")->first()->id,"ended_at"=>date("Y-m-d H:i:s")]);'
								];
							}
						}
					}
					
					//	processes end
					
					$statement_stage = $statement_view->stages()->create([
						'slug' => $stage['slug'],
						'conditions' => $conditions,
						'processes' => $processes,
					]);
					
					foreach ($stage['routes'] as $route) {
						if ($route['responsible_type'] == 'author') {
							$responsible_type = 'responsible';
						}
						
						if ($route['responsible_type'] == 'employee') {
							$responsible = StudentServiceCenterUserRole::query()
								->where('user_id', $route['responsible_employee_id'])
								->first();
							
							$responsible_type = 'user';
						}
						
						if ($route['responsible_type'] == 'role') {
							$responsible = StudentServiceCenterRole::query()
								->where('id', $route['responsible_role_id'])
								->first();
							
							$responsible_type = 'role';
						}
						
						if ($request->has('responsible_user_id') && $responsible_type == 'responsible') {
							$responsible_role_id = $request->responsible_role_id;
							$responsible_user_id = $request->responsible_user_id;
						}
						
						if (!$request->has('responsible_user_id') && $responsible_type == 'responsible') {
							$responsible_role_id = $request->responsible_role_id;
							$responsible_user_id = null;
						}
						
						if ($responsible_type == 'user') {
							$responsible_role_id = $responsible->role_id;
							$responsible_user_id = $responsible->user_id;
						}
						
						if ($responsible_type == 'role') {
							$responsible_role_id = $responsible->id;
							$responsible_user_id = null;
						}
						
						$statement_stage->routes()->create([
							'action_id' => $route['action_id'],
							'responsible_type' => $responsible_type,
							'responsible_role_id' => $responsible_role_id,
							'responsible_user_id' => $responsible_user_id,
							'term' => isset($route['term']) ? $route['term'] : null
						]);
					}
				}
			}

			return $statement_view;
		});

		return response()->json([
			'code' => 201,
			'data' => $data
		], 200);
	}

	public function update(Request $request, StatementView $statement_view)
	{
		$validator = Validator::make($request->all(), [
			'category_id' => 'required|exists:statement_categories,id',
			'pdf' => 'required|integer|in:0,1',
			'name.kz' => 'required|array',
			'name.ru' => 'required|exists:langs,slug',
			'name.en' => 'required|string|max:255',
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}

		DB::transaction(function() use ($request) {
			$statement_view->update([
				'category_id' => $request->category_id,
				'pdf' => $request->pdf
			]);

			foreach ($request->translations as $translation) {
				$statement_view->translation()->update([
					'name' => $translation->name
				])->where('lang', $translation->lang);
			}
		});

		return response()->json([
			'code' => 201
		], 200);
	}
	
	public function destroy(StatementView $statement_view)
	{
		DB::transaction(function() use ($statement_view) {
			//	$statement_view->components()->each(function($component) {
			//		$component->translations()->delete();
			//		$component->delete();
			//	});
			
			//	$statement_view->translations()->delete();
			$statement_view->delete();
		});
		
		return response()->json([
			'code' => 204
		], 200);
	}
}
