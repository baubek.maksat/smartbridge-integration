<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StatementActionResource;

use App\Models\StatementAction;

use Validator;

class StatementActionController extends Controller
{
    public function index(Request $request)
    {
		$statement_actions = StatementAction::query();
		
		$statement_actions = $statement_actions->paginate();

    	return StatementActionResource::collection($statement_actions);
    }
}
