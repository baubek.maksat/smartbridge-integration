<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\IdTypeResource;

use App\Models\IdType;

use Validator;

class IdTypeController extends Controller
{
    public function index(Request $request)
    {
    	$id_types = IdType::query();

        $id_types = $id_types->paginate();

    	return IdTypeResource::collection($id_types);
    }
}
