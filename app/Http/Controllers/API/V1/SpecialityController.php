<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\SpecialityResource;

use App\Models\Speciality;

use Validator;
use DB;

class SpecialityController extends Controller
{
	public function index(Request $request)
	{
		$specialities = Speciality::query();

		/*

		if (auth()->user()->currentRole()->slug == 'content-manager') {
			$specialities->whereHas('department.faculty.university', function($query) use ())
		}

		*/
		
		//	filter
		
		$specialities->whereHas('translation', function ($query) use ($request) {
			if (isset($request->filter['name'])) {
				$query->where('name', 'LIKE', $request->filter['name']);
			}
		});
		
		//	order by
		
		if ($request->has('orderBy.id')) {
			$specialities->orderBy('id', 'desc');
		}
		
		//	Role
		
		if (auth()->user()->currentRole()->slug == 'student') {
			
		}
		
		//

		$specialities = $specialities->paginate();

		return SpecialityResource::collection($specialities);
	}

	public function show(Request $request, Speciality $speciality)
	{
		return new SpecialityResource($speciality);
	}
 }
