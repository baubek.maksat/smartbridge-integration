<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\accountResource;

use App\Models\ReplenishmentView;
use App\Models\TransactionView;
use App\Models\TransferView;
use App\Models\Transaction;
use App\Models\Account;

use Validator;
use DB;

class UserMeTransferController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'view' => 'required|exists:transfer_views,slug',
			'account_id' => 'required|integer|exists:accounts,id',
			'sum' => 'required|numeric|min:1|max:10000000|lte:'.auth()->user()->account->balance
		]);

		if ($validator->fails() || (auth()->user()->account->balance < $request->sum)) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		$data = DB::transaction(function() use ($request) {
			if ($request->view == 'between-your-accounts') {
				auth()->user()->account()->update([
					'balance' => auth()->user()->account->balance - $request->sum
				]);

				$account = Account::query()
					->where('id', $request->account_id)
					->first();

				$account->update([
					'balance' => $account->balance + $request->sum
				]);

				$transaction = Transaction::create([
					'view_id' => TransactionView::where('slug', 'transfer')->first()->id,
					'account_id' => auth()->user()->account->id,
					'sum' => $request->sum
				]);

				$transaction->transfer()->create([
					'view_id' => TransferView::where('slug', 'between-your-accounts')->first()->id,
					'account_id' => $request->account_id
				]);

				$transaction = Transaction::create([
					'view_id' => TransactionView::where('slug', 'replenishment')->first()->id,
					'account_id' => $request->account_id,
					'sum' => $request->sum
				]);

				$transaction->replenishment()->create([
					'view_id' => ReplenishmentView::where('slug', 'transfer')->first()->id,
					'account_id' => $request->account_id
				]);
			}
			

			return [
				'id' => 1
			];
		});



		return response()->json([
			'code' => 201,
			'data' => $data
		], 200);
	}
}
