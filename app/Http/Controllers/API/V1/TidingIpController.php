<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Tiding;
use App\Models\Type;

class TidingIpController extends Controller
{
    public function store(Tiding $tiding, Request $request)
    {
		$tiding->ips()->create([
			'type_id' => Type::where('slug', $request->header('type'))->first()->id,
			'ip' => $request->server('REMOTE_ADDR')
        ]);
		
		return response()->json([
			'code' => 201
		], 200);
    }
}
