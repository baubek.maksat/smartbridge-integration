<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Id;

use Validator;

class AuthIdController extends Controller
{
    public function store()
    {
        $validator = Validator::make($request->all(), [
            'citizenship_id' => 'required|exists:citizenships,id',
            'type_id' => 'required|exists:id_types_citizenships,id_type_id,citizenship_id,'.$request->citizenship_id,
            'series' => 'required|string|max:255|alpha_num',
            'number' => 'required|string|max:255|alpha_num',
            'tin' => 'nullable|string|max:255',
            'src' => 'nullable|file|mimes:pdf|max:10240',
            'date_start' => 'required|date_format:Y-m-d',
            'date_end' => 'required|date_format:Y-m-d',
            'issuing_authority_id' => 'required|exists:id_issuing_authorities,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages(),
            ], 422);
        }

        Id::([
            'citizenship_id' => $request->citizenship_id,
            'type_id' => $request->type_id,
            'series' => mb_strtoupper($request->series),
            'number' => $request->number,
            'tin' => $request->has('tin') ? $request->tin : null,
            'src' => $request->has('src') ? Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->src) : $id->src,
            'date_start' => $request->date_start,
            'date_end' => $request->date_end,
            'issuing_authority_id' => $request->issuing_authority_id
        ]);

        return response()->json([], 200);
    }

    public function update(Request $request, Id $id)
    {
        $validator = Validator::make($request->all(), [
    		'citizenship_id' => 'required|exists:citizenships,id',
    		'type_id' => 'required|exists:id_types_citizenships,id_type_id,citizenship_id,'.$request->citizenship_id,
    		'series' => 'required|string|max:255|alpha_num',
            'number' => 'required|string|max:255|alpha_num',
    		'tin' => 'nullable|string|max:255',
            'src' => 'nullable|file|mimes:pdf|max:10240',
    		'date_start' => 'required|date_format:Y-m-d',
    		'date_end' => 'required|date_format:Y-m-d',
    		'issuing_authority_id' => 'required|exists:id_issuing_authorities,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages(),
            ], 422);
        }

        $id = auth()->user()->id()->first();

        auth()->user()->id()->update([
     		'citizenship_id' => $request->citizenship_id,
     		'type_id' => $request->type_id,
     		'series' => mb_strtoupper($request->series),
            'number' => $request->number,
     		'tin' => $request->has('tin') ? $request->tin : null,
            'src' => $request->has('src') ? Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->src) : $id->src,
     		'date_start' => $request->date_start,
     		'date_end' => $request->date_end,
     		'issuing_authority_id' => $request->issuing_authority_id
     	]);

        return response()->json([], 200);
    }
}
