<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\CountryResource;

use App\Models\Country;

use Validator;

class CountryController extends Controller
{
    public function index(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'per_page' => 'nullable|integer|max:1000'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		$per_page = $request->has('per_page') ? $request->per_page : 15;
		
		$countries = Country::query();
		
		$countries = $countries->paginate($per_page);

    	return CountryResource::collection($countries);
    }
}
