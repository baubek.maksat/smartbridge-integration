<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\CatoResource;

use App\Models\MobileAppRelease;

use Validator;

class MobileAppReleaseController extends Controller
{
    public function index(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'per_page' => 'nullable|integer|between:1,50'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $mobile_app_release = MobileAppRelease::query();

        if ($request->has('filter.parent_id') && $request->filter['parent_id'] == 'null') {
            $catos->whereNull('parent_id');
        }
		
        $mobile_app_release = $mobile_app_release->paginate();

        dd($mobile_app_release);

    	return CatoResource::collection($mobile_app_release);
    }

    public function show(Cato $cato)
    {
    	$result = $cato->with([
    		'citizenship'
    	])->first();

    	return response()->json([
            'result' => $result
        ], 200);
    }
}
