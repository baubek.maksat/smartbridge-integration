<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\TransactionResource;

use App\Models\Transaction;

use \Carbon\Carbon;

class UserMeTransactionController extends Controller
{
	public function index(Request $request)
	{
		$date = [
			'start' => Carbon::now()->subDays(30)->format('Y-m-d'),
			'end' => Carbon::now()->format('Y-m-d')
		];

		$transactions = Transaction::query()
			->where('account_id', auth()->user()->account->id);

		if ($request->has('orderBy')) $transactions->orderBy('created_at', 'desc');

		if ($request->has('filter.interval') && $request->filter['interval'] == 'week') {
			$current_date = Carbon::now();
			$last_week_date = Carbon::now()->subDays(7);

			$date['start'] = $last_week_date->format('Y-m-d');
			$date['end'] = $current_date->format('Y-m-d');

			$transactions->whereBetween('created_at', [
				$date['start'].' 00:00:00',
				$date['end'].' 23:59:59'
			]);
		}

		if ($request->has('filter.interval') && $request->filter['interval'] == 'month') {
			$current_date = Carbon::now();
			$last_week_date = Carbon::now()->subDays(30);

			$date['start'] = $last_week_date->format('Y-m-d');
			$date['end'] = $current_date->format('Y-m-d');

			$transactions->whereBetween('created_at', [
				$date['start'].' 00:00:00',
				$date['end'].' 23:59:59'
			]);
		}

		if ($request->has('filter.interval') && $request->filter['interval'] == 'month3') {
			$current_date = Carbon::now();
			$last_week_date = Carbon::now()->subDays(90);

			$date['start'] = $last_week_date->format('Y-m-d');
			$date['end'] = $current_date->format('Y-m-d');

			$transactions->whereBetween('created_at', [
				$date['start'].' 00:00:00',
				$date['end'].' 23:59:59'
			]);
		}

		if ($request->has('filter.interval') && $request->filter['interval'] == 'halfYear') {
			$current_date = Carbon::now();
			$last_week_date = Carbon::now()->subDays(182);

			$date['start'] = $last_week_date->format('Y-m-d');
			$date['end'] = $current_date->format('Y-m-d');

			$transactions->whereBetween('created_at', [
				$date['start'].' 00:00:00',
				$date['end'].' 23:59:59'
			]);
		}

		if ($request->has('filter.interval') && $request->filter['interval'] == 'year') {
			$current_date = Carbon::now();
			$last_week_date = Carbon::now()->subDays(365);

			$date['start'] = $last_week_date->format('Y-m-d');
			$date['end'] = $current_date->format('Y-m-d');

			$transactions->whereBetween('created_at', [
				$date['start'].' 00:00:00',
				$date['end'].' 23:59:59'
			]);
		}

		if ($request->has('filter.interval') && $request->filter['interval'] == 'period') {
			$current_date = Carbon::now();
			$last_week_date = Carbon::now()->subDays(365);

			$date['start'] = $request->filter['date_start'];
			$date['end'] = $request->filter['date_end'];

			$transactions->whereBetween('created_at', [
				$date['start'].' 00:00:00',
				$date['end'].' 23:59:59'
			]);
		}

		$transactions = $transactions->paginate();

		//	Dates

		$dates = collect();

		if ($transactions) {
			foreach ($transactions as $transaction) {
				$dates->push($transaction->created_at->format('Y-m-d'));
			}

			$dates = $dates->unique();
		}

		//	Statistics

		$coming = 0;
		$consumption = 0;

		if ($transactions) {
			foreach ($transactions as $transaction) {
				if ($transaction->view->id == 101) $coming += $transaction->sum;
				if ($transaction->view->id == 102) $consumption += $transaction->sum;
				if ($transaction->view->id == 103) $consumption += $transaction->sum;
			}
		}

		$statistic = collect([
			'date' => $date,
			'coming' => $coming,
			'consumption' => $consumption
		]);

		return TransactionResource::collection($transactions)->additional([
			'statistic' => $statistic,
			'dates' => $dates
		]);
	}
}
