<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\GenderResource;

use App\Models\Gender;

use Validator;

class GenderController extends Controller
{
    public function index(Request $request)
    {
        $genders = Gender::query()
            ->paginate();

    	return GenderResource::collection($genders);
    }
}
