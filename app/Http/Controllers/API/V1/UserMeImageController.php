<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Validator;

class UserMeImageController extends Controller
{
	public function update(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'image' => [
				'required',
				'file',
				'mimes:jpeg,jpg,png',
				'dimensions:ratio=1/1',
				'max:1024'
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		auth()->user()->update([
			'image' => $request->has('image') ? asset('storage/'.Storage::putFileAs('uploads/'.date('y/m/d/h/i/s'), $request->image, rand(1000, 9999).'.'.$request->image->extension())) : null
		]);
		
		return response()->json([
			'code' => 200,
			'data' => [
				'image' => auth()->user()->image
			]
		], 200);
	}

	public function destroy(Request $request)
	{
		auth()->user()->update([
			'image' => null
		]);
		
		return response()->json([
			'code' => 204
		], 200);
	}
}
