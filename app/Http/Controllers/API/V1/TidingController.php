<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Storage;

use App\Http\Resources\TidingResource;

use App\Events\TidingCreated;

use App\Models\Tiding;

use Validator;
use DB;

class TidingController extends Controller
{
    public function index(Request $request)
    {
    	$tidings = Tiding::query()
			->has('translation');
		
		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$tidings->where('university_id', auth()->user()->sscs[0]->university_id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$tidings->where('university_id', auth()->user()->sscs[0]->university_id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$tidings->where('university_id', auth()->user()->sscs[0]->university_id);
		}
		
		if ($request->has('filter.heading')) {
			$tidings->whereHas('translations', function($query) use ($request) {
				$query->where('heading', 'LIKE', $request->filter['heading']);
			});
		}
		
		if ($request->has('filter.state_id')) {
			$tidings->where('state_id', $request->filter['state_id']);
		}
		
    	$tidings = $tidings->paginate();

    	return TidingResource::collection($tidings);
    }

    public function show(Tiding $tiding)
    {
    	return new TidingResource($tiding);
    }
	
	public function store(Request $request)
	{
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'state_id' => 'required|exists:states,id',
			'langs' => 'required|array|exists:langs,slug',
            'image' => [
				'required',
				'image',
				'max:2048',
				'dimensions:ratio=2/1'
			],
			'date_of_publication' => 'required|date|date_format:Y-m-d',
			'translations.kz.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:255',
				Rule::unique('tiding_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->tidings()->pluck('id'));
				})
			],
			'translations.ru.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:255',
				Rule::unique('tiding_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->tidings()->pluck('id'));
				})
			],
			'translations.en.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('tiding_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->tidings()->pluck('id'));
				})
			],
			'translations.kz.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:15000'
			],
			'translations.ru.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:15000'
			],
			'translations.en.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:15000'
			],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$data = DB::transaction(function() use ($request, $ssc) {
			$tiding = Tiding::create([
				'state_id' => $request->state_id,
				'university_id' => $ssc->university->id,
				'date_of_publication' => $request->date_of_publication,
				'image' => 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $request->image, rand(1000, 9999).'.'.$request->image->extension())
			]);

			foreach ($request->translations as $lang => $translation) {
				$tiding->translations()->create([
					'lang' => $lang,
					'heading' => $translation['heading'],
					'discription' => $translation['discription']
				]);
			}

			return $tiding;
		});
		
		if ($data->state_id == 2) {
			TidingCreated::dispatch($data);
		}

		return response()->json([
			'code' => 201,
			'data' => new TidingResource($data)
		], 200);
    }
	
    public function update(Request $request, Tiding $tiding)
    {
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'state_id' => 'required|exists:states,id',
			'date_of_publication' => 'required|date|date_format:Y-m-d',
			'langs' => 'required|array|exists:langs,slug',
            'image' => [
				Rule::requiredIf(function () use ($request){
					return !$request->has('image_link');
				}),
				'image',
				'max:2048',
				'dimensions:ratio=2/1'
			],
			'image_link' => [
				Rule::requiredIf(function () use ($request){
					return !$request->has('image');
				}),
				'active_url'
			],
			'translations.kz.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:255',
				Rule::unique('tiding_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->tidings()->pluck('id'));
				})->ignore($tiding->id, 'id')
			],
			'translations.ru.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:255',
				Rule::unique('tiding_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->tidings()->pluck('id'));
				})->ignore($tiding->id, 'id')
			],
			'translations.en.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('tiding_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->tidings()->pluck('id'));
				})->ignore($tiding->id, 'id')
			],
			'translations.kz.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:15000'
			],
			'translations.ru.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:15000'
			],
			'translations.en.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:15000'
			],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$data = DB::transaction(function() use ($request, $ssc, $tiding) {
			if ($request->has('image_link')) {
				$image = $tiding->image;
			}
			
			if ($request->has('image')) {
				$image = 'storage/'.Storage::putFileAs('uploads/'.date('y/m/d/h/i/s'), $request->image, rand(1000, 9999).'.'.$request->image->extension());
			}
			
			$tiding->update([
				'state_id' => $request->state_id,
				'date_of_publication' => $request->date_of_publication,
				'university_id' => $ssc->university->id,
				'image' => $image
			]);

			$tiding->translations()->whereNotIn('lang', $request->langs)->delete();

			foreach ($request->translations as $lang => $translation) {
				if ($tiding->translations()->where('lang', $lang)->first()) {
					$tiding->translations()->where('lang', $lang)->update([
						'heading' => $translation['heading'],
						'discription' => $translation['discription']
					]);
				} else {
					$tiding->translations()->create([
						'lang' => $lang,
						'heading' => $translation['heading'],
						'discription' => $translation['discription']
					]);
				}
			}

			return $tiding;
		});

		return response()->json([
			'code' => 200,
			'data' => new TidingResource($data)
		], 200);
    }
	
    public function destroy(Tiding $tiding)
	{
		$tiding->delete();
		
		return response()->json([
			'code' => 204
		], 200);
	}
}
