<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\NotificationViewUserRoleResource;

use App\Models\NotificationViewUserRole;
use App\Models\NotificationView;

use Validator;

class UserMeNotificationViewController extends Controller
{
	public function index()
	{
		$nofitication_views = NotificationViewUserRole::query()
			->where('user_id', auth()->user()->id)
			->paginate();

		return NotificationViewUserRoleResource::collection($nofitication_views);
	}

	public function show(NotificationView $notification_view)
	{
		$nofitication_view = NotificationViewUserRole::query()
			->where('user_id', auth()->user()->id)
			->where('view_id', $notification_view->id)
			->first();

		return new NotificationViewUserRoleResource($nofitication_view);
	} 

	public function update(NotificationView $notification_view, Request $request)
	{
		$validator = Validator::make($request->all(), [
            'state' => 'required|in:0,1'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }

        NotificationViewUserRole::query()
        	->where('user_id', auth()->user()->id)
        	->where('view_id', $notification_view->id)
        	->update([
        		'state' => $request->state
        	]);

        return response()->json([
        	'code' => 200
        ]);
	} 
}
