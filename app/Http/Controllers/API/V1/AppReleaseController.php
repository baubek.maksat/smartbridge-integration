<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\AppReleaseResource;

use Illuminate\Validation\Rule;

use App\Models\AppRelease;

use Validator;

class AppReleaseController extends Controller
{
	public function index(Request $request)
	{
		$app_releases = AppRelease::query();

		if ($request->has('filter.id') && mb_substr($request->filter['id'], 0, 2) == '..') {
			$app_releases->where('id', '>', (int)str_replace('..', '', $request->filter['id']));
		}

		if ($request->has('filter.link') && $request->filter['link'] == 'not-null') {
			$app_releases->whereNotNull('link');
		}

		if ($request->has('filter.version')) {
			$app_releases->where('version', $request->filter['version']);
		}

		if ($request->has('filter.university_id') && $request->filter['university_id'] == 'null') {
			$app_releases->whereNull('university_id');
		}

		if ($request->has('filter.state_id')) {
			$app_releases->where('state_id', $request->filter['state_id']);
		}

		if ($request->has('filter.platform')) {
			$app_releases->whereHas('platform', function($query) use ($request) {
				$query->where('slug', $request->filter['platform']);
			});
		}

		$app_releases = $app_releases->paginate();

		return AppReleaseResource::collection($app_releases);
	}	

	public function show(Request $request, AppRelease $app_release)
	{
		return new AppReleaseResource($app_release);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'nullable|exists:universities,id',
			'platform_id' => 'required|exists:platforms,id',
			'state_id' => 'required|exists:states,id',
			'link' => 'nullable|active_url|max:5000',
			'name' => 'required|string|max:255',
			'version' => [
				'required',
				'string',
				'max:255',
				Rule::unique('app_releases')->where(function ($query) use ($request) {
					if ($request->has('university_id')) {
						return $query
							->where('university_id', $request->university_id)
							->where('platform_id', $request->platform_id)
							->where('name', $request->name);
					} else {
						return $query
							->where('platform_id', $request->platform_id)
							->where('name', $request->name);
						}
				})
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages()
			], 200);
		}
		
		$app_release = AppRelease::create([
			'university_id' => $request->has('university_id') ? $request->university_id : null,
			'platform_id' => $request->platform_id,
			'state_id' => $request->state_id,
			'version' => $request->version,
			'name' => $request->name,
			'link' => $request->has('link') ? $request->link : null
		]);
		
		return response()->json([
			'code' => 201,
			'data' => new AppReleaseResource($app_release)
		], 200);
	}

	public function update(Request $request, AppRelease $app_release)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'nullable|exists:universities,id',
			'platform_id' => 'required|exists:platforms,id',
			'state_id' => 'required|exists:states,id',
			'link' => 'nullable|active_url|max:5000',
			'name' => 'required|string|max:255',
			'version' => [
				'required',
				'string',
				'max:255',
				Rule::unique('app_releases')->where(function ($query) use ($request) {
					if ($request->has('university_id')) {
						return $query
							->where('university_id', $request->university_id)
							->where('platform_id', $request->platform_id)
							->where('name', $request->name);
					} else {
						return $query
							->where('platform_id', $request->platform_id)
							->where('name', $request->name);
						}
				})->ignore($app_release->id)
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages()
			], 200);
		}
		
		$app_release->update([
			'university_id' => $request->has('university_id') ? $request->university_id : null,
			'platform_id' => $request->platform_id,
			'state_id' => $request->state_id,
			'version' => $request->version,
			'name' => $request->name,
			'link' => $request->has('link') ? $request->link : null
		]);
		
		return response()->json([
			'code' => 200,
			'data' => new AppReleaseResource($app_release)
		], 200);
	}
}
