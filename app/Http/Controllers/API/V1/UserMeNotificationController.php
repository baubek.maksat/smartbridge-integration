<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\NotificationResource;

use App\Models\Notification;

class UserMeNotificationController extends Controller
{
    public function index(Request $request)
    {
		$nofitications = Notification::query()
			->has('translation')
			->whereHas('recipients', function($query) use ($request) {
				if ($request->has('filter') && $request->filter['recipient.reviewed_at'] == 'null') {
					$query->whereNull('reviewed_at');
				}
				
				if ($request->has('filter') && $request->filter['recipient.reviewed_at'] != 'null') {
					$query->whereNotNull('reviewed_at');
				}
				
    			$query->where('user_id', auth()->user()->id);
				$query->where('role_id', auth()->user()->currentRole()->id);
    		});

    	if ($request->has('order_by') && $request->order_by['id'] == 'asc') {
			$nofitications->orderBy('id', 'asc');
		}

		if ($request->has('order_by') && $request->order_by['id'] == 'desc') {
			$nofitications->orderBy('id', 'desc');
		}

    	$nofitications = $nofitications->paginate();
		
		return NotificationResource::collection($nofitications);
    }
	
	public function update(Request $request, Notification $notification) 
	{
		$notification->recipient()->update([
			'reviewed_at' => date('Y-m-d H:i:s')
		]);
		
		return $notification->recipient->reviewed_at;
	}
}
