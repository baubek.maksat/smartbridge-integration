<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\StudentCardResource;

use App\Models\StudentCard;
use App\Models\Country;
use App\Models\User;
use App\Models\Role;

use Validator;

class StudentCardController extends Controller
{
	public function index(Request $request)
	{
		$student_cards = StudentCard::query();

		if ($request->has('filter.faculty_id')) {
			$student_cards->whereHas('group.speciality.department.faculty', function($query) use ($request) {
				$query->where('id', $request->filter['faculty_id']);
			});
		}

		if ($request->has('filter.department_id')) {
			$student_cards->whereHas('group.speciality.department', function($query) use ($request) {
				$query->where('id', $request->filter['department_id']);
			});
		}

		if ($request->has('filter.speciality_id')) {
			$student_cards->whereHas('group.speciality.department', function($query) use ($request) {
				$query->where('id', $request->filter['speciality_id']);
			});
		}

		if ($request->has('filter.payment_form_id')) {
			$student_cards->where('payment_form_id', $request->filter['payment_form_id']);
		}

		if ($request->has('filter.course_number')) {
			$student_cards->where('course_number', $request->filter['course_number']);
		}

		if ($request->has('filter.study_language_id')) {
			$student_cards->where('study_language_id', $request->filter['study_language_id']);
		}

		if ($request->has('filter.study_form_id')) {
			$student_cards->where('study_form_id', $request->filter['study_form_id']);
		}

		if ($request->has('filter.academic_degree_id')) {
			$student_cards->whereHas('studyForm.academicDegree', function($query) use ($request) {
				$query->where('id', $request->filter['academic_degree_id']);
			});
		}

		if ($request->has('filter.surname')) {
			$student_cards->whereHas('user', function($query) use ($request) {
				$query->where('surname', 'LIKE', $request->filter['surname']);
			});
		}

		if ($request->has('filter.name')) {
			$student_cards->whereHas('user', function($query) use ($request) {
				$query->where('name', 'LIKE', $request->filter['name']);
			});
		}

		if ($request->has('filter.patronymic')) {
			$student_cards->whereHas('user', function($query) use ($request) {
				$query->where('patronymic', 'LIKE', $request->filter['patronymic']);
			});
		}

		$student_cards = $student_cards->paginate();

		return StudentCardResource::collection($student_cards);
	}

	public function show(StudentCard $student_card)
	{
		return new StudentCardResource($student_card);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'user_id' => [
				'nullable',
				'exists:users,id'
			],
			'surname' => [
				'required',
				'string',
				'max:255'
			],
			'name' => [
				'required',
				'string',
				'max:255'
			],
			'patronymic' => [
				'nullable',
				'string',
				'max:255'
			],
			'surname_en' => [
				'required',
				'string',
				'max:255'
			],
			'name_en' => [
				'required',
				'string',
				'max:255'
			],
			'patronymic_en' => [
				'nullable',
				'string',
				'max:255'
			],
			'date_of_birth' => [
				'required',
				'date',
				'date_format:Y-m-d'
			],
			'nationality_id' => [
				'nullable',
				'exists:nationalities,id'
			],
			'gender_id' => [
				'required',
				'exists:genders,id'
			],
			'marital_status_id' => [
				'nullable',
				'exists:marital_statuses,id'
			],
			'citizenship_id' => [
				'required',
				'exists:countries,id'
			],
			'tin' => [
				'required',
				'string',
				'max:255'
			],
			'arrived_country_id' => [
				'nullable',
				'exists:countries,id'
			],
			'arrived_cato_id' => [
				'nullable',
				'exists:catos,id'
			],
			'arrived_cato_name' => [
				'nullable',
				'string',
				'max:255'
			],
			'birth_country_id' => [
				'nullable',
				'exists:countries,id'
			],
			'birth_cato_id' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->birth_country_id)
						->where('code', 'KZ')
						->first();
				}),
				'exists:catos,id'
			],
			'birth_cato_name' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->birth_country_id)
						->where('code', '<>', 'KZ')
						->first();
				}),
				'string',
				'max:255'
			],
			'registration_cato_id' => [
				'nullable',
				'exists:catos,id'
			],
			'registration_address' => [
				'nullable',
				'exists:catos,id'
			],
			'living_cato_id' => [
				'nullable',
				'exists:catos,id'
			],
			'living_address' => [
				'nullable',
				'exists:catos,id'
			],
			'role_id' => [
				'required',
				'integer',
				'exists:roles,id',
				Rule::in(Role::whereIn('slug', ['student'])->get()->pluck('id')),
			],
			'image' => [
				'required',
				'image'				
			],
			'marital_status_id' => [
				'nullable',
				'exists:marital_statuses,id'
			],
			'id_type_id' => [
				'required',
				'exists:id_types,id'
			],
			'citizenship_id' => [
				'required',
				'exists:countries,id'
			],
			'id_issuing_authority_id' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->citizenship_id)
						->where('code', 'KZ')
						->first();
				}),
				'exists:id_issuing_authorities,id'
			],
			'id_number' => [
				'required',
				'string',
				'max:255'
			],
			'id_series' => [
				'required',
				'string',
				'max:255'
			],
			'id_src' => [
				'required',
				'file'
			],
			'id_date_start' => [
				'required',
				'date_format:Y-m-d'
			],
			'id_date_end' => [
				'nullable',
				'date_format:Y-m-d'
			],
			'phone' => [
				'required',
				'string',
				'max:11'
			],
			'birth_country_id' => [
				'required',
				'exists:countries,id'
			],
			'birth_cato_id' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->birth_country_id)
						->where('code', 'KZ')
						->first();
				}),
				'exists:catos,id'
			],
			'birth_cato_name' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->birth_country_id)
						->where('code', '<>', 'KZ')
						->first();
				}),
				'string',
				'max:255'
			],
			'arrived_country_id' => [
				'required',
				'exists:countries,id'
			],
			'arrived_cato_id' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->arrived_country_id)
						->where('code', 'KZ')
						->first();
				}),
				'exists:catos,id'
			],
			'arrived_cato_name' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->arrived_country_id)
						->where('code', '<>', 'KZ')
						->first();
				}),
				'string',
				'max:255'
			],
			'study_form_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:study_forms,id'
			],
			'study_language_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:study_languages,id'
			],
			'payment_form_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:payment_forms,id'
			],
			'group_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:groups,id'
			],
			'course_number' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'integer',
				'min:1',
				'max:9'
			],
			'grant_type_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:grant_types,id'
			],
			'date_start' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->whereIn('slug', ['student'])
						->first();
				}),
				'date_format:Y-m-d'
			]
		]);

		if ($validator->fails()) {
            return response()->json([
				'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }

        DB::transaction(function() use ($request){
        	if ($request->has('user_id')) {
        		$user = User::query()
        			->where('id', $request->user_id)
        			->first();
        	} else {
        		$user = User::create([
        			'surname' => $request->surname,
        			'name' => $request->name,
        			'patronymic' => $request->has('patronymic') ? $request->patronymic : null,
        			'surname_en' => $request->has('surname_en') ? $request->surname_en : null,
        			'name_en' => $request->has('name_en') ? $request->name_en : null,
        			'patronymic_en' => $request->has('patronymic_en') ? $request->patronymic_en : null,
        		]);

        		$id
        	}

        	$user->student->cards()->create([
        		'date_start' => $request->date_start,
        		'grant_type_id' => $request->grant_type_id,
        		'course_number' => $request->course_number,
        		'group_id' => $request->group_id,
        		'payment_form_id' => $request->payment_form_id,
        		'study_form_id' => $request->study_form_id,
        		'study_language_id' => $request->study_language_id										
        	]);
        });

        dd($request->all());
	}
}
