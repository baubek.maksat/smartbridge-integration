<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\TypeResource;

use App\Models\Type;

class TypeController extends Controller
{
	public function index(Request $request)
	{
		$types = Type::query();

		$types = $types->paginate();

		return TypeResource::collection($types);
	}
}
