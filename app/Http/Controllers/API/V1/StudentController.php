<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\Student;
use App\Models\User;

use Validator;

class StudentController extends Controller
{
	public function index(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'per_page' => 'integer|between:1,50'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$per_page = $request->has('per_page') ? $request->per_page : null;

		$students = Student::query()
			->with([
				'user',
				'card'
			])
			->paginate($per_page)
			->appends([
				'paginate' => $per_page
			]);

		return response()->json($students, 200);
	}

	public function show(Request $request, User $user)
	{
		$student = Student::query()
			->with([
				'user',
				'card'
			])
			->where([
				'user_id' => $user->id
			])
			->first();

		return response()->json($student, 200);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		dd($request->all());
	}
}
