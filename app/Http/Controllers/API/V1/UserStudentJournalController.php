<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\JournalResource;

use Illuminate\Validation\Rule;

use App\Models\StudyGroup;
use App\Models\Journal;

use Validator;
use DB;
use UA;

class UserStudentJournalController extends Controller
{
    public function index(Request $request)
    {
		$study_groups = StudyGroup::query()
			->whereHas('students', function($query) use ($request) {
				$query->where('id', auth()->user()->student->card->id);
			})
			->with(['journal' => function($query) use ($request) {
				$query->where('student_id', auth()->user()->student->card->id);
			}]);
		
		if ($request->has('filter.year')) {
			$study_groups->where('year', $request->filter['year']);
		} else {
			$study_groups->where('year', 2021);
		}
		
		if ($request->has('filter.term')) {
			$study_groups->where('term', $request->filter['term']);
		} else {
			$study_groups->where('term', 1);
		}
		
		$study_groups = $study_groups->paginate();
		
		return JournalResource::collection($study_groups)->additional([
			'meta' => [
				'filter' => [
					'year' => $request->has('filter.year') ? $request->filter['year'] : 2021,
					'term' => $request->has('filter.term') ? $request->filter['term'] : 1,
				]
			]
		]);
    }
}
