<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\AppResource;

use Illuminate\Validation\Rule;

use App\Models\User;
use App\Models\App;

use Validator;

class AppController extends Controller
{
	public function index(Request $request)
    {
    	$apps = App::query();

    	$apps = $apps->paginate();

    	return AppResource::collection($apps);
    }

    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'registration_id' => [
				'required',
				'string',
				'max:500'/*,
				Rule::unique('apps')->where(function ($query) use ($request) {
					if ($request->has('version')) {
						return $query->where('version', $request->version);
					}
				})
				*/
			],
			'release_id' => 'required|exists:app_releases,id',
			'device_id' => 'required|exists:devices,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$app = App::query()
			->where('registration_id', $request->registration_id)
			->where('release_id', $request->release_id)
			->where('device_id', $request->device_id)
			->first();
		
		if (!$app) {
			$app = App::create([
				'registration_id' => $request->registration_id,
				'release_id' => $request->release_id,
				'device_id' => $request->device_id
			]);
		}
		
		return response()->json([
            'code' => 201,
			'data' => $app
        ], 200);
    }
}
