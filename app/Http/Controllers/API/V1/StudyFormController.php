<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StudyFormResource;

use App\Models\StudyForm;

use Validator;
use DB;

class StudyFormController extends Controller
{
    public function index(Request $request)
    {
    	$study_forms = StudyForm::query();

        if ($request->has('filter.academic_degree_id')) {
            $study_forms->where('academic_degree_id', $request->filter['academic_degree_id']);
        }

        $study_forms = $study_forms->paginate();

        return StudyFormResource::collection($study_forms);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'academic_degree_id' => 'required|integer|exists:academic_degrees,id',
            'type_id' => 'required|exists:study_form_types,id', 
            'course_count' => 'required|integer',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $data = DB::transaction(function() use ($request) {
	        $study_form = StudyForm::create([
	        	'academic_degree_id' => $request->academic_degree_id,
                'type_id' => $request->id,
	        	'course_count' => $request->course_count
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                    $study_form->translations()->create([
                        'lang' => $key,
                        'name' => $value['name']
                    ]);
                }
            }

            return [
                'study_form' => $study_form
            ];
	    });

        return response()->json([
            'code' => 201,
            'data' => $data['study_form']
        ], 201);
    }

    public function show(StudyForm $study_form)
    {
    	$data = StudyForm::query()
            ->with('translation')
            ->with('translations')
            ->with('academicDegree.translation')
            ->with('academicDegree.translations')
            ->where('id', $study_form->id)
            ->first();

        return response()->json([
            'data' => $data
        ], 201);
    }

    public function update(Request $request, StudyForm $study_form)
    {
        $validator = Validator::make($request->all(), [
            'academic_degree_id' => 'required|integer|exists:academic_degrees,id',
            'course_count' => 'required|integer',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request, $study_form) {
	        $study_form->update([
	        	'university_id' => $request->university_id
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                	$study_form->translations()->where('lang', $key)->update([
                        'name' => $value['name']
                    ]);
                }
            }
	    });
        
    	return response()->json([
            'messages' => 'Ok'
        ], 200);
    }

    public function destroy(StudyForm $study_form)
    {
        $study_form->delete();
        
        return response()->json([
            'code' => 204
        ], 200);
    }
}
