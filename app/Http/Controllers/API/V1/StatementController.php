<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Validation\Rule;

use App\Http\Resources\StudentServiceCenterOperatingMode;
use App\Http\Resources\StatementResource;

use App\Events\StatementCompleted;
use App\Events\StatementCreated;

use App\Models\StudentServiceCenterUser;
use App\Models\StudentServiceCenter;
use App\Models\StatementComponent;
use App\Models\StudyLanguageLang;
use App\Models\StatementStudent;
use App\Models\StatementStatus;
use App\Models\StatementDetail;
use App\Models\StatementStage;
use App\Models\SpecialityLang;
use App\Models\StatementView;
use App\Models\StudyLanguage;
use App\Models\VariableWord;
use App\Models\SubjectLang;
use App\Models\StudentCard;
use App\Models\Speciality;
use App\Models\Statement;
use App\Models\UserLang;
use App\Models\Subject;
use App\Models\User;
use App\Models\Lang;

use App\Jobs\ConvertStatementDocument;

use \ConvertApi\ConvertApi;
use Carbon\Carbon;
use Validator;
use QrCode;
use PDF;
use DB;

class StatementController extends Controller
{
	public function getMonth($lang, $m) {
		if ($m == '01' && $lang == 'kz') return 'қаңтар'; 
		if ($m == '02' && $lang == 'kz') return 'ақпан'; 
		if ($m == '03' && $lang == 'kz') return 'наурыз'; 
		if ($m == '04' && $lang == 'kz') return 'сәуір'; 
		if ($m == '05' && $lang == 'kz') return 'мамыр'; 
		if ($m == '06' && $lang == 'kz') return 'маусым'; 
		if ($m == '07' && $lang == 'kz') return 'шілде'; 
		if ($m == '08' && $lang == 'kz') return 'тамыз'; 
		if ($m == '09' && $lang == 'kz') return 'қыркүйек'; 
		if ($m == '10' && $lang == 'kz') return 'қазан'; 
		if ($m == '11' && $lang == 'kz') return 'қараша'; 
		if ($m == '12' && $lang == 'kz') return 'желтоқсан'; 

		if ($m == '01' && $lang == 'ru') return 'января'; 
		if ($m == '02' && $lang == 'ru') return 'февраля'; 
		if ($m == '03' && $lang == 'ru') return 'марта'; 
		if ($m == '04' && $lang == 'ru') return 'апреля'; 
		if ($m == '05' && $lang == 'ru') return 'мая'; 
		if ($m == '06' && $lang == 'ru') return 'июня'; 
		if ($m == '07' && $lang == 'ru') return 'июля'; 
		if ($m == '08' && $lang == 'ru') return 'августа'; 
		if ($m == '09' && $lang == 'ru') return 'сентября'; 
		if ($m == '10' && $lang == 'ru') return 'октября'; 
		if ($m == '11' && $lang == 'ru') return 'ноября'; 
		if ($m == '12' && $lang == 'ru') return 'декабря'; 

		if ($m == '01' && $lang == 'en') return 'january'; 
		if ($m == '02' && $lang == 'en') return 'february'; 
		if ($m == '03' && $lang == 'en') return 'march'; 
		if ($m == '04' && $lang == 'en') return 'april'; 
		if ($m == '05' && $lang == 'en') return 'may'; 
		if ($m == '06' && $lang == 'en') return 'june'; 
		if ($m == '07' && $lang == 'en') return 'july'; 
		if ($m == '08' && $lang == 'en') return 'august'; 
		if ($m == '09' && $lang == 'en') return 'september'; 
		if ($m == '10' && $lang == 'en') return 'october'; 
		if ($m == '11' && $lang == 'en') return 'november'; 
		if ($m == '12' && $lang == 'en') return 'december'; 
	}

	public function addWorkDay($schedule, $date, $count)
	{
		$dates = collect();
			
		$i = 1;
			
		while (true) {
			$new_date = Carbon::parse($date)->addDay($i);
			
			if (!$schedule->where('week_id', $new_date->format('N'))->where('time_start', '00:00:00')->where('time_end', '00:00:00')->first()) {
				$dates->push($new_date->format('Y-m-d H:i:s'));
			}
				
			if ($dates->count() == $count) break;
				
			$i++;
		}
			
		return $dates->last();
	}

	function addWorkHour($schedule, $datetime, $minutes)
	{
		$datetime = Carbon::parse($datetime);

		$i = 0;

		while (true) {
			if (!$schedule->where('week_id', $datetime->format('N'))->where('time_start', '00:00:00')->where('time_end', '00:00:00')->first()) {
				$new_datetime = Carbon::parse($datetime)->addMinutes($minutes);

				if ($new_datetime->format('Y-m-d H:i:s') <= $datetime->format('Y-m-d').' '.$schedule->where('week_id', $datetime->format('N'))->first()->time_end) {
					return $new_datetime->format('Y-m-d H:i:s');
				} else {
					$minutes -= Carbon::parse($datetime)->diffInMinutes($datetime->format('Y-m-d').' '.$schedule->where('week_id', $datetime->format('N'))->first()->time_end, false);

					$datetime = Carbon::parse($datetime->format('Y-m-d').' '.$schedule->where('week_id', $datetime->format('N'))->first()->time_start);
				}

				$i++;
			}

			$datetime->addDay();
			
			if ($i == 100) break;
		}
	}

	public function index(Request $request)
	{
		$statements = Statement::query();
		
		//	Filter
		
		$statements->whereHas('view', function($query) use ($request) {
			if ($request->has('filter.category_id')) {
				$query->where('category_id', $request->filter['category_id']);
			}
		});
	
		if ($request->has('filter.status_id')) {
			$statements->where('status_id', $request->filter['status_id']);
		}

		if ($request->has('filter.view_id')) {
			$statements->where('view_id', $request->filter['view_id']);
		}

		if ($request->has('filter.responsible_role_id')) {
			$statements->where('responsible_role_id', $request->filter['responsible_role_id']);
		}

		if ($request->has('filter.responsible_user_id')) {
			$statements->where('responsible_user_id', $request->filter['responsible_user_id']);
		}
		
		$statements->whereHas('status', function($query) use ($request) {
			if ($request->has('filter.status')) {
				$query->whereIn('slug', explode(',', $request->filter['status']));
			}
		});

		$statements->whereHas('student.user', function($query) use ($request) {
			if ($request->has('filter.student')) {
				$query->where(DB::raw("CONCAT(`surname`, ' ', `name`, ' ', `patronymic`)"), 'LIKE', '%'.$request->filter['student'].'%');
			}
		});
		
		$statements->whereHas('view.translation', function ($query) use ($request) {
			if (isset($request->filter['view.name'])) {
				$query->where('name', 'LIKE', $request->filter['view.name']);
			}
		});

		if ($request->has('filter.expired') && $request->filter['expired'] == true) {
			$statements->where('expired_at', '<', date('Y-m-d H:i:s'));
		}
		
		//	Order by
		
		if ($request->has('orderBy.id')) {
			$statements->orderBy('id', 'desc');
		}
		
		//	Role
		
		if (auth()->user()->currentRole()->slug == 'student') {
			//	$statements->where('student_id', auth()->user()->student->card->id);

			$statements->whereHas('student', function($query) {
				$query->where('id', auth()->user()->student->card->id);
			});
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$statements->whereHas('view.category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
			
			//	$statements->where('responsible_role_id', auth()->user()->studentServiceCenterRoles[0]->id);
			
			$statements->where(function($query) {
				$query->where('responsible_role_id', auth()->user()->studentServiceCenterRoles[0]->id);
				$query->orWhereHas('stages.routes', function ($query) {
					$query->where('responsible_role_id', auth()->user()->studentServiceCenterRoles[0]->id);
				});
			});
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$statements->whereHas('view.category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$statements->whereHas('view.category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
		}
		
		//

		$statements = $statements->paginate();

		return StatementResource::collection($statements);
	}

	public function show(Request $request, Statement $statement)
	{
		return new StatementResource($statement);
	}

	public function store(Request $request)
	{
		$student_service_center = auth()->user()->student->card->group->speciality->department->faculty->university->studentServiceCenter;
		
		$current_datetime = Carbon::now();
		
		$current_operating_mode = [
			'time_start' => $student_service_center->currentOperatingMode->time_start,
			'time_end' => $student_service_center->currentOperatingMode->time_end
		];
		
		if ($current_operating_mode['time_start'] == '00:00:00' && $current_operating_mode['time_end'] == '00:00:00' && $student_service_center->accepting_applications_24_7 == 0) {
			return response()->json([
				'code' => 403,
				'data' => StudentServiceCenterOperatingMode::collection($student_service_center->operating_modes)
			], 200);
		}
		
		if ($current_operating_mode['time_start'] == '00:00:00' && $current_operating_mode['time_end'] == '00:00:00' && $student_service_center->accepting_applications_24_7 == 1) {
			$statement_status = StatementStatus::where('slug', 'waiting')->first();
		}
		
		if ((($current_operating_mode['time_start'] != '00:00:00' || $current_operating_mode['time_end'] != '00:00:00') && $student_service_center->accepting_applications_24_7 == 0) && !($current_operating_mode['time_start'] <= $current_datetime->format('H:i:s') && $current_operating_mode['time_end'] >= $current_datetime->format('H:i:s'))) {
			return response()->json([
				'code' => 403,
				'data' => StudentServiceCenterOperatingMode::collection($student_service_center->operating_modes)
			], 200);
		}
		
		if ((($current_operating_mode['time_start'] != '00:00:00' || $current_operating_mode['time_end'] != '00:00:00') && $student_service_center->accepting_applications_24_7 == 1) && !($current_operating_mode['time_start'] <= $current_datetime->format('H:i:s') && $current_operating_mode['time_end'] >= $current_datetime->format('H:i:s'))) {
			$statement_status = StatementStatus::where('slug', 'waiting')->first();
		}
		
		if (($current_operating_mode['time_start'] != '00:00:00' || $current_operating_mode['time_end'] != '00:00:00') && ($current_operating_mode['time_start'] <= $current_datetime->format('H:i:s') && $current_operating_mode['time_end'] >= $current_datetime->format('H:i:s'))) {
			$statement_status = StatementStatus::where('slug', 'processing')->first();
		}
		
		$statement_components = StatementComponent::query()
			->where('view_id', $request->view_id)
			->get();

		$rules = [
			'view_id' => 'required|integer|exists:statement_views,id'
		];

		foreach ($statement_components as $statement_component) {
			$rules['details.sc'.$statement_component->id] = implode('|', $statement_component->rules);

			if ($statement_component->type->slug == 'input-files') {
				if (collect($statement_component->rules)->search('required') > -1) {
					$rules['details.sc'.$statement_component->id.'.*'] = 'required|file';
				} else {
					$rules['details.sc'.$statement_component->id.'.*'] = 'nullable|file';
				}
			}

			if ($statement_component->type->slug == 'select-multiple') {
				$rules['details.sc'.$statement_component->id.'.*'] = 'required';
			}
		}

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		$statement = DB::transaction(function() use ($request, $statement_status, $current_datetime) {
			$datetime_now = Carbon::now();

			$role = auth()->user()->currentRole();

			$student = StudentCard::query()
				->where('id', auth()->user()->student->card->id)
				->first();
			
			$statement_view = StatementView::query()
				->where('id', $request->view_id)
				->first();

			//	QR render start

			$directory = 'uploads/'.$datetime_now->format('Y/m/d/H/i/s');

			if (!Storage::exists($directory)) {
				Storage::makeDirectory($directory);
			}

			$pieces = [
				$student->id,
				$statement_view->id,
				$datetime_now->format('Y-m-d H:i:s')
			];

			$hash = sha1(implode('.', $pieces));

			$qr = 'storage/'.$directory.'/'.$hash.'.svg';

			QrCode::encoding('UTF-8')->size(800)->generate(env('APP_FRONT_URL', NULL).'/statement-check?hash='.$hash, $qr);

			//	QR render end

			//  PDF render start
			
			if ($statement_view->template) {
				$document = new \PhpOffice\PhpWord\TemplateProcessor($statement_view->template);
				
				//
				
				if ($request->has('details')) {
					foreach ($request->details as $component_id => $component_value) {
						$component = StatementComponent::query()
							->where('id', (int)str_replace('sc', '', $component_id))
							->first();
						
						if ($component && $component->type->slug == 'input-text') {
							$variable_words = VariableWord::query()
								->where('title', $component_value)
								->where('university_id', $student->group->speciality->department->faculty->university->id)
								->get();

							if ($variable_words) {
								foreach ($variable_words as $variable_word) {
									$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
								}
							}
							
							$document->setValue($component->name, $component_value);
						}
						
						if ($component && $component->type->slug == 'input-number') {
							$variable_words = VariableWord::query()
								->where('title', $component_value)
								->where('university_id', $student->group->speciality->department->faculty->university->id)
								->get();

							if ($variable_words) {
								foreach ($variable_words as $variable_word) {
									$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
								}
							}
							
							$document->setValue($component->name, $component_value);
						}
						
						if ($component && $component->type->slug == 'textarea') {
							$variable_words = VariableWord::query()
								->where('title', $component_value)
								->where('university_id', $student->group->speciality->department->faculty->university->id)
								->get();

							if ($variable_words) {
								foreach ($variable_words as $variable_word) {
									$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
								}
							}
							
							$document->setValue($component->name, $component_value);
						}
						
						if ($component && $component->type->slug == 'select') {
							$variable_words = isset($component_value) ? VariableWord::query()
								->where('title', $component->data[$component_value])
								->where('university_id', $student->group->speciality->department->faculty->university->id)
								->get() : null;

							if ($variable_words) {
								foreach ($variable_words as $variable_word) {
									$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									//	$document->setValue($component->data[$component_value].' '.$variable_word->key, $variable_word->value);
								}
							}
							
							$document->setValue($component->name, isset($component_value) ? $component->data[$component_value] : null);
						}
						
						if ($component && $component->type->slug == 'select-multiple') {
							$selects = [];

							if (count($component_value) > 0) {
								foreach ($component_value as $value) {
									if (is_null($value)) unset($value);
								}
							}

							if (count($component_value) > 0) {
								foreach ($component_value as $value) {
									if (isset($value)) {
										$selects[] = $component->data[$value];

										$variable_words = VariableWord::query()
											->where('title', $component->data[$value])
											->where('university_id', $student->group->speciality->department->faculty->university->id)
											->get();

										if ($variable_words) {
											foreach ($variable_words as $variable_word) {
												//	$document->setValue($component->data[$value].' '.$variable_word->key, $variable_word->value);
												$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
											}
										}
									}
								}
							}

							$document->setValue($component->name, implode(', ', $selects));
						}
						
						if ($component && $component->type->slug == 'input-date') {
							$variable_words = VariableWord::query()
								->where('title', $component_value)
								->where('university_id', $student->group->speciality->department->faculty->university->id)
								->get();

							if ($variable_words) {
								foreach ($variable_words as $variable_word) {
									//	$document->setValue($component_value.' '.$variable_word->key, $variable_word->value);

									$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
								}
							}

							$document->setValue($component->name, $component_value);

							$document->setValue($component->name.'.format(«d» F Y)', Carbon::parse($component_value)->format('«d»').' '.$this->getMonth($student->user->defaultLang->slug, Carbon::parse($component_value)->format('m')).' '.Carbon::parse($component_value)->format('Y'));
						}
						
						if ($component && $component->type->slug == 'input-time') {
							$variable_words = VariableWord::query()
								->where('title', $component_value)
								->where('university_id', $student->group->speciality->department->faculty->university->id)
								->get();

							if ($variable_words) {
								foreach ($variable_words as $variable_word) {
									//	$document->setValue($component_value.' '.$variable_word->key, $variable_word->value);
									$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
								}
							}

							$document->setValue($component->name, $component_value);
						}
						
						if ($component && $component->type->slug == 'resource') {
							if ($component->resource == 'student-subjects') {
								$resource = $component_value ? Subject::query()
									->where('id', $component_value)
									->first() : null;

								$document->setValue($component->name, $component_value ? $resource->name : null);
								
								$variable_words = $component_value ? VariableWord::query()
									->where('title', $resource->name)
									->where('university_id', $student->group->speciality->department->faculty->university->id)
									->get() : null;

								if ($variable_words) {
									foreach ($variable_words as $variable_word) {
										$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									}
								}
							}
							
							if ($component->resource == 'specialties') {
								$resource = $component_value ? Speciality::query()
									->where('id', $component_value)
									->first() : null;

								$pieces = [];

								if ($resource && $resource->code) $pieces[] = $resource->code;
								if ($resource && $resource->name) $pieces[] = $resource->name;
								
								$document->setValue($component->name, $component_value ? implode(' ', $pieces) : null);
								
								$variable_words = $component_value ? VariableWord::query()
									->where('title', $resource->name)
									->where('university_id', $student->group->speciality->department->faculty->university->id)
									->get() : null;

								if ($variable_words) {
									foreach ($variable_words as $variable_word) {
										$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									}
								}
							}
							
							if ($component->resource == 'study-languages') {
								$resource = $component_value ? StudyLanguage::query()
									->where('id', $component_value)
									->first() : null;
								
								$document->setValue($component->name, $component_value ? $resource->name : null);
								
								$variable_words = $component_value ? VariableWord::query()
									->where('title', $resource->name)
									->where('university_id', $student->group->speciality->department->faculty->university->id)
									->get() : null;

								if ($variable_words) {
									foreach ($variable_words as $variable_word) {
										$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									}
								}
							}
							
							if ($component->resource == 'student-teachers') {
								$resource = $component_value ? User::query()
									->where('id', $component_value)
									->first() : null;

								$pieces = [];

								if ($resource && $resource->surname) $pieces[] = $resource->surname;
								if ($resource && $resource->name) $pieces[] = $resource->name;
								if ($resource && $resource->patronymic) $pieces[] = $resource->patronymic;
								
								$document->setValue($component->name, $component_value ? implode(' ', $pieces) : null);
								
								$variable_words = $component_value ? VariableWord::query()
									->where('title', $resource->name)
									->where('university_id', $student->group->speciality->department->faculty->university->id)
									->get() : null;

								if ($variable_words) {
									foreach ($variable_words as $variable_word) {
										$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									}
								}
							}
						}
						
						if ($component && $component->type->slug == 'resource-multiple') {
							if ($component->resource == 'student-subjects') {
								$resources = Subject::query()
									->whereIn('id', $component_value)
									->get();

								$pieces = [];
								$piece = [];

								if ($resources) {
									foreach ($resources as $key => $value) {
										$piece = [];

										if ($value->name) $piece[$key]['name'] = $value->name;

										$pieces[$key] = implode(' ', $piece[$key]);
									}
								}

								//

								$variable_words = VariableWord::query()
									->whereIn('title', $pieces)
									->where('university_id', $student->group->speciality->department->faculty->university->id)
									->get();

								if ($variable_words) {
									foreach ($variable_words as $variable_word) {
										$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									}
								}

								//
								
								$document->setValue($component->name, implode(', ', $pieces));
							}

							if ($component->resource == 'student-teachers') {
								$resources = User::query()
									->whereIn('id', $component_value)
									->get();

								$pieces = [];
								$piece = [];

								if ($resources) {
									foreach ($resources as $key => $value) {
										$piece = [];

										if ($value->surname) $piece[$key]['surname'] = $value->surname;
										if ($value->name) $piece[$key]['name'] = $value->name;
										if ($value->patronymic) $piece[$key]['patronymic'] = $value->patronymic;

										$pieces[$key] = implode(' ', $piece[$key]);
									}
								}

								//

								$variable_words = VariableWord::query()
									->whereIn('title', $pieces)
									->where('university_id', $student->group->speciality->department->faculty->university->id)
									->get();

								if ($variable_words) {
									foreach ($variable_words as $variable_word) {
										$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									}
								}

								//
								
								$document->setValue($component->name, implode(', ', $pieces));
							}
							
							if ($component->resource == 'specialties') {
								$resources = Speciality::query()
									->whereIn('id', $component_value)
									->get();

								$pieces = [];
								$piece = [];

								if ($resources) {
									foreach ($resources as $key => $value) {
										$piece = [];

										if ($value->code) $piece[$key]['code'] = $value->code;
										if ($value->name) $piece[$key]['name'] = $value->name;

										$pieces[$key] = implode(' ', $piece[$key]);
									}
								}

								//

								$variable_words = VariableWord::query()
									->whereIn('title', $pieces)
									->where('university_id', $student->group->speciality->department->faculty->university->id)
									->get();

								if ($variable_words) {
									foreach ($variable_words as $variable_word) {
										$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									}
								}

								//

								$document->setValue($component->name, implode(', ', $pieces));
							}
							
							if ($component->resource == 'study-languages') {
								$resources = StudyLanguage::query()
									->whereIn('id', $component_value)
									->get();

								$pieces = [];
								$piece = [];

								if ($resources) {
									foreach ($resources as $key => $value) {
										$piece = [];

										if ($value->name) $piece[$key]['name'] = $value->name;

										$pieces[$key] = implode(' ', $piece[$key]);
									}
								}

								//

								$variable_words = VariableWord::query()
									->whereIn('title', $pieces)
									->where('university_id', $student->group->speciality->department->faculty->university->id)
									->get();

								if ($variable_words) {
									foreach ($variable_words as $variable_word) {
										$document->setValue($component->name.' '.$variable_word->key, $variable_word->value);
									}
								}

								//
								
								$document->setValue($component->name, implode(', ', $pieces));
							}
						}
					}
				}

				if ($statement_view->components) {
					foreach ($statement_view->components as $component) {
						$document->setValue($component->name, '');

						//	Заполнение пустых

						$variable_words = VariableWord::query()
							->where('university_id', $student->group->speciality->department->faculty->university->id)
							->get();

						if ($variable_words) {
							foreach ($variable_words as $variable_word) {
								$document->setValue($component->name.' '.$variable_word->key, '');
							}
						}
					}
				}
				
				//	Личные данные студента

				$document->setValue('student.surname', $student->user->surname);
				$document->setValue('student.name', $student->user->name);
				$document->setValue('student.patronymic', $student->user->patronymic);
				$document->setValue('student.surname_en', $student->user->surname_en);
				$document->setValue('student.name_en', $student->user->name_en);
				$document->setValue('student.patronymic_en', $student->user->patronymic_en);
				$document->setValue('student.email', $student->user->email);
				$document->setValue('student.phone', $student->user->phone);
				$document->setValue('student.birth_cato.name', $student->user->birthCato ? $student->user->birthCato->name : null);
				$document->setValue('student.birth_country.name', $student->user->birthCountry->name);
				$document->setValue('student.living_address', $student->user->living_address);
				$document->setValue('student.registration_address', $student->user->registration_address);
				$document->setValue('student.gender.name', $student->user->gender->name);
				$document->setValue('student.marital_status.name', $student->user->maritalStatus ? $student->user->maritalStatus->name : null);
				
				//	Карточка студента
				
				$document->setValue('student.grant_type.name', $student->grantType->name);
				$document->setValue('student.grant_number', $student->grant_number);
				$document->setValue('student.date_start.format(Y)', $student->date_start->format('Y'));
				$document->setValue('student.date_start.format(m)', $student->date_start->format('m'));
				$document->setValue('student.date_start.format(d)', $student->date_start->format('d'));
				$document->setValue('student.date_start.format(F)', $this->getMonth($student->user->defaultLang->slug, $student->date_start->format('m')));
				$document->setValue('student.date_start.format(Y-m-d)', $student->date_start->format('Y-m-d'));
				$document->setValue('student.date_start.format(d.m.Y)', $student->date_start->format('d.m.Y'));
				$document->setValue('student.date_start.format(«d» F Y)', $student->date_start->format('«d»').' '.$this->getMonth($student->user->defaultLang->slug, $student->date_start->format('m')).' '.$student->date_start->format('Y'));
				$document->setValue('student.course_number', $student->course_number);
				$document->setValue('student.study_form.name', $student->studyForm->name);
				$document->setValue('student.payment_form.name', $student->paymentForm->name);
				$document->setValue('student.gpa', $student->gpa);
				$document->setValue('student.transcript_number', $student->transcript_number);
				
				//	Язык обучение
				
				$document->setValue('student.study_language.name', $student->studyLanguage->name);
				
				//	Группа
				
				$document->setValue('student.group.name', $student->group->name);
				
				//	Специальность
				
				$document->setValue('student.speciality.code', $student->group->speciality->code);
				$document->setValue('student.speciality.name', $student->group->speciality->name);
				
				//	Форма обучения
				
				$document->setValue('student.study_form.name', $student->studyForm->name);
				
				//	Научный степень
				
				$document->setValue('student.academic_degree.name', $student->studyForm->academicDegree->name);
				
				//	Кафедра
				
				$document->setValue('student.department.name', $student->group->speciality->department->name);

				//	Заведущий кафедрый

				$document->setValue('student.department.head.surname', $student->group->speciality->department->administrator->surname);
				$document->setValue('student.department.head.name', $student->group->speciality->department->administrator->name);
				$document->setValue('student.department.head.patronymic', $student->group->speciality->department->administrator->patronymic);

				$document->setValue('student.department.head.surname_en', $student->group->speciality->department->administrator->surname_en);
				$document->setValue('student.department.head.name_en', $student->group->speciality->department->administrator->name_en);
				$document->setValue('student.department.head.patronymic_en', $student->group->speciality->department->administrator->patronymic_en);
				
				//	Факультет
				
				$document->setValue('student.faculty.name', $student->group->speciality->department->faculty->name);
				
				//	Декан факультета
				
				$document->setValue('student.faculty.head.surname', $student->group->speciality->department->faculty->administrator->surname);
				$document->setValue('student.faculty.head.name', $student->group->speciality->department->faculty->administrator->name);
				$document->setValue('student.faculty.head.patronymic', $student->group->speciality->department->faculty->administrator->patronymic);

				$document->setValue('student.faculty.head.surname_en', $student->group->speciality->department->faculty->administrator->surname_en);
				$document->setValue('student.faculty.head.name_en', $student->group->speciality->department->faculty->administrator->name_en);
				$document->setValue('student.faculty.head.patronymic_en', $student->group->speciality->department->faculty->administrator->patronymic_en);
				
				//	Университет

				$document->setValue('student.university.name', $student->group->speciality->department->faculty->university->name);

				//	Ректор университета
				
				$document->setValue('student.university.head.surname', $student->group->speciality->department->faculty->university->administrator->surname);
				$document->setValue('student.university.head.name', $student->group->speciality->department->faculty->university->administrator->name);
				$document->setValue('student.university.head.patronymic', $student->group->speciality->department->faculty->university->administrator->patronymic);

				$document->setValue('student.university.head.surname_en', $student->group->speciality->department->faculty->university->administrator->surname_en);
				$document->setValue('student.university.head.name_en', $student->group->speciality->department->faculty->university->administrator->name_en);
				$document->setValue('student.university.head.patronymic_en', $student->group->speciality->department->faculty->university->administrator->patronymic_en);
				
				//	Системные
				
				$document->setValue('created_at.format(Y-m-d)', $current_datetime->format('Y-m-d'));
				$document->setValue('created_at.format(d.m.Y)', $current_datetime->format('d.m.Y'));
				$document->setValue('created_at.format(H:i:s)', $current_datetime->format('H:i:s'));
				$document->setValue('created_at.format(Y)', $current_datetime->format('Y'));
				$document->setValue('created_at.format(m)', $current_datetime->format('m'));
				$document->setValue('created_at.format(F)', $this->getMonth($student->user->defaultLang->slug, $current_datetime->format('m')));
				$document->setValue('created_at.format(d)', $current_datetime->format('d'));
				$document->setValue('created_at.format(H)', $current_datetime->format('H'));
				$document->setValue('created_at.format(i)', $current_datetime->format('i'));
				$document->setValue('created_at.format(s)', $current_datetime->format('s'));

				$document->setValue('created_at.format(«d» F Y)', $current_datetime->format('«d»').' '.$this->getMonth($student->user->defaultLang->slug, $current_datetime->format('m')).' '.$current_datetime->format('Y'));

				//	$document->setValue('statement.id', $statement->id);
				
				//

				//	Замена слов

				//	Личные данные студента

				$variable_words = VariableWord::query()
					->whereIn('title', [
						$student->grantType->name,
						$student->studyLanguage->name,
						$student->studyForm->name,
						$student->paymentForm->name,
						$student->group->speciality->code,
						$student->group->speciality->name,
						$student->studyForm->academicDegree->name,
						$student->group->speciality->department->faculty->university->name,
						$student->group->speciality->department->faculty->university->administrator->surname,
						$student->group->speciality->department->faculty->university->administrator->name,
						$student->group->speciality->department->faculty->university->administrator->patronymic,
						$student->group->speciality->department->faculty->university->administrator->surname_en,
						$student->group->speciality->department->faculty->university->administrator->name_en,
						$student->group->speciality->department->faculty->university->administrator->patronymic_en,
						$student->group->speciality->department->faculty->name,
						$student->group->speciality->department->faculty->administrator->surname,
						$student->group->speciality->department->faculty->administrator->name,
						$student->group->speciality->department->faculty->administrator->patronymic,
						$student->group->speciality->department->faculty->administrator->surname_en,
						$student->group->speciality->department->faculty->administrator->name_en,
						$student->group->speciality->department->faculty->administrator->patronymic_en,
						$student->group->speciality->department->name,
						$student->group->speciality->department->administrator->surname,
						$student->group->speciality->department->administrator->name,
						$student->group->speciality->department->administrator->patronymic,
						$student->group->speciality->department->administrator->surname_en,
						$student->group->speciality->department->administrator->name_en,
						$student->group->speciality->department->administrator->patronymic_en
					])
					->where('university_id', $student->group->speciality->department->faculty->university->id)
					->get();
				
				if ($variable_words) {
					foreach ($variable_words as $variable_word) {
						if ($student->grantType->name == $variable_word->title) $document->setValue('student.grant_type.name '.$variable_word->key, $variable_word->value);
						if ($student->studyLanguage->name == $variable_word->title) $document->setValue('student.study_language.name '.$variable_word->key, $variable_word->value);
						if ($student->studyForm->name == $variable_word->title) $document->setValue('student.study_form.name '.$variable_word->key, $variable_word->value);
						if ($student->paymentForm->name == $variable_word->title) $document->setValue('student.payment_form.name '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->code == $variable_word->title) $document->setValue('student.speciality.code '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->name == $variable_word->title) $document->setValue('student.speciality.name '.$variable_word->key, $variable_word->value);
						if ($student->studyForm->academicDegree->name == $variable_word->title) $document->setValue('student.academic_degree.name '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->university->name == $variable_word->title) $document->setValue('student.university.name '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->university->administrator->surname == $variable_word->title) $document->setValue('student.university.head.surname '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->university->administrator->name == $variable_word->title) $document->setValue('student.university.head.name '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->university->administrator->patronymic == $variable_word->title) $document->setValue('student.university.head.patronymic '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->university->administrator->surname_en == $variable_word->title) $document->setValue('student.university.head.surname_en '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->university->administrator->name_en == $variable_word->title) $document->setValue('student.university.head.name_en '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->university->administrator->patronymic_en == $variable_word->title) $document->setValue('student.university.head.patronymic_en '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->name == $variable_word->title) $document->setValue('student.faculty.name '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->administrator->surname == $variable_word->title) $document->setValue('student.faculty.head.surname '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->administrator->name == $variable_word->title) $document->setValue('student.faculty.head.name '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->administrator->patronymic == $variable_word->title) $document->setValue('student.faculty.head.patronymic '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->administrator->surname_en == $variable_word->title) $document->setValue('student.faculty.head.surname_en '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->administrator->name_en == $variable_word->title) $document->setValue('student.faculty.head.name_en '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->faculty->administrator->patronymic_en == $variable_word->title) $document->setValue('student.faculty.head.patronymic_en '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->name == $variable_word->title) $document->setValue('student.department.name '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->administrator->surname == $variable_word->title) $document->setValue('student.department.head.surname '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->administrator->name == $variable_word->title) $document->setValue('student.department.head.name '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->administrator->patronymic == $variable_word->title) $document->setValue('student.department.head.patronymic '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->administrator->surname_en == $variable_word->title) $document->setValue('student.department.head.surname_en '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->administrator->name_en == $variable_word->title) $document->setValue('student.department.head.name_en '.$variable_word->key, $variable_word->value);
						if ($student->group->speciality->department->administrator->patronymic_en == $variable_word->title) $document->setValue('student.department.head.patronymic_en '.$variable_word->key, $variable_word->value);
					}
				}

				/*

				$document->setValue('/[${} *а-яА-Я]/g', null);

				$document->setImageValue('qr', [
					'path' => public_path($qr),
					'width' => 150,
					'height' => 150,
					'ratio' => false
				]);

				*/

				$directory = 'pdfs/'.date('Y/m/d/H/i/s');

				if (!Storage::exists($directory)) {
					Storage::makeDirectory($directory);
				}

				$src = $directory.'/'.rand(100000, 999999);

				$document->saveAs('storage/'.$src.'.docx');

				ConvertStatementDocument::dispatch($src);
			}
			
			//  PDF render end

			$delivered_at = null;	//	Дата и время поcтавление задачи
			$started_at = null;		//	Дата и время подачи
			$expired_at = null;		//	Дата и время срок выполнение
			
			//	Если ответственный специалист назначен и студент подал заявление рабочие время

			if ($statement_view->responsible_user_id && $statement_status->slug == 'processing') {
				$delivered_at = $datetime_now->format('Y-m-d H:i:s');
				$started_at = $datetime_now->format('Y-m-d H:i:s');
				$expired_at = $this->addWorkDay($statement_view->category->ssc->operating_modes, $datetime_now->format('Y-m-d H:i:s'), $statement_view->term_of_consideration);
			}
			
			//	Если ответственный специалист не назначен и студент подал заявление рабочие время
			
			if (!$statement_view->responsible_user_id && $statement_status->slug == 'processing') {
				$statement_status = StatementStatus::where('slug', 'waiting')->first();
				
				$delivered_at = $datetime_now->format('Y-m-d H:i:s');
				$expired_at = $this->addWorkDay($statement_view->category->ssc->operating_modes, $datetime_now->format('Y-m-d H:i:s'), $statement_view->term_of_consideration);
			}
			
			//	Если ответственный специалист за заявление назначен и студент подал заявление вне рабочие время
			
			if ($statement_view->responsible_user_id && $statement_status->slug == 'waiting') {
				
			}

			//	Если ответственный специалист за заявление не назначен и студент подал специалист вне рабочие время
			
			if (!$statement_view->responsible_user_id && $statement_status->slug == 'waiting') {
				
			}
		
			$statement = Statement::create([
				'view_id' => $request->view_id,
				'responsible_role_id' => $statement_view->responsible_role_id,
				'responsible_user_id' => $statement_view->responsible_user_id,
				'role_id' => $role->id,
				'status_id' => $statement_status->id,
				'lang' => app()->getLocale(),
				'src' => isset($src) ? 'storage/'.$src.'.pdf' : null,
				'delivered_at' => $delivered_at ? $delivered_at : null,
				'started_at' => $started_at ? $started_at : null,
				'expired_at' => $expired_at ? $expired_at : null,
				'hash' => $hash,
				'qr' => $qr
			]);

			if ($role->slug == 'student') {
				StatementStudent::create([
					'statement_id' => $statement->id,
					'student_id' => $student->id
				]);
			}

			//	$state

			$langs = Lang::query()
				->get();

			$components = [];

			if ($request->has('details')) {
				foreach ($request->details as $component_id => $component_value) {
					$component_id = (int)str_replace('sc', '', $component_id);

					$component = StatementComponent::query()
						->where('id', $component_id)
						->first();

					$components[] = $component->id;

					if ($component->type->slug == 'input-text') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => $component_value
							]);
						}
					}

					if ($component->type->slug == 'input-number') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => (int)$component_value
							]);
						}
					}

					if ($component->type->slug == 'textarea') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => $component_value
							]);
						}
					}

					if ($component->type->slug == 'input-date') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => $component_value
							]);
						}
					}

					if ($component->type->slug == 'input-time') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => $component_value
							]);
						}
					}

					if ($component->type->slug == 'select') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => ($component_value || $component_value == 0) && !is_null($component_value) ? $component_lang_value->data[$component_value] : null
							]);
						}
					}

					if ($component->type->slug == 'select-multiple') {
						foreach ($component_value as $key => $value) {
							if (is_null($component_value[$key])) {
								unset($component_value[$key]);
							}
						}
						
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => collect($component_lang_value->data)->only($component_value)
							]);
						}
					}

					if ($component->type->slug == 'resource') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							if ($component->resource == 'student-subjects') {
								$resource = SubjectLang::query()
									->where('id', $component_value)
									->where('lang', $component_lang_value->lang)
									->first();

								$filtered = $resource ? $resource->name : null;
							}
							
							if ($component->resource == 'student-teachers') {
								$resource = UserLang::query()
									->where('id', $component_value)
									->where('lang', $component_lang_value->lang)
									->first();

								$filtered = $resource ? trim(implode(' ', [
									$resource->surname,
									$resource->name,
									$resource->patronymic
								])) : null;
							}
							
							if ($component->resource == 'specialties') {
								/*
								$resource = SpecialityLang::query()
									->where('id', $component_value)
									->where('lang', $component_lang_value->lang)
									->first();
								*/

								$resource = Speciality::query()
									->where('id', $component_value)
									->first();

								$filtered = $resource ? trim(implode(' ', [
									$resource->code,
									$resource->translations()->where('lang', $component_lang_value->lang)->first()->name
								])) : null;
							}
							
							if ($component->resource == 'study-languages') {
								$resource = StudyLanguageLang::query()
									->where('id', $component_value)
									->where('lang', $component_lang_value->lang)
									->first();

								$filtered = $resource ? $resource->name : null;
							}

							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => isset($filtered) ? $filtered : null
							]);
							
							$filtered = null;
						}
					}

					if ($component->type->slug == 'resource-multiple') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							if ($component->resource == 'student-subjects') {
								$resources = SubjectLang::query()
									->whereIn('id', $component_value)
									->where('lang', $component_lang_value->lang)
									->get();

								foreach ($resources as $resource) {
									$filtered[$component_lang_value->lang][] = $resource->name;
								}
							}
							
							if ($component->resource == 'student-teachers') {
								$resources = User::query()
									->whereIn('id', $component_value)
									->get();

								foreach ($resources as $resource) {
									$filtered[$component_lang_value->lang][] = trim(implode(' ', [
										$resource->surname,
										$resource->name,
										$resource->patronymic
									]));
								}
							}
							
							if ($component->resource == 'specialties') {
								/*
								$resources = SpecialityLang::query()
									->whereIn('id', $component_value)
									->where('lang', $component_lang_value->lang)
									->get();
								*/

								$resources = Speciality::query()
									->whereIn('id', $component_value)
									->get();

								foreach ($resources as $resource) {
									$filtered[$component_lang_value->lang][] = trim(implode(' ', [
										$resource->code,
										$resource->translations()->where('lang', $component_lang_value->lang)->first()->name
									]));
								}
							}
							
							if ($component->resource == 'study-languages') {
								$resources = StudyLanguageLang::query()
									->whereIn('id', $component_value)
									->where('lang', $component_lang_value->lang)
									->get();

								foreach ($resources as $resource) {
									$filtered[$component_lang_value->lang][] = $resource->name;
								}
							}
							
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => isset($filtered) ? $filtered[$component_lang_value->lang] : null
							]);
							
							$filtered = null;
						}
					}

					if ($component->type->slug == 'input-file') {
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => isset($component_value) ? [
									'name' => $component_value->getClientOriginalName(),
									'extension' => $component_value->extension(),
									'src' => asset('storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $component_value, rand(1000, 9999).'.'.$component_value->extension()))
								] : null
							]);
						}
					}

					if ($component->type->slug == 'input-files') {
						$srcs = [];

						if ($component_value) {
							foreach ($component_value as $photo) {
								$srcs[] = [
									'name' => $photo->getClientOriginalName(),
									'extension' => $photo->extension(),
									'src' => asset('storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $photo, rand(1000, 9999).'.'.$photo->extension()))
								];
							}
						}
						
						foreach ($component->translations as $component_lang_key => $component_lang_value) {
							$statement->details()->create([
								'lang' => $component_lang_value->lang,
								'type_id' => $component->type->id,
								'title' => $component_lang_value->name,
								'value' => count($srcs) > 0 ? $srcs : null
							]);
						}
					}
				}
			}

			$statement_components = StatementComponent::query()
				->where('view_id', $request->view_id)
				->whereNotIn('id', $components)
				->get();

			if ($statement_components) {
				foreach ($statement_components as $statement_component) {
					foreach ($statement_component->translations as $translation) {
						$statement->details()->create([
							'lang' => $translation->lang,
							'type_id' => $statement_component->type->id,
							'title' => $translation->name
						]);
					}
				}
			}
			
			//
			
			if ($statement->view->stages) {
				$i = 0;
				
				foreach ($statement->view->stages as $stage) {	
					$statement_stage = StatementStage::create([
						'slug' => $stage->slug,
						'statement_id' => $statement->id,
						'conditions' => $stage->conditions,
						'processes' => $stage->processes
					]);
					
					foreach ($stage->routes as $route) {
						
						$delivered_at = null;
						$started_at = null;
						$expired_at = null;

						//	Если ответственный специалист назначен и студент подал заявление рабочий время

						if ($route->responsible_user_id && $statement_status->slug == 'processing') {
							$route_status = 'waiting';

							$expired_at = $route->term ? $this->addWorkHour($statement_view->category->ssc->operating_modes, $datetime_now->format('Y-m-d H:i:s'), $route->term * 60) : null;
						}
						
						//	Если ответственный специалист назначен и студент подал заявление рабочий время
			
						if (!$route->responsible_user_id && $statement_status->slug == 'processing') {
							$route_status = 'waiting';

							$expired_at = $route->term ? $this->addWorkHour($statement_view->category->ssc->operating_modes, $datetime_now->format('Y-m-d H:i:s'), $route->term * 60) : null;
						}
						 
						//	Если студент подал заявление вне рабочий время

						if ($statement_status->slug == 'waiting') {
							$route_status = 'waiting';
						}
						
						$statement_stage->routes()->create([
							'action_id' => $route->action_id,
							'responsible_type' => $route->responsible_type,
							'responsible_role_id' => $route->responsible_role_id,
							'responsible_user_id' => $route->responsible_user_id,
							'status' => $i == 0 ? $route_status : 'queue',
							'term' => $route->term ? $route->term : null,
							'delivered_at' => $i == 0 && $delivered_at ? $delivered_at : null,
							'started_at' => $i == 0 && $started_at ? $started_at : null,
							'expired_at' => $i == 0 && $expired_at ? $expired_at : null
						]);
					}
					
					$i++;
				}
			}

			return $statement;
		});

		StatementCreated::dispatch($statement);

		return response()->json([
			'code' => 201,
			'data' => $statement
		], 200);
	}
	
	public function update(Request $request, Statement $statement) {
		$student_service_center = auth()->user()->sscs[0];

		$current_datetime = Carbon::now();
		
		$current_date = date('Y-m-d');
		$current_time = date('H:i:s');
		
		$current_operating_mode = [
			'time_start' => $student_service_center->currentOperatingMode->time_start,
			'time_end' => $student_service_center->currentOperatingMode->time_end
		];

		/*
		
		if ($current_operating_mode['time_start'] == '00:00:00' && $current_operating_mode['time_end'] == '00:00:00') {
			return response()->json([
				'code' => 403,
				'data' => StudentServiceCenterOperatingMode::collection($student_service_center->operating_modes)
			], 200);
		}
		
		if (($current_operating_mode['time_start'] != '00:00:00' || $current_operating_mode['time_end'] != '00:00:00') && !($current_operating_mode['time_start'] <= $current_time && $current_operating_mode['time_end'] >= $current_time)) {
			return response()->json([
				'code' => 403,
				'data' => StudentServiceCenterOperatingMode::collection($student_service_center->operating_modes)
			], 200);
		}

		*/

		if ($current_operating_mode['time_start'] == '00:00:00' && $current_operating_mode['time_end'] == '00:00:00' && $student_service_center->accepting_applications_24_7 == 1) {
			$statement_status = StatementStatus::where('slug', 'waiting')->first();
		}

		if ((($current_operating_mode['time_start'] != '00:00:00' || $current_operating_mode['time_end'] != '00:00:00') && $student_service_center->accepting_applications_24_7 == 1) && !($current_operating_mode['time_start'] <= $current_datetime->format('H:i:s') && $current_operating_mode['time_end'] >= $current_datetime->format('H:i:s'))) {
			$statement_status = StatementStatus::where('slug', 'waiting')->first();
		}
		
		if (($current_operating_mode['time_start'] != '00:00:00' || $current_operating_mode['time_end'] != '00:00:00') && ($current_operating_mode['time_start'] <= $current_datetime->format('H:i:s') && $current_operating_mode['time_end'] >= $current_datetime->format('H:i:s'))) {
			$statement_status = StatementStatus::where('slug', 'processing')->first();
		}
		
		$validator = Validator::make($request->all(), [
			'status' => 'required|string|exists:statement_statuses,slug',
			'message' => 'nullable|string|max:1023',
			'documents' => 'nullable|array',
			'documents.*' => 'required|file|max:5120',
			'expired_at' => 'nullable|date_format:Y-m-d H:i:s',
			'responsible_role_id' => 'nullable|exists:student_service_center_roles,id',
			'responsible_user_id' => 'nullable|exists:users,id'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		$employee_role = auth()->user()->studentServiceCenterRoles[0];

		if ($statement->responsible_role_id != $employee_role->id) {
			return response()->json([
				'code' => 403,
				'awdawd' => 'awd'
			], 200);
		}
		
		$statement = DB::transaction(function() use ($statement, $statement_status, $request, $student_service_center) {
			$datetime_now = Carbon::now();

			$delivered_at = null;
			$started_at = null;
			$ended_at = null;
			$expired_at = null;

			if ($request->has('expired_at')) {
				$expired_at = $request->expired_at;
			}

			if ($request->has('responsible_user_id')) {
				
			}
			
			//	Если ответственный специалист за заявление назначен и студент подал заявление вне рабочий время

			if ($request->status == 'processing' && $statement->responsible_user_id && $statement_status->slug == 'processing') {
				
			}
			
			//	Если ответственный специалист не назначен и студент подал заявление рабочие время
			
			if ($request->status == 'processing' && !$statement->view->responsible_user_id && $statement_status->slug == 'processing') {
				$statement_status = StatementStatus::where('slug', 'processing')->first();
				
				$started_at = $datetime_now->format('Y-m-d H:i:s');
			}
			
			//	Если ответственный специалист за заявление назначен и студент подал заявление вне рабочие время
			
			if ($request->status == 'processing' && $statement->view->responsible_user_id && $statement_status->slug == 'waiting') {
				$statement_status = StatementStatus::where('slug', 'processing')->first();
				
				$started_at = $datetime_now->format('Y-m-d H:i:s');
			}

			//	Если ответственный специалист за заявление не назначен и студент подал специалист вне рабочие время
			
			if ($request->status == 'processing' && !$statement->view->responsible_user_id && $statement_status->slug == 'waiting') {
				$statement_status = StatementStatus::where('slug', 'processing')->first();

				$started_at = $datetime_now->format('Y-m-d H:i:s');
			}
			
			if ($request->status == 'ready' || $request->status == 'renouncement') {
				$ended_at = date('Y-m-d H:i:s');
			}
			
			if ($request->has('documents') && ($request->status == 'ready' || $request->status == 'renouncement')) {
				$documents = [];
				
				foreach ($request->documents as $document) {
					$documents[] = [
						'name' => $document->getClientOriginalName(),
						'src' => 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $document, rand(1000, 9999).'.'.$document->extension())
					];
				}
			}

			$statement_id = $statement->id;
			
			$statement->update([
				'responsible_user_id' => auth()->user()->id,
				'status_id' => StatementStatus::query()->where('slug', $request->status)->first()->id,
				'delivered_at' => $delivered_at ? $delivered_at : $statement->delivered_at,
				'started_at' => $started_at ? $started_at : $statement->started_at,
				'ended_at' => $ended_at ? $ended_at : $statement->ended_at,
				'expired_at' => $expired_at ? $expired_at : $statement->expired_at,
				'message' => $request->has('message') ? $request->message : null,
				'files' => $request->has('documents') ? $documents : null
			]);

			$statement = Statement::query()
				->where('id', $statement_id)
				->first();

			$i = 0;

			if ($statement->status->slug == 'waiting' || $statement->status->slug == 'processing') {
				foreach ($statement->stages as $stage) {
					foreach ($stage->routes->where('responsible_type', 'responsible') as $route) {
						if ($i == 0) {
							$route->update([
								'status' => 'processing',
								'responsible_user_id' => auth()->user()->id
							]);
						} else {
							$route->update([
								'responsible_user_id' => auth()->user()->id
							]);
						}
					}
					
					foreach ($stage->routes as $route) {
						if ($i == 0 && $route->status == 'queue' && !$route->responsible_user_id ) {
							$route->update([
								'status' => 'waiting'
							]);
						}

						if ($i == 0 && $route->status == 'queue' && $route->responsible_user_id ) {
							$route->update([
								'status' => 'waiting'
							]);
						}
					}
					
					$i++;
				}
			}
			
			$statement = Statement::find($statement->id);
			
			return $statement;
		});

		if ($statement->status->slug == 'ready' || $statement->status->slug == 'renouncement') {
			StatementCompleted::dispatch($statement);
		}
		
		return response()->json([
			'code' => 200,
			'data' => new StatementResource($statement)
		], 200);
	}
	
	public function destroy(Request $request, Statement $statement) {
		
		$statement->delete();

		return response()->json([
			'code' => 204
		], 200);
	}
	
	public function check(Request $request, Statement $statement)
	{
		return response()->json([
			'code' => 200,
			'data' => [
				'id' => $statement->id,
				'view' => [
					'name' => $statement->view->name
				],
				'status' => [
					'name' => $statement->status->name
				],
				'student' => [
					'user' => [
						'surname' => $statement->student->user->surname,
						'name' => $statement->student->user->name,
						'patronymic' => $statement->student->user->patronymic
					]
				],
				'created_at' => $statement->created_at->format('Y-m-d H:i:s')
			]
		], 200);
	}
 }
