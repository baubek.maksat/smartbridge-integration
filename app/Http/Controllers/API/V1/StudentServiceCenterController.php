<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use Illuminate\Support\Str;


use App\Http\Resources\StudentServiceCenterResource;

use App\Models\StudentServiceCenterRoleAccess;
use App\Models\StudentServiceCenterLang;
use App\Models\StudentServiceCenter;
use App\Models\StatementView;
use App\Models\User;
use App\Models\Role;
use App\Models\Type;
use App\Models\Lang;

use App\Mail\RoleMail;
use App\Mail\Confirm;

use Validator;
use Mail;
use DB;
use UA;

class StudentServiceCenterController extends Controller
{
    public function index(Request $request)
    {
		$sscs = StudentServiceCenter::query();
		
		if ($request->has('filter.name')) {
			$sscs->whereHas('translation', function ($query) use ($request) {
				$query->where('name', 'like', $request->filter['name']);
			});
		}
		
		if ($request->has('filter.university_id')) {
			$sscs->where('university_id', $request->filter['university_id']);
		}
		
		if ($request->has('filter.state_id')) {
			$sscs->where('state_id', $request->filter['state_id']);
		}
		
		$sscs = $sscs->paginate();

		return StudentServiceCenterResource::collection($sscs);
    }
	
	public function show(StudentServiceCenter $student_service_center)
    {
		return new StudentServiceCenterResource($student_service_center);
    }
	
	public function store(Request $request)
	{
		$administrator = null;
		
		if ($request->has('administrator.surname') && $request->has('administrator.surname') && $request->has('administrator.gender_id') && $request->has('administrator.birth_date')) {
			$user = User::query()
				->where('surname', $request->administrator['surname'])
				->where('name', $request->administrator['name'])
				->where('birth_date', $request->administrator['birth_date'])
				->where('gender_id', $request->administrator['gender_id'])
				->first();
		}
		
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|exists:universities,id|unique:student_service_centers,university_id',
			'state_id' => 'required|exists:states,id',
			'langs' => 'required|array',
			'langs.*' => 'required|exists:langs,slug',
			'accepting_applications_24_7' => 'nullable|string|in:0,1',
			
			'translations' => 'required|array|size:'.count($request->langs),
			'translations.kz.name' => 'nullable|string|max:255',
			'translations.ru.name' => 'nullable|string|max:255',
			'translations.en.name' => 'nullable|string|max:255',
			
			'administrator.surname' => 'required|string|max:255',
			'administrator.name' => 'required|string|max:255',
			'administrator.patronymic' => 'nullable|string|max:255',
			'administrator.email' => 'required|email|max:255'.(!isset($administrator->id) ? '|unique:users,email' : ''),
			'administrator.gender_id' => 'required|exists:genders,id',
			'administrator.birth_date' => 'required|date|date_format:Y-m-d',
			'operating_modes' => 'required|array',
			'operating_modes.1.time_start' => 'required|date_format:H:i',
			'operating_modes.1.time_end' => 'required|date_format:H:i|after_or_equal:operating_modes.1.time_start',
			'operating_modes.2.time_start' => 'required|date_format:H:i',
			'operating_modes.2.time_end' => 'required|date_format:H:i|after_or_equal:operating_modes.2.time_start',
			'operating_modes.3.time_start' => 'required|date_format:H:i',
			'operating_modes.3.time_end' => 'required|date_format:H:i|after_or_equal:operating_modes.3.time_start',
			'operating_modes.4.time_start' => 'required|date_format:H:i',
			'operating_modes.4.time_end' => 'required|date_format:H:i|after_or_equal:operating_modes.4.time_start',
			'operating_modes.5.time_start' => 'required|date_format:H:i',
			'operating_modes.5.time_end' => 'required|date_format:H:i|after_or_equal:operating_modes.5.time_start',
			'operating_modes.6.time_start' => 'required|date_format:H:i',
			'operating_modes.6.time_end' => 'required|date_format:H:i|after_or_equal:operating_modes.6.time_start',
			'operating_modes.7.time_start' => 'required|date_format:H:i',
			'operating_modes.7.time_end' => 'required|date_format:H:i|after_or_equal:operating_modes.7.time_start'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		function convert_word($from, $to, $word) {
			return $word;
		}
		
		$data = DB::transaction(function() use ($request) {
			$user = User::query()
				->where('surname', $request->administrator['surname'])
				->where('name', $request->administrator['name'])
				->where('birth_date', $request->administrator['birth_date'])
				->where('gender_id', $request->administrator['gender_id'])
				->first();
			
			if (!$user) {
				$user = User::create([
					'surname' => $request->administrator['surname'],
					'name' => $request->administrator['name'],
					'patronymic' => $request->administrator['patronymic'],
					'email' => $request->administrator['email'],
					'username' => Str::slug($request->administrator['surname'].'_'.$request->administrator['name'].'_'.rand(1000, 9999), '_'),
					'password' => bcrypt(sha1($request->administrator['email'])),
					'gender_id' => $request->administrator['gender_id'],
					'birth_date' => $request->administrator['birth_date']
				]);
				
				$user->translations()->create([
					'lang' => 'kz',
					'surname' => convert_word(app()->getLocale(), 'kz', $request->administrator['surname']),
					'name' => convert_word(app()->getLocale(), 'kz', $request->administrator['name']),
					'patronymic' => convert_word(app()->getLocale(), 'kz', $request->administrator['patronymic'])
				]);
				
				$user->translations()->create([
					'lang' => 'ru',
					'surname' => convert_word(app()->getLocale(), 'ru', $request->administrator['surname']),
					'name' => convert_word(app()->getLocale(), 'ru', $request->administrator['name']),
					'patronymic' => convert_word(app()->getLocale(), 'ru', $request->administrator['patronymic'])
				]);
				
				$user->translations()->create([
					'lang' => 'en',
					'surname' => convert_word(app()->getLocale(), 'en', $request->administrator['surname']),
					'name' => convert_word(app()->getLocale(), 'en', $request->administrator['name']),
					'patronymic' => convert_word(app()->getLocale(), 'en', $request->administrator['patronymic'])
				]);
			}
			
			$ssc = StudentServiceCenter::create([
				'university_id' => $request->university_id,
				'head_id' => $user->id,
				'accepting_applications_24_7' => strval($request->accepting_applications_24_7),
				'state_id' => $request->state_id
			]);
			
			foreach ($request->langs as $lang) {
				$lang = Lang::query()
					->where('slug', $lang)
					->first();
				
				$ssc->langs()->attach($lang);
			}
			
			foreach($request->translations as $lang => $translation) {
				$ssc->translations()->create([
					'lang' => $lang,
					'name' => $translation['name']
				]);
			}
			
			$ssc_role_access = StudentServiceCenterRoleAccess::query()
				->where('slug', 'ssc-head')
				->first();
			
			$ssc_role = $ssc->roles()->create([
				'state_id' => 2,
				'access_id' => $ssc_role_access->id
			]);
			
			$ssc_role->translations()->create([
				'lang' => 'kz',
				'name' => 'СҚКО басшысы'
			]);
			
			$ssc_role->translations()->create([
				'lang' => 'ru',
				'name' => 'Руководитель ЦОС'
			]);
			
			$ssc_role->translations()->create([
				'lang' => 'en',
				'name' => 'SSC administrator'
			]);
			
			$role = Role::query()
				->where('slug', 'ssc-head')
				->first();
			
			$user->roles()->attach($role);
			
			$user->sscs()->attach($ssc);
			
			$user->studentServiceCenterRoles()->attach($ssc_role);
			
			if ($user->roles()->count() == 1) {
				$user->emailConfirms()->create([
					'type_id' => Type::where('slug', $request->header('type'))->first()->id,
					'lang_id' => Lang::where('slug', app()->getLocale())->first()->id,
					'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
					'ip' => $request->server('REMOTE_ADDR'),
					'ua' => $request->server('HTTP_USER_AGENT'),
					'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
					'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
					'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
					'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
					'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
					'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
					'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
					'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
					'hash' => sha1(rand(1000, 9999)),
					'src' => mb_strtolower($request->administrator['email'])
				]);
				
				$confirm = $user->emailConfirms()->orderBy('id', 'desc')->first();
			}
			
			foreach ($request->operating_modes as $operating_mode_key => $operating_mode_value) {
				$ssc->operating_modes()->create([
					'week_id' => $operating_mode_key,
					'time_start' => $operating_mode_value['time_start'],
					'time_end' => $operating_mode_value['time_end']
				]);
			}
			
			return [
				'user' => $user,
				'role' => $role,
				'university' => $ssc->university
			];
		});
		
		if ($data['user']->roles()->count() == 1) {
			$confirm = $data['user']->emailConfirms()->orderBy('id', 'desc')->first();
			
			Mail::to($data['user']->email)
				->queue(new Confirm($confirm));
		}
		
		Mail::to($data['user']->email)
			->queue(new RoleMail($data['user'], $data['role'], $data['university']));
		
		return response()->json([
			'code' => 201
		], 200);
	}
	
	public function update(Request $request, StudentServiceCenter $student_service_center)
	{
		$head = null;
		
		if ($request->has('administrator.surname') && $request->has('administrator.name') && $request->has('administrator.gender_id') && $request->has('administrator.birth_date')) {
			$head = User::query()
				->where('surname', $request->administrator['surname'])
				->where('name', $request->administrator['name'])
				->where('birth_date', $request->administrator['birth_date'])
				->where('gender_id', $request->administrator['gender_id'])
				->first();
		}
		
		$validator = Validator::make($request->all(), [
			'university_id' => [
				'required',
				'exists:universities,id',
				Rule::unique('student_service_centers', 'university_id')->ignore($student_service_center->id),
			],
			'state_id' => 'required|exists:states,id',
			'langs' => 'required|array',
			'langs.*' => 'required|exists:langs,slug',
			'accepting_applications_24_7' => 'required|string|in:0,1',
			
			'translations' => 'required|array|size:'.count($request->langs),
			'translations.kz.name' => 'nullable|string|max:255',
			'translations.ru.name' => 'nullable|string|max:255',
			'translations.en.name' => 'nullable|string|max:255',
			
			'administrator.surname' => 'required|string|max:255',
			'administrator.name' => 'required|string|max:255',
			'administrator.patronymic' => 'nullable|string|max:255',
			'administrator.email' => 'required|email|max:255'.(!isset($head->id) ? '|unique:users,email' : ''),
			'administrator.gender_id' => 'required|exists:genders,id',
			'administrator.birth_date' => 'required|date|date_format:Y-m-d',
			
			'operating_modes' => 'required|array',
			'operating_modes.1.time_start' => 'required',
			'operating_modes.1.time_end' => 'required',
			'operating_modes.2.time_start' => 'required',
			'operating_modes.2.time_end' => 'required',
			'operating_modes.3.time_start' => 'required',
			'operating_modes.3.time_end' => 'required',
			'operating_modes.4.time_start' => 'required',
			'operating_modes.4.time_end' => 'required',
			'operating_modes.5.time_start' => 'required',
			'operating_modes.5.time_end' => 'required',
			'operating_modes.6.time_start' => 'required',
			'operating_modes.6.time_end' => 'required',
			'operating_modes.7.time_start' => 'required',
			'operating_modes.7.time_end' => 'required'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		$student_service_center_head_id = $student_service_center->head_id;
		
		$data = DB::transaction(function() use ($request, $student_service_center, $student_service_center_head_id) {
			$user = User::query()
				->where('surname', $request->administrator['surname'])
				->where('name', $request->administrator['name'])
				->where('birth_date', $request->administrator['birth_date'])
				->where('gender_id', $request->administrator['gender_id'])
				->first();
			
			if (!$user) {
				$user = User::create([
					'surname' => $request->administrator['surname'],
					'name' => $request->administrator['name'],
					'patronymic' => $request->administrator['patronymic'],
					'email' => $request->administrator['email'],
					'username' => Str::slug($request->administrator['surname'].'_'.$request->administrator['name'].'_'.rand(1000, 9999), '_'),
					'password' => bcrypt(sha1($request->administrator['email'])),
					'gender_id' => $request->administrator['gender_id'],
					'birth_date' => $request->administrator['birth_date']
				]);
				
				$user->translations()->create([
					'lang' => 'kz'
				]);
				
				$user->translations()->create([
					'lang' => 'ru'
				]);
				
				$user->translations()->create([
					'lang' => 'en'
				]);
			}
		
			$student_service_center->update([
				'university_id' => $request->university_id,
				'head_id' => $user->id,
				'accepting_applications_24_7' => strval($request->accepting_applications_24_7),
				'state_id' => $request->state_id
			]);
			
			//
			
			if ($student_service_center->langs()->whereNotIn('slug', $request->langs)->count() > 0) {
				$langs = $student_service_center->langs()->whereNotIn('slug', $request->langs)->get();

				$student_service_center->langs()->detach($langs);
			}
			
			for ($i = 0; $i < count($request->langs); $i++) {
				$lang = Lang::query()
					->where('slug', $request->langs[$i])
					->first();

				if (!$student_service_center->hasLang($lang->slug)){
					$student_service_center->langs()->attach($lang);
					
					//
						
					$statement_views = StatementView::query()
						->whereHas('category', function($query) use ($student_service_center) {
							$query->where('ssc_id', $student_service_center->id);
						})
						->get();
					
					if ($statement_views) {
						foreach ($statement_views as $statement_view) {
							if (!$statement_view->translations()->where('lang', $lang->slug)->first()) {
								$statement_view->translations()->create([
									'lang' => $lang->slug
								]);
							}
						}
					}
					
					//
				}
			}
			
			//
			
			foreach($request->translations as $lang => $translation) {
				$student_service_center->translations()->where('lang', $lang)->update([
					'name' => $translation['name']
				]);
				
				if (!$student_service_center->translations()->where('lang', $lang)->first()) {
					$student_service_center->translations()->create([
						'lang' => $lang,
						'name' => $translation['name']
					]);
				}
			}
			
			if ($user->id != $student_service_center_head_id) {
				$ssc_role = $student_service_center->roles()->create([
					'state_id' => 2
				]);

				$ssc_role->translations()->create([
					'lang' => 'kz',
					'name' => 'СҚКО басшысы'
				]);

				$ssc_role->translations()->create([
					'lang' => 'ru',
					'name' => 'Руководитель ЦОС'
				]);

				$ssc_role->translations()->create([
					'lang' => 'en',
					'name' => 'SSC administrator'
				]);
				
				$role = Role::query()
					->where('slug', 'ssc-head')
					->first();
				
				if (!$user->hasRole($role->slug)) {
					$user->roles()->attach($role);
				}
			
				$user->sscs()->attach($student_service_center);

				$user->studentServiceCenterRoles()->attach($ssc_role);
			}
			
			if ($user->email_verified == 0) {
				$confirm = $user->emailConfirms()->create([
					'type_id' => Type::where('slug', $request->header('type'))->first()->id,
					'lang_id' => Lang::where('slug', app()->getLocale())->first()->id,
					'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
					'ip' => $request->server('REMOTE_ADDR'),
					'ua' => $request->server('HTTP_USER_AGENT'),
					'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
					'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
					'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
					'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
					'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
					'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
					'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
					'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
					'hash' => sha1(rand(1000, 9999)),
					'src' => mb_strtolower($request->administrator['email'])
				]);
			}
			
			foreach ($request->operating_modes as $operating_mode_key => $operating_mode_value) {
				$student_service_center->operating_modes()->where('week_id', $operating_mode_key)->update([
					'time_start' => $operating_mode_value['time_start'],
					'time_end' => $operating_mode_value['time_end']
				]);
			}
			
			return [
				'user' => $user,
				'role' => isset($role) ? $role : null,
				'confirm' => isset($confirm) ? $confirm : null,
				'university' => $student_service_center->university
			];
		});
		
		if ($student_service_center_head_id != $data['user']->id && $data['user']->email_verified == 0) {
			Mail::to($data['user']->email)
				->queue(new Confirm($data['confirm']));
		}
		
		if ($student_service_center_head_id != $data['user']->id) {
			Mail::to($data['user']->email)
				->queue(new RoleMail($data['user'], $data['role'], $data['university']));
		}
		
		return response()->json([
			'code' => 200
		], 200);
	}
	
	public function destroy(StudentServiceCenter $student_service_center)
	{
		$student_service_center->delete();
		
		return response()->json([
			'code' => 204
		], 200); 
	}
}
