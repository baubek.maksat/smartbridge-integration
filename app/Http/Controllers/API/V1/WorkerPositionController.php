<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\WorkerPositionResource;

use App\Models\WorkerPosition;

use Validator;

class WorkerPositionController extends Controller
{
    public function index(Request $request)
    {
		$worker_positions = WorkerPosition::query();
		
		$worker_positions = $worker_positions->paginate();

    	return WorkerPositionResource::collection($worker_positions);
    }
}
