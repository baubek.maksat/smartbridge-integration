<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use Illuminate\Support\Facades\Storage;

use App\Http\Resources\EventResource;

use App\Events\EventCreated;

use App\Models\Event;

use Validator;
use DB;

class EventController extends Controller
{
    public function index(Request $request)
    {
    	$events = Event::query()
			->has('translation');
		
		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$events->where('university_id', auth()->user()->sscs[0]->university_id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$events->where('university_id', auth()->user()->sscs[0]->university_id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$events->where('university_id', auth()->user()->sscs[0]->university_id);
		}
		
		if ($request->has('filter.state_id')) {
			$events->where('state_id', $request->filter['state_id']);
		}
		
		if ($request->has('filter.heading')) {
			$events->whereHas('translations', function($query) use ($request) {
				$query->where('heading', 'LIKE', $request->filter['heading']);
			});
		}
		
    	$events = $events->paginate();

    	return EventResource::collection($events);
    }

    public function show(Event $event)
    {
    	return new EventResource($event);
    }
	
	public function store(Request $request)
	{
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'state_id' => 'required|exists:states,id',
			'langs' => 'required|array|exists:langs,slug',
			'date_of_publication' => 'required|date|date_format:Y-m-d',
			'holding_date' => 'required|date_format:Y-m-d',
			'holding_time' => 'required|date_format:H:i',
            'image' => [
				'required',
				'image',
				'max:2048',
				'dimensions:ratio=2/1'
			],
			'translations.kz.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:255',
				Rule::unique('event_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->events()->pluck('id'));
				})
			],
			'translations.ru.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:255',
				Rule::unique('event_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->events()->pluck('id'));
				})
			],
			'translations.en.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('event_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->events()->pluck('id'));
				})
			],
			'translations.kz.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:15000'
			],
			'translations.ru.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:15000'
			],
			'translations.en.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:15000'
			],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$data = DB::transaction(function() use ($request, $ssc) {
			$event = Event::create([
				'state_id' => $request->state_id,
				'university_id' => $ssc->university->id,
				'date_of_publication' => $request->date_of_publication,
				'holding_date' => $request->holding_date,
				'holding_time' => $request->holding_time,
				'image' => 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $request->image, rand(1000, 9999).'.'.$request->image->extension())
			]);

			foreach ($request->translations as $lang => $translation) {
				$event->translations()->create([
					'lang' => $lang,
					'heading' => $translation['heading'],
					'discription' => $translation['discription']
				]);
			}

			return $event;
		});
		
		if ($data->state_id == 2) {
			EventCreated::dispatch($data);
		}

		return response()->json([
			'code' => 201,
			'data' => new EventResource($data)
		], 200);
    }
	
    public function update(Request $request, Event $event)
    {
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'state_id' => 'required|exists:states,id',
			'langs' => 'required|array|exists:langs,slug',
			'date_of_publication' => 'required|date|date_format:Y-m-d',
			'holding_date' => 'required|date_format:Y-m-d',
			'holding_time' => 'required|date_format:H:i',
            'image' => [
				Rule::requiredIf(function () use ($request){
					return !$request->has('image_link');
				}),
				'image',
				'max:2048',
				'dimensions:ratio=2/1'
			],
			'image_link' => [
				Rule::requiredIf(function () use ($request){
					return !$request->has('image');
				}),
				'active_url'
			],
			'translations.kz.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:255',
				Rule::unique('event_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->events()->pluck('id'));
				})->ignore($event->id, 'id')
			],
			'translations.ru.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:255',
				Rule::unique('event_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->events()->pluck('id'));
				})->ignore($event->id, 'id')
			],
			'translations.en.heading' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('event_langs', 'heading')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->events()->pluck('id'));
				})->ignore($event->id, 'id')
			],
			'translations.kz.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz');
				}),
				'string',
				'max:15000'
			],
			'translations.ru.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru');
				}),
				'string',
				'max:15000'
			],
			'translations.en.discription' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:15000'
			],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$data = DB::transaction(function() use ($request, $ssc, $event) {
			if ($request->has('image_link')) {
				$image = $event->image;
			}
			
			if ($request->has('image')) {
				$image = 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $request->image, rand(1000, 9999).'.'.$request->image->extension());
			}
			
			$event->update([
				'state_id' => $request->state_id,
				'university_id' => $ssc->university->id,
				'image' => $image,
				'date_of_publication' => $request->date_of_publication,
				'holding_date' => $request->holding_date,
				'holding_time' => $request->holding_time
			]);

			$event->translations()->whereNotIn('lang', $request->langs)->delete();

			foreach ($request->translations as $lang => $translation) {
				if ($event->translations()->where('lang', $lang)->first()) {
					$event->translations()->where('lang', $lang)->update([
						'heading' => $translation['heading'],
						'discription' => $translation['discription']
					]);
				} else {
					$event->translations()->create([
						'lang' => $lang,
						'heading' => $translation['heading'],
						'discription' => $translation['discription']
					]);
				}
			}

			return $event;
		});

		return response()->json([
			'code' => 200,
			'data' => new EventResource($data)
		], 200);
    }
	
    public function destroy(Event $event)
	{
		$event->delete();
		
		return response()->json([
			'code' => 204
		], 200);
	}
}
