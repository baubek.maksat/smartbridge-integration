<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Resources\UniversityResource;

use Illuminate\Validation\Rule;

use App\Models\UniversityAdministrator;
use App\Models\University;
use App\Models\Cato;
use App\Models\User;
use App\Models\Lang;

use Validator;
use DB;

class UniversityController extends Controller
{
    public function index(Request $request)
    {
        $universities = University::query();

        //  filter
		
		if ($request->has('filter.state_id')) {
			$universities->where('state_id', $request->filter['state_id']);
		}

        $universities->whereHas('translation', function ($query) use ($request) {
            if ($request->has('filter.name')) {
                $query->where('name', 'LIKE', $request->filter['name']);
            }
        });
		
		$universities = $universities->paginate();

        return UniversityResource::collection($universities);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'type_id' => 'nullable|integer|exists:university_types,id',
            'country_id' => 'required|integer|exists:countries,id',
			'state_id' => 'required|integer|exists:states,id',
            'study_languages' => 'required|array',
            'study_languages.*' => 'required|integer|exists:study_languages,id',
			'cato_id' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('country_id', $request->country_id)->count() > 0;
                }),
                'integer',
                'exists:catos,id'
            ],
			'logo' => 'required|file|max:2000',
            'administrator_id' => 'nullable|integer|exists:users,id',
            'faculty_type_id' => 'required|integer|exists:faculty_types,id',
            'department_type_id' => 'required|integer|exists:department_types,id',
            'speciality_type_id' => 'required|integer|exists:speciality_types,id',
            'administrator_worker_position_id' => 'required|integer|exists:worker_positions,id',
            'faculty_administrator_worker_position_id' => 'required|integer|exists:worker_positions,id',
            'department_administrator_worker_position_id' => 'required|integer|exists:worker_positions,id',
            'bin' => 'required|string|max:255',
            'website' => 'required|active_url|string|max:255',
            'begin_of_work' => 'required|date_format:Y-m-d',
            'emails' => 'array',
            'emails.*' => 'required|email|max:255|distinct|unique:university_emails,value',
            'phones' => 'array',
            'phones.*' => 'required|max:255|distinct|unique:university_phones,value',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255',
                'unique:university_langs,name'
            ],
			'translations.kz.cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('country_id', $request->country_id)->count() == 0;
                }),
                'string',
                'max:255'
            ],
            'translations.kz.address' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255',
                'unique:university_langs,name'
            ],
			'translations.ru.cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('country_id', $request->country_id)->count() == 0;
                }),
                'string',
                'max:255'
            ],
            'translations.ru.address' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'nullable',
                'array'
            ],
            'translations.en.name' => [
                'nullable',
                'string',
                'max:255',
                'unique:university_langs,name'
            ],
			'translations.en.cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('country_id', $request->country_id)->count() == 0;
                }),
                'string',
                'max:255'
            ],
            'translations.en.address' => [
                'nullable',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }

        $university = DB::transaction(function() use ($request) {
	        $university = University::create([
               'type_id' => 101,
	            'country_id' => $request->country_id,
				'state_id' => $request->state_id,
	            'cato_id' => Cato::where('country_id', $request->country_id)->count() > 0 ? $request->cato_id : null,
	            'administrator_id' => $request->administrator_id,
                'faculty_type_id' => $request->faculty_type_id,
	            'department_type_id' => $request->department_type_id,
	            'speciality_type_id' => $request->speciality_type_id,
	            'administrator_worker_position_id' => $request->administrator_worker_position_id,
                'faculty_administrator_worker_position_id' => $request->faculty_administrator_worker_position_id,
	            'department_administrator_worker_position_id' => $request->department_administrator_worker_position_id,
	            'bin' => $request->bin,
				'logo' => $request->has('logo') ? 'storage/'.Storage::putFileAs('uploads/'.date('y/m/d/h/i/s'), $request->logo, rand(1000, 9999).'.'.$request->logo->extension()) : null,
	            'website' => $request->website,
	            'begin_of_work' => $request->begin_of_work
	        ]);

            $university->studyLanguages()->attach($request->study_languages);

            $university->langs()->attach(Lang::all());

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                    $university->translations()->create([
                        'lang' => $key,
                        'name' => $value['name'],
                        'address' => isset($value['address']) ? $value['address'] : null,
                        'cato_name' => Cato::where('country_id', $request->country_id)->count() == 0 ? $value->cato_name : null
                    ]);
                }
            }

            if ($request->has('administrator_id')) {
                UniversityAdministrator::create([
                    'id' => $request->administrator_id
                ]);

                $card = $university->administratorCards()->create([
                    'worker_position_id' => $request->administrator_worker_position_id,
                    'date_start' => date('Y-m-d')
                ]);

                $card->administrator()->attach($request->administrator_id);
            }

            if ($request->has('emails')) {
                foreach ($request->emails as $email) {
                    $university->emails()->create([
                        'value' => $email
                    ]);
                }
            }

            if ($request->has('phones')) {
                foreach ($request->phones as $phone) {
                    $university->phones()->create([
                        'value' => $phone
                    ]);
                }
            }

            return $university;
	    });

        return response()->json([
            'code' => 201,
			'data' => $university
        ], 200);
    }

    public function show(University $university)
    {
    	return new UniversityResource($university);
    }

    public function update(Request $request, University $university)
    {
        $validator = Validator::make($request->all(), [
            'type_id' => 'required|integer|exists:university_types,id',
            'country_id' => 'required|integer|exists:countries,id',
            'study_languages' => 'required|array',
            'study_languages.*' => 'required|integer|exists:study_languages,id',
            'cato_id' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('country_id', $request->country_id)->count() > 0;
                }),
                'integer',
                'exists:catos,id'
            ],
            'faculty_type_id' => 'required|integer|exists:faculty_types,id',
            'department_type_id' => 'required|integer|exists:department_types,id',
            'speciality_type_id' => 'required|integer|exists:speciality_types,id',
            'administrator_worker_position_id' => 'required|integer|exists:worker_positions,id',
            'faculty_administrator_worker_position_id' => 'required|integer|exists:worker_positions,id',
            'department_administrator_worker_position_id' => 'required|integer|exists:worker_positions,id',
            'bin' => 'required|string|max:255',
            'website' => 'required|active_url|string|max:255',
            'begin_of_work' => 'required|date_format:Y-m-d',
            'emails' => 'array',
            'emails.*' => 'required|email|max:255|distinct|unique:university_emails,value',
            'phones' => 'array',
            'phones.*' => 'required|max:255|distinct|unique:university_phones,value',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255',
                'unique:university_langs,name'
            ],
            'translations.kz.cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('country_id', $request->country_id)->count() == 0;
                }),
                'string',
                'max:255'
            ],
            'translations.kz.address' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255',
                'unique:university_langs,name'
            ],
            'translations.ru.cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('country_id', $request->country_id)->count() == 0;
                }),
                'string',
                'max:255'
            ],
            'translations.ru.address' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255',
                'unique:university_langs,name'
            ],
            'translations.en.cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('country_id', $request->country_id)->count() == 0;
                }),
                'string',
                'max:255'
            ],
            'translations.en.address' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        DB::transaction(function() use ($university, $request) {
            $university->update([
                'faculty_type_id' => $request->faculty_type_id,
                'department_type_id' => $request->department_type_id,
                'speciality_type_id' => $request->speciality_type_id,
                'administrator_worker_position_id' => $request->administrator_worker_position_id,
                'faculty_administrator_worker_position_id' => $request->faculty_administrator_worker_position_id,
                'department_administrator_worker_position_id' => $request->department_administrator_worker_position_id,
                'bin' => $request->bin,
                'website' => $request->website,
                'begin_of_work' => $request->begin_of_work
            ]);
        });
        
    	return response()->json([
            'code' => 200
        ], 200);
    }

    public function destroy(University $university)
    {
        DB::transaction(function() use ($university) {
            $university = University::create([
                'type_id' => $request->type_id,
                'country_id' => $request->country_id,
                'cato_id' => Cato::where('country_id', $request->country_id)->count() > 0 ? $request->cato_id : null,
                'faculty_type_id' => $request->faculty_type_id,
                'department_type_id' => $request->department_type_id,
                'speciality_type_id' => $request->speciality_type_id,
                'administrator_worker_position_id' => $request->administrator_worker_position_id,
                'faculty_administrator_worker_position_id' => $request->faculty_administrator_worker_position_id,
                'department_administrator_worker_position_id' => $request->department_administrator_worker_position_id,
                'bin' => $request->bin,
                'website' => $request->website,
                'begin_of_work' => $request->begin_of_work
            ]);

            $university->studyLanguages()->attach($request->study_languages);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                    $university->translations()->create([
                        'lang' => $key,
                        'name' => $value['name'],
                        'address' => $value['address'],
                        'cato_name' => Cato::where('country_id', $request->country_id)->count() == 0 ? $value->cato_name : null
                    ]);
                }
            }

            if ($request->has('emails')) {
                foreach ($request->emails as $email) {
                    $university->emails()->create([
                        'value' => $email
                    ]);
                }
            }

            if ($request->has('phones')) {
                foreach ($request->phones as $phone) {
                    $university->phones()->create([
                        'value' => $phone
                    ]);
                }
            }
        });
    	dd($university);
    }
}
