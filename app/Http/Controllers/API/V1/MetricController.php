<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\MetricResource;

use Illuminate\Validation\Rule;

use App\Models\Metric;
use App\Models\Visit;
use App\Models\Type;
use App\Models\App;

use Validator;

class MetricController extends Controller
{
	public function index(Request $request)
    {
    	$metrics = Metric::query();

    	if ($request->has('filter.type')) {
    		$metrics->whereHas('type', function($query) use ($request) {
    			$query->where('slug', $request->type);
    		});
    	}

    	$metrics = $metrics->paginate();

    	return MetricResource::collection($metrics);
    }

    public function show(Request $request, Metric $metric)
    {
    	return new MetricResource($metric);
    }

    public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'app_id' => 'nullable|exists:apps,id',
            'route' => 'required|string|max:255',
            'params' => 'nullable|array'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		if ($request->bearerToken()) {
			$visit = Visit::query()
				->where('token', $request->bearerToken())
				->first();
		}
		
		if ($request->has('app_id')) {
			$app = App::query()
				->where('id', $request->app_id)
				->first();
		}	
		
		$type = Type::query()
            ->where('slug', $request->header('type'))
            ->first();
	
		Metric::create([
			'visit_id' => isset($visit) && $visit ? $visit->id : null,
			'type_id' => $type->id,
			'app_id' => isset($app) && $app ? $app->id : null,
			'route' => $request->route,
            'params' => $request->has('params') ? $request->params : null
		]);
		
		return response()->json([
            'code' => 201
        ], 200);
    }
}
