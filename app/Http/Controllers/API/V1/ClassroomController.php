<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\ClassroomResource;

use App\Models\ClassroomLang;
use App\Models\Classroom;

use Validator;
use DB;

class ClassroomController extends Controller
{
    public function index(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'per_page' => 'integer|between:1,50'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $classrooms = Classroom::query();

        if ($request->has('filter.university_id') && $request->filter['university_id']) {
            $classrooms->where('university_id', $request->filter['university_id']);
        }

        $classrooms = $classrooms->paginate();

        return ClassroomResource::collection($classrooms);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'university_id' => 'required|integer|exists:universities,id',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request) {
	        $academic_degree = Classroom::create([
	        	'university_id' => $request->university_id
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                    $academic_degree->translations()->create([
                        'lang' => $key,
                        'name' => $value['name']
                    ]);
                }
            }
	    });

        return response()->json([
            'messages' => 'Created'
        ], 201);
    }

    public function show(Classroom $academic_degree)
    {
    	$data = Classroom::query()
            ->with('translation')
            ->with('translations')
            ->with('university.translation')
            ->with('university.translations')
            ->where('id', $academic_degree->id)
            ->first();

        return response()->json([
            'data' => $data
        ], 201);
    }

    public function update(Request $request, Classroom $academic_degree)
    {
        $validator = Validator::make($request->all(), [
            'university_id' => 'required|integer|exists:universities,id',
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request, $academic_degree) {
	        $academic_degree->update([
	        	'university_id' => $request->university_id
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                	$academic_degree->translations()->where('lang', $key)->update([
                        'name' => $value['name']
                    ]);
                }
            }
	    });
        
    	return response()->json([
            'messages' => 'Ok'
        ], 200);
    }

    public function destroy(Classroom $academic_degree)
    {
        
    }
}
