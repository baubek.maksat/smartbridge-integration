<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StudentServiceCenterRoleAccessResource;

use App\Models\StudentServiceCenterRoleAccess;

use App\Mail\Confirm;


class StudentServiceCenterRoleAccessController extends Controller
{
    public function index(Request $request)
    {
		$accesses = StudentServiceCenterRoleAccess::query();
		
		//	filter
		
		if ($request->has('filterNot.slug')) {
			$accesses->where('slug', '<>', $request->filterNot['slug']);
		}
		
		$accesses = $accesses->paginate();

        return StudentServiceCenterRoleAccessResource::collection($accesses);
    }
}
