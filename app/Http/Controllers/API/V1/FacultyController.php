<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\FacultyResource;

use App\Models\FacultyAdministrator;
use App\Models\Faculty;
use App\Models\Cato;
use App\Models\User;

use Validator;
use DB;

class FacultyController extends Controller
{
	public function index(Request $request)
	{
		$faculties = Faculty::query();

		if ($request->has('filter.university_id') && $request->filter['university_id']) {
			$faculties->where('university_id', $request->filter['university_id']);
		}

		$faculties = $faculties->paginate($request->has('per_page') ? $request->per_page : 15);

		return FacultyResource::collection($faculties);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'administrator_id' => 'required|integer|exists:users,id',
			'state_id' => 'nullable|integer|exists:states,id',
			'translations' => [
				'required',
				'array'
			],
			'translations.kz' => [
				'required',
				'array'
			],
			'translations.kz.name' => [
				'required',
				'string',
				'max:255'
			],
			'translations.ru' => [
				'required',
				'array'
			],
			'translations.ru.name' => [
				'required',
				'string',
				'max:255'
			],
			'translations.en' => [
				'required',
				'array'
			],
			'translations.en.name' => [
				'required',
				'string',
				'max:255'
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$data = DB::transaction(function() use ($request) {
			if ($request->has('administrator_id')) {
				$faculty_administrator = FacultyAdministrator::query()
					->where('id', $request->administrator_id)
					->first();

				if (!$faculty_administrator) {
					$faculty_administrator = FacultyAdministrator::create([
						'id' => $request->administrator_id
					]);
				}
			}

			$faculty = Faculty::create([
				'administrator_id' => $request->has('administrator_id') ? $request->administrator_id : null,
				'state_id' => $request->state_id
			]);

			if ($request->has('translations')) {
				foreach ($request->translations as $key => $value) {
					$faculty->translations()->create([
						'lang' => $key,
						'name' => $value['name']
					]);
				}
			}

			if ($request->has('administrator_id')) {
				$card = $faculty->administratorCards()->create([
					'faculty_id' => $faculty->id,
					'user_id' => $request->administrator_id,
					'date_start' => date('Y-m-d'),
				]);
			}

			return [
				'faculty' => $faculty
			];
		});

		return response()->json([
			'code' => 201,
			'data' => $data['faculty']
		], 200);
	}

	public function show(Faculty $faculty)
	{
		return new FacultyResource($faculty);
	}

	public function update(Request $request, Faculty $faculty)
	{
		$validator = Validator::make($request->all(), [
			'state_id' => [
				'required',
				'integer',
				'exists:states,id'
			],
			'administrator_id' => [
				'nullable',
				'integer',
				'exists:users,id'
			],
			'translations' => [
				'required',
				'array'
			],
			'translations.kz' => [
				'required',
				'array'
			],
			'translations.kz.name' => [
				'required',
				'string',
				'max:255'
			],
			'translations.ru' => [
				'required',
				'array'
			],
			'translations.ru.name' => [
				'required',
				'string',
				'max:255'
			],
			'translations.en' => [
				'required',
				'array'
			],
			'translations.en.name' => [
				'required',
				'string',
				'max:255'
			]
		]);

		DB::transaction(function() use ($faculty, $request) {
			$faculty->update([
				'state_id' => $request->state_id
			]);

			if ($request->has('translations')) {
				foreach ($request->translations as $key => $value) {
					$faculty->translations()->where('lang', $key)->update([
						'name' => $value['name']
					]);
				}
			}
		});


		$data = DB::transaction(function() use ($request, $faculty) {
			if ($request->has('administrator_id')) {
				$faculty_administrator = FacultyAdministrator::query()
					->where('id', $request->administrator_id)
					->first();

				if (!$faculty_administrator) {
					$faculty_administrator = FacultyAdministrator::create([
						'id' => $request->administrator_id
					]);
				}

				if ($request->administrator_id != $faculty->administrator_id) {
					$faculty->administratorCards()->where('user_id', $faculty->administrator_id)->update([
						'date_end' => date('Y-m-d')
					]);

					$faculty->administratorCards()->create([
						'faculty_id' => $faculty->id,
						'user_id' => $request->administrator_id,
						'date_start' => date('Y-m-d'),
					]);
				}
			}

			$faculty->update([
				'administrator_id' => $request->has('administrator_id') ? $request->administrator_id : null,
				'state_id' => $request->state_id
			]);

			if ($request->has('translations')) {
				foreach ($request->translations as $key => $value) {
					$faculty->translations()->where('lang', $key)->update([
						'name' => $value['name']
					]);
				}
			}
		});
		
		return response()->json([
			'code' => 200
		], 201);
	}

	public function destroy(University $university)
	{
		DB::transaction(function() use ($university) {
			$university = University::create([
				'type_id' => $request->type_id,
				'country_id' => $request->country_id,
				'cato_id' => Cato::where('country_id', $request->country_id)->count() > 0 ? $request->cato_id : null,
				'faculty_type_id' => $request->faculty_type_id,
				'department_type_id' => $request->department_type_id,
				'speciality_type_id' => $request->speciality_type_id,
				'administrator_worker_position_id' => $request->administrator_worker_position_id,
				'faculty_administrator_worker_position_id' => $request->faculty_administrator_worker_position_id,
				'department_administrator_worker_position_id' => $request->department_administrator_worker_position_id,
				'bin' => $request->bin,
				'website' => $request->website,
				'begin_of_work' => $request->begin_of_work
			]);

			$university->studyLanguages()->attach($request->study_languages);

			if ($request->has('translations')) {
				foreach ($request->translations as $key => $value) {
					$university->translations()->create([
						'lang' => $key,
						'name' => $value['name'],
						'address' => $value['address'],
						'cato_name' => Cato::where('country_id', $request->country_id)->count() == 0 ? $value->cato_name : null
					]);
				}
			}

			if ($request->has('emails')) {
				foreach ($request->emails as $email) {
					$university->emails()->create([
						'value' => $email
					]);
				}
			}

			if ($request->has('phones')) {
				foreach ($request->phones as $phone) {
					$university->phones()->create([
						'value' => $phone
					]);
				}
			}
		});
		dd($university);
	}
}
