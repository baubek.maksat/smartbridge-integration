<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StateResource;

use App\Models\State;

use Validator;

class StateController extends Controller
{
	public function index(Request $request)
	{
		$states = State::query();

		$states = $states->paginate();

		return StateResource::collection($states);
	}
}
