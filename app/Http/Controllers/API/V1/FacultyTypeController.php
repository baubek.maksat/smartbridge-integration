<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\FacultyTypeResource;

use App\Models\FacultyType;

use Validator;

class FacultyTypeController extends Controller
{
    public function index(Request $request)
    {
		$faculty_types = FacultyType::query();
		
		$faculty_types = $faculty_types->paginate();

    	return FacultyTypeResource::collection($faculty_types);
    }
}
