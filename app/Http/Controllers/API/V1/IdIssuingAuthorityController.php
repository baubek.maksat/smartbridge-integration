<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\IdIssuingAuthorityResource;

use App\Models\IdIssuingAuthority;

use Validator;

class IdIssuingAuthorityController extends Controller
{
    public function index(Request $request)
    {
    	$id_issuing_authorities = IdIssuingAuthority::query();

        $id_issuing_authorities = $id_issuing_authorities->paginate();

    	return IdIssuingAuthorityResource::collection($id_issuing_authorities);
    }
}
