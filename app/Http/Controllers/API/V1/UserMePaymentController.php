<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\PaymentResource;

use App\Models\Payment;

use Validator;
use DB;

class UserMePaymentController extends Controller
{
    public function index(Request $request)
    {
        $payments = Payment::query()
            ->paginate();

        return PaymentResource::collection($payments);
    }

    public function show(Request $request, Payment $payment)
    {
        return new PaymentResource($payment);
    }
}
