<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\Type;
use App\Models\Lang;

use App\Mail\Confirm;

use Validator;
use Mail;
use DB;
use UA;

class UserMeEmailController extends Controller
{
	public function update(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => [
				'required',
				'email',
				Rule::unique('users', 'email')
			]
		]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }
		
		$data = DB::transaction(function() use ($request) {
			$user = auth()->user();
			
			$user->update([
				'email' => $request->email
			]);
			
			$confirm = $user->emailConfirms()->create([
				'type_id' => Type::where('slug', $request->header('type'))->first()->id,
				'lang_id' => Lang::where('slug', app()->getLocale())->first()->id,
				'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
				'ip' => $request->server('REMOTE_ADDR'),
				'ua' => $request->server('HTTP_USER_AGENT'),
				'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
				'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
				'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
				'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
				'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
				'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
				'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
				'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
				'hash' => sha1(rand(1000, 9999)),
				'src' => mb_strtolower($request->email)
			]);
			
			 return [
				 'confirm' => $confirm
			 ];
		});
		
		Mail::to($request->email)->send(new Confirm($data['confirm']));
		
		return response()->json([
			'code' => 200,
			'data' => [
				'email' => auth()->user()->email
			]
		], 200);
	}
}
