<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\GrantTypeResource;

use App\Models\GrantType;

use Validator;

class GrantTypeController extends Controller
{
    public function index(Request $request)
    {
		$grant_types = GrantType::query();

		if ($request->has('filter.payment_form_id')) {
			$grant_types->where('payment_form_id', $request->filter['payment_form_id']);
		}
		
		$grant_types = $grant_types->paginate();

    	return GrantTypeResource::collection($grant_types);
    }
}
