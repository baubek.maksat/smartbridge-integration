<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\HsResource;

use App\Models\User;
use App\Models\Hs;

use Validator;
use DB;

class HsController extends Controller
{
	public function index(Request $request)
    {
		$hss = Hs::query()
			->has('category');
		
		$hss->whereHas('category', function($query) {
			if (auth()->user()->currentRole()->slug == 'ssc-head') {
				$query->where('university_id', auth()->user()->sscs[0]->university_id);
			}
			
			if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
				$query->where('university_id', auth()->user()->sscs[0]->university_id);
			}
			
			if (auth()->user()->currentRole()->slug == 'ssc-employee') {
				$query->where('university_id', auth()->user()->sscs[0]->university_id);
			}
		});
		
		if ($request->has('filter.category_id')) {
			$hss->where('category_id', $request->filter['category_id']);
		}
		
		if ($request->has('filter.state_id')) {
			$hss->where('state_id', $request->filter['state_id']);
		}
		
		if ($request->has('filter.query')) {
			$hss->whereHas('translations', function($query) use ($request) {
				$query
					->where('question', 'LIKE', $request->filter['query'])
					->orWhere('answer', 'LIKE', $request->filter['query']);
			});
		}
			
		$hss = $hss->paginate();
			
    	return HsResource::collection($hss);
    }
	
    public function show(Hs $hs)
    {
    	return new HsResource($hs);
    }

    public function store(Request $request)
    {
    	$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
            'state_id' => 'required|exists:states,id',
            'langs' => 'required|array|exists:langs,slug',
			'category_id' => 'required|exists:hs_categories,id',
			'translations' => 'required',
			'translations.kz.question' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})
			],
			'translations.kz.answer' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:5000'
			],
			'translations.ru.question' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})
			],
			'translations.ru.answer' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:5000'
			],
			'translations.en.question' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:255',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})
			],
			'translations.en.answer' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en');
				}),
				'string',
				'max:5000'
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }

        $data = DB::transaction(function() use ($request, $ssc) {
			$hs = Hs::create([
				'university_id' => $ssc->university->id,
				'category_id' => $request->category_id,
				'state_id' => $request->state_id
			]);
			
            foreach ($request->translations as $lang => $translation) {
				$hs->translations()->create([
					'lang' => $lang,
					'question' => $translation['question'],
					'answer' => $translation['answer']
				]);
			}

			return $hs;
        });

        return response()->json([
			'code' => 201,
			'data' => new HsResource($data)
		], 200);
    }

    public function update(Hs $hs, Request $request)
    {
    	if (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee') {
    		$ssc = auth()->user()->sscs[0];
    	}

    	if (auth()->user()->currentRole()->slug == 'student') {
    		$ssc = auth()->user()->student->card->group->speciality->department->faculty->university->studentServiceCenter;
    	}
    	
		
		$validator = Validator::make($request->all(), [
            'quiz' => [
				'nullable',
				Rule::requiredIf(function () use ($request, $ssc){
					return !$request->has('translations');
				})
			],
			'state_id' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return !$request->has('quiz');
				}),
				'exists:states,id'
			],
			'category_id' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return !$request->has('quiz');
				}),
				'exists:hs_categories,id'
			],
			'translations' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return !$request->has('quiz');
				})
			],
			'translations.kz.question' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz') && (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee');
				}),
				'string',
				'max:255',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs->id, 'id')
			],
			'translations.kz.answer' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('kz') && (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee');
				}),
				'string',
				'max:5000',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs->id, 'id')
			],
			'translations.ru.question' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru') && (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee');
				}),
				'string',
				'max:255',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs->id, 'id')
			],
			'translations.ru.answer' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('ru') && (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee');
				}),
				'string',
				'max:5000',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs->id, 'id')
			],
			'translations.en.question' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en') && (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee');
				}),
				'string',
				'max:255',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs->id, 'id')
			],
			'translations.en.answer' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return collect($request->langs)->contains('en') && (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee');
				}),
				'string',
				'max:5000',
				Rule::unique('hs_langs', 'question')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->university->hsCategories()->pluck('id'));
				})->ignore($hs->id, 'id')
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $data = DB::transaction(function() use ($request, $hs, $ssc) {
            if (!$hs->hasUser(auth()->user()->id) && $request->has('quiz')) {
                $hs->update([
                    'per' => $request->quiz == 1 ? $hs->per + 1 : $hs->per,
                    'vs' => $request->quiz == 0 ? $hs->vs + 1 : $hs->vs
                ]);

                $hs->users()->attach(auth()->user(), [
                	'vote' => $request->quiz == 1 ? 'per' : 'vs'
                ]);
            }
			
			if ($request->has('translations') && !$request->has('quiz')) {
				$hs->update([
					'university_id' => $ssc->university->id,
					'category_id' => $request->category_id,
					'state_id' => $request->state_id
				]);

				$hs->translations()->whereNotIn('lang', $request->langs)->delete();

				foreach ($request->translations as $lang => $translation) {
					if ($hs->translations()->where('lang', $lang)->first()) {
						$hs->translations()->where('lang', $lang)->update([
							'question' => $translation['question'],
							'answer' => $translation['answer']
						]);
					} else {
						$hs->translations()->create([
							'lang' => $lang,
							'question' => $translation['question'],
							'answer' => $translation['answer']
						]);
					}
				}
			}

			return $hs;
        });

        return response()->json([
			'code' => 200,
			'data' => $data
		], 200);
    }
	
	public function destroy(Hs $hs)
	{
		$hs->delete();
		
		return response()->json([
			'code' => 204
		], 200);
	}
}
