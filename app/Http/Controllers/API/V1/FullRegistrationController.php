<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\StudentCard;
use App\Models\Country;
use App\Models\Lang;
use App\Models\User;
use App\Models\Type;
use App\Models\Role;

use App\Mail\Confirm;

use Validator;
use Storage;
use Mail;
use Str;
use DB;
use UA;

class FullRegistrationController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'role_id' => [
				'required',
				'integer',
				'exists:roles,id',
				Rule::in(Role::whereIn('slug', ['student'])->get()->pluck('id')),
			],
			'image' => [
				'required',
				'image'				
			],
			'nationality_id' => [
				'required',
				'exists:nationalities,id'
			],
			'marital_status_id' => [
				'required',
				'exists:marital_statuses,id'
			],
			'id_type_id' => [
				'required',
				'exists:id_types,id'
			],
			'citizenship_id' => [
				'required',
				'exists:countries,id'
			],
			'id_issuing_authority_id' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->citizenship_id)
						->where('code', 'KZ')
						->first();
				}),
				'exists:id_issuing_authorities,id'
			],
			'id_number' => [
				'required',
				'string',
				'max:255'
			],
			'id_series' => [
				'required',
				'string',
				'max:255'
			],
			'id_src' => [
				'required',
				'file'
			],
			'id_date_start' => [
				'required',
				'date_format:Y-m-d'
			],
			'id_date_end' => [
				'nullable',
				'date_format:Y-m-d'
			],
			'phone' => [
				'required',
				'string',
				'max:11',
				'unique:users,phone'
			],
			'registration_cato_id' => [
				'required',
				'exists:catos,id'
			],
			'registration_address' => [
				'required',
				'string',
				'max:255'
			],
			'living_cato_id' => [
				'required',
				'exists:catos,id'
			],
			'living_address' => [
				'required',
				'string',
				'max:255'
			],
			'birth_country_id' => [
				'required',
				'exists:countries,id'
			],
			'birth_cato_id' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->birth_country_id)
						->where('code', 'KZ')
						->first();
				}),
				'exists:catos,id'
			],
			'birth_cato_name' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->birth_country_id)
						->where('code', '<>', 'KZ')
						->first();
				}),
				'string',
				'max:255'
			],
			'arrived_country_id' => [
				'required',
				'exists:countries,id'
			],
			'arrived_cato_id' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->arrived_country_id)
						->where('code', 'KZ')
						->first();
				}),
				'exists:catos,id'
			],
			'arrived_cato_name' => [
				Rule::requiredIf(function () use ($request) {
					return Country::query()
						->where('id', $request->arrived_country_id)
						->where('code', '<>', 'KZ')
						->first();
				}),
				'string',
				'max:255'
			],
			'study_form_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:study_forms,id'
			],
			'study_language_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:study_languages,id'
			],
			'payment_form_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:payment_forms,id'
			],
			'group_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:groups,id'
			],
			'course_number' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'integer',
				'min:1',
				'max:9'
			],
			'grant_type_id' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'exists:grant_types,id'
			],
			'date_start' => [
				Rule::requiredIf(function () use ($request) {
					return Role::query()
						->where('id', $request->role_id)
						->where('slug', 'student')
						->first();
				}),
				'date_format:Y-m-d'
			]
		]);

		if ($validator->fails()) {
            return response()->json([
				'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }

        $data = DB::transaction(function() use ($request) {
			$role = Role::query()
				->where('id', $request->role_id)
				->first();

			$user = User::find(auth()->user()->id);

			$user->update([
				'surname_en' => $request->surname_en,
				'name_en' => $request->name_en,
				'patronymic_en' => $request->has('patronymic_en') ? $request->patronymic_en : null,
				'phone' => $request->phone,
				'image' => 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $request->image, rand(1000, 9999).'.'.$request->image->extension()),
				'marital_status_id' => $request->marital_status_id,
				'nationality_id' => $request->nationality_id,
				'registration_cato_id' => $request->registration_cato_id,
				'living_cato_id' => $request->living_cato_id,
				'birth_country_id' => $request->birth_country_id,
				'birth_cato_id' => $request->has('birth_cato_id') ? $request->birth_cato_id : null
			]);

			$user = User::find(auth()->user()->id);

			foreach (['kz', 'ru', 'en'] as $lang) {
				$user->translations()->create([
					'lang' => $lang,
					'birth_cato_name' => $request->has('birth_cato_name') ? $request->birth_cato_name : null,
					'registration_address' => $request->registration_address,
					'living_address' => $request->living_address
				]);
			}

			$user->identificationDocument()->create([
				'user_id' => auth()->user()->id,
				'type_id' => $request->id_type_id,
				'citizenship_id' => $request->citizenship_id,
				'issuing_authority_id' => $request->has('id_issuing_authority_id') ? $request->id_issuing_authority_id : null,
				'number' => $request->id_number,
				'series' => $request->id_series,
				'date_start' => $request->has('id_date_start') ? $request->id_date_start : null,
				'date_end' => $request->has('id_date_end') ? $request->id_date_end : null,
				'src' => 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $request->id_src, rand(1000, 9999).'.'.$request->id_src->extension())
			]);
			
			if ($role->slug == 'student') {
				$student_card = StudentCard::create([
					'user_id' => auth()->user()->id,
					'arrived_country_id' => $request->arrived_country_id,
					'arrived_cato_id' => $request->has('arrived_cato_id') ? $request->arrived_cato_id : null,
					'study_language_id' => $request->study_language_id,
					'study_form_id' => $request->study_form_id,
					'grant_type_id' => $request->grant_type_id,
					'date_start' => $request->date_start,
					'course_number' => $request->course_number,
					'group_id' => $request->group_id,
					'payment_form_id' => $request->payment_form_id,
					'gpa' => 0.00,	//	Потом нахер удалить этот кусок кода
					'state_id' => '1'
				]);

				foreach (['kz', 'ru', 'en'] as $lang) {
					$student_card->translations()->create([
						'lang' => $lang,
						'arrived_cato_name' => $request->has('arrived_cato_name') ? $request->arrived_cato_name : null,
					]);
				}
			}
		});

		return response()->json([
			'code' => 200
		], 200);
	}
}
