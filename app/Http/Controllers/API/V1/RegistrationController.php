<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Lang;
use App\Models\User;
use App\Models\Type;
use App\Models\Role;

use App\Mail\Confirm;

use Validator;
use Mail;
use Str;
use DB;
use UA;

class RegistrationController extends Controller
{
    public function convert_word($to, $word) {
        return $word;
    } 

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'surname' => 'required|string|max:255',
            'name' => 'required|string|max:255' ,
            'patronymic' => 'nullable|string|max:255',
            'birth_date' => 'required|date_format:Y-m-d',
            'gender_id' => 'required|exists:genders,id',
            'email' => 'required|email|unique:users,email'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }

        $user = User::query()
            ->where('surname', $request->surname)
            ->where('name', $request->name)
            ->where('birth_date', $request->birth_date)
            ->first();

        if ($user) {
            return response()->json([
                'code' => 422,
                'messages' => [
                    'user' => 'The user has already been taken.'
                ]
            ], 200);
        }

        $confirm = DB::transaction(function() use ($request) {
            $lang = Lang::query()
                ->where('slug', app()->getLocale())
                ->first();

            $role = Role::query()
                ->where('slug', 'no-role')
                ->first();
			
			$user = User::create([
                'surname' => mb_convert_case(mb_strtolower($request->surname), MB_CASE_TITLE),
                'name' => mb_convert_case(mb_strtolower($request->name), MB_CASE_TITLE),
                'patronymic' => mb_convert_case(mb_strtolower($request->patronymic), MB_CASE_TITLE),
                'username' => Str::slug($request->surname.'_'.$request->name.'_'.rand(1000, 9999), '_'),
				'password' => bcrypt(sha1($request->email)),
                'birth_date' => $request->birth_date,
                'gender_id' => $request->gender_id,
                'email' => mb_strtolower($request->email),
                'default_lang_id' => $lang->id,
                'default_role_id' => $role->id
            ]);

            /*

            foreach (['kz', 'ru', 'en'] as $lang) {
            	$user->translations()->create([
					'lang' => $lang,
					'surname' => $this->convert_word($lang, $request->surname),
					'name' => $this->convert_word($lang, $request->name),
					'patronymic' => $request->patronymic ? $this->convert_word($lang, $request->patronymic) : null
				]);
            }

            */

            $user->roles()->attach($role);

            $confirm = $user->emailConfirms()->create([
				'type_id' => Type::where('slug', $request->header('type'))->first()->id,
				'lang_id' => Lang::where('slug', app()->getLocale())->first()->id,
				'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
				'ip' => $request->server('REMOTE_ADDR'),
				'ua' => $request->server('HTTP_USER_AGENT'),
				'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
				'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
				'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
				'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
				'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
				'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
				'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
				'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
				'hash' => sha1(rand(1000, 9999)),
				'src' => mb_strtolower($request->email)
			]);

            return $confirm;
        });
		
		Mail::to($confirm->src)
			->queue(new Confirm($confirm));

        return response()->json([
            'code' => 201
        ], 200);
    }
}
