<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\MaritalStatusResource;

use App\Models\MaritalStatus;

use Validator;

class MaritalStatusController extends Controller
{
    public function index(Request $request)
    {
    	$marital_statuses = MaritalStatus::query();

        $marital_statuses = $marital_statuses->paginate();

    	return MaritalStatusResource::collection($marital_statuses);
    }
}
