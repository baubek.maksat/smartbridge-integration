<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Feedback;
use App\Models\Visit;

use Validator;

class FeedbackController extends Controller
{
    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
			'essence' => 'required|string|max:255',
            'more' => 'required|string|max:5000',
            'files' => 'nullable|array|max:20',
            'files.*' => 'required|file|max:5000'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		Feedback::create([
			'university_id' => auth()->user()->student->card->group->speciality->department->faculty->university->id,
            'visit_id' => Visit::where('token', request()->bearerToken())->first()->id,
            'essence' => $request->essence,
            'more' => $request->more
        ]);

    	return response()->json([
            'code' => 201
        ], 200);
    }
}
