<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StatementSectionResource;

use App\Models\StatementSection;

class StatementSectionController extends Controller
{
    public function index(Request $request)
    {
    	$statement_sections = StatementSection::query()
            ->paginate();

        return StatementSectionResource::collection($statement_sections);
    }

    public function show(Request $request, StatementSection $statement_section)
    {
    	return new StatementSectionResource($statement_section);
    }
}
