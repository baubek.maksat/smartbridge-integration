<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\SpecialityTypeResource;

use App\Models\SpecialityType;

use Validator;

class SpecialityTypeController extends Controller
{
    public function index(Request $request)
    {
		$speciality_types = SpecialityType::query();
		
		$speciality_types = $speciality_types->paginate();

    	return SpecialityTypeResource::collection($speciality_types);
    }
}
