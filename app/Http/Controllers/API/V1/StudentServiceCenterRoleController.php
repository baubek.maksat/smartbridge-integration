<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

use App\Http\Resources\StudentServiceCenterEmployeeResource;
use App\Http\Resources\StudentServiceCenterRoleResource;

use App\Models\Type;
use App\Models\Lang;
use App\Models\Role;
use App\Models\User;
use App\Models\WorkerPosition;
use App\Models\StudentServiceCenter;
use App\Models\StudentServiceCenterRole;

use App\Mail\Confirm;

use Validator;
use Mail;
use DB;
use UA;

class StudentServiceCenterRoleController extends Controller
{
    public function index(Request $request)
    {
		$roles = StudentServiceCenterRole::query();

		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$roles->whereHas('studentServiceCenter', function($query){
				$query->where('id', auth()->user()->sscs[0]->id);
			});
		}

		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$roles->whereHas('studentServiceCenter', function($query){
				$query->where('id', auth()->user()->sscs[0]->id);
			});
		}

		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$roles->whereHas('studentServiceCenter', function($query){
				$query->where('id', auth()->user()->sscs[0]->id);
			});
		}
		
		//	filter
		
		if ($request->has('filter.state_id')) {
			$roles->where('state_id', $request->filter['state_id']);
		}
		
		if ($request->has('filter.name')) {
			$roles->whereHas('translations', function($query) use ($request) {
				$query->where('name', 'LIKE', $request->filter['name']);
			});
		}
		
		$roles = $roles->paginate(100);

        return StudentServiceCenterRoleResource::collection($roles);
    }
	
	public function store(Request $request)
	{
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'state_id' => [
				'required',
				'exists:states,id'
			],
			'access_id' => [
				'required',
				'exists:student_service_center_role_accesses,id'
			],
			'translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz');
				}),
				'string',
				'max:255',
				Rule::unique('student_service_center_role_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->roles()->pluck('id'));
				})
			],
			'translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('ru');
				}),
				'string',
				'max:255',
				Rule::unique('student_service_center_role_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->roles()->pluck('id'));
				})
			],
			'translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en');
				}),
				'string',
				'max:255',
				Rule::unique('student_service_center_role_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->roles()->pluck('id'));
				})
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}

		$data = DB::transaction(function() use ($request, $ssc) {
			$role = StudentServiceCenterRole::create([
				'student_service_center_id' => $ssc->id,
				'state_id' => $request->state_id,
				'access_id' => $request->access_id
			]);

			foreach ($request->translations as $lang => $translation) {
				$role->translation()->create([
					'lang' => $lang,
					'name' => $translation['name']
				]);
			}
			
			return $role;
		});
		
		return response()->json([
			'code' => 201,
			'data' => $data
		], 200);
	}
	
	public function update(Request $request, StudentServiceCenterRole $role)
	{
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'state_id' => [
				'required',
				'exists:states,id'
			],
			'access_id' => [
				'required',
				'exists:student_service_center_role_accesses,id'
			],
			'translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz');
				}),
				'string',
				'max:255',
				Rule::unique('student_service_center_role_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->roles()->pluck('id'));
				})->ignore($role->id)
			],
			'translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('ru');
				}),
				'string',
				'max:255',
				Rule::unique('student_service_center_role_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->roles()->pluck('id'));
				})->ignore($role->id)
			],
			'translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en');
				}),
				'string',
				'max:255',
				Rule::unique('student_service_center_role_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->roles()->pluck('id'));
				})->ignore($role->id)
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		DB::transaction(function() use ($request, $role) {
			$role->update([
				'state_id' => $request->state_id,
				'access_id' => $request->access_id
			]);
			
			foreach ($request->translations as $lang => $translation) {
				if ($role->translations()->where('lang', $lang)->first()) {
					$role->translations()->where('lang', $lang)->update([
						'name' => $translation['name']
					]);
				}
				
				if (!$role->translations()->where('lang', $lang)->first()) {
					$role->translations()->create([
						'lang' => $lang,
						'name' => $translation['name']
					]);
				}
			}
		});
		
		return response()->json([
			'code' => 200
		], 200);
	}

    public function show(StudentServiceCenterRole $role)
    {
		return new StudentServiceCenterRoleResource($role);
    }
	
	public function destroy(StudentServiceCenterRole $role)
    {
		if ($role->access->slug == 'ssc-head' || $role->users()->count() > 0) {
			return response()->json([
				'code' => 403
			], 200);
		}
		
		$role->delete();

		return response()->json([
			'code' => 204
		], 200);
    }
}
