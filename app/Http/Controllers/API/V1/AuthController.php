<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\UserResource;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Validation\Rule;

use App\Mail\Confirm;
use App\Mail\Welcome;
use App\Mail\Restore;

use App\Models\UserEmailConfirm;
use App\Models\UserEmailRestore;
use App\Models\UserRole;
use App\Models\Visit;
use App\Models\User;
use App\Models\Type;
use App\Models\Lang;
use App\Models\Cato;
use App\Models\App;

use Validator;
use Mail;
use UA;
use DB;

class AuthController extends Controller
{
	public function login(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'username' => 'required',
			'password' => 'required',
			'lang' => 'nullable|exists:langs,slug',
			'app_id' => 'nullable|exists:apps,id'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages(),
			], 200);
		}

		$user = User::query()
			->where('integration_fields->platonus->username', $request->username)
			->first();

		$attempt = [
			'with_md5' => [
				'username' => $user ? $user->username : $request->username,
				'password' => md5($request->password),
				'email_verified' => '1',
				'access' => '1'
			],
			'without_md5' => [
				'username' => $user ? $user->username : $request->username,
				'password' => $request->password,
				'email_verified' => '1',
				'access' => '1'
			]
		];

		if (!auth()->attempt($attempt['with_md5']) && !auth()->attempt($attempt['without_md5'])) {
			return response()->json([
				'code' => 401
			], 200);
		}

		$data = DB::transaction(function() use ($request) {
			$type = Type::query()
				->where('slug', $request->header('type'))
				->first();
			
			$lang = Lang::query()
				->where('slug', app()->getLocale())
				->first();

			$token = auth()->user()->createToken($type->slug)->accessToken;

			if (!auth()->user()->default_lang_id) {
				auth()->user()->update([
					'default_lang_id' => Lang::where('slug', $request->has('lang') ? $request->lang : 'kz')->first()->id
				]);
			}
			
			$visit = auth()->user()->visits()->create([
				'role_id' => auth()->user()->default_role_id ? auth()->user()->default_role_id : auth()->user()->roles[0]->id,
				'lang_id' => $lang->id,
				'type_id' => $type->id,
				'app_id' => $request->has('app_id') ? $request->app_id : null,
				'ip' => $request->server('REMOTE_ADDR'),
				'ua' => $request->server('HTTP_USER_AGENT'),
				'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
				'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
				'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
				'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
				'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
				'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
				'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
				'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
				'token' => $token
			]);
			
			//

			if ($request->has('app_id')) {
				$app = App::find($request->app_id);

				$app_old = auth()->user()->apps()->whereHas('release', function($query) use ($app){
					$query->where('version', '<', $app->release->version);
				})->where('registration_id', $app->registration_id)->first();

				if ($app_old) {
					auth()->user()->apps()->updateExistingPivot($app_old->id, [
						'deleted_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					]);
				}

				if (auth()->user()->hasApp($app->id)) {
					auth()->user()->apps()->updateExistingPivot($request->app_id, [
						'deleted_at' => null,
						'updated_at' => date('Y-m-d H:i:s')
					]);
				}

				if (!auth()->user()->hasApp($app->id)) {
					auth()->user()->apps()->attach($app, [
						'created_at' => date('Y-m-d H:i:s'),
						'updated_at' => date('Y-m-d H:i:s')
					]);
				}
			}
			
			//
			
			//	session(['token' => $token]);

			return [
				'token' => $token,
				'lang' => [
					'slug' => $visit->lang->slug,
					'name' => $visit->lang->name
				],
				'role' => [
					'slug' => $visit->role->slug,
					'name' => $visit->role->name
				]
			];
		});

		return response()->json([
			'code' => 200,
			'data' => $data
		], 200);
	}

	public function logout(Request $request)
	{
		if ($request->has('app_id')) {
			auth()->user()->apps()->updateExistingPivot($request->app_id, [
				'deleted_at' => date('Y-m-d H:i:s')
			]);
		}
		
		return response()->json([
			'code' => 200
		], 200);
	}

	public function registration(Request $request)
	{
		
	}





	public function confirm(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'hash' => [
				'required',
				Rule::exists('user_email_confirms')->where(function ($query) use ($request) {
					$query->where('fit', 1)->where('hash', $request->hash);
				})
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages() 
			], 422);
		}

		$result = DB::transaction(function() use ($request) {
			$email_confirm = UserEmailConfirm::query()
				->where('hash', $request->hash)
				->first();

			$email_confirm->update([
				'fit' => 0,
				'verified_at' => date('Y-m-d H:i:s')
			]);

			return $email_confirm;
		});

		return response()->json([], 200);
	}

	public function user()
	{
		$result = auth()->user();

		return (new UserResource($result))->additional([
			'code' => 200
		]);
	}

	public function updateImage(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'image' => 'required|file|mimes:jpg,jpeg,png|max:2048|dimensions:min_width=300,min_height=300,ratio=1/1'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages(),
			], 422);
		}

		auth()->user()->update([
			'image' => Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->image)
		]);

		return response()->json([], 200);
	}

	public function updateAddress(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'registration_cato_id' => 'required|exists:catos,id',
			'registration_address' => 'required|string|max:255',
			'living_cato_id' => 'required|exists:catos,id',
			'living_address' => 'required|string|max:255'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		DB::transaction(function() use ($request) {
			auth()->user()->update([
				'registration_cato_id' => $request->registration_cato_id,
				'living_cato_id' => $request->living_cato_id,
			]);

			auth()->user()->translation()->update([
				'registration_address' => $request->registration_address,
				'living_address' => $request->living_address
			]);
		});

		return response()->json([], 200);
	}

	public function updateAccount(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'username' => 'required|string|min:1|max:255|unique:users,username,'.Auth::user()->id,
			'password' => 'required|string|min:10|max:255'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		auth()->user()->update([
			'username' => $request->username,
			'password' => bcrypt($request->password)
		]);

		return response()->json([], 200);
	}

	public function updateEmail(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|email|max:255|unique:users'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$result = DB::transaction(function() use ($request) {
			$lang = Lang::query()
				->where('slug', app()->getLocale())
				->first();
			
			auth()->user()->update([
				'email' => $request->email
			]);

			auth()->user()->emailConfirms()->update([
				'fit' => 0
			]);

			$email_confirm = auth()->user()->emailConfirms()->create([
				'type_id' => $request->header('type_id'),
				'lang_id' => $lang->id,
				'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
				'ip' => $request->server('REMOTE_ADDR'),
				'ua' => $request->server('HTTP_USER_AGENT'),
				'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
				'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
				'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
				'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
				'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
				'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
				'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
				'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
				'hash' => sha1(rand(1000, 9999)),
				'src' => mb_strtolower($request->email)
			]);

			return $email_confirm;
		});

		Mail::to($result->src)->send(new Confirm($result));

		return response()->json([], 200);
	}

	public function updatePhone(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'phone' => 'required|regex:/[7][7]\d{9}/|unique:users'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		DB::transaction(function() use ($request) {
			auth()->user()->update([
				'phone' => $request->phone
			]);
		});

		return response()->json([], 200);
	}

	public function updatePersonal(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'surname' => 'required|string|max:255',
			'surname_en' => 'nullable|string|max:255',
			'name' => 'required|string|max:255',
			'name_en' => 'nullable|string|max:255',
			'patronymic' => 'nullable|string|max:255',
			'patronymic_en' => 'nullable|string|max:255',
			'birth_date' => 'required|date_format:Y-m-d',
			'birth_country_id' => 'required|exists:citizenships,id',
			'birth_cato_id' => [
				Rule::requiredIf(function () use ($request) {
					return Cato::where('citizenship_id', $request->birth_country_id)->count() > 0;
				}),
				'exists:catos,id'
			],
			'birth_cato_name' => [
				Rule::requiredIf(function () use ($request) {
					return Cato::where('citizenship_id', $request->birth_country_id)->count() == 0;
				}),
				'string',
				'max:255'
			],
			'nationality_id' => 'required|exists:nationalities,id',
			'gender_id' => 'required|exists:genders,id',
			'marital_status_id' => 'required|exists:marital_statuses,id',
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages(),
			], 422);
		}

		DB::transaction(function() use ($request) {
			auth()->user()->update([
				'birth_date' => $request->birth_date,
				'birth_citizenship_id' => $request->birth_citizenship_id,
				'birth_cato_id' => Cato::where('citizenship_id', $request->birth_citizenship_id)->count() > 0 ? $request->birth_cato_id : null,
				'nationality_id' => $request->nationality_id,
				'gender_id' => $request->gender_id,
				'marital_status_id' => $request->marital_status_id
			]);

			auth()->user()->translation()->update([
				'surname' => mb_convert_case(mb_strtolower($request->surname), MB_CASE_TITLE),
				'name' => mb_convert_case(mb_strtolower($request->name), MB_CASE_TITLE),
				'patronymic' => $request->has('patronymic') ? mb_convert_case(mb_strtolower($request->patronymic), MB_CASE_TITLE) : auth()->user()->translation()->patronymic,
				'birth_cato_name' => Cato::where('citizenship_id', $request->birth_citizenship_id)->count() == 0 ? $request->birth_cato_name : auth()->user()->translation()->birth_cato_name
			]);
		});

		return response()->json([], 200);
	}

	public function updatePreference(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'role_id' => 'required|exists:users_roles,role_id,user_id,'.auth()->user()->id,
			'lang_id' => 'required|exists:langs,id'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		auth()->user()->update([
			'default_role_id' => $request->role_id,
			'default_lang_id' => $request->lang_id
		]);

		return response()->json([], 200);
	}
}
