<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\UserResource;

use Illuminate\Validation\Rule;

use App\Models\User;
use App\Models\Lang;
use App\Models\Cato;
use App\Models\Type;

use Validator;
use DB;
use UA;

class UserController extends Controller
{
    public function index(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'per_page' => 'integer|between:1,50'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $per_page = $request->has('per_page') ? $request->per_page : null;

    	$users = User::query()
    		->paginate($per_page)
    		->appends([
    			'paginate' => $per_page
    		]);

    	return response()->json($users, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255|unique:users,username',
            'password' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'patronymic' => 'nullable|string|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            'phone' => 'nullable|string|max:255|unique:users,phone',
            'image' => 'nullable|file|mimes:jpg,jpeg,png|max:2048|dimensions:min_width=300,min_height=300,ratio=1/1',
            'birth_date' => 'required|date_format:Y-m-d',
            'birth_country_id' => 'nullable|integer|exists:countries,id',
            'birth_cato_id' => [
                Rule::requiredIf(function () use ($request) {
                    if ($request->birth_country_id != null) {
                        return Cato::where('country_id', $request->birth_country_id)->count() > 0;
                    } else {
                        return false;
                    }
                }),
                'required_with:birth_country_id',
                'integer',
                'exists:catos,id'
            ],
            'birth_cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    if ($request->birth_country_id != null) {
                        return Cato::where('country_id', $request->birth_country_id)->count() == 0;
                    } else {
                        return false;
                    }
                }),
                'required_with:birth_country_id',
                'string',
                'max:255'
            ],
            'arrived_country_id' => 'nullable|integer|exists:countries,id',
            'arrived_cato_id' => [
                Rule::requiredIf(function () use ($request) {
                    if ($request->arrived_country_id != null) {
                        return Cato::where('country_id', $request->arrived_country_id)->count() > 0;
                    } else {
                        return false;
                    }
                }),
                'integer',
                'exists:catos,id'
            ],
            'arrived_cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    if ($request->arrived_country_id != null) {
                        return Cato::where('country_id', $request->arrived_country_id)->count() == 0;
                    } else {
                        return false;
                    } 
                }),
                'string',
                'max:255'
            ],
            'registration_cato_id' => 'nullable|integer|exists:cato,id',
            'registration_address' => 'nullable|string|max:255',
            'living_cato_id' => 'nullable|integer|exists:cato,id',
            'living_address' => 'required_with:living_cato_id|string|max:255',
            'nationality_id' => 'nullable|integer|exists:nationalities,id',
            'gender_id' => 'required|integer|exists:genders,id',
            'marital_status_id' => 'nullable|integer|exists:marital_statuses,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request) {
            $lang = Lang::query()
                ->where('slug', app()->getLocale())
                ->first();

            $type = Type::query()
                ->where('slug', $request->header('type'))
                ->first();

            dd($type);

            $user = User::create([
                'username' => $request->username, 
                'password' => bcrypt($request->password),
                'email' => $request->email,
                'phone' => $request->phone,
                'birth_date' => $request->birth_date,
                'birth_country_id' => $request->birth_country_id,
                'birth_cato_id' => $request->birth_cato_id,
                'arrived_country_id' => $request->arrived_country_id,
                'arrived_cato_id' => $request->arrived_cato_id,
                'image' => Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->image),
                'registration_cato_id' => $request->registration_cato_id,
                'living_cato_id' => $request->living_cato_id,
                'gender_id' => $request->gender_id,
                'marital_status_id' => $request->marital_status_id,
                'nationality_id' => $request->nationality_id,
                'default_role_id' => $request->default_role_id,
                'default_lang_id' => $request->default_lang_id,
            ]);

            $user->translation([
                'surname' => $request->surname,
                'name' => $request->name,
                'patronymic' => $request->patronymic,
                'birth_cato_name' => $request->birth_cato_name,
                'arrived_cato_name' => $request->arrived_cato_name,
                'registration_address' => $request->registration_address,
                'living_address' => $request->living_address
            ]);

            $user->emailConfirms([
                'lang_id' => $lang->id,
                'type_id' => $type->id,
                'value' => $request->email,
            ]);
        });

        return response()->json([
            'message' => 'Created'
        ], 201);
    }

    public function show(Request $request, User $user)
    {
    	$user = User::query()
    		->with([
    			'birthCountry',
	            'birthCato',
	            'registrationCato',
	            'livingCato',
	            'gender',
	            'maritalStatus.genders',
	            'nationality.genders',
	            'roles',
	            'sec.country',
	            'sec.cato',
	            'id.type',
	            'id.citizenship'
    		])
    		->where('id', $user->id)
    		->first();

    	return response()->json($user, 200);
    }
}
