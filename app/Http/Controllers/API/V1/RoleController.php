<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\RoleResource;

use App\Models\Role;

use Validator;

class RoleController extends Controller
{
	public function index(Request $request)
    {
    	$roles = Role::query();

    	if (auth()->user()->currentRole()->slug == 'no-role') {
    		//	$roles->whereIn('slug', ['student', 'enrollee', 'graduate']);
    		$roles->whereIn('slug', ['student']);
    	}

    	$roles = $roles->paginate();

    	return RoleResource::collection($roles);
    }
}
