<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\TransactionResource;

use App\Models\Transaction;

use Validator;
use DB;

class TransactionController extends Controller
{
	public function index(Request $request)
	{
		$transactions = Transaction::query();

		/*

		if (auth()->user()->currentRole()->slug == 'content-manager') {
			$transactions->whereHas('department.faculty.university', function($query) use ())
		}

		*/
		
		//	filter

		if ($request->has('filter.account_id')) {
			$transactions->where('account_id', $request->filter['account_id']);
		}

		$transactions->whereHas('account.user', function ($query) use ($request) {
			if (isset($request->filter['fullname'])) {
				$query->where('name', 'LIKE', $request->filter['fullname']);
			}
		});

		$transactions->whereHas('account.user.identityDocument', function ($query) use ($request) {
			if ($request->has('filter.tin')) {
				$query->where('tin', 'LIKE', $request->filter['tin']);
			}
		});
		
		//	order by
		
		if ($request->has('orderBy.id')) {
			$transactions->orderBy('id', 'desc');
		}
		
		//	Role
		
		if (auth()->user()->currentRole()->slug == 'student') {
			
		}
		
		//

		$transactions = $transactions->paginate();

		return TransactionResource::collection($transactions);
	}

	public function show(Request $request, Transaction $Transaction)
	{
		return new TransactionResource($Transaction);
	}
 }
