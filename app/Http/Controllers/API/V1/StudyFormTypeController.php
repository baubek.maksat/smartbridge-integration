<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StudyFormTypeResource;

use App\Models\StudyFormType;

use Validator;

class StudyFormTypeController extends Controller
{
    public function index(Request $request)
    {
		$study_form_types = StudyFormType::query();
		
		$study_form_types = $study_form_types->paginate();

    	return StudyFormTypeResource::collection($study_form_types);
    }
}
