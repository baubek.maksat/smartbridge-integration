<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\ReplenishmentView;
use App\Models\TransactionView;
use App\Models\Transaction;
use App\Models\Lang;

use App\Mail\Confirm;

use Validator;
use Mail;
use DB;
use UA;

class UserMeReplenishmentController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'view' => [
				'required',
				'exists:replenishment_views,slug'
			],
			'sum' => [
				'required',
				'integer',
				'min:1',
				'max:1000000'
			],
			'card' => [
				'required_if:view,online-banking',
				'array'
			],
			'card.number' => [
				'required_if:view,online-banking',
				'digits:16'
			],
			'card.y' => [
				'required_if:view,online-banking',
				'digits:2'
			],
			'card.m' => [
				'required_if:view,online-banking',
				'digits:2'
			],
			'card.cvc' => [
				'required_if:view,online-banking',
				'digits:3'
			]
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}

		$data = DB::transaction(function() use ($request) {
			if ($request->view == 'online-banking') {
				$transaction_view = TransactionView::query()
					->where('slug', 'replenishment')
					->first();

				$replenishment_view = ReplenishmentView::query()
					->where('slug', 'online-banking')
					->first();

				$transaction = Transaction::create([
					'account_id' => auth()->user()->account->id,
					'view_id' => $transaction_view->id,
					'sum' => $request->sum
				]);

				$transaction->replenishment()->create([
					'view_id' => $replenishment_view->id
				]);

				auth()->user()->account()->update([
					'balance' => $request->sum + auth()->user()->account->balance
				]);
			}
		});
		
		return response()->json([
			'code' => 201,
			'data' => $data
		], 201);
	}
}
