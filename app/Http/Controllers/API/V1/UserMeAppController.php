<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;

class UserMeAppController extends Controller
{
    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'id' => 'required|exists:apps,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }
		
		if (!auth()->user()->hasApp($request->id)) {
			auth()->user()->apps()->attach($request->id, [
                'created_at' => date('Y-m-d H:i:s')
            ]);
		}

    	return response()->json([
			'code' => 201
		], 200);
    }
}
