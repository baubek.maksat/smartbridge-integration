<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StatementStatusResource;

use App\Models\StatementStatus;

class StatementStatusController extends Controller
{
    public function index(Request $request)
    {
		$statement_statuses = StatementStatus::query()
			->paginate();
		
		return StatementStatusResource::collection($statement_statuses);
    }
}
