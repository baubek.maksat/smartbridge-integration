<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\UserResource;

use Illuminate\Validation\Rule;

use App\Models\StatementCategory;
use App\Models\StatementView;
use App\Models\PaymentView;
use App\Models\Speciality;
use App\Models\Account;
use App\Models\Tiding;
use App\Models\Event;
use App\Models\Hs;

use Validator;
use Mail;
use App;
use UA;
use DB;

class SearchController extends Controller
{
    public function search(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'inquiry' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'messages' => $validator->messages(),
            ], 200);
        }
		
		$data = collect();
		
		$statement_views = StatementView::query()
			->where('state_id', 2)
			->whereHas('translation', function($query) use ($request) {
				$query->where('name', 'LIKE', $request->inquiry);
			})
			->get();
		
		if ($statement_views) {
			foreach ($statement_views as $statement_view) {
				$data->push([
					'resource' => [
						'slug' => 'statement-view',
						'name' => __('messages.statement view')
					],
					'id' => $statement_view->id,
					'title' => $statement_view->name
				]);
			}
		}
		
		$hss = Hs::query()
			->where('state_id', 2)
			->whereHas('translation', function($query) use ($request) {
				$query->where('question', 'LIKE', $request->inquiry)->orWhere('answer', 'LIKE', $request->inquiry);
			})
			->get();
		
		if ($hss) {
			foreach ($hss as $hs) {
				$data->push([
					'resource' => [
						'slug' => 'hs',
						'name' => __('help and support')
					],
					'id' => $hs->id,
					'title' => $hs->question
				]);
			}
		}
		
		$tidings = Tiding::query()
			->where('state_id', 2)
			->whereHas('translation', function($query) use ($request) {
				$query->where('heading', 'LIKE', $request->inquiry)->orWhere('discription', 'LIKE', $request->inquiry);
			})
			->get();
		
		if ($tidings) {
			foreach ($tidings as $tiding) {
				$data->push([
					'resource' => [
						'slug' => 'tiding',
						'name' => __('tiding')
					],
					'id' => $tiding->id,
					'title' => $tiding->heading
				]);
			}
		}
		
		$events = Event::query()
			->where('state_id', 2)
			->whereHas('translation', function($query) use ($request) {
				$query->where('heading', 'LIKE', $request->inquiry)->orWhere('discription', 'LIKE', $request->inquiry);
			})
			->get();
		
		if ($events) {
			foreach ($events as $event) {
				$data->push([
					'resource' => [
						'slug' => 'event',
						'name' => __('event')
					],
					'id' => $event->id,
					'title' => $event->heading
				]);
			}
		}

        $payment_views = PaymentView::query()
            ->where('state_id', 2)
            ->whereHas('translation', function($query) use ($request) {
                $query->where('name', 'LIKE', $request->inquiry);
            })
            ->get();
        
        if ($payment_views) {
            foreach ($payment_views as $payment_view) {
                $data->push([
                    'resource' => [
                        'slug' => 'payment-view',
                        'name' => __('payment view')
                    ],
                    'id' => $payment_view->id,
                    'title' => $payment_view->name
                ]);
            }
        }

        $accounts = Account::query()
            //  ->where('state_id', 2)
            ->whereHas('view.translation', function($query) use ($request) {
                $query->where('name', 'LIKE', $request->inquiry);
            })
            ->get();
        
        if ($accounts) {
            foreach ($accounts as $account) {
                $data->push([
                    'resource' => [
                        'slug' => 'account',
                        'name' => __('account view')
                    ],
                    'id' => $account->id,
                    'title' => $account->view->name
                ]);
            }
        }

        $specialities = Speciality::query()
            //  ->where('state_id', 2)
            ->whereHas('translation', function($query) use ($request) {
                $query->where('name', 'LIKE', $request->inquiry);
            })
            ->get();
        
        if ($specialities) {
            foreach ($specialities as $speciality) {
                $data->push([
                    'resource' => [
                        'slug' => 'speciality',
                        'name' => __('speciality')
                    ],
                    'id' => $speciality->id,
                    'title' => $speciality->name
                ]);
            }
        }
		
		/*
		
		$data = [
			[
				'resource' => [
					'slug' => 'classroom',
					'name' => 'Аудиторий'
				],
				'id' => 1,
				'title' => 'Кувейта'
			],[
				'resource' => [
					'slug' => 'classroom',
					'name' => 'Справка и поддержка'
				],
				'id' => 1,
				'title' => 'как я могу'
			],[
				'resource' => [
					'slug' => 'statement',
					'name' => 'Заявлений'
				],
				'id' => 1,
				'title' => 'Транскрипт'
			]
		];
		
		*/

        return response()->json([
            'code' => 200,
            'data' => $data
        ], 200);
    }

    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return response()->json([
            'code' => 200,
            'data' => [
                'redirect' => route('frontend.home')
            ]
        ], 200);
    }

    public function registration(Request $request)
    {
        
    }





    public function confirm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hash' => [
                'required',
                Rule::exists('user_email_confirms')->where(function ($query) use ($request) {
                    $query->where('fit', 1)->where('hash', $request->hash);
                })
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages() 
            ], 422);
        }

        $result = DB::transaction(function() use ($request) {
            $email_confirm = UserEmailConfirm::query()
                ->where('hash', $request->hash)
                ->first();

            $email_confirm->update([
                'fit' => 0,
                'verified_at' => date('Y-m-d H:i:s')
            ]);

            return $email_confirm;
        });

        return response()->json([], 200);
    }

    public function user()
    {
        $result = auth()->user();

        return (new UserResource($result))->additional([
            'code' => 200
        ]);
    }

    public function updateImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|file|mimes:jpg,jpeg,png|max:2048|dimensions:min_width=300,min_height=300,ratio=1/1'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages(),
            ], 422);
        }

        auth()->user()->update([
            'image' => Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->image)
        ]);

        return response()->json([], 200);
    }

    public function updateAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'registration_cato_id' => 'required|exists:catos,id',
            'registration_address' => 'required|string|max:255',
            'living_cato_id' => 'required|exists:catos,id',
            'living_address' => 'required|string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request) {
            auth()->user()->update([
                'registration_cato_id' => $request->registration_cato_id,
                'living_cato_id' => $request->living_cato_id,
            ]);

            auth()->user()->translation()->update([
                'registration_address' => $request->registration_address,
                'living_address' => $request->living_address
            ]);
        });

        return response()->json([], 200);
    }

    public function updateAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|min:1|max:255|unique:users,username,'.Auth::user()->id,
            'password' => 'required|string|min:10|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages()
            ], 422);
        }

        auth()->user()->update([
            'username' => $request->username,
            'password' => bcrypt($request->password)
        ]);

        return response()->json([], 200);
    }

    public function updateEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255|unique:users'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages()
            ], 422);
        }

        $result = DB::transaction(function() use ($request) {
            $lang = Lang::query()
                ->where('slug', app()->getLocale())
                ->first();
            
            auth()->user()->update([
                'email' => $request->email
            ]);

            auth()->user()->emailConfirms()->update([
                'fit' => 0
            ]);

            $email_confirm = auth()->user()->emailConfirms()->create([
                'type_id' => $request->header('type_id'),
                'lang_id' => $lang->id,
                'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
                'ip' => $request->server('REMOTE_ADDR'),
                'ua' => $request->server('HTTP_USER_AGENT'),
                'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
                'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
                'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
                'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
                'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
                'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
                'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
                'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
                'hash' => sha1(rand(1000, 9999)),
                'src' => mb_strtolower($request->email)
            ]);

            return $email_confirm;
        });

        Mail::to($result->src)->send(new Confirm($result));

        return response()->json([], 200);
    }

    public function updatePhone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/[7][7]\d{9}/|unique:users'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request) {
            auth()->user()->update([
                'phone' => $request->phone
            ]);
        });

        return response()->json([], 200);
    }

    public function updatePersonal(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'surname' => 'required|string|max:255',
            'surname_en' => 'nullable|string|max:255',
            'name' => 'required|string|max:255',
            'name_en' => 'nullable|string|max:255',
            'patronymic' => 'nullable|string|max:255',
            'patronymic_en' => 'nullable|string|max:255',
            'birth_date' => 'required|date_format:Y-m-d',
            'birth_country_id' => 'required|exists:citizenships,id',
            'birth_cato_id' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('citizenship_id', $request->birth_country_id)->count() > 0;
                }),
                'exists:catos,id'
            ],
            'birth_cato_name' => [
                Rule::requiredIf(function () use ($request) {
                    return Cato::where('citizenship_id', $request->birth_country_id)->count() == 0;
                }),
                'string',
                'max:255'
            ],
            'nationality_id' => 'required|exists:nationalities,id',
            'gender_id' => 'required|exists:genders,id',
            'marital_status_id' => 'required|exists:marital_statuses,id',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages(),
            ], 422);
        }

        DB::transaction(function() use ($request) {
            auth()->user()->update([
                'birth_date' => $request->birth_date,
                'birth_citizenship_id' => $request->birth_citizenship_id,
                'birth_cato_id' => Cato::where('citizenship_id', $request->birth_citizenship_id)->count() > 0 ? $request->birth_cato_id : null,
                'nationality_id' => $request->nationality_id,
                'gender_id' => $request->gender_id,
                'marital_status_id' => $request->marital_status_id
            ]);

            auth()->user()->translation()->update([
                'surname' => mb_convert_case(mb_strtolower($request->surname), MB_CASE_TITLE),
                'name' => mb_convert_case(mb_strtolower($request->name), MB_CASE_TITLE),
                'patronymic' => $request->has('patronymic') ? mb_convert_case(mb_strtolower($request->patronymic), MB_CASE_TITLE) : auth()->user()->translation()->patronymic,
                'birth_cato_name' => Cato::where('citizenship_id', $request->birth_citizenship_id)->count() == 0 ? $request->birth_cato_name : auth()->user()->translation()->birth_cato_name
            ]);
        });

        return response()->json([], 200);
    }

    public function updatePreference(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'role_id' => 'required|exists:users_roles,role_id,user_id,'.auth()->user()->id,
            'lang_id' => 'required|exists:langs,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages()
            ], 422);
        }

        auth()->user()->update([
            'default_role_id' => $request->role_id,
            'default_lang_id' => $request->lang_id
        ]);

        return response()->json([], 200);
    }
}
