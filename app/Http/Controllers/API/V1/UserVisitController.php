<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserVisitController extends Controller
{
    public function index(User $user)
    {
    	$validator = Validator::make($request->all(), [
            'per_page' => 'integer|between:1,50'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $per_page = $request->has('per_page') ? $request->per_page : null;

    	$data = $user->visits()->paginate($per_page)->appends([
    		'paginate' => $per_page
    	]);

    	return response()->json($data, 200);
    }
}
