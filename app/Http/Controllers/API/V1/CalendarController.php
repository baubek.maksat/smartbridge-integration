<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\Week;
use App\Models\User;
use App\Models\App;

use Validator;

use Carbon\Carbon;

class CalendarController extends Controller
{
    public function index(Request $request)
    {
		if ($request->has('period') && $request->period == 'thisweek') {

			$calendars = collect();

			$weeks = Week::query()
				->get();

			$date_start = Carbon::now()->startOfWeek();

			if ($weeks) {
				foreach ($weeks as $week) {
					$date = Carbon::parse($date_start)->addDay($week->id - 1);

					$calendars->push([
						'day' => (int)$date->format('d'),
						'week' => [
							'id' => $week->id,
							'name' => $week->name,
							'short_name' => $week->short_name
						]
					]);
				}
			}
		}
		
		if ($request->has('period') && $request->period == 'thismonth') {
			$calendars = collect();
			
			$date_start = Carbon::now()->startOfMonth();
			
			for ($i = 0; $i <= 30; $i++) {
				$date = Carbon::parse($date_start)->addDay($i);
				
				$calendars->push([
					'day' => (int)$date->format('d'),
					'week' => [
						'id' => (int)$date->format('N'),
						'name' => Week::where('id', (int)$date->format('N'))->first()->name,
						'short_name' => Week::where('id', (int)$date->format('N'))->first()->short_name
					]
				]);
				
				if (Carbon::now()->endOfMonth()->format('Y-m-d') == $date->format('Y-m-d')) {
					break;
				}
			}
		}
	
		return response()->json([
            'code' => 200,
			'data' => $calendars
        ], 200);
    }
}
