<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatementCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
		return [
            'id' => $this->id,
            'name' => $this->name,
			'parent' => [
                'id' => $this->parent_id,
                'name' => $this->parent ? $this->parent->name : null
            ],
			'state' => [
				'id' => $this->state->id,
				'name' => $this->state->name
			],
			'translations' => $this->translations,
            'views' => StatementCategoryViewResource::collection($this->views),
            //  'views' => StatementViewResource::collection($this->views),
			'childrens' => StatementCategoryResource::collection($this->childrens)
        ];
    }
}
