<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'student_id' => $this->student_id,
			'payment_view' => [
				'id' => $this->paymentView->id,
				'name' => $this->paymentView->translation->name
			],
			'debt' => $this->debt,
			'paid' => $this->paid,
			'due_date' => $this->due_date,
			'stasus' => [
				'id' => $this->status->id,
				'name' => $this->status->translation->name
			]
		];
	}
}
