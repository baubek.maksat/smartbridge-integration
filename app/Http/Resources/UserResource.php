<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'surname' => $this->surname,
            'name' => $this->name,
            'patronymic' => $this->patronymic,
            'email' => $this->email,
            'phone' => $this->phone,
            'image' => $this->image ? asset($this->image) : null,
            'birth_date' => $this->birth_date,
            'birth_country' => $this->birthCountry ? [
                'id' => $this->birthCountry->id,
                'name' => $this->birthCountry->name
            ] : null,
            'birth_cato' => $this->birthCato ? [
                'id' => $this->birthCato->id,
                'code' => $this->birthCato->code,
                'name' => $this->birthCato->name
            ] : null,
			'current_role' => new RoleResource($this->currentRole()),
			'default_lang' => $this->defaultLang ? new LangResource($this->defaultLang) : null,
            'default_role' => $this->defaultRole ? new RoleResource($this->defaultRole) : null,
			'roles' => RoleResource::collection($this->roles),
			'student_service_center_role' => count($this->studentServiceCenterRoles) > 0 ? [
				'id' => $this->studentServiceCenterRoles[0]->id,
				'name' => $this->studentServiceCenterRoles[0]->name
			] : null,
            'gender' => [
                'id' => $this->gender->id,
                'name' => $this->gender->name
            ],
            'marital_status' => $this->maritalStatus ? [
                'id' => $this->maritalStatus->id,
                'name' => $this->maritalStatus->name
            ] : null,
            'living' => [
                'cato' => $this->livingCato ? [
                    'id' => $this->livingCato->id,
                    'code' => $this->livingCato->code,
                    'name' => $this->livingCato->name
                ]: null,
                'address' => $this->living_address
            ],
            'registration' => [
                'cato' => $this->registrationCato ? [
                    'id' => $this->registrationCato->id,
                    'code' => $this->registrationCato->code,
                    'name' => $this->registrationCato->name
                ] : null,
                'address' => $this->registration_address
            ],
            'nationality' => $this->nationality ? new NationalityResource($this->nationality) : null,
            'account' => $this->currentRole()->slug == 'student' ? new AccountResource($this->account) : null,
			'card' => $this->currentRole()->slug == 'student' ? [
                'id' => $this->student->card->id,
				'date_start' => $this->student->card->date_start,
                'group' => [
                    'name' => $this->student->card->group->name,
                    'speciality' => [
                        'code' => $this->student->card->group->speciality->code,
                        'name' => $this->student->card->group->speciality->name,
                        'profession' => [
                            'code' => $this->student->card->group->speciality->profession->code,
                            'name' => $this->student->card->group->speciality->profession->name
                        ],
						'department' => [
							'name' => $this->student->card->group->speciality->department->name,
							'faculty' => [
								'name' => $this->student->card->group->speciality->department->faculty->name,
								'university' => new UniversityResource($this->student->card->group->speciality->department->faculty->university)
							]
						]
                    ]
                ],
                'study_language' => [
                    'name' => $this->student->card->studyLanguage->name
                ],
                'study_form' => [
                    'name' => $this->student->card->studyForm->name,
                    'academic_degree' => [
                        'name' => $this->student->card->studyForm->academicDegree->name
                    ]
                ],
                'payment_form' => [
                    'name' => $this->student->card->paymentForm->name
                ],
                'gpa' => $this->student->card->gpa
            ] : null,
            'identification_document' => $this->identificationDocument ? new IdentificationDocumentResource($this->identificationDocument) : null,
            'email_verified' => $this->email_verified
        ];
    }
}
