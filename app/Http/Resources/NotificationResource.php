<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'view' => [
                'id' => $this->view->id,
                'slug' => $this->view->slug,
                'icon' => asset($this->view->icon),
                'name' => $this->view->name
            ],
			'params' => $this->params,
            'text' => $this->text,
			'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'reviewed_at' =>  $this->recipient ? $this->recipient->reviewed_at : null
        ];
    }
}
