<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatementCommentResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'user' => [
				'id' => $this->user->id,
				'surname' => $this->user->surname,
				'name' => $this->user->name,
				'patronymic' => $this->user->patronymic,
				'image' => $this->user->image ? asset($this->user->image) : null
			],
			'message' => $this->message,
			'files' => $this->files ? collect($this->files)->map(function ($file) {
				return [
					'src' => asset($file['src']),
					'name' => $file['name']
				];
			}) : null,
			'created_at' => $this->created_at->format('Y-m-d H:i:s'),
		];
	}
}
