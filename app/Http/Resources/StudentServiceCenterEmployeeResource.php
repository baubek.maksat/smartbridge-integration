<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentServiceCenterEmployeeResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'surname' => $this->surname,
			'name' => $this->name,
			'patronymic' => $this->patronymic,
			'image' => $this->image,
			'birth_date' => $this->birth_date,
			'email' => $this->email,
			'phone' => $this->phone,
			'gender' => [
				'id' => $this->gender_id
			],
			'role' => [
				'id' => $this->studentServiceCenterRoles()->wherePivotNull('deleted_at')->first()->id,
				'name' => $this->studentServiceCenterRoles()->wherePivotNull('deleted_at')->first()->name,
				'access' => [
					'id' => $this->studentServiceCenterRoles()->wherePivotNull('deleted_at')->first()->access->id,
					'name' => $this->studentServiceCenterRoles()->wherePivotNull('deleted_at')->first()->access->name,
				]
			],
			'email_verified' => (int)$this->email_verified
		];
	}
}
