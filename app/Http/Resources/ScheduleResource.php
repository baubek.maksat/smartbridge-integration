<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use Carbon\Carbon;

class ScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
		$datetime_now = Carbon::now()->format('H:i:s');
		
        return [
            'id' => $this->id,
            'subject' => [
                'name' => $this->studyGroup->subject->name
            ],
			'teacher' => $this->studyGroup->teacher ? [
                'surname' => $this->studyGroup->teacher->user->surname,
				'name' => $this->studyGroup->teacher->user->name,
				'patronymic' => $this->studyGroup->teacher->user->patronymic,
				'image' => $this->studyGroup->teacher->user->image
            ] : null,
			'week' => [
				'id' => $this->week->id,
				'name' => $this->week->name
			],
            'classroom' => $this->online == 0 ? [
                'name' => $this->classroom->name,
                'campus' => [
                    'name' => $this->classroom->campus->name
                ]
            ] : null,
            'online' => $this->online,
			'link' => null,
            'lesson_hour' => [
                'start' => mb_substr($this->lessonHour->start, 0, 5),
                'end' => mb_substr($this->lessonHour->end, 0, 5)
            ],
			'now' => $datetime_now >= $this->lessonHour->start && $datetime_now <= $this->lessonHour->end ? 1 : 0
        ];
    }
}
