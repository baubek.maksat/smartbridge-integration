<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
			'show' => null,
            'question' => $this->translation->question,
            'answer' => $this->translation->answer,
			'state' => [
				'id' => $this->state->id,
				'name' => $this->state->name
			],
			'category' => [
				'id' => $this->category->id,
				'name' => $this->category->name
			],
			'translations' => $this->translations,
            'per' => $this->per,
            'vs' => $this->vs
        ];
    }
}
