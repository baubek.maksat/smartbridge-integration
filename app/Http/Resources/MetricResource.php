<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MetricResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'visit' => [
                'id' => $this->visit->id
            ],
            'type' => [
                'id' => $this->type->id,
                'slug' => $this->type->slug,
                'name' => $this->type->name
            ],
            'app' => [
                'id' => $this->app->id
            ],
            'route' => $this->route,
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
