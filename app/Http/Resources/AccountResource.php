<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'balance' => $this->balance,
            'view' => [
                'id' => $this->view->id,
                'name' => $this->view->name
            ],
            'user' => [
                'id' => $this->user->id,
                'surname' => $this->user->surname,
                'name' => $this->user->name,
                'patronymic' => $this->user->patronymic
            ],
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ]
        ];
    }
}
