<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentCardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
			'id' => $this->id,
            'user' => new UserResource($this->user),
            'study_language' => new StudyLanguageResource($this->studyLanguage),
            'study_form' => [
                'name' => $this->studyForm->name,
                'academic_degree' => [
                    'name' => $this->studyForm->academicDegree->name
                ]
            ],
            'payment_form' => new PaymentFormResource($this->paymentForm),
            'group' => [
                'name' => $this->group->name,
                'speciality' => [
                    'code' => $this->group->speciality->code,
                    'name' => $this->group->speciality->name,
                    'profession' => [
                        'code' => $this->group->speciality->profession->code,
                        'name' => $this->group->speciality->profession->name
                    ],
                    'department' => [
                        'name' => $this->group->speciality->department->name,
                        'faculty' => [
                            'name' => $this->group->speciality->department->faculty->name,
                            'university' => new UniversityResource($this->group->speciality->department->faculty->university)
                        ]
                    ]
                ]
            ],
            'quota' => new QuotaResource($this->quota),
            'course_number' => $this->course_number,
            'date_start' => $this->date_start->format('Y-m-d'),
            'date_end' => $this->date_end,
            'grant_type' => $this->grantType ? [
                'id' => $this->grantType->id,
                'name' => $this->grantType->name
            ] : null,
            'transcript_number' => $this->transcript_number,
            'gpa' => $this->gpa,
        ];
    }
}
