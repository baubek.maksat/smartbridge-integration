<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use Carbon\Carbon;

class StatementResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'view' => [
				'name' => $this->view->name,
				'category' => [
					'name' => $this->view->category->name
				],
				'term_of_consideration' => $this->view->term_of_consideration
			],
			'lang' => [
				'name' => $this->language->name
			],
			'responsible' => [
				'role' => [
					'id' => $this->responsibleRole->id,
					'name' => $this->responsibleRole->name
				],
				'user' => $this->responsibleUser ? [
					'id' => $this->responsibleUser->id,
					'surname' => $this->responsibleUser->surname,
					'name' => $this->responsibleUser->name,
					'patronymic' => $this->responsibleUser->patronymic,
					'image' => $this->responsibleUser->image
				] : null
			],
			'student' => [
				'id' => $this->student->id,
				'surname' => $this->student->user->surname,
				'name' => $this->student->user->name,
				'patronymic' => $this->student->user->patronymic
			],
			'details' => StatementDetailResource::collection($this->details),
			'status' => new StatementStatusResource($this->status),
			'qr' => asset($this->qr),
			'stages' => StatementStageResource::collection($this->stages),
			'message' => $this->message,
			'files' => $this->files ? collect($this->files)->map(function ($file) {
				return [
					'name' => $file['name'],
					'src' => asset($file['src'])
				];
			}) : null,
			//	'src' => $this->src && file_exists($this->src) ? asset($this->src) : null,
			'src' => $this->src && file_exists($this->src) ? asset($this->src) : null,
			'comments' => StatementCommentResource::collection($this->comments),
			'created_at' => $this->created_at->format('Y-m-d H:i:s'),
			'started_at' => $this->started_at,
			'ended_at' => $this->ended_at,
			'expired_at' => $this->expired_at,
			'expired' => ($this->expired_at && strtotime($this->expired_at) < strtotime(date('Y-m-d H:i:s'))) ? true : false,
			'loading' => [
				'delete' => null,
				'begin' => null
			]
		];
	}
}
