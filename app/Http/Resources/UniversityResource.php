<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UniversityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'logo' => $this->logo ? asset($this->logo) : null,
            'name' => $this->name,
            'website' => $this->website,
            'address' => $this->address,
            'images' => $this->images,
            'phones' => $this->phones,
            'emails' => $this->emails,
			'langs' => $this->langs,
            /*
            'state_id' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ],
            */
			'cato' => new CatoResource($this->cato),
            'country' => $this->country ? [
                'id' => $this->country->id,
                'name' => $this->country->name
            ] : null,
			'faculty_type' => $this->faculty_type ? [
                'id' => $this->faculty_type->id,
				'name' => $this->faculty_type->name
			] : null,
			'department_type' => $this->department_type ? [
                'id' => $this->department_type->id,
				'name' => $this->department_type->name
			] : null,
            'speciality_type' => $this->speciality_type ? [
                'id' => $this->speciality_type->id,
                'name' => $this->speciality_type->name
            ] : null,
            'administrator_worker_position' => $this->administrator_worker_position ? [
                'id' => $this->administrator_worker_position->id,
                'name' => $this->administrator_worker_position->name
            ] : null,
            'faculty_administrator_worker_position' => $this->faculty_administrator_worker_position ? [
                'id' => $this->faculty_administrator_worker_position->id,
                'name' => $this->faculty_administrator_worker_position->name
            ] : null,
            'department_administrator_worker_position' => $this->department_administrator_worker_position ? [
                'id' => $this->department_administrator_worker_position->id,
                'name' => $this->department_administrator_worker_position->name
            ] : null,
            'state' => $this->state ? [
                'id' => $this->state->id,
                'name' => $this->state->name
            ] : null,
			'head' => [
				'worker_position' => $this->administratorWorkerPosition ? [
					'name' => $this->administratorWorkerPosition->name
				] : null,
				'user' => $this->administrator ? [
                    'surname' => $this->administrator->surname,
                    'name' => $this->administrator->name,
                    'patronymic' => $this->administrator->patronymic
                ] : null
			],
            'study_languages' => StudyLanguageResource::collection($this->studyLanguages),
			'student_service_center' => new StudentServiceCenterResource($this->studentServiceCenter)
        ];
    }
}
