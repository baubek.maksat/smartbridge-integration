<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'view' => [
				'id' => $this->view->id,
				'icon' => asset($this->view->icon),
				'name' => $this->view->name
			],
			'account' => [
				'id' => $this->account->id,
				'view' => [
					'id' => $this->account->view->id,
					'name' => $this->account->view->name
				],
				'user' => [
					'id' => $this->account->user->id,
					'surname' => $this->account->user->surname,
					'name' => $this->account->user->name,
					'patronymic' => $this->account->user->patronymic,
					'identity_document' => [
						'id' => $this->account->user->identityDocument->id,
						'tin' => $this->account->user->identityDocument->tin
					],
					'student_cards' => $this->account->user->student->cards
				]
			],
			'payment' => $this->payment ? [
				'id' => $this->payment->id,
				'view' => [
					'id' => $this->payment->view->id,
					'name' => $this->payment->view->translation->name
				]
			] : null,
			'replenishment' => $this->replenishment ? [
				'id' => $this->replenishment->id,
				'from' => $this->replenishment->account ? [
					'account' => [
						'user' => [
							'surname' => $this->replenishment->account->user->surname,
							'name' => $this->replenishment->account->user->name
						]
					]
				] : null,
				'view' => [
					'id' => $this->replenishment->view->id,
					'slug' => $this->replenishment->view->slug,
					'name' => $this->replenishment->view->name
				]
			] : null,
			'accrual' => $this->accrual ? [
				'id' => $this->accrual->id,
				'from' => $this->accrual->account ? [
					'account' => [
						'user' => [
							'surname' => $this->accrual->account->user->surname,
							'name' => $this->accrual->account->user->name
						]
					]
				] : null,
			] : null,
			'transfer' => $this->transfer ? [
				'id' => $this->transfer->id,
				'view' => [
					'id' => $this->transfer->view->id,
					'slug' => $this->transfer->view->slug,
					'name' => $this->transfer->view->name
				],
				'recipient' => [
					'account' => [
						'view' => [
							'id' => $this->transfer->account->view->id,
							'name' => $this->transfer->account->view->name
						],
						'user' => [
							'surname' => $this->transfer->account->user->surname,
							'name' => $this->transfer->account->user->name
						]
					]
				]
			] : null,
			'refund' => $this->refund ? [
				'id' => $this->refund->id,
				'reference' => [
					'id' => $this->refund->reference->id,
					'view' => [
						'id' => $this->refund->reference->view->id,
						'icon' => asset($this->refund->reference->view->icon),
						'name' => $this->refund->reference->view->name
					],
					'payment' => $this->refund->reference->payment ? [
						'id' => $this->refund->reference->payment->id,
						'view' => [
							'id' => $this->refund->reference->payment->view->id,
							'name' => $this->refund->reference->payment->view->translation->name
						]
					] : null,
					'replenishment' => $this->refund->reference->replenishment ? [
						'id' => $this->refund->reference->replenishment->id,
						'from' => $this->refund->reference->replenishment->account ? [
							'account' => [
								'user' => [
									'surname' => $this->refund->reference->replenishment->account->user->surname,
									'name' => $this->refund->reference->replenishment->account->user->name
								]
							]
						] : null,
						'view' => [
							'id' => $this->refund->reference->replenishment->view->id,
							'slug' => $this->refund->reference->replenishment->view->slug,
							'name' => $this->refund->reference->replenishment->view->name
						]
					] : null,
					'transfer' => $this->refund->reference->transfer ? [
						'id' => $this->refund->reference->transfer->id,
						'view' => [
							'id' => $this->refund->reference->transfer->view->id,
							'slug' => $this->refund->reference->transfer->view->slug,
							'name' => $this->refund->reference->transfer->view->name
						],
						'recipient' => [
							'account' => [
								'view' => [
									'id' => $this->refund->reference->transfer->account->view->id,
									'name' => $this->refund->reference->transfer->account->view->name
								],
								'user' => [
									'surname' => $this->refund->reference->transfer->account->user->surname,
									'name' => $this->refund->reference->transfer->account->user->name
								]
							]
						]
					] : null,
				]
			] : null,
			'additional_fields' => $this->additional_fields,
			'sum' => $this->sum,
			'datetime' => $this->datetime,
			'created_at' => $this->created_at->format('Y-m-d H:i:s')
		];
	}
}
