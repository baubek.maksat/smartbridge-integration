<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatementViewStageResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'slug' => $this->slug,
			'conditions' => $this->conditions,
			'processes' => $this->processes,
			'routes' => StatementViewRouteResource::collection($this->routes)
		];
	}
}
