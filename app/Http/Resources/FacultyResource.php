<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FacultyResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'name' => $this->name,
			'head' => [
				'worker_position' => $this->university->faculty_administrator_worker_position ? [
					'id' => $this->university->faculty_administrator_worker_position->id,
					'name' => $this->university->faculty_administrator_worker_position->name
				] : null,
				'user' => $this->administrator ? [
					'surname' => $this->administrator->surname,
					'name' => $this->administrator->name,
					'patronymic' => $this->administrator->patronymic
				] : null
			],
			'university' => [
				'id' => $this->university->id,
				'name' => $this->university->name,
			],
			'state' => [
				'id' => $this->state->id,
				'name' => $this->state->name
			],
			'translations' => $this->translations()->get(['id', 'lang', 'name'])
		];
	}
}
