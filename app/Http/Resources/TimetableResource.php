<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TimetableResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'study_group' => [
                'name' => $this->studyGroup->name,
                'subject' => [
                    'id' => $this->studyGroup->subject->id,
                    'name' => $this->studyGroup->subject->translation->name
                ]
            ],
            'classroom' => $this->classroom ? [
                'id' => $this->classroom->id,
                'name' => $this->classroom->translation->name,
                'campus' => [
                    'name' => $this->classroom->campus->id,
                    'name' => $this->classroom->campus->translation->name
                ]
            ] : null,
            'lesson_hour' => [
                'start' => $this->lessonHour->start,
                'end' => $this->lessonHour->end
            ],
            'week_number' => $this->week_number,
            'week_id' => $this->week_id,
            'online' => $this->online
        ];
    }
}
