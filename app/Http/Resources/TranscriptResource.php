<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TranscriptResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
	
	public function toArray($request)
    {
        return [
            'id' => $this->id,
			'subject' => [
				'code' => $this->subject_code,
            	'name' => $this->subject_name
			],
            'course_number' => $this->course_number,
			'term' => $this->term,
			'credits' => $this->credits,
			'traditional_mark' => $this->traditional_mark,
			'alpha_mark' => $this->alpha_mark,
			'numeral_mark' => $this->numeral_mark,
			'exam_mark' => $this->exam_mark,
			'total_mark' => $this->total_mark
        ];
    }
}
