<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JournalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
	
	public function toArray($request)
    {
        return [
			'id' => $this->id,
			'subject' => [
				'id' => $this->subject->id,
				'name' => $this->subject->name
			],
			'teacher' => $this->teacher ? [
				'id' => $this->teacher->user->id,
				'surname' => $this->teacher->user->surname,
				'name' => $this->teacher->user->name,
				'patronymic' => $this->teacher->user->patronymic
			] : null,
			'mark' => $this->journal ? $this->journal->mark : null
        ];
    }
}
