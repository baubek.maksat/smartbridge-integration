<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->code.' '.$this->name,
			'value' => $this->id,
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'department' => [
                'id' => $this->department->id,
                'name' => $this->department->name,
                'faculty' => [
                    'id' => $this->department->faculty->id,
                    'name' => $this->department->faculty->name,
                    'university' => [
                        'id' => $this->department->faculty->university->id,
                        'name' => $this->department->faculty->university->name,
                        'department_type' => [
                            'id' => $this->department->faculty->university->department_type->id,
                            'name' => $this->department->faculty->university->department_type->name
                        ]
                    ]
                ]
            ],
            'academic_degree' => [
                'id' => $this->academic_degree->id,
                'name' => $this->academic_degree->name
            ],
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ]
        ];
    }
}
