<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentServiceCenterRoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
			'state' => [
				'id' => $this->state->id,
				'name' => $this->state->name
			],
            'name' => $this->name,
			'access' => [
				'id' => $this->access->id,
				'name' => $this->access->name
			],
			'translations' => $this->translations,
			'users' => $this->users
        ];
    }
}
