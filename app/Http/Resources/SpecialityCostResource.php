<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SpecialityCostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'year_of_admission' => $this->year_of_admission,
            'study_year' => $this->study_year,
            'value' => $this->value,
            'speciality' => [
                'id' => $this->speciality->id,
                'name' => $this->speciality->name
            ],
            'study_form' => [
                'id' => $this->study_form->id,
                'name' => $this->study_form->name
            ],
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ]
        ];
    }
}
