<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IdentificationDocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'series' => $this->series,
            'tin' => $this->tin,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'type' => [
                'id' => $this->type->id,
                'name' => $this->type->name
            ],
            'issuing_authority' => $this->issuingAuthority ? [
                'id' => $this->issuingAuthority->id,
                'name' => $this->issuingAuthority->name
            ] : null,
            'citizenship' => [
                'id' => $this->citizenship->id,
                'name' => $this->citizenship->name
            ]
        ];
    }
}
