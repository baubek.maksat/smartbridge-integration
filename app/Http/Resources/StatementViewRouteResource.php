<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatementViewRouteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'action' => [
				'id' => $this->action->id,
				'name' => $this->action->name
			],
			'type' => $this->responsible_type,
			'statement_responsible_user' => (int)$this->statement_responsible_user,
			'responsible' => [
				'role' => [
					'id' => $this->role->id,
					'name' => $this->role->name
				],
				'user' => $this->user ? [
					'id' => $this->user->id,
					'surname' => $this->user->surname,
					'name' => $this->user->name,
					'patronymic' => $this->user->patronymic,
					'image' => $this->user->image
				] : null
			],
			'term' => $this->term
        ];
    }
}
