<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AcademicDegreeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'name' => $this->name,
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ],
            'translations' => $this->translations()->get(['id', 'lang', 'name'])
        ];
    }
}
