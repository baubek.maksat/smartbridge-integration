<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IntegrationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'table' => $this->table,
            'system' => $this->system,
            'university' => new UniversityResource($this->university),
            'created' => $this->created,
            'updated' => $this->updated,
            'deleted' => $this->deleted,
            'time' => $this->time,
            'created_at' => $this->created_at
        ];
    }
}
