<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TindingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => asset($this->image),
            'heading' => $this->heading,
            'discription' => $this->discription,
			'views' => $this->views,
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
