<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HsCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'icon' => $this->icon ? asset($this->icon) : null,
			'state' => [
				'id' => $this->state->id,
				'name' => $this->state->name
			],
            'hss' => HsResource::collection($this->hss),
			'translations' => $this->translations
        ];
    }
}
