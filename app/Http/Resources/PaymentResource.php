<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'account' => [
                'view' => [
                    'id' => $this->transaction->account->view->id,
                    'name' => $this->transaction->account->view->name
                ]
            ],
            'view' => [
                'id' => $this->id,
                'name' => $this->view->name
            ],
            'sum' => $this->transaction->sum,
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
