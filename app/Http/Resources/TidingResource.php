<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TidingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => asset($this->image),
            'heading' => $this->heading,
			'state' => [
				'id' => $this->state->id,
				'name' => $this->state->name
			],
            'date_of_publication' => $this->date_of_publication,
            'discription' => $this->discription,
			'views' => $this->ips()->count(),
			'translations' => $this->translations
        ];
    }
}
