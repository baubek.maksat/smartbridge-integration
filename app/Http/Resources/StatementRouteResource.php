<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatementRouteResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'status' => $this->status,
			'action' => [
				'id' => $this->action->id,
				'name' => $this->action->name,
				'make' => $this->action->make,
				'process' => $this->action->process,
				'success' => $this->action->success,
				'fail' => $this->action->fail
			],
			'responsible_type' => $this->responsible_type,
			'responsible' => [
				'role' => [
					'id' => $this->role->id,
					'name' => $this->role->name
				],
				'user' => $this->user ? [
					'id' => $this->user->id,
					'surname' => $this->user->surname,
					'name' => $this->user->name,
					'patronymic' => $this->user->patronymic,
					'image' => $this->user->image
				] : null
			],
			'message' => $this->message,
			'files' => collect($this->files)->map(function ($file) {
				return [
					'src' => asset($file['src']),
					'name' => $file['name']
				];
			}),
			'delivered_at' => $this->delivered_at,
			'created_at' => $this->created_at,
			'started_at' => $this->started_at,
			'ended_at' => $this->ended_at,
			'expired_at' => $this->expired_at,
			'term' => $this->term
		];
	}
}
