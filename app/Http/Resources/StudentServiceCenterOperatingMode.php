<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentServiceCenterOperatingMode extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'time_start' => $this->time_start,
			'time_end' => $this->time_end,
			'week' => [
				'id' => $this->week->id,
				'name' => $this->week->name,
				'short_name' => $this->week->short_name
			]
		];
	}
}
