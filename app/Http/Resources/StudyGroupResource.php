<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudyGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'name' => $this->name,
			'code' => $this->code,
			'year' => $this->year,
			'term' => $this->term,
			'hours' => $this->hours,
			'student_count' => $this->student_count,
			'student_count_min' => $this->student_count_min,
			'student_count_max' => $this->student_count_max,
			'type' => [
				'id' => $this->type->id,
				'name' => $this->type->name
			],
			'subject' => $this->subject ? [
				'id' => $this->subject->id,
				'name' => $this->subject->name
			] : null,
			'teacher' => $this->teacher ? [
				'id' => $this->teacher->user->id,
				'surname' => $this->teacher->user->surname,
				'name' => $this->teacher->user->name,
				'patronymic' => $this->teacher->user->patronymic,
			] : null,
			'study_form' => $this->study_form ? [
				'id' => $this->study_form->id,
				'name' => $this->study_form->name
			] : null,
			'study_language' => $this->study_language ? [
				'id' => $this->study_language->id,
				'name' => $this->study_language->name
			] : null,
			'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ],
        ];
    }
}
