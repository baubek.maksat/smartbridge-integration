<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudyFormResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'name' => $this->name,
            'course_count' => $this->course_count,
            'academic_degree' => [
                'id' => $this->academic_degree->id,
                'name' => $this->academic_degree->name
            ],
            'type' => [
                'id' => $this->type->id,
                'name' => $this->type->name
            ],
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ]
        ];
    }
}
