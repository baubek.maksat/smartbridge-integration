<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
		$locales = collect(['kz', 'ru', 'en']);
		
        $locale = $request->header('accept-language') && ($request->header('accept-language') == 'kz' || $request->header('accept-language') == 'ru' || $request->header('accept-language') == 'en')
			? $request->header('accept-language')
			: app()->getLocale();

        app()->setLocale($locale);

        return $next($request);
    }
}
