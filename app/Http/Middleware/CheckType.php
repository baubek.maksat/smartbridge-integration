<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use App\Models\Type;

use Validator;
use UA;

class CheckType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $validator = Validator::make($request->header(), [
            'type' => 'required|exists:types,slug',
            'user-agent' => 'nullable'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages()
            ], 422);
        }

        return $next($request);
    }
}
