<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrantTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grant_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('payment_form_id')->nullable();
            $table->unsignedBigInteger('state_id');
            $table->json('integration_fields')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('payment_form_id')->references('id')->on('payment_forms');
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE grant_types AUTO_INCREMENT = 100001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grant_types');
    }
}
