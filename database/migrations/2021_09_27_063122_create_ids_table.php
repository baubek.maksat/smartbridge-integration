<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ids', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('citizenship_id');
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();
            $table->string('series')->nullable();
            $table->string('number')->nullable();
            $table->string('tin')->nullable();
            $table->text('src')->nullable();
            $table->unsignedBigInteger('issuing_authority_id')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('id_types');
            $table->foreign('citizenship_id')->references('id')->on('countries');
            $table->foreign('issuing_authority_id')->references('id')->on('id_issuing_authorities');
        });

        DB::statement('ALTER TABLE ids AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ids');
    }
}
