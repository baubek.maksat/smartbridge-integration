<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->id();
            $table->string('platonus_key')->nullable();
            $table->unsignedBigInteger('student_id');
            $table->unsignedBigInteger('study_group_id');
            $table->unsignedBigInteger('state_id');
            $table->decimal('mark', 5, 2)->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('study_group_id')->references('id')->on('study_groups');
            $table->foreign('student_id')->references('id')->on('student_cards');
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE journals AUTO_INCREMENT = 100000000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journals');
    }
}
