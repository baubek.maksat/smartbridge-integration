<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationRoutesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_routes', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('stage_id');
			$table->unsignedBigInteger('action_id');
			$table->unsignedBigInteger('responsible_role_id');
			$table->unsignedBigInteger('responsible_user_id')->nullable();
			$table->enum('responsible_type', ['responsible', 'user', 'role']);
			$table->enum('status', ['queue','waiting','processing','success','fail','pass']);
			$table->text('message')->nullable();
			$table->json('files')->nullable();
			$table->unsignedBigInteger('term')->nullable();
			$table->timestamp('delivered_at')->nullable();
			$table->timestamp('started_at')->nullable();
			$table->timestamp('ended_at')->nullable();
			$table->timestamp('expired_at')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			
			$table->foreign('stage_id')->references('id')->on('application_stages');
			$table->foreign('action_id')->references('id')->on('application_actions');
			$table->foreign('responsible_user_id')->references('id')->on('users');
		});

		DB::statement('ALTER TABLE application_routes AUTO_INCREMENT = 100000000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('statement_routes');
	}
}
