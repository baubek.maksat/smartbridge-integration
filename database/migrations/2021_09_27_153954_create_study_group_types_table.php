<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudyGroupTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_group_types', function (Blueprint $table) {
            $table->id();
            $table->string('platonus_key')->nullable();
            $table->unsignedBigInteger('academic_degree_id');
            $table->unsignedBigInteger('state_id');
            $table->string('suffix');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('academic_degree_id')->references('id')->on('academic_degrees');
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE study_group_types AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_group_types');
    }
}
