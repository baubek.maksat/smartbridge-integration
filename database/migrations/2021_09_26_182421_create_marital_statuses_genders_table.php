<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaritalStatusesGendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marital_statuses_genders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('marital_status_id');
            $table->unsignedBigInteger('gender_id');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('marital_status_id')->references('id')->on('marital_statuses');
            $table->foreign('gender_id')->references('id')->on('genders');
        });

        DB::statement('ALTER TABLE marital_statuses_genders AUTO_INCREMENT = 1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marital_statuses_genders');
    }
}
