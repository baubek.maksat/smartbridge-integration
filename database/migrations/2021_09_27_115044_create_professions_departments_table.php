<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionsDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professions_departments', function (Blueprint $table) {
            $table->unsignedBigInteger('profession_id');
            $table->unsignedBigInteger('department_id');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('profession_id')->references('id')->on('professions');
            $table->foreign('department_id')->references('id')->on('departments');

            $table->primary([
                'profession_id',
                'department_id'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professions_departments');
    }
}
