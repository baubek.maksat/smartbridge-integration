<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentFormsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_forms', function (Blueprint $table) {
			$table->id();
            $table->unsignedBigInteger('state_id');
            $table->json('integration_fields')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('state_id')->references('id')->on('states');
		});

		DB::statement('ALTER TABLE payment_forms AUTO_INCREMENT = 100001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('payment_forms');
	}
}
