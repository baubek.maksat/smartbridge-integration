<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->id();
			$table->string('username')->unique();
			$table->string('password');
			$table->string('surname');
			$table->string('name');
			$table->string('patronymic')->nullable();
			$table->string('surname_en')->nullable();
			$table->string('name_en')->nullable();
			$table->string('patronymic_en')->nullable();
			$table->string('email')->unique()->nullable();
			$table->string('phone')->unique()->nullable();
			$table->text('image')->nullable();
			$table->date('birth_date');
			$table->unsignedBigInteger('birth_country_id')->nullable();
			$table->unsignedBigInteger('birth_cato_id')->nullable();
			$table->unsignedBigInteger('registration_cato_id')->nullable();
			$table->unsignedBigInteger('living_cato_id')->nullable();
			$table->unsignedBigInteger('default_role_id')->nullable();
			$table->unsignedBigInteger('default_lang_id')->nullable();
			$table->unsignedBigInteger('nationality_id')->nullable();
			$table->unsignedBigInteger('marital_status_id')->nullable();
			$table->unsignedBigInteger('gender_id');
			$table->string('remember_token')->nullable();
			$table->enum('access', [0, 1])->default(0);
			$table->enum('email_verified', [0, 1])->default(0);
            $table->json('integration_fields')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

            $table->foreign('birth_country_id')->references('id')->on('countries');
            $table->foreign('birth_cato_id')->references('id')->on('catos');
            $table->foreign('registration_cato_id')->references('id')->on('catos');
            $table->foreign('living_cato_id')->references('id')->on('catos');
            $table->foreign('default_role_id')->references('id')->on('roles');
            $table->foreign('default_lang_id')->references('id')->on('langs');
            $table->foreign('nationality_id')->references('id')->on('nationalities');
            $table->foreign('marital_status_id')->references('id')->on('marital_statuses');
            $table->foreign('gender_id')->references('id')->on('genders');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
