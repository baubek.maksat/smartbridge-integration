<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('faculty_id');
            $table->unsignedBigInteger('classroom_id')->nullable();
            $table->unsignedBigInteger('administrator_id')->nullable();
            $table->unsignedBigInteger('state_id');
            $table->text('logo')->nullable();
            $table->json('integration_fields')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('faculty_id')->references('id')->on('faculties');
            $table->foreign('classroom_id')->references('id')->on('classrooms');
            $table->foreign('administrator_id')->references('id')->on('users');
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE departments AUTO_INCREMENT = 100001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
    }
}
