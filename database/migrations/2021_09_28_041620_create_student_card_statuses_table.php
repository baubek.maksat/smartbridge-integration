<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentCardStatusesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_card_statuses', function (Blueprint $table) {
			$table->id();
			$table->json('integration_fields')->nullable();
			$table->timestamp('verified_at')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();
		});

		DB::statement('ALTER TABLE student_card_statuses AUTO_INCREMENT = 101');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('student_card_statuses');
	}
}
