<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('lang');
            $table->string('name')->unique();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('id')->references('id')->on('types');
            $table->foreign('lang')->references('slug')->on('langs');

            $table->primary([
                'id',
                'lang'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_langs');
    }
}
