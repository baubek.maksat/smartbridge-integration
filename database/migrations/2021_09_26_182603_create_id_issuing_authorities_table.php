<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdIssuingAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('id_issuing_authorities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('country_id');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('country_id')->references('id')->on('countries');
        });

        DB::statement('ALTER TABLE id_issuing_authorities AUTO_INCREMENT = 101');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('id_issuing_authorities');
    }
}
