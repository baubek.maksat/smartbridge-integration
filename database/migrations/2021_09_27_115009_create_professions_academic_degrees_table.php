<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionsAcademicDegreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professions_academic_degrees', function (Blueprint $table) {
            $table->unsignedBigInteger('profession_id');
            $table->unsignedBigInteger('academic_degree_id');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('profession_id')->references('id')->on('professions');
            $table->foreign('academic_degree_id')->references('id')->on('academic_degrees');

            $table->primary([
                'profession_id',
                'academic_degree_id'
            ], 'profession_id_academic_degree_id_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professions_academic_degrees');
    }
}
