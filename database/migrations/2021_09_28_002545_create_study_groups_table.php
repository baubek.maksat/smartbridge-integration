<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudyGroupsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('study_groups', function (Blueprint $table) {
			$table->id();
			$table->string('platonus_key')->nullable();
			$table->year('year');
			$table->unsignedBigInteger('term');
			$table->string('code');
			$table->string('name');
			$table->unsignedBigInteger('hours');
			$table->unsignedBigInteger('type_id');
			$table->unsignedBigInteger('subject_id');
			$table->unsignedBigInteger('teacher_id')->nullable();
			$table->unsignedBigInteger('study_form_id');
			$table->unsignedBigInteger('study_language_id');
			$table->unsignedBigInteger('student_count');
			$table->unsignedBigInteger('student_count_min');
			$table->unsignedBigInteger('student_count_max');
            $table->unsignedBigInteger('state_id');
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('type_id')->references('id')->on('study_group_types');
			$table->foreign('subject_id')->references('id')->on('subjects');
			$table->foreign('teacher_id')->references('id')->on('teacher_cards');
			$table->foreign('study_form_id')->references('id')->on('study_forms');
			$table->foreign('study_language_id')->references('id')->on('study_languages');
            $table->foreign('state_id')->references('id')->on('states');
		});

		DB::statement('ALTER TABLE study_groups AUTO_INCREMENT = 100000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('study_groups');
	}
}
