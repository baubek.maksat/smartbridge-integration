<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentCardsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('student_cards', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('user_id');
			$table->unsignedBigInteger('group_id')->nullable();
			$table->unsignedBigInteger('speciality_id');
			$table->unsignedBigInteger('status_id');
			$table->unsignedBigInteger('study_language_id');
			$table->unsignedBigInteger('study_form_id');
			$table->unsignedBigInteger('payment_form_id');
			$table->unsignedBigInteger('grant_type_id')->nullable();
			$table->string('grant_number')->nullable();
			$table->unsignedBigInteger('arrived_country_id')->nullable();
			$table->unsignedBigInteger('arrived_cato_id')->nullable();
			$table->unsignedBigInteger('dormitory_need_status_id')->nullable();
			$table->unsignedBigInteger('academic_calendar_id')->nullable();
			$table->unsignedBigInteger('curriculum_id')->nullable();
			$table->unsignedBigInteger('benefit_id')->nullable();
			$table->unsignedBigInteger('quota_id')->nullable();
			$table->unsignedBigInteger('course_number')->nullable();
            $table->unsignedBigInteger('state_id');
			$table->decimal('gpa', 3, 2)->nullable();
			$table->date('date_start');
			$table->date('date_end')->nullable();
			$table->string('transcript_number')->nullable();
			$table->json('integration_fields')->nullable();
			$table->timestamp('verified_at')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('user_id')->references('id')->on('users');
			$table->foreign('group_id')->references('id')->on('groups');
			$table->foreign('speciality_id')->references('id')->on('specialities');
			$table->foreign('study_language_id')->references('id')->on('study_languages');
			$table->foreign('study_form_id')->references('id')->on('study_forms');
			$table->foreign('payment_form_id')->references('id')->on('payment_forms');
			$table->foreign('grant_type_id')->references('id')->on('grant_types');
			$table->foreign('arrived_country_id')->references('id')->on('countries');
			$table->foreign('arrived_cato_id')->references('id')->on('catos');
			$table->foreign('dormitory_need_status_id')->references('id')->on('dormitory_need_statuses');
			$table->foreign('academic_calendar_id')->references('id')->on('academic_calendars');
			$table->foreign('curriculum_id')->references('id')->on('curriculums');
			$table->foreign('benefit_id')->references('id')->on('benefits');
			$table->foreign('status_id')->references('id')->on('student_card_statuses');
			$table->foreign('quota_id')->references('id')->on('quotas');
            $table->foreign('state_id')->references('id')->on('states');
		});

		DB::statement('ALTER TABLE student_cards AUTO_INCREMENT = 100000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('student_cards');
	}
}
