<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountViewsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('account_views', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('state_id');
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('state_id')->references('id')->on('states');
		});

		DB::statement('ALTER TABLE account_views AUTO_INCREMENT = 100001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('account_views');
	}
}
