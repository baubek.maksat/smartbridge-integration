<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLangsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('langs', function (Blueprint $table) {
			$table->id('id');
			$table->string('slug')->unique();
			$table->string('name')->unique();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();
		});

		DB::statement('ALTER TABLE langs AUTO_INCREMENT = 101');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('langs');
	}
}
