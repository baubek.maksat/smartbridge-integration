<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('lang_id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('app_id')->nullable();
            $table->string('ip');
            $table->text('ua');
            $table->string('ua_device_brand')->nullable();
            $table->string('ua_device_model')->nullable();
            $table->string('ua_os')->nullable();
            $table->unsignedBigInteger('ua_os_major')->nullable();
            $table->unsignedBigInteger('ua_os_minor')->nullable();
            $table->string('ua_browser')->nullable();
            $table->unsignedBigInteger('ua_browser_major')->nullable();
            $table->unsignedBigInteger('ua_browser_minor')->nullable();
            $table->text('token');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('lang_id')->references('id')->on('langs');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('app_id')->references('id')->on('apps');
        });

        DB::statement('ALTER TABLE visits AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
