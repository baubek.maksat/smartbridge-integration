<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsStudyGroupsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students_study_groups', function (Blueprint $table) {
			$table->unsignedBigInteger('student_id');
			$table->unsignedBigInteger('study_group_id');
			$table->unsignedBigInteger('cicle_id');
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('cicle_id')->references('id')->on('cicles');
			$table->foreign('student_id')->references('id')->on('student_cards');
			$table->foreign('study_group_id')->references('id')->on('study_groups');

			$table->primary([
				'student_id',
				'study_group_id'
			]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('students_study_groups');
	}
}
