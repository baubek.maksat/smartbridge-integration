<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('cato_id')->nullable();
            $table->unsignedBigInteger('school_id')->nullable();
            $table->string('series');
            $table->string('number');
            $table->date('date_of_issue');
            $table->decimal('average_score', 8, 2)->nullable();
            $table->enum('with_distinction', [0, 1])->default(0);
            $table->enum('gold_mark', [0, 1])->default(0);
            $table->text('src')->nullable();
            $table->text('application_src')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();
        });

        DB::statement('ALTER TABLE secs AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('secs');
    }
}
