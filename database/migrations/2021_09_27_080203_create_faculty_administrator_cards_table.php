<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacultyAdministratorCardsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('faculty_administrator_cards', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('faculty_id');
			$table->unsignedBigInteger('user_id');
			$table->date('date_start');
			$table->date('date_end')->nullable();
            $table->json('integration_fields')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('faculty_id')->references('id')->on('faculties');
			$table->foreign('user_id')->references('id')->on('users');
		});

		DB::statement('ALTER TABLE faculty_administrator_cards AUTO_INCREMENT = 100000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('faculty_administrator_cards');
	}
}
