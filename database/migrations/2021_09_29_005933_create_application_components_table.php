<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationComponentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_components', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('type_id');
			$table->unsignedBigInteger('view_id');
			$table->json('rules');
			$table->string('resource')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('type_id')->references('id')->on('application_component_types');
			$table->foreign('view_id')->references('id')->on('application_views');
		});

		DB::statement('ALTER TABLE application_components AUTO_INCREMENT = 100000000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('application_components');
	}
}
