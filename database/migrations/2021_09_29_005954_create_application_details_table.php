<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_details', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->unsignedBigInteger('type_id');
            $table->string('lang');
            $table->string('title', 511);
            $table->text('value')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('id')->references('id')->on('applications');
            $table->foreign('type_id')->references('id')->on('application_component_types');
            $table->foreign('lang')->references('slug')->on('langs');

            $table->primary([
                'id',
                'title',
                'lang'
            ]);
        });

        DB::statement('ALTER TABLE application_details AUTO_INCREMENT = 100000000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_details');
    }
}
