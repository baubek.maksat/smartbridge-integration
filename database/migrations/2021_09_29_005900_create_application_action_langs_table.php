<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationActionLangsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_action_langs', function (Blueprint $table) {
			$table->unsignedBigInteger('id');
			$table->string('lang');
			$table->string('name');
			$table->string('make');
			$table->string('process');
			$table->string('success');
			$table->string('fail');
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('id')->references('id')->on('application_actions');
			$table->foreign('lang')->references('slug')->on('langs');

			$table->primary([
				'id',
				'lang'
			]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('application_action_langs');
	}
}
