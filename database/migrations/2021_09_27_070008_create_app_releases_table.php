<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppReleasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_releases', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('platform_id');
            $table->unsignedBigInteger('state_id');
            $table->string('name');
            $table->string('version');
            $table->text('link')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('platform_id')->references('id')->on('platforms');
            $table->foreign('state_id')->references('id')->on('states');

            $table->unique([
                'platform_id',
                'name',
                'version'
            ]);
        });

        DB::statement('ALTER TABLE app_releases AUTO_INCREMENT = 100001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_releases');
    }
}
