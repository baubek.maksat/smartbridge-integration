<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationStagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_stages', function (Blueprint $table) {
			$table->id();
			$table->string('slug');
			$table->unsignedBigInteger('application_id');
			$table->json('conditions')->nullable();
			$table->json('processes')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('application_id')->references('id')->on('applications');
		});

		DB::statement('ALTER TABLE application_stages AUTO_INCREMENT = 100000000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('application_stages');
	}
}
