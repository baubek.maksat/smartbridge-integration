<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranscriptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transcripts', function (Blueprint $table) {
            $table->id();
            $table->string('platonus_key')->nullable();
            $table->unsignedBigInteger('student_id');
            $table->unsignedBigInteger('course_number');
            $table->unsignedBigInteger('term');
            $table->unsignedBigInteger('credits')->nullable();
            $table->string('traditional_mark')->nullable();
            $table->string('alpha_mark')->nullable();
            $table->decimal('numeral_mark', 2, 1)->nullable();
            $table->decimal('total_mark', 4, 1)->nullable();
            $table->string('exam_mark')->nullable();
            $table->unsignedBigInteger('state_id');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('student_id')->references('id')->on('student_cards');
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE transcripts AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transcripts');
    }
}
