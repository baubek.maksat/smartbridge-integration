<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserEmailConfirmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_email_confirms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('lang_id');
            $table->unsignedBigInteger('type_id');
            $table->string('ip');
            $table->text('ua');
            $table->string('ua_device_brand')->nullable();
            $table->string('ua_device_model')->nullable();
            $table->string('ua_os')->nullable();
            $table->unsignedBigInteger('ua_os_major')->nullable();
            $table->unsignedBigInteger('ua_os_minor')->nullable();
            $table->string('ua_browser')->nullable();
            $table->unsignedBigInteger('ua_browser_major')->nullable();
            $table->unsignedBigInteger('ua_browser_minor')->nullable();
            $table->string('src');
            $table->enum('fit', [0, 1])->default(1);
            $table->string('hash');
            $table->datetime('valid_until');
            $table->timestamp('verified_at')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('lang_id')->references('id')->on('langs');
            $table->foreign('type_id')->references('id')->on('types');
        });

        DB::statement('ALTER TABLE user_email_confirms AUTO_INCREMENT = 100000000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_email_confirms');
    }
}
