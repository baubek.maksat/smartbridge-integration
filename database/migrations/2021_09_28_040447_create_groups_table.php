<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('speciality_id');
            $table->unsignedBigInteger('adviser_id')->nullable();
            $table->unsignedBigInteger('state_id');
            $table->string('name');
            $table->json('integration_fields')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('speciality_id')->references('id')->on('specialities');
            $table->foreign('adviser_id')->references('id')->on('users');
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE groups AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
