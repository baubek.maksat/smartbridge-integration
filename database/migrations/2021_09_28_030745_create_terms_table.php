<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTermsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('terms', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('academic_calendar_id');
			$table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('state_id');
			$table->date('date_start')->nullable();
			$table->date('date_end')->nullable();
            $table->json('integration_fields')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('academic_calendar_id')->references('id')->on('academic_calendars');
			$table->foreign('type_id')->references('id')->on('term_types');
            $table->foreign('state_id')->references('id')->on('states');
		});

		DB::statement('ALTER TABLE terms AUTO_INCREMENT = 100001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('terms');
	}
}
