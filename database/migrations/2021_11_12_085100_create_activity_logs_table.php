<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_logs', function (Blueprint $table) {
            $table->id();
            $table->string('table');
            $table->enum('action', ['insert', 'update', 'delete']);
            $table->unsignedBigInteger('visit_id')->nullable();
            $table->string('key');
            $table->json('old')->nullable();
            $table->json('new')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('visit_id')->references('id')->on('visits');
        });

        DB::statement('ALTER TABLE activity_logs AUTO_INCREMENT = 100000000000000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_logs');
    }
}
