<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationViewStagesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_view_stages', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('application_view_id');
			$table->string('slug');
			$table->json('conditions')->nullable();
			$table->json('processes')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('application_view_id')->references('id')->on('application_views');
		});

		DB::statement('ALTER TABLE application_view_stages AUTO_INCREMENT = 100000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('application_view_stages');
	}
}
