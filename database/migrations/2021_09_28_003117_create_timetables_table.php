<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('week_number');
            $table->unsignedBigInteger('week_id');
            $table->unsignedBigInteger('classroom_id')->nullable();
            $table->unsignedBigInteger('lesson_hour_id');
            $table->unsignedBigInteger('study_group_id');
            $table->unsignedBigInteger('state_id');
            $table->enum('online', [0, 1]);
            $table->json('integration_fields')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('week_id')->references('id')->on('weeks');
            $table->foreign('classroom_id')->references('id')->on('classrooms');
            $table->foreign('lesson_hour_id')->references('id')->on('lesson_hours');
            $table->foreign('study_group_id')->references('id')->on('study_groups');
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE timetables AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetables');
    }
}
