<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationViewsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_views', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('category_id');
			$table->unsignedBigInteger('responsible_role_id')->nullable();
			$table->unsignedBigInteger('responsible_user_id')->nullable();
			$table->unsignedBigInteger('state_id');
			$table->unsignedBigInteger('term_of_consideration');
			$table->json('conditions')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

            $table->foreign('category_id')->references('id')->on('application_categories');
            $table->foreign('responsible_user_id')->references('id')->on('users');
		});

		DB::statement('ALTER TABLE application_views AUTO_INCREMENT = 100001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('application_views');
	}
}
