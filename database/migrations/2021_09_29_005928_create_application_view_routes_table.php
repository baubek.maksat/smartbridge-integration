<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationViewRoutesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_view_routes', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('stage_id');
			$table->unsignedBigInteger('action_id');
			$table->enum('responsible_type', ['responsible', 'role', 'user']);
			$table->unsignedBigInteger('responsible_role_id');
			$table->unsignedBigInteger('responsible_user_id')->nullable();
			$table->unsignedBigInteger('term')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('stage_id')->references('id')->on('application_view_stages')->onDelete('cascade');
			$table->foreign('action_id')->references('id')->on('application_actions');
			$table->foreign('responsible_user_id')->references('id')->on('users');
		});

		DB::statement('ALTER TABLE application_view_routes AUTO_INCREMENT = 100000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('application_view_routes');
	}
}
