<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDormitoryNeedStatusesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dormitory_need_statuses', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('state_id');
            $table->json('integration_fields')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('state_id')->references('id')->on('states');
		});

		DB::statement('ALTER TABLE dormitory_need_statuses AUTO_INCREMENT = 101');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('dormitory_need_statuses');
	}
}
