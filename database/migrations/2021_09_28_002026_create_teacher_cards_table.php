<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherCardsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teacher_cards', function (Blueprint $table) {
			$table->id();
			$table->string('platonus_key')->nullable();
			$table->unsignedBigInteger('department_id')->nullable();
			$table->unsignedBigInteger('user_id');
			$table->date('date_start');
			$table->date('date_end')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('department_id')->references('id')->on('departments');
			$table->foreign('user_id')->references('id')->on('users');
		});

		DB::statement('ALTER TABLE teacher_cards AUTO_INCREMENT = 100000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('teacher_cards');
	}
}
