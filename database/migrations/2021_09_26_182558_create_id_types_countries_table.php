<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdTypesCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('id_types_countries', function (Blueprint $table) {
            $table->unsignedBigInteger('id_type_id');
            $table->unsignedBigInteger('country_id');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('id_type_id')->references('id')->on('id_types');
            $table->foreign('country_id')->references('id')->on('countries');

            $table->primary([
                'id_type_id',
                'country_id'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('id_types_countries');
    }
}
