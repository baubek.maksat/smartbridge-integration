<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('visit_id')->nullable();
            $table->unsignedBigInteger('app_id')->nullable();
            $table->text('route');
            $table->json('params')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('visit_id')->references('id')->on('visits');
            $table->foreign('app_id')->references('id')->on('apps');
        });

        DB::statement('ALTER TABLE metrics AUTO_INCREMENT = 100000000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metrics');
    }
}
