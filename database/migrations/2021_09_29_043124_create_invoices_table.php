<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('account_id');
			$table->unsignedBigInteger('payment_view_id');
			$table->unsignedBigInteger('status_id');
			$table->datetime('due_date')->nullable()->comment('kz: мерзімнің өту күні, ru: срок оплаты');
			$table->decimal('debt', 9, 2)->comment('kz: қарыз, ru: долг');
			$table->decimal('paid', 9, 2)->default(0)->comment('kz: төленді, ru: погашено');
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('account_id')->references('id')->on('accounts');
			$table->foreign('payment_view_id')->references('id')->on('payment_views');
			$table->foreign('status_id')->references('id')->on('invoice_statuses');
		});

		DB::statement('ALTER TABLE invoices AUTO_INCREMENT = 100000000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('invoices');
	}
}
