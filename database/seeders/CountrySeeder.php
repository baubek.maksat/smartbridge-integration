<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\CountryLang;
use App\Models\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                'main' => [
                	'id' => 101,
                	'vmp_id' => 64,
                	'flag' => 'frontend/images/flags/png/101.png',
                	'code' => 'AW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Аруба',
                	],[
                		'lang' => 'ru',
                		'name' => 'Аруба',
                	],[
                		'lang' => 'en',
                		'name' => 'Aruba'
                	]
            	]
            ],[
                'main' => [
                	'id' => 102,
                	'vmp_id' => 149,
                	'flag' => 'frontend/images/flags/png/102.png',
                	'code' => 'AB'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Абхазия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Абхазия',
                	],[
                		'lang' => 'en',
                		'name' => 'Abkhazia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 103,
                	'vmp_id' => 315,
                	'flag' => 'frontend/images/flags/png/315.png',
                	'code' => 'IR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Иран',
                	],[
                		'lang' => 'ru',
                		'name' => 'Иран',
                	],[
                		'lang' => 'en',
                		'name' => 'Iran'
                	]
            	]
            ],[
                'main' => [
                	'id' => 104,
                	'vmp_id' => 263,
                	'flag' => 'frontend/images/flags/png/263.png',
                	'code' => 'BH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бахрейн',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бахрейн',
                	],[
                		'lang' => 'en',
                		'name' => 'Bahrain'
                	]
            	]
            ],[
                'main' => [
                	'id' => 105,
                	'vmp_id' => 1,
                	'flag' => 'frontend/images/flags/png/1.png',
                	'code' => 'AR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Аргентина',
                	],[
                		'lang' => 'ru',
                		'name' => 'Аргентина',
                	],[
                		'lang' => 'en',
                		'name' => 'Argentine'
                	]
            	]
            ],[
                'main' => [
                	'id' => 106,
                	'vmp_id' => 4,
                	'flag' => 'frontend/images/flags/png/4.png',
                	'code' => 'AU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Австралия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Австралия',
                	],[
                		'lang' => 'en',
                		'name' => 'Australia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 107,
                	'vmp_id' => 3,
                	'flag' => 'frontend/images/flags/png/3.png',
                	'code' => 'AT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Австрия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Австрия',
                	],[
                		'lang' => 'en',
                		'name' => 'Austria'
                	]
            	]
            ],[
                'main' => [
                	'id' => 108,
                	'vmp_id' => 148,
                	'flag' => 'frontend/images/flags/png/148.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Аджария',
                	],[
                		'lang' => 'ru',
                		'name' => 'Аджария',
                	],[
                		'lang' => 'en',
                		'name' => 'Adjara'
                	]
            	]
            ],[
                'main' => [
                	'id' => 109,
                	'vmp_id' => 266,
                	'flag' => 'frontend/images/flags/png/266.png',
                	'code' => 'AL'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Албания',
                	],[
                		'lang' => 'ru',
                		'name' => 'Албания',
                	],[
                		'lang' => 'en',
                		'name' => 'Albania'
                	]
            	]
            ],[
                'main' => [
                	'id' => 110,
                	'vmp_id' => 267,
                	'flag' => 'frontend/images/flags/png/267.png',
                	'code' => 'DZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Алжир',
                	],[
                		'lang' => 'ru',
                		'name' => 'Алжир',
                	],[
                		'lang' => 'en',
                		'name' => 'Algeria'
                	]
            	]
            ],[
                'main' => [
                	'id' => 111,
                	'vmp_id' => 325,
                	'flag' => 'frontend/images/flags/png/325.png',
                	'code' => 'AI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ангилья',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ангилья',
                	],[
                		'lang' => 'en',
                		'name' => 'Anguilla'
                	]
            	]
            ],[
                'main' => [
                	'id' => 112,
                	'vmp_id' => 59,
                	'flag' => 'frontend/images/flags/png/59.png',
                	'code' => 'AO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ангола',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ангола',
                	],[
                		'lang' => 'en',
                		'name' => 'Angola'
                	]
            	]
            ],[
                'main' => [
                	'id' => 113,
                	'vmp_id' => 81,
                	'flag' => 'frontend/images/flags/png/81.png',
                	'code' => 'AD'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Андорра',
                	],[
                		'lang' => 'ru',
                		'name' => 'Андорра',
                	],[
                		'lang' => 'en',
                		'name' => 'Andorra'
                	]
            	]
            ],[
                'main' => [
                	'id' => 114,
                	'vmp_id' => 322,
                	'flag' => 'frontend/images/flags/png/322.png',
                	'code' => 'AQ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Антарктида',
                	],[
                		'lang' => 'ru',
                		'name' => 'Антарктида',
                	],[
                		'lang' => 'en',
                		'name' => 'Antarctica'
                	]
            	]
            ],[
                'main' => [
                	'id' => 115,
                	'vmp_id' => 63,
                	'flag' => 'frontend/images/flags/png/63.png',
                	'code' => 'AN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Антиль аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Антильские острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Antilles'
                	]
            	]
            ],[
                'main' => [
                	'id' => 116,
                	'vmp_id' => 70,
                	'flag' => 'frontend/images/flags/png/70.png',
                	'code' => 'BD'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бангладеш',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бангладеш',
                	],[
                		'lang' => 'en',
                		'name' => 'Bangladesh'
                	]
            	]
            ],[
                'main' => [
                	'id' => 117,
                	'vmp_id' => 71,
                	'flag' => 'frontend/images/flags/png/71.png',
                	'code' => 'BB'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Барбадос',
                	],[
                		'lang' => 'ru',
                		'name' => 'Барбадос',
                	],[
                		'lang' => 'en',
                		'name' => 'Barbados'
                	]
            	]
            ],[
                'main' => [
                	'id' => 118,
                	'vmp_id' => 29,
                	'flag' => 'frontend/images/flags/png/29.png',
                	'code' => 'BZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Белиз',
                	],[
                		'lang' => 'ru',
                		'name' => 'Белиз',
                	],[
                		'lang' => 'en',
                		'name' => 'Belize'
                	]
            	]
            ],[
                'main' => [
                	'id' => 119,
                	'vmp_id' => 264,
                	'flag' => 'frontend/images/flags/png/264.png',
                	'code' => 'BE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бельгия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бельгия',
                	],[
                		'lang' => 'en',
                		'name' => 'Belgium'
                	]
            	]
            ],[
                'main' => [
                	'id' => 120,
                	'vmp_id' => 319,
                	'flag' => 'frontend/images/flags/png/319.png',
                	'code' => 'BJ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бенин',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бенин',
                	],[
                		'lang' => 'en',
                		'name' => 'Benin'
                	]
            	]
            ],[
                'main' => [
                	'id' => 121,
                	'vmp_id' => 78,
                	'flag' => 'frontend/images/flags/png/78.png',
                	'code' => 'BM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бермуд аралы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бермудские острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Bermuda'
                	]
            	]
            ],[
                'main' => [
                	'id' => 122,
                	'vmp_id' => 50,
                	'flag' => 'frontend/images/flags/png/50.png',
                	'code' => 'BG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Болгария',
                	],[
                		'lang' => 'ru',
                		'name' => 'Болгария',
                	],[
                		'lang' => 'en',
                		'name' => 'Bulgaria'
                	]
            	]
            ],[
                'main' => [
                	'id' => 123,
                	'vmp_id' => 52,
                	'flag' => 'frontend/images/flags/png/52.png',
                	'code' => 'BO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Боливия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Боливия',
                	],[
                		'lang' => 'en',
                		'name' => 'Bolivia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 124,
                	'vmp_id' => 53,
                	'flag' => 'frontend/images/flags/png/53.png',
                	'code' => 'BW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ботсвана',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ботсвана',
                	],[
                		'lang' => 'en',
                		'name' => 'Botswana'
                	]
            	]
            ],[
                'main' => [
                	'id' => 125,
                	'vmp_id' => 310,
                	'flag' => 'frontend/images/flags/png/310.png',
                	'code' => 'ID'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Индонезия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Индонезия',
                	],[
                		'lang' => 'en',
                		'name' => 'Indonesia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 126,
                	'vmp_id' => 35,
                	'flag' => 'frontend/images/flags/png/35.png',
                	'code' => 'VN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Вьетнам',
                	],[
                		'lang' => 'ru',
                		'name' => 'Вьетнам',
                	],[
                		'lang' => 'en',
                		'name' => 'Vietnam'
                	]
            	]
            ],[
                'main' => [
                	'id' => 127,
                	'vmp_id' => 87,
                	'flag' => 'frontend/images/flags/png/87.png',
                	'code' => 'VG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Британ Виргин аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Британские Виргинские острова',
                	],[
                		'lang' => 'en',
                		'name' => 'The British Virgin Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 128,
                	'vmp_id' => 55,
                	'flag' => 'frontend/images/flags/png/55.png',
                	'code' => 'BN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бруней-Даруссалам',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бруней-Даруссалам',
                	],[
                		'lang' => 'en',
                		'name' => 'Brunei Darussalam'
                	]
            	]
            ],[
                'main' => [
                	'id' => 129,
                	'vmp_id' => 84,
                	'flag' => 'frontend/images/flags/png/84.png',
                	'code' => 'VA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ватикан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ватикан',
                	],[
                		'lang' => 'en',
                		'name' => 'Vatican'
                	]
            	]
            ],[
                'main' => [
                	'id' => 130,
                	'vmp_id' => 56,
                	'flag' => 'frontend/images/flags/png/56.png',
                	'code' => 'BI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бурунди',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бурунди',
                	],[
                		'lang' => 'en',
                		'name' => 'Burundi'
                	]
            	]
            ],[
                'main' => [
                	'id' => 131,
                	'vmp_id' => 289,
                	'flag' => 'frontend/images/flags/png/289.png',
                	'code' => 'VU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Вануату',
                	],[
                		'lang' => 'ru',
                		'name' => 'Вануату',
                	],[
                		'lang' => 'en',
                		'name' => 'Vanuatu'
                	]
            	]
            ],[
                'main' => [
                	'id' => 132,
                	'vmp_id' => 33,
                	'flag' => 'frontend/images/flags/png/33.png',
                	'code' => 'HU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Венгрия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Венгрия',
                	],[
                		'lang' => 'en',
                		'name' => 'Hungary'
                	]
            	]
            ],[
                'main' => [
                	'id' => 133,
                	'vmp_id' => 85,
                	'flag' => 'frontend/images/flags/png/85.png',
                	'code' => 'VE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Венесуэла',
                	],[
                		'lang' => 'ru',
                		'name' => 'Венесуэла',
                	],[
                		'lang' => 'en',
                		'name' => 'Venezuela'
                	]
            	]
            ],[
                'main' => [
                	'id' => 134,
                	'vmp_id' => 89,
                	'flag' => 'frontend/images/flags/png/89.png',
                	'code' => 'GA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Габон',
                	],[
                		'lang' => 'ru',
                		'name' => 'Габон',
                	],[
                		'lang' => 'en',
                		'name' => 'Gabon'
                	]
            	]
            ],[
                'main' => [
                	'id' => 135,
                	'vmp_id' => 38,
                	'flag' => 'frontend/images/flags/png/38.png',
                	'code' => 'HT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гаити',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гаити',
                	],[
                		'lang' => 'en',
                		'name' => 'Haiti'
                	]
            	]
            ],[
                'main' => [
                	'id' => 136,
                	'vmp_id' => 39,
                	'flag' => 'frontend/images/flags/png/39.png',
                	'code' => 'GY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гайана',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гайана',
                	],[
                		'lang' => 'en',
                		'name' => 'Guyana'
                	]
            	]
            ],[
                'main' => [
                	'id' => 137,
                	'vmp_id' => 40,
                	'flag' => 'frontend/images/flags/png/40.png',
                	'code' => 'GM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гамбия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гамбия',
                	],[
                		'lang' => 'en',
                		'name' => 'Gambia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 138,
                	'vmp_id' => 44,
                	'flag' => 'frontend/images/flags/png/44.png',
                	'code' => 'GH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гана',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гана',
                	],[
                		'lang' => 'en',
                		'name' => 'Ghana'
                	]
            	]
            ],[
                'main' => [
                	'id' => 139,
                	'vmp_id' => 76,
                	'flag' => 'frontend/images/flags/png/76.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гвиана',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гвиана',
                	],[
                		'lang' => 'en',
                		'name' => 'Guiana'
                	]
            	]
            ],[
                'main' => [
                	'id' => 140,
                	'vmp_id' => 77,
                	'flag' => 'frontend/images/flags/png/77.png',
                	'code' => 'GN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гвинея',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гвинея',
                	],[
                		'lang' => 'en',
                		'name' => 'Guinea'
                	]
            	]
            ],[
                'main' => [
                	'id' => 141,
                	'vmp_id' => 80,
                	'flag' => 'frontend/images/flags/png/80.png',
                	'code' => 'GW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гвинея-Бисау',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гвинея-Бисау',
                	],[
                		'lang' => 'en',
                		'name' => 'Guinea-Bissau'
                	]
            	]
            ],[
                'main' => [
                	'id' => 142,
                	'vmp_id' => 58,
                	'flag' => 'frontend/images/flags/png/58.png',
                	'code' => 'GG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гернси',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гернси',
                	],[
                		'lang' => 'en',
                		'name' => 'Guernsey'
                	]
            	]
            ],[
                'main' => [
                	'id' => 143,
                	'vmp_id' => 22,
                	'flag' => 'frontend/images/flags/png/22.png',
                	'code' => 'GI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гибралтар',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гибралтар',
                	],[
                		'lang' => 'en',
                		'name' => 'Gibraltar'
                	]
            	]
            ],[
                'main' => [
                	'id' => 144,
                	'vmp_id' => 25,
                	'flag' => 'frontend/images/flags/png/25.png',
                	'code' => 'HN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гондурас',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гондурас',
                	],[
                		'lang' => 'en',
                		'name' => 'Honduras'
                	]
            	]
            ],[
                'main' => [
                	'id' => 145,
                	'vmp_id' => 30,
                	'flag' => 'frontend/images/flags/png/30.png',
                	'code' => 'HK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гонконг = Сянган',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гонконг = Сянган',
                	],[
                		'lang' => 'en',
                		'name' => 'Hong Kong'
                	]
            	]
            ],[
                'main' => [
                	'id' => 146,
                	'vmp_id' => 321,
                	'flag' => 'frontend/images/flags/png/321.png',
                	'code' => 'GD'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гренада',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гренада',
                	],[
                		'lang' => 'en',
                		'name' => 'Grenada'
                	]
            	]
            ],[
                'main' => [
                	'id' => 147,
                	'vmp_id' => 31,
                	'flag' => 'frontend/images/flags/png/31.png',
                	'code' => 'GL'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гренландия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гренландия',
                	],[
                		'lang' => 'en',
                		'name' => 'Greenland'
                	]
            	]
            ],[
                'main' => [
                	'id' => 148,
                	'vmp_id' => 32,
                	'flag' => 'frontend/images/flags/png/32.png',
                	'code' => 'GR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Греция',
                	],[
                		'lang' => 'ru',
                		'name' => 'Греция',
                	],[
                		'lang' => 'en',
                		'name' => 'Greece'
                	]
            	]
            ],[
                'main' => [
                	'id' => 149,
                	'vmp_id' => 318,
                	'flag' => 'frontend/images/flags/png/318.png',
                	'code' => 'GU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гуам',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гуам',
                	],[
                		'lang' => 'en',
                		'name' => 'Guam'
                	]
            	]
            ],[
                'main' => [
                	'id' => 150,
                	'vmp_id' => 320,
                	'flag' => 'frontend/images/flags/png/320.png',
                	'code' => 'DK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Дания',
                	],[
                		'lang' => 'ru',
                		'name' => 'Дания',
                	],[
                		'lang' => 'en',
                		'name' => 'Denmark'
                	]
            	]
            ],[
                'main' => [
                	'id' => 151,
                	'vmp_id' => 79,
                	'flag' => 'frontend/images/flags/png/79.png',
                	'code' => 'JE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Джерси',
                	],[
                		'lang' => 'ru',
                		'name' => 'Джерси',
                	],[
                		'lang' => 'en',
                		'name' => 'Jersey'
                	]
            	]
            ],[
                'main' => [
                	'id' => 152,
                	'vmp_id' => 60,
                	'flag' => 'frontend/images/flags/png/60.png',
                	'code' => 'DJ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Джибути',
                	],[
                		'lang' => 'ru',
                		'name' => 'Джибути',
                	],[
                		'lang' => 'en',
                		'name' => 'Djibouti'
                	]
            	]
            ],[
                'main' => [
                	'id' => 153,
                	'vmp_id' => 83,
                	'flag' => 'frontend/images/flags/png/83.png',
                	'code' => 'DM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Доминика',
                	],[
                		'lang' => 'ru',
                		'name' => 'Доминика',
                	],[
                		'lang' => 'en',
                		'name' => 'Dominica'
                	]
            	]
            ],[
                'main' => [
                	'id' => 154,
                	'vmp_id' => 317,
                	'flag' => 'frontend/images/flags/png/317.png',
                	'code' => 'DO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Доминикан республикасы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Доминиканская республика',
                	],[
                		'lang' => 'en',
                		'name' => 'Dominican Republic'
                	]
            	]
            ],[
                'main' => [
                	'id' => 155,
                	'vmp_id' => 88,
                	'flag' => 'frontend/images/flags/png/88.png',
                	'code' => 'EG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Египет',
                	],[
                		'lang' => 'ru',
                		'name' => 'Египет',
                	],[
                		'lang' => 'en',
                		'name' => 'Egypt'
                	]
            	]
            ],[
                'main' => [
                	'id' => 156,
                	'vmp_id' => 159,
                	'flag' => 'frontend/images/flags/png/159.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Закавказье',
                	],[
                		'lang' => 'ru',
                		'name' => 'Закавказье',
                	],[
                		'lang' => 'en',
                		'name' => 'Transcaucasia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 157,
                	'vmp_id' => 302,
                	'flag' => 'frontend/images/flags/png/302.png',
                	'code' => 'ZM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Замбия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Замбия',
                	],[
                		'lang' => 'en',
                		'name' => 'Zambia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 158,
                	'vmp_id' => 303,
                	'flag' => 'frontend/images/flags/png/303.png',
                	'code' => 'EH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Батыс Сахара',
                	],[
                		'lang' => 'ru',
                		'name' => 'Западная Сахара',
                	],[
                		'lang' => 'en',
                		'name' => 'Western Sahara'
                	]
            	]
            ],[
                'main' => [
                	'id' => 159,
                	'vmp_id' => 106,
                	'flag' => 'frontend/images/flags/png/106.png',
                	'code' => 'ZW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Зимбабве',
                	],[
                		'lang' => 'ru',
                		'name' => 'Зимбабве',
                	],[
                		'lang' => 'en',
                		'name' => 'Zimbabwe'
                	]
            	]
            ],[
                'main' => [
                	'id' => 160,
                	'vmp_id' => 308,
                	'flag' => 'frontend/images/flags/png/308.png',
                	'code' => 'IL'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Израиль',
                	],[
                		'lang' => 'ru',
                		'name' => 'Израиль',
                	],[
                		'lang' => 'en',
                		'name' => 'Israel'
                	]
            	]
            ],[
                'main' => [
                	'id' => 161,
                	'vmp_id' => 311,
                	'flag' => 'frontend/images/flags/png/311.png',
                	'code' => 'JO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Иордания',
                	],[
                		'lang' => 'ru',
                		'name' => 'Иордания',
                	],[
                		'lang' => 'en',
                		'name' => 'Jordan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 162,
                	'vmp_id' => 312,
                	'flag' => 'frontend/images/flags/png/312.png',
                	'code' => 'IQ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ирак',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ирак',
                	],[
                		'lang' => 'en',
                		'name' => 'Iraq'
                	]
            	]
            ],[
                'main' => [
                	'id' => 163,
                	'vmp_id' => 20,
                	'flag' => 'frontend/images/flags/png/20.png',
                	'code' => 'DE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Германия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Германия',
                	],[
                		'lang' => 'en',
                		'name' => 'Germany'
                	]
            	]
            ],[
                'main' => [
                	'id' => 164,
                	'vmp_id' => 57,
                	'flag' => 'frontend/images/flags/png/57.png',
                	'code' => 'BT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бутан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бутан',
                	],[
                		'lang' => 'en',
                		'name' => 'Butane'
                	]
            	]
            ],[
                'main' => [
                	'id' => 165,
                	'vmp_id' => 73,
                	'flag' => 'frontend/images/flags/png/73.png',
                	'code' => 'GP'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гваделупа',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гваделупа',
                	],[
                		'lang' => 'en',
                		'name' => 'Gvadelupe'
                	]
            	]
            ],[
                'main' => [
                	'id' => 166,
                	'vmp_id' => 75,
                	'flag' => 'frontend/images/flags/png/75.png',
                	'code' => 'GT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Гватемала',
                	],[
                		'lang' => 'ru',
                		'name' => 'Гватемала',
                	],[
                		'lang' => 'en',
                		'name' => 'Gwatemala'
                	]
            	]
            ],[
                'main' => [
                	'id' => 167,
                	'vmp_id' => 314,
                	'flag' => 'frontend/images/flags/png/314.png',
                	'code' => 'IE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ирландия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ирландия',
                	],[
                		'lang' => 'en',
                		'name' => 'Ireland'
                	]
            	]
            ],[
                'main' => [
                	'id' => 168,
                	'vmp_id' => 286,
                	'flag' => 'frontend/images/flags/png/286.png',
                	'code' => 'IS'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Исландия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Исландия',
                	],[
                		'lang' => 'en',
                		'name' => 'Iceland'
                	]
            	]
            ],[
                'main' => [
                	'id' => 169,
                	'vmp_id' => 292,
                	'flag' => 'frontend/images/flags/png/292.png',
                	'code' => 'ES'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Испания',
                	],[
                		'lang' => 'ru',
                		'name' => 'Испания',
                	],[
                		'lang' => 'en',
                		'name' => 'Spain'
                	]
            	]
            ],[
                'main' => [
                	'id' => 170,
                	'vmp_id' => 293,
                	'flag' => 'frontend/images/flags/png/293.png',
                	'code' => 'IT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Италия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Италия',
                	],[
                		'lang' => 'en',
                		'name' => 'Italy'
                	]
            	]
            ],[
                'main' => [
                	'id' => 171,
                	'vmp_id' => 297,
                	'flag' => 'frontend/images/flags/png/297.png',
                	'code' => 'YE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Йемен',
                	],[
                		'lang' => 'ru',
                		'name' => 'Йемен',
                	],[
                		'lang' => 'en',
                		'name' => 'Yemen'
                	]
            	]
            ],[
                'main' => [
                	'id' => 172,
                	'vmp_id' => 301,
                	'flag' => 'frontend/images/flags/png/301.png',
                	'code' => 'CO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Колумбия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Колумбия',
                	],[
                		'lang' => 'en',
                		'name' => 'Colombia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 173,
                	'vmp_id' => 235,
                	'flag' => 'frontend/images/flags/png/235.png',
                	'code' => 'KW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Кувейт',
                	],[
                		'lang' => 'ru',
                		'name' => 'Кувейт',
                	],[
                		'lang' => 'en',
                		'name' => 'Kuwait'
                	]
            	]
            ],[
                'main' => [
                	'id' => 174,
                	'vmp_id' => 259,
                	'flag' => 'frontend/images/flags/png/259.png',
                	'code' => 'CN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'ҚХР',
                	],[
                		'lang' => 'ru',
                		'name' => 'КНР',
                	],[
                		'lang' => 'en',
                		'name' => 'China'
                	]
            	]
            ],[
                'main' => [
                	'id' => 175,
                	'vmp_id' => 313,
                	'flag' => 'frontend/images/flags/png/313.png',
                	'code' => 'KY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Кайман аралы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Каймановы острова',
                	],[
                		'lang' => 'en',
                		'name' => 'The Cayman Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 176,
                	'vmp_id' => 168,
                	'flag' => 'frontend/images/flags/png/168.png',
                	'code' => 'KH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Камбоджа',
                	],[
                		'lang' => 'ru',
                		'name' => 'Камбоджа',
                	],[
                		'lang' => 'en',
                		'name' => 'Cambodia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 177,
                	'vmp_id' => 251,
                	'flag' => 'frontend/images/flags/png/251.png',
                	'code' => 'CM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Камерун',
                	],[
                		'lang' => 'ru',
                		'name' => 'Камерун',
                	],[
                		'lang' => 'en',
                		'name' => 'Cameroon'
                	]
            	]
            ],[
                'main' => [
                	'id' => 178,
                	'vmp_id' => 252,
                	'flag' => 'frontend/images/flags/png/252.png',
                	'code' => 'CA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Канада',
                	],[
                		'lang' => 'ru',
                		'name' => 'Канада',
                	],[
                		'lang' => 'en',
                		'name' => 'Canada'
                	]
            	]
            ],[
                'main' => [
                	'id' => 179,
                	'vmp_id' => 253,
                	'flag' => 'frontend/images/flags/png/253.png',
                	'code' => 'QA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Катар',
                	],[
                		'lang' => 'ru',
                		'name' => 'Катар',
                	],[
                		'lang' => 'en',
                		'name' => 'Qatar'
                	]
            	]
            ],[
                'main' => [
                	'id' => 180,
                	'vmp_id' => 257,
                	'flag' => 'frontend/images/flags/png/257.png',
                	'code' => 'KE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Кения',
                	],[
                		'lang' => 'ru',
                		'name' => 'Кения',
                	],[
                		'lang' => 'en',
                		'name' => 'Kenya'
                	]
            	]
            ],[
                'main' => [
                	'id' => 181,
                	'vmp_id' => 258,
                	'flag' => 'frontend/images/flags/png/258.png',
                	'code' => 'CY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Кипр',
                	],[
                		'lang' => 'ru',
                		'name' => 'Кипр',
                	],[
                		'lang' => 'en',
                		'name' => 'Cyprus'
                	]
            	]
            ],[
                'main' => [
                	'id' => 182,
                	'vmp_id' => 23,
                	'flag' => 'frontend/images/flags/png/23.png',
                	'code' => 'KI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Кирибати',
                	],[
                		'lang' => 'ru',
                		'name' => 'Кирибати',
                	],[
                		'lang' => 'en',
                		'name' => 'Kiribati'
                	]
            	]
            ],[
                'main' => [
                	'id' => 183,
                	'vmp_id' => 283,
                	'flag' => 'frontend/images/flags/png/283.png',
                	'code' => 'KM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Комор аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Коморские острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Comoros'
                	]
            	]
            ],[
                'main' => [
                	'id' => 184,
                	'vmp_id' => 306,
                	'flag' => 'frontend/images/flags/png/306.png',
                	'code' => 'CG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Конго республикасы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Конго республика',
                	],[
                		'lang' => 'en',
                		'name' => 'Republic of Congo'
                	]
            	]
            ],[
                'main' => [
                	'id' => 185,
                	'vmp_id' => 104,
                	'flag' => 'frontend/images/flags/png/104.png',
                	'code' => 'XK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Косово',
                	],[
                		'lang' => 'ru',
                		'name' => 'Косово',
                	],[
                		'lang' => 'en',
                		'name' => 'Kosovo'
                	]
            	]
            ],[
                'main' => [
                	'id' => 186,
                	'vmp_id' => 227,
                	'flag' => 'frontend/images/flags/png/227.png',
                	'code' => 'CR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Коста-Рика',
                	],[
                		'lang' => 'ru',
                		'name' => 'Коста-Рика',
                	],[
                		'lang' => 'en',
                		'name' => 'Costa Rica'
                	]
            	]
            ],[
                'main' => [
                	'id' => 187,
                	'vmp_id' => 294,
                	'flag' => 'frontend/images/flags/png/294.png',
                	'code' => 'CU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Куба',
                	],[
                		'lang' => 'ru',
                		'name' => 'Куба',
                	],[
                		'lang' => 'en',
                		'name' => 'Cuba'
                	]
            	]
            ],[
                'main' => [
                	'id' => 188,
                	'vmp_id' => 171,
                	'flag' => 'frontend/images/flags/png/171.png',
                	'code' => 'CK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Кука аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Кука острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Cook Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 189,
                	'vmp_id' => 236,
                	'flag' => 'frontend/images/flags/png/236.png',
                	'code' => 'LA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Лаос',
                	],[
                		'lang' => 'ru',
                		'name' => 'Лаос',
                	],[
                		'lang' => 'en',
                		'name' => 'Laos'
                	]
            	]
            ],[
                'main' => [
                	'id' => 190,
                	'vmp_id' => 126,
                	'flag' => 'frontend/images/flags/png/126.png',
                	'code' => 'LV'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Латвия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Латвия',
                	],[
                		'lang' => 'en',
                		'name' => 'Latvia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 191,
                	'vmp_id' => 296,
                	'flag' => 'frontend/images/flags/png/296.png',
                	'code' => 'LS'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Лесото',
                	],[
                		'lang' => 'ru',
                		'name' => 'Лесото',
                	],[
                		'lang' => 'en',
                		'name' => 'Lesotho'
                	]
            	]
            ],[
                'main' => [
                	'id' => 192,
                	'vmp_id' => 237,
                	'flag' => 'frontend/images/flags/png/237.png',
                	'code' => 'LR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Либерия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Либерия',
                	],[
                		'lang' => 'en',
                		'name' => 'Liberia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 193,
                	'vmp_id' => 239,
                	'flag' => 'frontend/images/flags/png/239.png',
                	'code' => 'LB'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ливан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ливан',
                	],[
                		'lang' => 'en',
                		'name' => 'Lebanon'
                	]
            	]
            ],[
                'main' => [
                	'id' => 194,
                	'vmp_id' => 240,
                	'flag' => 'frontend/images/flags/png/240.png',
                	'code' => 'LY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ливия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ливия',
                	],[
                		'lang' => 'en',
                		'name' => 'Libya'
                	]
            	]
            ],[
                'main' => [
                	'id' => 195,
                	'vmp_id' => 127,
                	'flag' => 'frontend/images/flags/png/127.png',
                	'code' => 'LT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Литва',
                	],[
                		'lang' => 'ru',
                		'name' => 'Литва',
                	],[
                		'lang' => 'en',
                		'name' => 'Lithuania'
                	]
            	]
            ],[
                'main' => [
                	'id' => 196,
                	'vmp_id' => 241,
                	'flag' => 'frontend/images/flags/png/241.png',
                	'code' => 'LI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Лихтенштейн',
                	],[
                		'lang' => 'ru',
                		'name' => 'Лихтенштейн',
                	],[
                		'lang' => 'en',
                		'name' => 'Liechtenstein'
                	]
            	]
            ],[
                'main' => [
                	'id' => 197,
                	'vmp_id' => 242,
                	'flag' => 'frontend/images/flags/png/242.png',
                	'code' => 'LU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Люксембург',
                	],[
                		'lang' => 'ru',
                		'name' => 'Люксембург',
                	],[
                		'lang' => 'en',
                		'name' => 'Luxembourg'
                	]
            	]
            ],[
                'main' => [
                	'id' => 198,
                	'vmp_id' => 300,
                	'flag' => 'frontend/images/flags/png/300.png',
                	'code' => 'MU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Маврикий',
                	],[
                		'lang' => 'ru',
                		'name' => 'Маврикий',
                	],[
                		'lang' => 'en',
                		'name' => 'Mauritius'
                	]
            	]
            ],[
                'main' => [
                	'id' => 199,
                	'vmp_id' => 243,
                	'flag' => 'frontend/images/flags/png/243.png',
                	'code' => 'MR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мавритания',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мавритания',
                	],[
                		'lang' => 'en',
                		'name' => 'Mauritania'
                	]
            	]
            ],[
                'main' => [
                	'id' => 200,
                	'vmp_id' => 245,
                	'flag' => 'frontend/images/flags/png/245.png',
                	'code' => 'MG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мадагаскар',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мадагаскар',
                	],[
                		'lang' => 'en',
                		'name' => 'Madagascar'
                	]
            	]
            ],[
                'main' => [
                	'id' => 201,
                	'vmp_id' => 284,
                	'flag' => 'frontend/images/flags/png/284.png',
                	'code' => 'YT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Майотта',
                	],[
                		'lang' => 'ru',
                		'name' => 'Майотта',
                	],[
                		'lang' => 'en',
                		'name' => 'Mayotte'
                	]
            	]
            ],[
                'main' => [
                	'id' => 202,
                	'vmp_id' => 65,
                	'flag' => 'frontend/images/flags/png/65.png',
                	'code' => 'MO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Макао (Аомынь)',
                	],[
                		'lang' => 'ru',
                		'name' => 'Макао (Аомынь)',
                	],[
                		'lang' => 'en',
                		'name' => 'Macau'
                	]
            	]
            ],[
                'main' => [
                	'id' => 203,
                	'vmp_id' => 103,
                	'flag' => 'frontend/images/flags/png/103.png',
                	'code' => 'MK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Македония ',
                	],[
                		'lang' => 'ru',
                		'name' => 'Македония',
                	],[
                		'lang' => 'en',
                		'name' => 'Macedonia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 204,
                	'vmp_id' => 244,
                	'flag' => 'frontend/images/flags/png/244.png',
                	'code' => 'MW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Малави',
                	],[
                		'lang' => 'ru',
                		'name' => 'Малави',
                	],[
                		'lang' => 'en',
                		'name' => 'Malawi'
                	]
            	]
            ],[
                'main' => [
                	'id' => 205,
                	'vmp_id' => 246,
                	'flag' => 'frontend/images/flags/png/246.png',
                	'code' => 'MY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Малайзия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Малайзия',
                	],[
                		'lang' => 'en',
                		'name' => 'Malaysia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 206,
                	'vmp_id' => 247,
                	'flag' => 'frontend/images/flags/png/247.png',
                	'code' => 'ML'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мали',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мали',
                	],[
                		'lang' => 'en',
                		'name' => 'Mali'
                	]
            	]
            ],[
                'main' => [
                	'id' => 207,
                	'vmp_id' => 248,
                	'flag' => 'frontend/images/flags/png/248.png',
                	'code' => 'MV'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мальдив республикасы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мальдивская республика',
                	],[
                		'lang' => 'en',
                		'name' => 'Republic of Maldives'
                	]
            	]
            ],[
                'main' => [
                	'id' => 208,
                	'vmp_id' => 249,
                	'flag' => 'frontend/images/flags/png/249.png',
                	'code' => 'MT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мальта',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мальта',
                	],[
                		'lang' => 'en',
                		'name' => 'Malta'
                	]
            	]
            ],[
                'main' => [
                	'id' => 209,
                	'vmp_id' => 145,
                	'flag' => 'frontend/images/flags/png/145.png',
                	'code' => 'MA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Марокко',
                	],[
                		'lang' => 'ru',
                		'name' => 'Марокко',
                	],[
                		'lang' => 'en',
                		'name' => 'Morocco'
                	]
            	]
            ],[
                'main' => [
                	'id' => 210,
                	'vmp_id' => 256,
                	'flag' => 'frontend/images/flags/png/256.png',
                	'code' => 'MH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Маршал аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Маршалловы острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Marshall Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 211,
                	'vmp_id' => 112,
                	'flag' => 'frontend/images/flags/png/112.png',
                	'code' => 'RW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Руанда',
                	],[
                		'lang' => 'ru',
                		'name' => 'Руанда',
                	],[
                		'lang' => 'en',
                		'name' => 'Rwanda'
                	]
            	]
            ],[
                'main' => [
                	'id' => 212,
                	'vmp_id' => 282,
                	'flag' => 'frontend/images/flags/png/282.png',
                	'code' => 'RO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Румыния',
                	],[
                		'lang' => 'ru',
                		'name' => 'Румыния',
                	],[
                		'lang' => 'en',
                		'name' => 'Romania'
                	]
            	]
            ],[
                'main' => [
                	'id' => 213,
                	'vmp_id' => 160,
                	'flag' => 'frontend/images/flags/png/160.png',
                	'code' => 'MQ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мартиника',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мартиника',
                	],[
                		'lang' => 'en',
                		'name' => 'Martinique'
                	]
            	]
            ],[
                'main' => [
                	'id' => 214,
                	'vmp_id' => 161,
                	'flag' => 'frontend/images/flags/png/161.png',
                	'code' => 'MX'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мексика',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мексика',
                	],[
                		'lang' => 'en',
                		'name' => 'Mexico'
                	]
            	]
            ],[
                'main' => [
                	'id' => 215,
                	'vmp_id' => 114,
                	'flag' => 'frontend/images/flags/png/114.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мелилья',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мелилья',
                	],[
                		'lang' => 'en',
                		'name' => 'Melilla'
                	]
            	]
            ],[
                'main' => [
                	'id' => 216,
                	'vmp_id' => 255,
                	'flag' => 'frontend/images/flags/png/255.png',
                	'code' => 'FM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Микронезия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Микронезия',
                	],[
                		'lang' => 'en',
                		'name' => 'Micronesia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 217,
                	'vmp_id' => 162,
                	'flag' => 'frontend/images/flags/png/162.png',
                	'code' => 'MZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мозамбик',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мозамбик',
                	],[
                		'lang' => 'en',
                		'name' => 'Mozambique'
                	]
            	]
            ],[
                'main' => [
                	'id' => 218,
                	'vmp_id' => 187,
                	'flag' => 'frontend/images/flags/png/187.png',
                	'code' => 'MC'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Монако',
                	],[
                		'lang' => 'ru',
                		'name' => 'Монако',
                	],[
                		'lang' => 'en',
                		'name' => 'Monaco'
                	]
            	]
            ],[
                'main' => [
                	'id' => 219,
                	'vmp_id' => 188,
                	'flag' => 'frontend/images/flags/png/188.png',
                	'code' => 'MS'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Монтсеррат',
                	],[
                		'lang' => 'ru',
                		'name' => 'Монтсеррат',
                	],[
                		'lang' => 'en',
                		'name' => 'Montserrat'
                	]
            	]
            ],[
                'main' => [
                	'id' => 220,
                	'vmp_id' => 45,
                	'flag' => 'frontend/images/flags/png/45.png',
                	'code' => 'MM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Мъянма (Бирма)',
                	],[
                		'lang' => 'ru',
                		'name' => 'Мъянма (Бирма)',
                	],[
                		'lang' => 'en',
                		'name' => 'Myanma'
                	]
            	]
            ],[
                'main' => [
                	'id' => 221,
                	'vmp_id' => 189,
                	'flag' => 'frontend/images/flags/png/189.png',
                	'code' => 'NA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Намибия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Намибия',
                	],[
                		'lang' => 'en',
                		'name' => 'Namibia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 222,
                	'vmp_id' => 190,
                	'flag' => 'frontend/images/flags/png/190.png',
                	'code' => 'NR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Науру',
                	],[
                		'lang' => 'ru',
                		'name' => 'Науру',
                	],[
                		'lang' => 'en',
                		'name' => 'Nauru'
                	]
            	]
            ],[
                'main' => [
                	'id' => 223,
                	'vmp_id' => 192,
                	'flag' => 'frontend/images/flags/png/192.png',
                	'code' => 'NP'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Непал',
                	],[
                		'lang' => 'ru',
                		'name' => 'Непал',
                	],[
                		'lang' => 'en',
                		'name' => 'Nepal'
                	]
            	]
            ],[
                'main' => [
                	'id' => 224,
                	'vmp_id' => 118,
                	'flag' => 'frontend/images/flags/png/118.png',
                	'code' => 'NE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Нигер',
                	],[
                		'lang' => 'ru',
                		'name' => 'Нигер',
                	],[
                		'lang' => 'en',
                		'name' => 'Niger'
                	]
            	]
            ],[
                'main' => [
                	'id' => 225,
                	'vmp_id' => 226,
                	'flag' => 'frontend/images/flags/png/226.png',
                	'code' => 'NG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Нигерия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Нигерия',
                	],[
                		'lang' => 'en',
                		'name' => 'Nigeria'
                	]
            	]
            ],[
                'main' => [
                	'id' => 226,
                	'vmp_id' => 24,
                	'flag' => 'frontend/images/flags/png/24.png',
                	'code' => 'NL'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Нидерланд',
                	],[
                		'lang' => 'ru',
                		'name' => 'Нидерланды',
                	],[
                		'lang' => 'en',
                		'name' => 'Netherlands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 227,
                	'vmp_id' => 287,
                	'flag' => 'frontend/images/flags/png/287.png',
                	'code' => 'NI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Никарагуа',
                	],[
                		'lang' => 'ru',
                		'name' => 'Никарагуа',
                	],[
                		'lang' => 'en',
                		'name' => 'Nicaragua'
                	]
            	]
            ],[
                'main' => [
                	'id' => 228,
                	'vmp_id' => 191,
                	'flag' => 'frontend/images/flags/png/191.png',
                	'code' => 'NU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ниуэ',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ниуэ',
                	],[
                		'lang' => 'en',
                		'name' => 'Niue'
                	]
            	]
            ],[
                'main' => [
                	'id' => 229,
                	'vmp_id' => 120,
                	'flag' => 'frontend/images/flags/png/120.png',
                	'code' => 'NO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Норвегия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Норвегия',
                	],[
                		'lang' => 'en',
                		'name' => 'Norway'
                	]
            	]
            ],[
                'main' => [
                	'id' => 230,
                	'vmp_id' => 316,
                	'flag' => 'frontend/images/flags/png/316.png',
                	'code' => 'AE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Біріккен Араб Эмираттары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Объединенные Арабские Эмираты',
                	],[
                		'lang' => 'en',
                		'name' => 'The United Arab Emirates'
                	]
            	]
            ],[
                'main' => [
                	'id' => 231,
                	'vmp_id' => 122,
                	'flag' => 'frontend/images/flags/png/122.png',
                	'code' => 'OM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Оман',
                	],[
                		'lang' => 'ru',
                		'name' => 'Оман',
                	],[
                		'lang' => 'en',
                		'name' => 'Oman'
                	]
            	]
            ],[
                'main' => [
                	'id' => 232,
                	'vmp_id' => 123,
                	'flag' => 'frontend/images/flags/png/123.png',
                	'code' => 'CX'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Рождество аралы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Остров Рождества',
                	],[
                		'lang' => 'en',
                		'name' => 'Christmas Island'
                	]
            	]
            ],[
                'main' => [
                	'id' => 233,
                	'vmp_id' => 19,
                	'flag' => 'frontend/images/flags/png/19.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Уэйк аралы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Остров Уэйк',
                	],[
                		'lang' => 'en',
                		'name' => 'Wake Island'
                	]
            	]
            ],[
                'main' => [
                	'id' => 234,
                	'vmp_id' => 295,
                	'flag' => 'frontend/images/flags/png/295.png',
                	'code' => 'CK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Кука аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Острова Кука',
                	],[
                		'lang' => 'en',
                		'name' => 'The Cook Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 235,
                	'vmp_id' => 234,
                	'flag' => 'frontend/images/flags/png/234.png',
                	'code' => 'WF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Уоллис жєне Футуна аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Острова Уоллис и Футуна',
                	],[
                		'lang' => 'en',
                		'name' => 'Wallis and Futuna'
                	]
            	]
            ],[
                'main' => [
                	'id' => 236,
                	'vmp_id' => 323,
                	'flag' => 'frontend/images/flags/png/323.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Херда и Макдональд аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Острова Херда и Макдональда',
                	],[
                		'lang' => 'en',
                		'name' => 'Heard and McDonald Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 237,
                	'vmp_id' => 115,
                	'flag' => 'frontend/images/flags/png/115.png',
                	'code' => 'PS'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Палестина',
                	],[
                		'lang' => 'ru',
                		'name' => 'Палестина',
                	],[
                		'lang' => 'en',
                		'name' => 'Palestine'
                	]
            	]
            ],[
                'main' => [
                	'id' => 238,
                	'vmp_id' => 124,
                	'flag' => 'frontend/images/flags/png/124.png',
                	'code' => 'PA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Панама',
                	],[
                		'lang' => 'ru',
                		'name' => 'Панама',
                	],[
                		'lang' => 'en',
                		'name' => 'Panama'
                	]
            	]
            ],[
                'main' => [
                	'id' => 239,
                	'vmp_id' => 134,
                	'flag' => 'frontend/images/flags/png/134.png',
                	'code' => 'PY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Парагвай',
                	],[
                		'lang' => 'ru',
                		'name' => 'Парагвай',
                	],[
                		'lang' => 'en',
                		'name' => 'Paraguay'
                	]
            	]
            ],[
                'main' => [
                	'id' => 240,
                	'vmp_id' => 141,
                	'flag' => 'frontend/images/flags/png/141.png',
                	'code' => 'PE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Перу',
                	],[
                		'lang' => 'ru',
                		'name' => 'Перу',
                	],[
                		'lang' => 'en',
                		'name' => 'Peru'
                	]
            	]
            ],[
                'main' => [
                	'id' => 241,
                	'vmp_id' => 261,
                	'flag' => 'frontend/images/flags/png/261.png',
                	'code' => 'PN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Питкерн',
                	],[
                		'lang' => 'ru',
                		'name' => 'Питкерн',
                	],[
                		'lang' => 'en',
                		'name' => 'Pitcairn'
                	]
            	]
            ],[
                'main' => [
                	'id' => 242,
                	'vmp_id' => 184,
                	'flag' => 'frontend/images/flags/png/184.png',
                	'code' => 'PF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Француз Полинезиясы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Полинезия Французская',
                	],[
                		'lang' => 'en',
                		'name' => 'French Polynesia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 243,
                	'vmp_id' => 101,
                	'flag' => 'frontend/images/flags/png/101.png',
                	'code' => 'PL'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Польша',
                	],[
                		'lang' => 'ru',
                		'name' => 'Польша',
                	],[
                		'lang' => 'en',
                		'name' => 'Poland'
                	]
            	]
            ],[
                'main' => [
                	'id' => 244,
                	'vmp_id' => 109,
                	'flag' => 'frontend/images/flags/png/109.png',
                	'code' => 'PT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Португалия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Португалия',
                	],[
                		'lang' => 'en',
                		'name' => 'Portugal'
                	]
            	]
            ],[
                'main' => [
                	'id' => 245,
                	'vmp_id' => 110,
                	'flag' => 'frontend/images/flags/png/110.png',
                	'code' => 'PR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Пуэрто-Рико',
                	],[
                		'lang' => 'ru',
                		'name' => 'Пуэрто-Рико',
                	],[
                		'lang' => 'en',
                		'name' => 'Puerto Rico'
                	]
            	]
            ],[
                'main' => [
                	'id' => 246,
                	'vmp_id' => 111,
                	'flag' => 'frontend/images/flags/png/111.png',
                	'code' => 'RE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Реюньон',
                	],[
                		'lang' => 'ru',
                		'name' => 'Реюньон',
                	],[
                		'lang' => 'en',
                		'name' => 'Reunion'
                	]
            	]
            ],[
                'main' => [
                	'id' => 247,
                	'vmp_id' => 117,
                	'flag' => 'frontend/images/flags/png/117.png',
                	'code' => 'RU'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ресей Федерациясы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Российская Федерация',
                	],[
                		'lang' => 'en',
                		'name' => 'The Russian Federation'
                	]
            	]
            ],[
                'main' => [
                	'id' => 248,
                	'vmp_id' => 121,
                	'flag' => 'frontend/images/flags/png/121.png',
                	'code' => 'NF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Норфолк',
                	],[
                		'lang' => 'ru',
                		'name' => 'Норфолк',
                	],[
                		'lang' => 'en',
                		'name' => 'Norfolk'
                	]
            	]
            ],[
                'main' => [
                	'id' => 249,
                	'vmp_id' => 201,
                	'flag' => 'frontend/images/flags/png/201.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Серб Республикасы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сербская республика',
                	],[
                		'lang' => 'en',
                		'name' => 'Serbian Republic'
                	]
            	]
            ],[
                'main' => [
                	'id' => 250,
                	'vmp_id' => 269,
                	'flag' => 'frontend/images/flags/png/269.png',
                	'code' => 'SV'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сальвадор',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сальвадор',
                	],[
                		'lang' => 'en',
                		'name' => 'Salvador'
                	]
            	]
            ],[
                'main' => [
                	'id' => 251,
                	'vmp_id' => 271,
                	'flag' => 'frontend/images/flags/png/271.png',
                	'code' => 'WS'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Самоа',
                	],[
                		'lang' => 'ru',
                		'name' => 'Самоа',
                	],[
                		'lang' => 'en',
                		'name' => 'Samoa'
                	]
            	]
            ],[
                'main' => [
                	'id' => 252,
                	'vmp_id' => 272,
                	'flag' => 'frontend/images/flags/png/272.png',
                	'code' => 'SM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сан-Марино',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сан-Марино',
                	],[
                		'lang' => 'en',
                		'name' => 'San Marino'
                	]
            	]
            ],[
                'main' => [
                	'id' => 253,
                	'vmp_id' => 274,
                	'flag' => 'frontend/images/flags/png/274.png',
                	'code' => 'SA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сауд Аравиясы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Саудовская Аравия',
                	],[
                		'lang' => 'en',
                		'name' => 'Saudi Arabia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 254,
                	'vmp_id' => 275,
                	'flag' => 'frontend/images/flags/png/275.png',
                	'code' => 'SZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Свазиленд',
                	],[
                		'lang' => 'ru',
                		'name' => 'Свазиленд',
                	],[
                		'lang' => 'en',
                		'name' => 'Swaziland'
                	]
            	]
            ],[
                'main' => [
                	'id' => 255,
                	'vmp_id' => 260,
                	'flag' => 'frontend/images/flags/png/260.png',
                	'code' => 'SC'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сейшель аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сейшельские острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Seychelles'
                	]
            	]
            ],[
                'main' => [
                	'id' => 256,
                	'vmp_id' => 74,
                	'flag' => 'frontend/images/flags/png/74.png',
                	'code' => 'MQ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сен-Бартельми',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сен-Бартельми',
                	],[
                		'lang' => 'en',
                		'name' => 'Saint-Barthelemy'
                	]
            	]
            ],[
                'main' => [
                	'id' => 257,
                	'vmp_id' => 277,
                	'flag' => 'frontend/images/flags/png/277.png',
                	'code' => 'SN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сенегал',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сенегал',
                	],[
                		'lang' => 'en',
                		'name' => 'Senegal'
                	]
            	]
            ],[
                'main' => [
                	'id' => 258,
                	'vmp_id' => 90,
                	'flag' => 'frontend/images/flags/png/90.png',
                	'code' => 'KN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сент-Китс-Невис',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сент-Китс и Невис',
                	],[
                		'lang' => 'en',
                		'name' => 'St. Kitts and Nevis'
                	]
            	]
            ],[
                'main' => [
                	'id' => 259,
                	'vmp_id' => 91,
                	'flag' => 'frontend/images/flags/png/91.png',
                	'code' => 'LC'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сент-Люсия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сент-Люсия',
                	],[
                		'lang' => 'en',
                		'name' => 'St. Lucia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 260,
                	'vmp_id' => 94,
                	'flag' => 'frontend/images/flags/png/94.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сеута',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сеута',
                	],[
                		'lang' => 'en',
                		'name' => 'Ceuta'
                	]
            	]
            ],[
                'main' => [
                	'id' => 261,
                	'vmp_id' => 93,
                	'flag' => 'frontend/images/flags/png/93.png',
                	'code' => 'SG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сингапур',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сингапур',
                	],[
                		'lang' => 'en',
                		'name' => 'Singapore'
                	]
            	]
            ],[
                'main' => [
                	'id' => 262,
                	'vmp_id' => 95,
                	'flag' => 'frontend/images/flags/png/95.png',
                	'code' => 'SY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сирия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сирия',
                	],[
                		'lang' => 'en',
                		'name' => 'Syria'
                	]
            	]
            ],[
                'main' => [
                	'id' => 263,
                	'vmp_id' => 202,
                	'flag' => 'frontend/images/flags/png/202.png',
                	'code' => 'SK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Словакия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Словакия',
                	],[
                		'lang' => 'en',
                		'name' => 'Slovakia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 264,
                	'vmp_id' => 203,
                	'flag' => 'frontend/images/flags/png/203.png',
                	'code' => 'SI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Словения',
                	],[
                		'lang' => 'ru',
                		'name' => 'Словения',
                	],[
                		'lang' => 'en',
                		'name' => 'Slovenia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 265,
                	'vmp_id' => 96,
                	'flag' => 'frontend/images/flags/png/96.png',
                	'code' => 'SB'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Соломон аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Соломоновы острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Solomon Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 266,
                	'vmp_id' => 97,
                	'flag' => 'frontend/images/flags/png/97.png',
                	'code' => 'SO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сомали',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сомали',
                	],[
                		'lang' => 'en',
                		'name' => 'Somalia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 267,
                	'vmp_id' => 26,
                	'flag' => 'frontend/images/flags/png/26.png',
                	'code' => 'PH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Филиппин',
                	],[
                		'lang' => 'ru',
                		'name' => 'Филиппины',
                	],[
                		'lang' => 'en',
                		'name' => 'Philippines'
                	]
            	]
            ],[
                'main' => [
                	'id' => 268,
                	'vmp_id' => 61,
                	'flag' => 'frontend/images/flags/png/61.png',
                	'code' => 'SD'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Судан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Судан',
                	],[
                		'lang' => 'en',
                		'name' => 'Sudan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 269,
                	'vmp_id' => 66,
                	'flag' => 'frontend/images/flags/png/66.png',
                	'code' => 'SR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Суринам',
                	],[
                		'lang' => 'ru',
                		'name' => 'Суринам',
                	],[
                		'lang' => 'en',
                		'name' => 'Surinam'
                	]
            	]
            ],[
                'main' => [
                	'id' => 270,
                	'vmp_id' => 250,
                	'flag' => 'frontend/images/flags/png/250.png',
                	'code' => 'SL'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сьерра-Леоне',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сьерра-Леоне',
                	],[
                		'lang' => 'en',
                		'name' => 'Sierra Leone'
                	]
            	]
            ],[
                'main' => [
                	'id' => 271,
                	'vmp_id' => 262,
                	'flag' => 'frontend/images/flags/png/262.png',
                	'code' => 'TH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Таиланд',
                	],[
                		'lang' => 'ru',
                		'name' => 'Таиланд',
                	],[
                		'lang' => 'en',
                		'name' => 'Thailand'
                	]
            	]
            ],[
                'main' => [
                	'id' => 272,
                	'vmp_id' => 36,
                	'flag' => 'frontend/images/flags/png/36.png',
                	'code' => 'TW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Тайвань',
                	],[
                		'lang' => 'ru',
                		'name' => 'Тайвань',
                	],[
                		'lang' => 'en',
                		'name' => 'Taiwan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 273,
                	'vmp_id' => 37,
                	'flag' => 'frontend/images/flags/png/37.png',
                	'code' => 'TZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Танзания',
                	],[
                		'lang' => 'ru',
                		'name' => 'Танзания',
                	],[
                		'lang' => 'en',
                		'name' => 'Tanzania'
                	]
            	]
            ],[
                'main' => [
                	'id' => 274,
                	'vmp_id' => 42,
                	'flag' => 'frontend/images/flags/png/42.png',
                	'code' => 'TC'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Теркс жєне Кайкос',
                	],[
                		'lang' => 'ru',
                		'name' => 'Теркс и Кайкос',
                	],[
                		'lang' => 'en',
                		'name' => 'Turks and Caicos'
                	]
            	]
            ],[
                'main' => [
                	'id' => 275,
                	'vmp_id' => 132,
                	'flag' => 'frontend/images/flags/png/132.png',
                	'code' => 'UA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Украина',
                	],[
                		'lang' => 'ru',
                		'name' => 'Украина',
                	],[
                		'lang' => 'en',
                		'name' => 'Ukraine'
                	]
            	]
            ],[
                'main' => [
                	'id' => 276,
                	'vmp_id' => 46,
                	'flag' => 'frontend/images/flags/png/46.png',
                	'code' => 'TG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Того',
                	],[
                		'lang' => 'ru',
                		'name' => 'Того',
                	],[
                		'lang' => 'en',
                		'name' => 'Togo'
                	]
            	]
            ],[
                'main' => [
                	'id' => 277,
                	'vmp_id' => 47,
                	'flag' => 'frontend/images/flags/png/47.png',
                	'code' => 'TK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Токелау (Юнион)',
                	],[
                		'lang' => 'ru',
                		'name' => 'Токелау (Юнион)',
                	],[
                		'lang' => 'en',
                		'name' => 'Tokelau'
                	]
            	]
            ],[
                'main' => [
                	'id' => 278,
                	'vmp_id' => 48,
                	'flag' => 'frontend/images/flags/png/48.png',
                	'code' => 'TO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Тонга',
                	],[
                		'lang' => 'ru',
                		'name' => 'Тонга',
                	],[
                		'lang' => 'en',
                		'name' => 'Tonga'
                	]
            	]
            ],[
                'main' => [
                	'id' => 279,
                	'vmp_id' => 15,
                	'flag' => 'frontend/images/flags/png/15.png',
                	'code' => 'TV'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Тувалу',
                	],[
                		'lang' => 'ru',
                		'name' => 'Тувалу',
                	],[
                		'lang' => 'en',
                		'name' => 'Tuvalu'
                	]
            	]
            ],[
                'main' => [
                	'id' => 280,
                	'vmp_id' => 49,
                	'flag' => 'frontend/images/flags/png/49.png',
                	'code' => 'TN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Тунис',
                	],[
                		'lang' => 'ru',
                		'name' => 'Тунис',
                	],[
                		'lang' => 'en',
                		'name' => 'Tunisia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 281,
                	'vmp_id' => 12,
                	'flag' => 'frontend/images/flags/png/12.png',
                	'code' => 'UG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Уганда',
                	],[
                		'lang' => 'ru',
                		'name' => 'Уганда',
                	],[
                		'lang' => 'en',
                		'name' => 'Uganda'
                	]
            	]
            ],[
                'main' => [
                	'id' => 282,
                	'vmp_id' => 220,
                	'flag' => 'frontend/images/flags/png/220.png',
                	'code' => 'WF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Уоллис пен Футуна аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Уоллис и Футуна острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Wallis and Futuna Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 283,
                	'vmp_id' => 18,
                	'flag' => 'frontend/images/flags/png/18.png',
                	'code' => 'UY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Уругвай',
                	],[
                		'lang' => 'ru',
                		'name' => 'Уругвай',
                	],[
                		'lang' => 'en',
                		'name' => 'Uruguay'
                	]
            	]
            ],[
                'main' => [
                	'id' => 284,
                	'vmp_id' => 221,
                	'flag' => 'frontend/images/flags/png/221.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Уэйк аралы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Уэйк остров',
                	],[
                		'lang' => 'en',
                		'name' => 'Wake island'
                	]
            	]
            ],[
                'main' => [
                	'id' => 285,
                	'vmp_id' => 82,
                	'flag' => 'frontend/images/flags/png/82.png',
                	'code' => 'FO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Фарерские острова',
                	],[
                		'lang' => 'ru',
                		'name' => 'Фарерские острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Faroe Islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 286,
                	'vmp_id' => 21,
                	'flag' => 'frontend/images/flags/png/21.png',
                	'code' => 'FJ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Фиджи',
                	],[
                		'lang' => 'ru',
                		'name' => 'Фиджи',
                	],[
                		'lang' => 'en',
                		'name' => 'Fiji'
                	]
            	]
            ],[
                'main' => [
                	'id' => 287,
                	'vmp_id' => 27,
                	'flag' => 'frontend/images/flags/png/27.png',
                	'code' => 'FI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Финляндия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Финляндия',
                	],[
                		'lang' => 'en',
                		'name' => 'Finland'
                	]
            	]
            ],[
                'main' => [
                	'id' => 288,
                	'vmp_id' => 238,
                	'flag' => 'frontend/images/flags/png/238.png',
                	'code' => 'FK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Фолкленд (Мальвин) аралдары',
                	],[
                		'lang' => 'ru',
                		'name' => 'Фолклендские (Мальвинские) острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Falkland Islands (Malvinas)'
                	]
            	]
            ],[
                'main' => [
                	'id' => 289,
                	'vmp_id' => 28,
                	'flag' => 'frontend/images/flags/png/28.png',
                	'code' => 'FR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Франция',
                	],[
                		'lang' => 'ru',
                		'name' => 'Франция',
                	],[
                		'lang' => 'en',
                		'name' => 'France'
                	]
            	]
            ],[
                'main' => [
                	'id' => 290,
                	'vmp_id' => 100,
                	'flag' => 'frontend/images/flags/png/100.png',
                	'code' => 'PF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Француз Полинезиясы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Французская Полинезия',
                	],[
                		'lang' => 'en',
                		'name' => 'French Polynesia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 291,
                	'vmp_id' => 225,
                	'flag' => 'frontend/images/flags/png/225.png',
                	'code' => 'HR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Хорватия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Хорватия',
                	],[
                		'lang' => 'en',
                		'name' => 'Croatia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 292,
                	'vmp_id' => 7,
                	'flag' => 'frontend/images/flags/png/7.png',
                	'code' => 'TD'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Чад',
                	],[
                		'lang' => 'ru',
                		'name' => 'Чад',
                	],[
                		'lang' => 'en',
                		'name' => 'Chad'
                	]
            	]
            ],[
                'main' => [
                	'id' => 293,
                	'vmp_id' => 105,
                	'flag' => 'frontend/images/flags/png/105.png',
                	'code' => 'ME'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Черногория (Монтенегро)',
                	],[
                		'lang' => 'ru',
                		'name' => 'Черногория (Монтенегро)',
                	],[
                		'lang' => 'en',
                		'name' => 'Montenegro '
                	]
            	]
            ],[
                'main' => [
                	'id' => 294,
                	'vmp_id' => 233,
                	'flag' => 'frontend/images/flags/png/233.png',
                	'code' => 'CH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Швейцария',
                	],[
                		'lang' => 'ru',
                		'name' => 'Швейцария',
                	],[
                		'lang' => 'en',
                		'name' => 'Switzerland'
                	]
            	]
            ],[
                'main' => [
                	'id' => 295,
                	'vmp_id' => 92,
                	'flag' => 'frontend/images/flags/png/92.png',
                	'code' => 'SE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Швеция',
                	],[
                		'lang' => 'ru',
                		'name' => 'Швеция',
                	],[
                		'lang' => 'en',
                		'name' => 'Sweden'
                	]
            	]
            ],[
                'main' => [
                	'id' => 296,
                	'vmp_id' => 2,
                	'flag' => 'frontend/images/flags/png/2.png',
                	'code' => 'LK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Шри-Ланка',
                	],[
                		'lang' => 'ru',
                		'name' => 'Шри-Ланка',
                	],[
                		'lang' => 'en',
                		'name' => 'Sri Lanka'
                	]
            	]
            ],[
                'main' => [
                	'id' => 297,
                	'vmp_id' => 98,
                	'flag' => 'frontend/images/flags/png/98.png',
                	'code' => 'EC'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Эквадор',
                	],[
                		'lang' => 'ru',
                		'name' => 'Эквадор',
                	],[
                		'lang' => 'en',
                		'name' => 'Ecuador'
                	]
            	]
            ],[
                'main' => [
                	'id' => 298,
                	'vmp_id' => 5,
                	'flag' => 'frontend/images/flags/png/5.png',
                	'code' => 'ER'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Эритрея',
                	],[
                		'lang' => 'ru',
                		'name' => 'Эритрея',
                	],[
                		'lang' => 'en',
                		'name' => 'Eritrea'
                	]
            	]
            ],[
                'main' => [
                	'id' => 299,
                	'vmp_id' => 128,
                	'flag' => 'frontend/images/flags/png/128.png',
                	'code' => 'EE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Эстония',
                	],[
                		'lang' => 'ru',
                		'name' => 'Эстония',
                	],[
                		'lang' => 'en',
                		'name' => 'Estonia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 300,
                	'vmp_id' => 99,
                	'flag' => 'frontend/images/flags/png/99.png',
                	'code' => 'ET'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Эфиопия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Эфиопия',
                	],[
                		'lang' => 'en',
                		'name' => 'Ethiopia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 301,
                	'vmp_id' => 102,
                	'flag' => 'frontend/images/flags/png/102.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Югославия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Югославия',
                	],[
                		'lang' => 'en',
                		'name' => 'Yugoslavia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 302,
                	'vmp_id' => 10,
                	'flag' => 'frontend/images/flags/png/10.png',
                	'code' => 'JM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ямайка',
                	],[
                		'lang' => 'ru',
                		'name' => 'Ямайка',
                	],[
                		'lang' => 'en',
                		'name' => 'Jamaica'
                	]
            	]
            ],[
                'main' => [
                	'id' => 303,
                	'vmp_id' => 108,
                	'flag' => 'frontend/images/flags/png/108.png',
                	'code' => 'JP'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Жапония',
                	],[
                		'lang' => 'ru',
                		'name' => 'Япония',
                	],[
                		'lang' => 'en',
                		'name' => 'Japan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 304,
                	'vmp_id' => 6,
                	'flag' => 'frontend/images/flags/png/6.png',
                	'code' => 'CF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Орталық Африка республикасы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Центральноафриканская республика',
                	],[
                		'lang' => 'en',
                		'name' => 'The Central African Republic'
                	]
            	]
            ],[
                'main' => [
                	'id' => 305,
                	'vmp_id' => 9,
                	'flag' => 'frontend/images/flags/png/9.png',
                	'code' => 'BA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Босния және Герцеговина республикасы ЮСФР',
                	],[
                		'lang' => 'ru',
                		'name' => 'Босния и Герцеговина',
                	],[
                		'lang' => 'en',
                		'name' => 'Bosnia and Herzegovina'
                	]
            	]
            ],[
                'main' => [
                	'id' => 306,
                	'vmp_id' => 13,
                	'flag' => 'frontend/images/flags/png/13.png',
                	'code' => 'IO'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Үнді мұхитындағы Британ аумағы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Британская территория в Индийском океане',
                	],[
                		'lang' => 'en',
                		'name' => 'British Indian Ocean Territory'
                	]
            	]
            ],[
                'main' => [
                	'id' => 307,
                	'vmp_id' => 41,
                	'flag' => 'frontend/images/flags/png/41.png',
                	'code' => 'TL'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Тимор-Лесте',
                	],[
                		'lang' => 'ru',
                		'name' => 'Тимор-Лесте',
                	],[
                		'lang' => 'en',
                		'name' => 'Timor-Leste'
                	]
            	]
            ],[
                'main' => [
                	'id' => 308,
                	'vmp_id' => 16,
                	'flag' => 'frontend/images/flags/png/16.png',
                	'code' => 'GQ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Экваториалық Гвинея',
                	],[
                		'lang' => 'ru',
                		'name' => 'Экваториальная Гвинея',
                	],[
                		'lang' => 'en',
                		'name' => 'Equatorial Guinea'
                	]
            	]
            ],[
                'main' => [
                	'id' => 309,
                	'vmp_id' => 43,
                	'flag' => 'frontend/images/flags/png/43.png',
                	'code' => 'TT'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Тринидад және Тобаго',
                	],[
                		'lang' => 'ru',
                		'name' => 'Тринидад и Тобаго',
                	],[
                		'lang' => 'en',
                		'name' => 'Trinidad and Tobago'
                	]
            	]
            ],[
                'main' => [
                	'id' => 310,
                	'vmp_id' => 62,
                	'flag' => 'frontend/images/flags/png/62.png',
                	'code' => 'AG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Антигуа және Барбуда',
                	],[
                		'lang' => 'ru',
                		'name' => 'Антигуа и Барбуда',
                	],[
                		'lang' => 'en',
                		'name' => 'Antigua and Barbuda'
                	]
            	]
            ],[
                'main' => [
                	'id' => 311,
                	'vmp_id' => 67,
                	'flag' => 'frontend/images/flags/png/67.png',
                	'code' => 'US'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'АҚШ',
                	],[
                		'lang' => 'ru',
                		'name' => 'США',
                	],[
                		'lang' => 'en',
                		'name' => 'USA'
                	]
            	]
            ],[
                'main' => [
                	'id' => 312,
                	'vmp_id' => 68,
                	'flag' => 'frontend/images/flags/png/68.png',
                	'code' => 'AF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ауғаныстан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Афганистан',
                	],[
                		'lang' => 'en',
                		'name' => 'Afganistan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 313,
                	'vmp_id' => 69,
                	'flag' => 'frontend/images/flags/png/69.png',
                	'code' => 'BS'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бағам аралы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Багамские острова',
                	],[
                		'lang' => 'en',
                		'name' => 'Bagam islands'
                	]
            	]
            ],[
                'main' => [
                	'id' => 314,
                	'vmp_id' => 86,
                	'flag' => 'frontend/images/flags/png/86.png',
                	'code' => 'VI'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Виргин аралы (АҚШ)',
                	],[
                		'lang' => 'ru',
                		'name' => 'Виргинские острова (США)',
                	],[
                		'lang' => 'en',
                		'name' => 'Virgin Islands (U.S.A)'
                	]
            	]
            ],[
                'main' => [
                	'id' => 315,
                	'vmp_id' => 133,
                	'flag' => 'frontend/images/flags/png/133.png',
                	'code' => 'PG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Папуа-Жаңа Гвинея',
                	],[
                		'lang' => 'ru',
                		'name' => 'Папуа-Новая Гвинея',
                	],[
                		'lang' => 'en',
                		'name' => 'Papua New Guinea'
                	]
            	]
            ],[
                'main' => [
                	'id' => 316,
                	'vmp_id' => 140,
                	'flag' => 'frontend/images/flags/png/140.png',
                	'code' => 'KZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Қазақстан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Казахстан',
                	],[
                		'lang' => 'en',
                		'name' => 'Kazakhstan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 317,
                	'vmp_id' => 119,
                	'flag' => 'frontend/images/flags/png/119.png',
                	'code' => 'NC'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Жаңа Каледония',
                	],[
                		'lang' => 'ru',
                		'name' => 'Новая Каледония',
                	],[
                		'lang' => 'en',
                		'name' => 'New Caledonia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 318,
                	'vmp_id' => 107,
                	'flag' => 'frontend/images/flags/png/107.png',
                	'code' => 'ZA'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Оңтүстік-Африка республикасы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Южно-Африканская республика',
                	],[
                		'lang' => 'en',
                		'name' => 'Republic of South Africa'
                	]
            	]
            ],[
                'main' => [
                	'id' => 319,
                	'vmp_id' => 144,
                	'flag' => 'frontend/images/flags/png/144.png',
                	'code' => 'TM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Түркменістан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Туркменистан',
                	],[
                		'lang' => 'en',
                		'name' => 'Turkmenistan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 320,
                	'vmp_id' => 152,
                	'flag' => 'frontend/images/flags/png/152.png',
                	'code' => 'BF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Буркина Фасо - Верхняя Вольта',
                	],[
                		'lang' => 'ru',
                		'name' => 'Буркина Фасо - Верхняя Вольта',
                	],[
                		'lang' => 'en',
                		'name' => 'Burkina Faso - Upper Volta'
                	]
            	]
            ],[
                'main' => [
                	'id' => 321,
                	'vmp_id' => 158,
                	'flag' => 'frontend/images/flags/png/158.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Еуропа одағы (ЕО)',
                	],[
                		'lang' => 'ru',
                		'name' => 'Европейский союз (ЕС)',
                	],[
                		'lang' => 'en',
                		'name' => 'The European Union (EU)'
                	]
            	]
            ],[
                'main' => [
                	'id' => 322,
                	'vmp_id' => 164,
                	'flag' => 'frontend/images/flags/png/164.png',
                	'code' => 'CV'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Кабо-Верде (Зеленого мыса острова)',
                	],[
                		'lang' => 'ru',
                		'name' => 'Кабо-Верде (Зеленого мыса острова)',
                	],[
                		'lang' => 'en',
                		'name' => 'Cape Verde (Cape Verde Islands)'
                	]
            	]
            ],[
                'main' => [
                	'id' => 323,
                	'vmp_id' => 14,
                	'flag' => 'frontend/images/flags/png/14.png',
                	'code' => 'TF'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Француздік Оңтүстік аумақ',
                	],[
                		'lang' => 'ru',
                		'name' => 'Французские Южные территории',
                	],[
                		'lang' => 'en',
                		'name' => 'French Southern Territories'
                	]
            	]
            ],[
                'main' => [
                	'id' => 324,
                	'vmp_id' => 136,
                	'flag' => 'frontend/images/flags/png/136.png',
                	'code' => 'AZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Әзірбайжан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Азербайджан',
                	],[
                		'lang' => 'en',
                		'name' => 'Azerbaijan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 325,
                	'vmp_id' => 72,
                	'flag' => 'frontend/images/flags/png/72.png',
                	'code' => 'CD'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Конго Демократическая республика (Заир)',
                	],[
                		'lang' => 'ru',
                		'name' => 'Конго Демократическая республика (Заир)',
                	],[
                		'lang' => 'en',
                		'name' => 'The Democratic Republic Kongo(Zair)'
                	]
            	]
            ],[
                'main' => [
                	'id' => 326,
                	'vmp_id' => 228,
                	'flag' => 'frontend/images/flags/png/228.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Піл сүйегі жағалауындағы - Кот-Д Ивуар',
                	],[
                		'lang' => 'ru',
                		'name' => 'Кот-Д Ивуар - Берег Слоновой Кости',
                	],[
                		'lang' => 'en',
                		'name' => 'Cote d\'Ivoire - Ivory Coast'
                	]
            	]
            ],[
                'main' => [
                	'id' => 327,
                	'vmp_id' => 268,
                	'flag' => 'frontend/images/flags/png/268.png',
                	'code' => 'GB'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Ұлыбритания',
                	],[
                		'lang' => 'ru',
                		'name' => 'Великобритания',
                	],[
                		'lang' => 'en',
                		'name' => 'United Kingdom'
                	]
            	]
            ],[
                'main' => [
                	'id' => 328,
                	'vmp_id' => 270,
                	'flag' => 'frontend/images/flags/png/270.png',
                	'code' => 'AS'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Шығыс Самоа',
                	],[
                		'lang' => 'ru',
                		'name' => 'Американское (Восточное) Самоа',
                	],[
                		'lang' => 'en',
                		'name' => 'American (Eastern) Samoa'
                	]
            	]
            ],[
                'main' => [
                	'id' => 329,
                	'vmp_id' => 273,
                	'flag' => 'frontend/images/flags/png/273.png',
                	'code' => 'ST'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сан-Томе және Принсипи',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сан-Томе и Принсипи',
                	],[
                		'lang' => 'en',
                		'name' => 'Sao Tome and Principe'
                	]
            	]
            ],[
                'main' => [
                	'id' => 330,
                	'vmp_id' => 278,
                	'flag' => 'frontend/images/flags/png/278.png',
                	'code' => 'PM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сен-Пьер және Микелон',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сен-Пьер и Микелон',
                	],[
                		'lang' => 'en',
                		'name' => 'Saint-Pierre and Miquelon'
                	]
            	]
            ],[
                'main' => [
                	'id' => 331,
                	'vmp_id' => 279,
                	'flag' => 'frontend/images/flags/png/279.png',
                	'code' => 'VC'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Сент-Винсент және Гренадины',
                	],[
                		'lang' => 'ru',
                		'name' => 'Сент-Винсент и Гренадины',
                	],[
                		'lang' => 'en',
                		'name' => 'St. Vincent And Grenadines'
                	]
            	]
            ],[
                'main' => [
                	'id' => 332,
                	'vmp_id' => 281,
                	'flag' => 'frontend/images/flags/png/281.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Лицо без гражданства',
                	],[
                		'lang' => 'ru',
                		'name' => 'Лицо без гражданства',
                	],[
                		'lang' => 'en',
                		'name' => 'A stateless person'
                	]
            	]
            ],[
                'main' => [
                	'id' => 333,
                	'vmp_id' => 288,
                	'flag' => 'frontend/images/flags/png/288.png',
                	'code' => 'NZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Жаңа Зеландия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Новая Зеландия',
                	],[
                		'lang' => 'en',
                		'name' => 'New Zealand'
                	]
            	]
            ],[
                'main' => [
                	'id' => 334,
                	'vmp_id' => 290,
                	'flag' => 'frontend/images/flags/png/290.png',
                	'code' => 'PK'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Пәкістан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Пакистан',
                	],[
                		'lang' => 'en',
                		'name' => 'Pakistan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 335,
                	'vmp_id' => 305,
                	'flag' => 'frontend/images/flags/png/305.png',
                	'code' => 'CV'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Зеленый Мыс аралы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Острова Зеленого Мыса',
                	],[
                		'lang' => 'en',
                		'name' => 'Cape Verde'
                	]
            	]
            ],[
                'main' => [
                	'id' => 336,
                	'vmp_id' => 142,
                	'flag' => 'frontend/images/flags/png/142.png',
                	'code' => 'KG'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Қырғызстан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Кыргызстан',
                	],[
                		'lang' => 'en',
                		'name' => 'Kyrgyzstan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 337,
                	'vmp_id' => 309,
                	'flag' => 'frontend/images/flags/png/309.png',
                	'code' => 'IN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Үндістан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Индия',
                	],[
                		'lang' => 'en',
                		'name' => 'India'
                	]
            	]
            ],[
                'main' => [
                	'id' => 338,
                	'vmp_id' => 137,
                	'flag' => 'frontend/images/flags/png/137.png',
                	'code' => 'AM'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Армения',
                	],[
                		'lang' => 'ru',
                		'name' => 'Армения',
                	],[
                		'lang' => 'en',
                		'name' => 'Armenia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 339,
                	'vmp_id' => 130,
                	'flag' => 'frontend/images/flags/png/130.png',
                	'code' => 'BY'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Беларусь',
                	],[
                		'lang' => 'ru',
                		'name' => 'Беларусь',
                	],[
                		'lang' => 'en',
                		'name' => 'Belarus'
                	]
            	]
            ],[
                'main' => [
                	'id' => 340,
                	'vmp_id' => 54,
                	'flag' => 'frontend/images/flags/png/54.png',
                	'code' => 'BR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Бразилия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Бразилия',
                	],[
                		'lang' => 'en',
                		'name' => 'Brazil'
                	]
            	]
            ],[
                'main' => [
                	'id' => 341,
                	'vmp_id' => 138,
                	'flag' => 'frontend/images/flags/png/138.png',
                	'code' => 'GE'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Грузия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Грузия',
                	],[
                		'lang' => 'en',
                		'name' => 'Georgia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 342,
                	'vmp_id' => 285,
                	'flag' => 'frontend/images/flags/png/285.png',
                	'code' => 'KP'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'КХДР',
                	],[
                		'lang' => 'ru',
                		'name' => 'КНДР',
                	],[
                		'lang' => 'en',
                		'name' => 'North Korea'
                	]
            	]
            ],[
                'main' => [
                	'id' => 343,
                	'vmp_id' => 131,
                	'flag' => 'frontend/images/flags/png/131.png',
                	'code' => 'MD'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Молдова',
                	],[
                		'lang' => 'ru',
                		'name' => 'Молдова',
                	],[
                		'lang' => 'en',
                		'name' => 'Moldova'
                	]
            	]
            ],[
                'main' => [
                	'id' => 344,
                	'vmp_id' => 291,
                	'flag' => 'frontend/images/flags/png/291.png',
                	'code' => 'MN'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Монғолия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Монголия',
                	],[
                		'lang' => 'en',
                		'name' => 'Mongolia'
                	]
            	]
            ],[
                'main' => [
                	'id' => 345,
                	'vmp_id' => 10603,
                	'flag' => 'frontend/images/flags/png/10603.png',
                	'code' => 'PW'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Палау',
                	],[
                		'lang' => 'ru',
                		'name' => 'Палау',
                	],[
                		'lang' => 'en',
                		'name' => 'Palau'
                	]
            	]
            ],[
                'main' => [
                	'id' => 346,
                	'vmp_id' => 195,
                	'flag' => 'frontend/images/flags/png/195.png',
                	'code' => NULL
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Солтүстік Ирландия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Северная Ирландия',
                	],[
                		'lang' => 'en',
                		'name' => 'Northern Ireland'
                	]
            	]
            ],[
                'main' => [
                	'id' => 347,
                	'vmp_id' => 276,
                	'flag' => 'frontend/images/flags/png/276.png',
                	'code' => 'SH'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Қасиетті Елена аралы',
                	],[
                		'lang' => 'ru',
                		'name' => 'Святой Елены Остров',
                	],[
                		'lang' => 'en',
                		'name' => 'Saint Helena Island'
                	]
            	]
            ],[
                'main' => [
                	'id' => 348,
                	'vmp_id' => 143,
                	'flag' => 'frontend/images/flags/png/143.png',
                	'code' => 'TJ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Тәжікстан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Таджикистан',
                	],[
                		'lang' => 'en',
                		'name' => 'Tajikistan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 349,
                	'vmp_id' => 51,
                	'flag' => 'frontend/images/flags/png/51.png',
                	'code' => 'TR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Түркия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Турция',
                	],[
                		'lang' => 'en',
                		'name' => 'Turkey'
                	]
            	]
            ],[
                'main' => [
                	'id' => 350,
                	'vmp_id' => 146,
                	'flag' => 'frontend/images/flags/png/146.png',
                	'code' => 'UZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Өзбекстан',
                	],[
                		'lang' => 'ru',
                		'name' => 'Узбекистан',
                	],[
                		'lang' => 'en',
                		'name' => 'Uzbekistan'
                	]
            	]
            ],[
                'main' => [
                	'id' => 351,
                	'vmp_id' => 8,
                	'flag' => 'frontend/images/flags/png/8.png',
                	'code' => 'CZ'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Чехия',
                	],[
                		'lang' => 'ru',
                		'name' => 'Чехия',
                	],[
                		'lang' => 'en',
                		'name' => 'Czech Republic'
                	]
            	]
            ],[
                'main' => [
                	'id' => 352,
                	'vmp_id' => 11,
                	'flag' => 'frontend/images/flags/png/11.png',
                	'code' => 'CL'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Чили',
                	],[
                		'lang' => 'ru',
                		'name' => 'Чили',
                	],[
                		'lang' => 'en',
                		'name' => 'Chile'
                	]
            	]
            ],[
                'main' => [
                	'id' => 353,
                	'vmp_id' => 307,
                	'flag' => 'frontend/images/flags/png/307.png',
                	'code' => 'KR'
                ],
                'translations' => [
                	[
                		'lang' => 'kz',
                		'name' => 'Оңтүстік Корея',
                	],[
                		'lang' => 'ru',
                		'name' => 'Южная Корея',
                	],[
                		'lang' => 'en',
                		'name' => 'South Korea'
                	]
                ]
            ]
        ];

        for ($i = 0; $i < count($countries); $i++) { 
        	$country = Country::query()
        		->where('id', $countries[$i]['main']['id'])
        		->first();

        	if ($country) {
                $country->update($countries[$i]['main']);

        		for ($j = 0; $j < count($countries[$i]['translations']); $j++) { 
        			$country_lang = CountryLang::query()
                        ->where('id', $countries[$i]['main']['id'])
                        ->where('lang', $countries[$i]['translations'][$j]['lang'])
                        ->first();

                    $country_lang->update([
                        'name' => $countries[$i]['translations'][$j]['name']
                    ]);
        		}
        	} else {
        		$country = Country::create($countries[$i]['main']);

        		for ($j = 0; $j < count($countries[$i]['translations']); $j++) { 
        			CountryLang::create([
                        'id' => $countries[$i]['main']['id'],
                        'lang' => $countries[$i]['translations'][$j]['lang'],
                        'name' => $countries[$i]['translations'][$j]['name']
                    ]);
        		}
            }
        }
    }
}
