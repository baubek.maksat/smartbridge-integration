<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\UniversityTypeLang;
use App\Models\UniversityType;

class UniversityTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $university_types = [
            [
                'main' => [
                    'id' => 101,
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Университет'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Университет'
                    ],[
                        'lang' => 'en',
                        'name' => 'University'
                    ]
                ]
            ]
        ];

        if (count($university_types) > 0) {
            for ($i = 0; $i < count($university_types); $i++) { 
                $university_type = UniversityType::query()
                    ->where('id', $university_types[$i]['main']['id'])
                    ->first();

                if ($university_type) {
                    $university_type->update($university_types[$i]['main']);

                    for ($j = 0; $j < count($university_types[$i]['translations']); $j++) { 
                        $university_type_lang = UniversityTypeLang::query()
                            ->where('id', $university_types[$i]['main']['id'])
                            ->where('lang', $university_types[$i]['translations'][$j]['lang'])
                            ->first();

                        $university_type_lang->update([
                            'name' => $university_types[$i]['translations'][$j]['name']
                        ]);
                    }
                } else {
                    $university_type = UniversityType::create($university_types[$i]['main']);

                    for ($j = 0; $j < count($university_types[$i]['translations']); $j++) { 
                        UniversityTypeLang::create([
                            'id' => $university_types[$i]['main']['id'],
                            'lang' => $university_types[$i]['translations'][$j]['lang'],
                            'name' => $university_types[$i]['translations'][$j]['name']
                        ]);
                    }
                }
            }
        }
    }
}
