<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\PlatformLang;
use App\Models\Platform;

class PlatformSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$platforms = [
			[
				'main' => [
					'id' => 1,
					'slug' => 'site',
					'type_id' => 1
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Сайт'
					],[
						'lang' => 'ru',
						'name' => 'Сайт'
					],[
						'lang' => 'en',
						'name' => 'Site'
					]
				]
			],[
				'main' => [
					'id' => 2,
					'slug' => 'ios',
					'type_id' => 2
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'iOS'
					],[
						'lang' => 'ru',
						'name' => 'iOS'
					],[
						'lang' => 'en',
						'name' => 'iOS'
					]
				]
			],[
				'main' => [
					'id' => 3,
					'slug' => 'android',
					'type_id' => 2
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Android'
					],[
						'lang' => 'ru',
						'name' => 'Android'
					],[
						'lang' => 'en',
						'name' => 'Android'
					]
				]
			]
		];

		for ($i = 0; $i < count($platforms); $i++) { 
			$platform = Platform::query()
				->where('id', $platforms[$i]['main']['id'])
				->first();

			if ($platform) {
				$platform->update($platforms[$i]['main']);
				
				for ($j = 0; $j < count($platforms[$i]['translations']); $j++) { 
					$platform_lang = PlatformLang::query()
                        ->where('id', $platforms[$i]['main']['id'])
                        ->where('lang', $platforms[$i]['translations'][$j]['lang'])
                        ->first();

                    $platform_lang->update([
                        'name' => $platforms[$i]['translations'][$j]['name']
                    ]);
				}
			} else {
				$platform = Platform::create($platforms[$i]['main']);

				for ($j = 0; $j < count($platforms[$i]['translations']); $j++) { 
					PlatformLang::create([
						'id' => $platforms[$i]['main']['id'],
						'lang' => $platforms[$i]['translations'][$j]['lang'],
						'name' => $platforms[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
