<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\IdTypeCountry;

class IdTypeCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id_types_countries = [
            [
                'id_type_id' => 101,
                'country_id' => 101
            ],[
                'id_type_id' => 102,
                'country_id' => 101
            ],[
                'id_type_id' => 101,
                'country_id' => 102
            ],[
                'id_type_id' => 102,
                'country_id' => 102
            ],[
                'id_type_id' => 101,
                'country_id' => 103
            ],[
                'id_type_id' => 102,
                'country_id' => 103
            ],[
                'id_type_id' => 101,
                'country_id' => 104
            ],[
                'id_type_id' => 102,
                'country_id' => 104
            ],[
                'id_type_id' => 101,
                'country_id' => 105
            ],[
                'id_type_id' => 102,
                'country_id' => 105
            ],[
                'id_type_id' => 101,
                'country_id' => 106
            ],[
                'id_type_id' => 102,
                'country_id' => 106
            ],[
                'id_type_id' => 101,
                'country_id' => 107
            ],[
                'id_type_id' => 102,
                'country_id' => 107
            ],[
                'id_type_id' => 101,
                'country_id' => 108
            ],[
                'id_type_id' => 102,
                'country_id' => 108
            ],[
                'id_type_id' => 101,
                'country_id' => 109
            ],[
                'id_type_id' => 102,
                'country_id' => 109
            ],[
                'id_type_id' => 101,
                'country_id' => 110
            ],[
                'id_type_id' => 102,
                'country_id' => 110
            ],[
                'id_type_id' => 101,
                'country_id' => 111
            ],[
                'id_type_id' => 102,
                'country_id' => 111
            ],[
                'id_type_id' => 101,
                'country_id' => 112
            ],[
                'id_type_id' => 102,
                'country_id' => 112
            ],[
                'id_type_id' => 101,
                'country_id' => 113
            ],[
                'id_type_id' => 102,
                'country_id' => 113
            ],[
                'id_type_id' => 101,
                'country_id' => 114
            ],[
                'id_type_id' => 102,
                'country_id' => 114
            ],[
                'id_type_id' => 101,
                'country_id' => 115
            ],[
                'id_type_id' => 102,
                'country_id' => 115
            ],[
                'id_type_id' => 101,
                'country_id' => 116
            ],[
                'id_type_id' => 102,
                'country_id' => 116
            ],[
                'id_type_id' => 101,
                'country_id' => 117
            ],[
                'id_type_id' => 102,
                'country_id' => 117
            ],[
                'id_type_id' => 101,
                'country_id' => 118
            ],[
                'id_type_id' => 102,
                'country_id' => 118
            ],[
                'id_type_id' => 101,
                'country_id' => 119
            ],[
                'id_type_id' => 102,
                'country_id' => 119
            ],[
                'id_type_id' => 101,
                'country_id' => 120
            ],[
                'id_type_id' => 102,
                'country_id' => 120
            ],[
                'id_type_id' => 101,
                'country_id' => 121
            ],[
                'id_type_id' => 102,
                'country_id' => 121
            ],[
                'id_type_id' => 101,
                'country_id' => 122
            ],[
                'id_type_id' => 102,
                'country_id' => 122
            ],[
                'id_type_id' => 101,
                'country_id' => 123
            ],[
                'id_type_id' => 102,
                'country_id' => 123
            ],[
                'id_type_id' => 101,
                'country_id' => 124
            ],[
                'id_type_id' => 102,
                'country_id' => 124
            ],[
                'id_type_id' => 101,
                'country_id' => 125
            ],[
                'id_type_id' => 102,
                'country_id' => 125
            ],[
                'id_type_id' => 101,
                'country_id' => 126
            ],[
                'id_type_id' => 102,
                'country_id' => 126
            ],[
                'id_type_id' => 101,
                'country_id' => 127
            ],[
                'id_type_id' => 102,
                'country_id' => 127
            ],[
                'id_type_id' => 101,
                'country_id' => 128
            ],[
                'id_type_id' => 102,
                'country_id' => 128
            ],[
                'id_type_id' => 101,
                'country_id' => 129
            ],[
                'id_type_id' => 102,
                'country_id' => 129
            ],[
                'id_type_id' => 101,
                'country_id' => 130
            ],[
                'id_type_id' => 102,
                'country_id' => 130
            ],[
                'id_type_id' => 101,
                'country_id' => 131
            ],[
                'id_type_id' => 102,
                'country_id' => 131
            ],[
                'id_type_id' => 101,
                'country_id' => 132
            ],[
                'id_type_id' => 102,
                'country_id' => 132
            ],[
                'id_type_id' => 101,
                'country_id' => 133
            ],[
                'id_type_id' => 102,
                'country_id' => 133
            ],[
                'id_type_id' => 101,
                'country_id' => 134
            ],[
                'id_type_id' => 102,
                'country_id' => 134
            ],[
                'id_type_id' => 101,
                'country_id' => 135
            ],[
                'id_type_id' => 102,
                'country_id' => 135
            ],[
                'id_type_id' => 101,
                'country_id' => 136
            ],[
                'id_type_id' => 102,
                'country_id' => 136
            ],[
                'id_type_id' => 101,
                'country_id' => 137
            ],[
                'id_type_id' => 102,
                'country_id' => 137
            ],[
                'id_type_id' => 101,
                'country_id' => 138
            ],[
                'id_type_id' => 102,
                'country_id' => 138
            ],[
                'id_type_id' => 101,
                'country_id' => 139
            ],[
                'id_type_id' => 102,
                'country_id' => 139
            ],[
                'id_type_id' => 101,
                'country_id' => 140
            ],[
                'id_type_id' => 102,
                'country_id' => 140
            ],[
                'id_type_id' => 101,
                'country_id' => 141
            ],[
                'id_type_id' => 102,
                'country_id' => 141
            ],[
                'id_type_id' => 101,
                'country_id' => 142
            ],[
                'id_type_id' => 102,
                'country_id' => 142
            ],[
                'id_type_id' => 101,
                'country_id' => 143
            ],[
                'id_type_id' => 102,
                'country_id' => 143
            ],[
                'id_type_id' => 101,
                'country_id' => 144
            ],[
                'id_type_id' => 102,
                'country_id' => 144
            ],[
                'id_type_id' => 101,
                'country_id' => 145
            ],[
                'id_type_id' => 102,
                'country_id' => 145
            ],[
                'id_type_id' => 101,
                'country_id' => 146
            ],[
                'id_type_id' => 102,
                'country_id' => 146
            ],[
                'id_type_id' => 101,
                'country_id' => 147
            ],[
                'id_type_id' => 102,
                'country_id' => 147
            ],[
                'id_type_id' => 101,
                'country_id' => 148
            ],[
                'id_type_id' => 102,
                'country_id' => 148
            ],[
                'id_type_id' => 101,
                'country_id' => 149
            ],[
                'id_type_id' => 102,
                'country_id' => 149
            ],[
                'id_type_id' => 101,
                'country_id' => 150
            ],[
                'id_type_id' => 102,
                'country_id' => 150
            ],[
                'id_type_id' => 101,
                'country_id' => 151
            ],[
                'id_type_id' => 102,
                'country_id' => 151
            ],[
                'id_type_id' => 101,
                'country_id' => 152
            ],[
                'id_type_id' => 102,
                'country_id' => 152
            ],[
                'id_type_id' => 101,
                'country_id' => 153
            ],[
                'id_type_id' => 102,
                'country_id' => 153
            ],[
                'id_type_id' => 101,
                'country_id' => 154
            ],[
                'id_type_id' => 102,
                'country_id' => 154
            ],[
                'id_type_id' => 101,
                'country_id' => 155
            ],[
                'id_type_id' => 102,
                'country_id' => 155
            ],[
                'id_type_id' => 101,
                'country_id' => 156
            ],[
                'id_type_id' => 102,
                'country_id' => 156
            ],[
                'id_type_id' => 101,
                'country_id' => 157
            ],[
                'id_type_id' => 102,
                'country_id' => 157
            ],[
                'id_type_id' => 101,
                'country_id' => 158
            ],[
                'id_type_id' => 102,
                'country_id' => 158
            ],[
                'id_type_id' => 101,
                'country_id' => 159
            ],[
                'id_type_id' => 102,
                'country_id' => 159
            ],[
                'id_type_id' => 101,
                'country_id' => 160
            ],[
                'id_type_id' => 102,
                'country_id' => 160
            ],[
                'id_type_id' => 101,
                'country_id' => 161
            ],[
                'id_type_id' => 102,
                'country_id' => 161
            ],[
                'id_type_id' => 101,
                'country_id' => 162
            ],[
                'id_type_id' => 102,
                'country_id' => 162
            ],[
                'id_type_id' => 101,
                'country_id' => 163
            ],[
                'id_type_id' => 102,
                'country_id' => 163
            ],[
                'id_type_id' => 101,
                'country_id' => 164
            ],[
                'id_type_id' => 102,
                'country_id' => 164
            ],[
                'id_type_id' => 101,
                'country_id' => 165
            ],[
                'id_type_id' => 102,
                'country_id' => 165
            ],[
                'id_type_id' => 101,
                'country_id' => 166
            ],[
                'id_type_id' => 102,
                'country_id' => 166
            ],[
                'id_type_id' => 101,
                'country_id' => 167
            ],[
                'id_type_id' => 102,
                'country_id' => 167
            ],[
                'id_type_id' => 101,
                'country_id' => 168
            ],[
                'id_type_id' => 102,
                'country_id' => 168
            ],[
                'id_type_id' => 101,
                'country_id' => 169
            ],[
                'id_type_id' => 102,
                'country_id' => 169
            ],[
                'id_type_id' => 101,
                'country_id' => 170
            ],[
                'id_type_id' => 102,
                'country_id' => 170
            ],[
                'id_type_id' => 101,
                'country_id' => 171
            ],[
                'id_type_id' => 102,
                'country_id' => 171
            ],[
                'id_type_id' => 101,
                'country_id' => 172
            ],[
                'id_type_id' => 102,
                'country_id' => 172
            ],[
                'id_type_id' => 101,
                'country_id' => 173
            ],[
                'id_type_id' => 102,
                'country_id' => 173
            ],[
                'id_type_id' => 101,
                'country_id' => 174
            ],[
                'id_type_id' => 102,
                'country_id' => 174
            ],[
                'id_type_id' => 101,
                'country_id' => 175
            ],[
                'id_type_id' => 102,
                'country_id' => 175
            ],[
                'id_type_id' => 101,
                'country_id' => 176
            ],[
                'id_type_id' => 102,
                'country_id' => 176
            ],[
                'id_type_id' => 101,
                'country_id' => 177
            ],[
                'id_type_id' => 102,
                'country_id' => 177
            ],[
                'id_type_id' => 101,
                'country_id' => 178
            ],[
                'id_type_id' => 102,
                'country_id' => 178
            ],[
                'id_type_id' => 101,
                'country_id' => 179
            ],[
                'id_type_id' => 102,
                'country_id' => 179
            ],[
                'id_type_id' => 101,
                'country_id' => 180
            ],[
                'id_type_id' => 102,
                'country_id' => 180
            ],[
                'id_type_id' => 101,
                'country_id' => 181
            ],[
                'id_type_id' => 102,
                'country_id' => 181
            ],[
                'id_type_id' => 101,
                'country_id' => 182
            ],[
                'id_type_id' => 102,
                'country_id' => 182
            ],[
                'id_type_id' => 101,
                'country_id' => 183
            ],[
                'id_type_id' => 102,
                'country_id' => 183
            ],[
                'id_type_id' => 101,
                'country_id' => 184
            ],[
                'id_type_id' => 102,
                'country_id' => 184
            ],[
                'id_type_id' => 101,
                'country_id' => 185
            ],[
                'id_type_id' => 102,
                'country_id' => 185
            ],[
                'id_type_id' => 101,
                'country_id' => 186
            ],[
                'id_type_id' => 102,
                'country_id' => 186
            ],[
                'id_type_id' => 101,
                'country_id' => 187
            ],[
                'id_type_id' => 102,
                'country_id' => 187
            ],[
                'id_type_id' => 101,
                'country_id' => 188
            ],[
                'id_type_id' => 102,
                'country_id' => 188
            ],[
                'id_type_id' => 101,
                'country_id' => 189
            ],[
                'id_type_id' => 102,
                'country_id' => 189
            ],[
                'id_type_id' => 101,
                'country_id' => 190
            ],[
                'id_type_id' => 102,
                'country_id' => 190
            ],[
                'id_type_id' => 101,
                'country_id' => 191
            ],[
                'id_type_id' => 102,
                'country_id' => 191
            ],[
                'id_type_id' => 101,
                'country_id' => 192
            ],[
                'id_type_id' => 102,
                'country_id' => 192
            ],[
                'id_type_id' => 101,
                'country_id' => 193
            ],[
                'id_type_id' => 102,
                'country_id' => 193
            ],[
                'id_type_id' => 101,
                'country_id' => 194
            ],[
                'id_type_id' => 102,
                'country_id' => 194
            ],[
                'id_type_id' => 101,
                'country_id' => 195
            ],[
                'id_type_id' => 102,
                'country_id' => 195
            ],[
                'id_type_id' => 101,
                'country_id' => 196
            ],[
                'id_type_id' => 102,
                'country_id' => 196
            ],[
                'id_type_id' => 101,
                'country_id' => 197
            ],[
                'id_type_id' => 102,
                'country_id' => 197
            ],[
                'id_type_id' => 101,
                'country_id' => 198
            ],[
                'id_type_id' => 102,
                'country_id' => 198
            ],[
                'id_type_id' => 101,
                'country_id' => 199
            ],[
                'id_type_id' => 102,
                'country_id' => 199
            ],[
                'id_type_id' => 101,
                'country_id' => 200
            ],[
                'id_type_id' => 102,
                'country_id' => 200
            ],[
                'id_type_id' => 101,
                'country_id' => 201
            ],[
                'id_type_id' => 102,
                'country_id' => 201
            ],[
                'id_type_id' => 101,
                'country_id' => 202
            ],[
                'id_type_id' => 102,
                'country_id' => 202
            ],[
                'id_type_id' => 101,
                'country_id' => 203
            ],[
                'id_type_id' => 102,
                'country_id' => 203
            ],[
                'id_type_id' => 101,
                'country_id' => 204
            ],[
                'id_type_id' => 102,
                'country_id' => 204
            ],[
                'id_type_id' => 101,
                'country_id' => 205
            ],[
                'id_type_id' => 102,
                'country_id' => 205
            ],[
                'id_type_id' => 101,
                'country_id' => 206
            ],[
                'id_type_id' => 102,
                'country_id' => 206
            ],[
                'id_type_id' => 101,
                'country_id' => 207
            ],[
                'id_type_id' => 102,
                'country_id' => 207
            ],[
                'id_type_id' => 101,
                'country_id' => 208
            ],[
                'id_type_id' => 102,
                'country_id' => 208
            ],[
                'id_type_id' => 101,
                'country_id' => 209
            ],[
                'id_type_id' => 102,
                'country_id' => 209
            ],[
                'id_type_id' => 101,
                'country_id' => 210
            ],[
                'id_type_id' => 102,
                'country_id' => 210
            ],[
                'id_type_id' => 101,
                'country_id' => 211
            ],[
                'id_type_id' => 102,
                'country_id' => 211
            ],[
                'id_type_id' => 101,
                'country_id' => 212
            ],[
                'id_type_id' => 102,
                'country_id' => 212
            ],[
                'id_type_id' => 101,
                'country_id' => 213
            ],[
                'id_type_id' => 102,
                'country_id' => 213
            ],[
                'id_type_id' => 101,
                'country_id' => 214
            ],[
                'id_type_id' => 102,
                'country_id' => 214
            ],[
                'id_type_id' => 101,
                'country_id' => 215
            ],[
                'id_type_id' => 102,
                'country_id' => 215
            ],[
                'id_type_id' => 101,
                'country_id' => 216
            ],[
                'id_type_id' => 102,
                'country_id' => 216
            ],[
                'id_type_id' => 101,
                'country_id' => 217
            ],[
                'id_type_id' => 102,
                'country_id' => 217
            ],[
                'id_type_id' => 101,
                'country_id' => 218
            ],[
                'id_type_id' => 102,
                'country_id' => 218
            ],[
                'id_type_id' => 101,
                'country_id' => 219
            ],[
                'id_type_id' => 102,
                'country_id' => 219
            ],[
                'id_type_id' => 101,
                'country_id' => 220
            ],[
                'id_type_id' => 102,
                'country_id' => 220
            ],[
                'id_type_id' => 101,
                'country_id' => 221
            ],[
                'id_type_id' => 102,
                'country_id' => 221
            ],[
                'id_type_id' => 101,
                'country_id' => 222
            ],[
                'id_type_id' => 102,
                'country_id' => 222
            ],[
                'id_type_id' => 101,
                'country_id' => 223
            ],[
                'id_type_id' => 102,
                'country_id' => 223
            ],[
                'id_type_id' => 101,
                'country_id' => 224
            ],[
                'id_type_id' => 102,
                'country_id' => 224
            ],[
                'id_type_id' => 101,
                'country_id' => 225
            ],[
                'id_type_id' => 102,
                'country_id' => 225
            ],[
                'id_type_id' => 101,
                'country_id' => 226
            ],[
                'id_type_id' => 102,
                'country_id' => 226
            ],[
                'id_type_id' => 101,
                'country_id' => 227
            ],[
                'id_type_id' => 102,
                'country_id' => 227
            ],[
                'id_type_id' => 101,
                'country_id' => 228
            ],[
                'id_type_id' => 102,
                'country_id' => 228
            ],[
                'id_type_id' => 101,
                'country_id' => 229
            ],[
                'id_type_id' => 102,
                'country_id' => 229
            ],[
                'id_type_id' => 101,
                'country_id' => 230
            ],[
                'id_type_id' => 102,
                'country_id' => 230
            ],[
                'id_type_id' => 101,
                'country_id' => 231
            ],[
                'id_type_id' => 102,
                'country_id' => 231
            ],[
                'id_type_id' => 101,
                'country_id' => 232
            ],[
                'id_type_id' => 102,
                'country_id' => 232
            ],[
                'id_type_id' => 101,
                'country_id' => 233
            ],[
                'id_type_id' => 102,
                'country_id' => 233
            ],[
                'id_type_id' => 101,
                'country_id' => 234
            ],[
                'id_type_id' => 102,
                'country_id' => 234
            ],[
                'id_type_id' => 101,
                'country_id' => 235
            ],[
                'id_type_id' => 102,
                'country_id' => 235
            ],[
                'id_type_id' => 101,
                'country_id' => 236
            ],[
                'id_type_id' => 102,
                'country_id' => 236
            ],[
                'id_type_id' => 101,
                'country_id' => 237
            ],[
                'id_type_id' => 102,
                'country_id' => 237
            ],[
                'id_type_id' => 101,
                'country_id' => 238
            ],[
                'id_type_id' => 102,
                'country_id' => 238
            ],[
                'id_type_id' => 101,
                'country_id' => 239
            ],[
                'id_type_id' => 102,
                'country_id' => 239
            ],[
                'id_type_id' => 101,
                'country_id' => 240
            ],[
                'id_type_id' => 102,
                'country_id' => 240
            ],[
                'id_type_id' => 101,
                'country_id' => 241
            ],[
                'id_type_id' => 102,
                'country_id' => 241
            ],[
                'id_type_id' => 101,
                'country_id' => 242
            ],[
                'id_type_id' => 102,
                'country_id' => 242
            ],[
                'id_type_id' => 101,
                'country_id' => 243
            ],[
                'id_type_id' => 102,
                'country_id' => 243
            ],[
                'id_type_id' => 101,
                'country_id' => 244
            ],[
                'id_type_id' => 102,
                'country_id' => 244
            ],[
                'id_type_id' => 101,
                'country_id' => 245
            ],[
                'id_type_id' => 102,
                'country_id' => 245
            ],[
                'id_type_id' => 101,
                'country_id' => 246
            ],[
                'id_type_id' => 102,
                'country_id' => 246
            ],[
                'id_type_id' => 101,
                'country_id' => 247
            ],[
                'id_type_id' => 102,
                'country_id' => 247
            ],[
                'id_type_id' => 101,
                'country_id' => 248
            ],[
                'id_type_id' => 102,
                'country_id' => 248
            ],[
                'id_type_id' => 101,
                'country_id' => 249
            ],[
                'id_type_id' => 102,
                'country_id' => 249
            ],[
                'id_type_id' => 101,
                'country_id' => 250
            ],[
                'id_type_id' => 102,
                'country_id' => 250
            ],[
                'id_type_id' => 101,
                'country_id' => 251
            ],[
                'id_type_id' => 102,
                'country_id' => 251
            ],[
                'id_type_id' => 101,
                'country_id' => 252
            ],[
                'id_type_id' => 102,
                'country_id' => 252
            ],[
                'id_type_id' => 101,
                'country_id' => 253
            ],[
                'id_type_id' => 102,
                'country_id' => 253
            ],[
                'id_type_id' => 101,
                'country_id' => 254
            ],[
                'id_type_id' => 102,
                'country_id' => 254
            ],[
                'id_type_id' => 101,
                'country_id' => 255
            ],[
                'id_type_id' => 102,
                'country_id' => 255
            ],[
                'id_type_id' => 101,
                'country_id' => 256
            ],[
                'id_type_id' => 102,
                'country_id' => 256
            ],[
                'id_type_id' => 101,
                'country_id' => 257
            ],[
                'id_type_id' => 102,
                'country_id' => 257
            ],[
                'id_type_id' => 101,
                'country_id' => 258
            ],[
                'id_type_id' => 102,
                'country_id' => 258
            ],[
                'id_type_id' => 101,
                'country_id' => 259
            ],[
                'id_type_id' => 102,
                'country_id' => 259
            ],[
                'id_type_id' => 101,
                'country_id' => 260
            ],[
                'id_type_id' => 102,
                'country_id' => 260
            ],[
                'id_type_id' => 101,
                'country_id' => 261
            ],[
                'id_type_id' => 102,
                'country_id' => 261
            ],[
                'id_type_id' => 101,
                'country_id' => 262
            ],[
                'id_type_id' => 102,
                'country_id' => 262
            ],[
                'id_type_id' => 101,
                'country_id' => 263
            ],[
                'id_type_id' => 102,
                'country_id' => 263
            ],[
                'id_type_id' => 101,
                'country_id' => 264
            ],[
                'id_type_id' => 102,
                'country_id' => 264
            ],[
                'id_type_id' => 101,
                'country_id' => 265
            ],[
                'id_type_id' => 102,
                'country_id' => 265
            ],[
                'id_type_id' => 101,
                'country_id' => 266
            ],[
                'id_type_id' => 102,
                'country_id' => 266
            ],[
                'id_type_id' => 101,
                'country_id' => 267
            ],[
                'id_type_id' => 102,
                'country_id' => 267
            ],[
                'id_type_id' => 101,
                'country_id' => 268
            ],[
                'id_type_id' => 102,
                'country_id' => 268
            ],[
                'id_type_id' => 101,
                'country_id' => 269
            ],[
                'id_type_id' => 102,
                'country_id' => 269
            ],[
                'id_type_id' => 101,
                'country_id' => 270
            ],[
                'id_type_id' => 102,
                'country_id' => 270
            ],[
                'id_type_id' => 101,
                'country_id' => 271
            ],[
                'id_type_id' => 102,
                'country_id' => 271
            ],[
                'id_type_id' => 101,
                'country_id' => 272
            ],[
                'id_type_id' => 102,
                'country_id' => 272
            ],[
                'id_type_id' => 101,
                'country_id' => 273
            ],[
                'id_type_id' => 102,
                'country_id' => 273
            ],[
                'id_type_id' => 101,
                'country_id' => 274
            ],[
                'id_type_id' => 102,
                'country_id' => 274
            ],[
                'id_type_id' => 101,
                'country_id' => 275
            ],[
                'id_type_id' => 102,
                'country_id' => 275
            ],[
                'id_type_id' => 101,
                'country_id' => 276
            ],[
                'id_type_id' => 102,
                'country_id' => 276
            ],[
                'id_type_id' => 101,
                'country_id' => 277
            ],[
                'id_type_id' => 102,
                'country_id' => 277
            ],[
                'id_type_id' => 101,
                'country_id' => 278
            ],[
                'id_type_id' => 102,
                'country_id' => 278
            ],[
                'id_type_id' => 101,
                'country_id' => 279
            ],[
                'id_type_id' => 102,
                'country_id' => 279
            ],[
                'id_type_id' => 101,
                'country_id' => 280
            ],[
                'id_type_id' => 102,
                'country_id' => 280
            ],[
                'id_type_id' => 101,
                'country_id' => 281
            ],[
                'id_type_id' => 102,
                'country_id' => 281
            ],[
                'id_type_id' => 101,
                'country_id' => 282
            ],[
                'id_type_id' => 102,
                'country_id' => 282
            ],[
                'id_type_id' => 101,
                'country_id' => 283
            ],[
                'id_type_id' => 102,
                'country_id' => 283
            ],[
                'id_type_id' => 101,
                'country_id' => 284
            ],[
                'id_type_id' => 102,
                'country_id' => 284
            ],[
                'id_type_id' => 101,
                'country_id' => 285
            ],[
                'id_type_id' => 102,
                'country_id' => 285
            ],[
                'id_type_id' => 101,
                'country_id' => 286
            ],[
                'id_type_id' => 102,
                'country_id' => 286
            ],[
                'id_type_id' => 101,
                'country_id' => 287
            ],[
                'id_type_id' => 102,
                'country_id' => 287
            ],[
                'id_type_id' => 101,
                'country_id' => 288
            ],[
                'id_type_id' => 102,
                'country_id' => 288
            ],[
                'id_type_id' => 101,
                'country_id' => 289
            ],[
                'id_type_id' => 102,
                'country_id' => 289
            ],[
                'id_type_id' => 101,
                'country_id' => 290
            ],[
                'id_type_id' => 102,
                'country_id' => 290
            ],[
                'id_type_id' => 101,
                'country_id' => 291
            ],[
                'id_type_id' => 102,
                'country_id' => 291
            ],[
                'id_type_id' => 101,
                'country_id' => 292
            ],[
                'id_type_id' => 102,
                'country_id' => 292
            ],[
                'id_type_id' => 101,
                'country_id' => 293
            ],[
                'id_type_id' => 102,
                'country_id' => 293
            ],[
                'id_type_id' => 101,
                'country_id' => 294
            ],[
                'id_type_id' => 102,
                'country_id' => 294
            ],[
                'id_type_id' => 101,
                'country_id' => 295
            ],[
                'id_type_id' => 102,
                'country_id' => 295
            ],[
                'id_type_id' => 101,
                'country_id' => 296
            ],[
                'id_type_id' => 102,
                'country_id' => 296
            ],[
                'id_type_id' => 101,
                'country_id' => 297
            ],[
                'id_type_id' => 102,
                'country_id' => 297
            ],[
                'id_type_id' => 101,
                'country_id' => 298
            ],[
                'id_type_id' => 102,
                'country_id' => 298
            ],[
                'id_type_id' => 101,
                'country_id' => 299
            ],[
                'id_type_id' => 102,
                'country_id' => 299
            ],[
                'id_type_id' => 101,
                'country_id' => 300
            ],[
                'id_type_id' => 102,
                'country_id' => 300
            ],[
                'id_type_id' => 101,
                'country_id' => 301
            ],[
                'id_type_id' => 102,
                'country_id' => 301
            ],[
                'id_type_id' => 101,
                'country_id' => 302
            ],[
                'id_type_id' => 102,
                'country_id' => 302
            ],[
                'id_type_id' => 101,
                'country_id' => 303
            ],[
                'id_type_id' => 102,
                'country_id' => 303
            ],[
                'id_type_id' => 101,
                'country_id' => 304
            ],[
                'id_type_id' => 102,
                'country_id' => 304
            ],[
                'id_type_id' => 101,
                'country_id' => 305
            ],[
                'id_type_id' => 102,
                'country_id' => 305
            ],[
                'id_type_id' => 101,
                'country_id' => 306
            ],[
                'id_type_id' => 102,
                'country_id' => 306
            ],[
                'id_type_id' => 101,
                'country_id' => 307
            ],[
                'id_type_id' => 102,
                'country_id' => 307
            ],[
                'id_type_id' => 101,
                'country_id' => 308
            ],[
                'id_type_id' => 102,
                'country_id' => 308
            ],[
                'id_type_id' => 101,
                'country_id' => 309
            ],[
                'id_type_id' => 102,
                'country_id' => 309
            ],[
                'id_type_id' => 101,
                'country_id' => 310
            ],[
                'id_type_id' => 102,
                'country_id' => 310
            ],[
                'id_type_id' => 101,
                'country_id' => 311
            ],[
                'id_type_id' => 102,
                'country_id' => 311
            ],[
                'id_type_id' => 101,
                'country_id' => 312
            ],[
                'id_type_id' => 102,
                'country_id' => 312
            ],[
                'id_type_id' => 101,
                'country_id' => 313
            ],[
                'id_type_id' => 102,
                'country_id' => 313
            ],[
                'id_type_id' => 101,
                'country_id' => 314
            ],[
                'id_type_id' => 102,
                'country_id' => 314
            ],[
                'id_type_id' => 101,
                'country_id' => 315
            ],[
                'id_type_id' => 102,
                'country_id' => 315
            ],[
                'id_type_id' => 101,
                'country_id' => 316
            ],[
                'id_type_id' => 103,
                'country_id' => 316
            ],[
                'id_type_id' => 101,
                'country_id' => 317
            ],[
                'id_type_id' => 102,
                'country_id' => 317
            ],[
                'id_type_id' => 101,
                'country_id' => 318
            ],[
                'id_type_id' => 102,
                'country_id' => 318
            ],[
                'id_type_id' => 101,
                'country_id' => 319
            ],[
                'id_type_id' => 102,
                'country_id' => 319
            ],[
                'id_type_id' => 101,
                'country_id' => 320
            ],[
                'id_type_id' => 102,
                'country_id' => 320
            ],[
                'id_type_id' => 101,
                'country_id' => 321
            ],[
                'id_type_id' => 102,
                'country_id' => 321
            ],[
                'id_type_id' => 101,
                'country_id' => 322
            ],[
                'id_type_id' => 102,
                'country_id' => 322
            ],[
                'id_type_id' => 101,
                'country_id' => 323
            ],[
                'id_type_id' => 102,
                'country_id' => 323
            ],[
                'id_type_id' => 101,
                'country_id' => 324
            ],[
                'id_type_id' => 102,
                'country_id' => 324
            ],[
                'id_type_id' => 101,
                'country_id' => 325
            ],[
                'id_type_id' => 102,
                'country_id' => 325
            ],[
                'id_type_id' => 101,
                'country_id' => 326
            ],[
                'id_type_id' => 102,
                'country_id' => 326
            ],[
                'id_type_id' => 101,
                'country_id' => 327
            ],[
                'id_type_id' => 102,
                'country_id' => 327
            ],[
                'id_type_id' => 101,
                'country_id' => 328
            ],[
                'id_type_id' => 102,
                'country_id' => 328
            ],[
                'id_type_id' => 101,
                'country_id' => 329
            ],[
                'id_type_id' => 102,
                'country_id' => 329
            ],[
                'id_type_id' => 101,
                'country_id' => 330
            ],[
                'id_type_id' => 102,
                'country_id' => 330
            ],[
                'id_type_id' => 101,
                'country_id' => 331
            ],[
                'id_type_id' => 102,
                'country_id' => 331
            ],[
                'id_type_id' => 101,
                'country_id' => 332
            ],[
                'id_type_id' => 102,
                'country_id' => 332
            ],[
                'id_type_id' => 101,
                'country_id' => 333
            ],[
                'id_type_id' => 102,
                'country_id' => 333
            ],[
                'id_type_id' => 101,
                'country_id' => 334
            ],[
                'id_type_id' => 102,
                'country_id' => 334
            ],[
                'id_type_id' => 101,
                'country_id' => 335
            ],[
                'id_type_id' => 102,
                'country_id' => 335
            ],[
                'id_type_id' => 101,
                'country_id' => 336
            ],[
                'id_type_id' => 102,
                'country_id' => 336
            ],[
                'id_type_id' => 101,
                'country_id' => 337
            ],[
                'id_type_id' => 102,
                'country_id' => 337
            ],[
                'id_type_id' => 101,
                'country_id' => 338
            ],[
                'id_type_id' => 102,
                'country_id' => 338
            ],[
                'id_type_id' => 101,
                'country_id' => 339
            ],[
                'id_type_id' => 102,
                'country_id' => 339
            ],[
                'id_type_id' => 101,
                'country_id' => 340
            ],[
                'id_type_id' => 102,
                'country_id' => 340
            ],[
                'id_type_id' => 101,
                'country_id' => 341
            ],[
                'id_type_id' => 102,
                'country_id' => 341
            ],[
                'id_type_id' => 101,
                'country_id' => 342
            ],[
                'id_type_id' => 102,
                'country_id' => 342
            ],[
                'id_type_id' => 101,
                'country_id' => 343
            ],[
                'id_type_id' => 102,
                'country_id' => 343
            ],[
                'id_type_id' => 101,
                'country_id' => 344
            ],[
                'id_type_id' => 102,
                'country_id' => 344
            ],[
                'id_type_id' => 101,
                'country_id' => 345
            ],[
                'id_type_id' => 102,
                'country_id' => 345
            ],[
                'id_type_id' => 101,
                'country_id' => 346
            ],[
                'id_type_id' => 102,
                'country_id' => 346
            ],[
                'id_type_id' => 101,
                'country_id' => 347
            ],[
                'id_type_id' => 102,
                'country_id' => 347
            ],[
                'id_type_id' => 101,
                'country_id' => 348
            ],[
                'id_type_id' => 102,
                'country_id' => 348
            ],[
                'id_type_id' => 101,
                'country_id' => 349
            ],[
                'id_type_id' => 102,
                'country_id' => 349
            ],[
                'id_type_id' => 101,
                'country_id' => 350
            ],[
                'id_type_id' => 102,
                'country_id' => 350
            ],[
                'id_type_id' => 101,
                'country_id' => 351
            ],[
                'id_type_id' => 102,
                'country_id' => 351
            ],[
                'id_type_id' => 101,
                'country_id' => 352
            ],[
                'id_type_id' => 102,
                'country_id' => 352
            ],[
                'id_type_id' => 101,
                'country_id' => 353
            ],[
                'id_type_id' => 102,
                'country_id' => 353
            ]
        ];

        if (count($id_types_countries) > 0) {
            for ($i = 0; $i < count($id_types_countries); $i++) { 
            	$id_types_country = IdTypeCountry::query()
            		->where('id_type_id', $id_types_countries[$i]['id_type_id'])
            		->where('country_id', $id_types_countries[$i]['country_id'])
            		->first();

                if (!$id_types_country) {
                    IdTypeCountry::create($id_types_countries[$i]);
                }
            }
        }
    }
}
