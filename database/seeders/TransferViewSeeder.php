<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\TransferViewLang;
use App\Models\TransferView;

class TransferViewSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$transfer_views = [
			[  
				'main' => [
					'id' => 1,
					'icon' => 'awdawdawd',
					'slug' => 'between-your-accounts'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Есептік жазбаларыңыздың арасында'
					],[
						'lang' => 'ru',
						'name' => 'Между своими счетами'
					],[
						'lang' => 'en',
						'name' => 'Between your accounts'
					]
				]
			],[
				'main' => [
					'id' => 2,
					'icon' => 'awdawdawd',
					'slug' => 'between-clients'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Клиенттер арасында'
					],[
						'lang' => 'ru',
						'name' => 'Между клиентами'
					],[
						'lang' => 'en',
						'name' => 'Between clients'
					]
				]
			]
		];

		for ($i = 0; $i < count($transfer_views); $i++) { 
			$transfer_view = TransferView::query()
				->where('id', $transfer_views[$i]['main']['id'])
				->first();

			if ($transfer_view) {
				$transfer_view->update($transfer_views[$i]['main']);

				for ($j = 0; $j < count($transfer_views[$i]['translations']); $j++) { 
					$transfer_view_lang = TransferViewLang::query()
						->where('id', $transfer_views[$i]['main']['id'])
						->where('lang', $transfer_views[$i]['translations'][$j]['lang'])
						->first();

					$transfer_view_lang->update([
						'name' => $transfer_views[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$transfer_view = TransferView::create($transfer_views[$i]['main']);

				for ($j = 0; $j < count($transfer_views[$i]['translations']); $j++) { 
					TransferViewLang::create([
						'id' => $transfer_views[$i]['main']['id'],
						'lang' => $transfer_views[$i]['translations'][$j]['lang'],
						'name' => $transfer_views[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
