<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\NotificationViewLang;
use App\Models\NotificationView;

class NotificationViewSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$notification_views = [
			[
				'main' => [
					'id' => 101,
					'slug' => 'schedule',
					'icon' => 'frontend/images/icon-time-management.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Кесте'
					],[
						'lang' => 'ru',
						'name' => 'Расписание'
					],[
						'lang' => 'en',
						'name' => 'Schedule'
					]
				]
			],[
				'main' => [
					'id' => 102,
					'slug' => 'tiding',
					'icon' => 'frontend/images/icon-text-lines.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Жаңалықтар'
					],[
						'lang' => 'ru',
						'name' => 'Новости'
					],[
						'lang' => 'en',
						'name' => 'News'
					]
				]
			],[
				'main' => [
					'id' => 103,
					'slug' => 'event',
					'icon' => 'frontend/images/icon-confetti.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Іс-шаралар'
					],[
						'lang' => 'ru',
						'name' => 'Мероприятий'
					],[
						'lang' => 'en',
						'name' => 'Events'
					]
				]
			],[
				'main' => [
					'id' => 104,
					'slug' => 'finance',
					'icon' => 'frontend/images/icon-coins-FFD949.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қаржы'
					],[
						'lang' => 'ru',
						'name' => 'Финансы'
					],[
						'lang' => 'en',
						'name' => 'Finance'
					]
				]
			],[
				'main' => [
					'id' => 105,
					'slug' => 'statement',
					'icon' => 'frontend/images/icon-worldwide.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Өтініштер'
					],[
						'lang' => 'ru',
						'name' => 'Заявления'
					],[
						'lang' => 'en',
						'name' => 'Applications'
					]
				]
			],[
				'main' => [
					'id' => 106,
					'slug' => 'service',
					'icon' => 'frontend/images/icon-consulting-services.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қызмет'
					],[
						'lang' => 'ru',
						'name' => 'Служебные'
					],[
						'lang' => 'en',
						'name' => 'Service'
					]
				]
			]
		];

		if (count($notification_views) > 0) {
			for ($i = 0; $i < count($notification_views); $i++) { 
				$notification_view = NotificationView::query()
					->where('id', $notification_views[$i]['main']['id'])
					->first();

				if ($notification_view) {
					$notification_view->update($notification_views[$i]['main']);

					for ($j = 0; $j < count($notification_views[$i]['translations']); $j++) { 
						$notification_view_lang = NotificationViewLang::query()
	                        ->where('id', $notification_views[$i]['main']['id'])
	                        ->where('lang', $notification_views[$i]['translations'][$j]['lang'])
	                        ->first();

	                    $notification_view_lang->update([
	                        'name' => $notification_views[$i]['translations'][$j]['name']
	                    ]);
					}
				} else {
					$notification_view = NotificationView::create($notification_views[$i]['main']);

					for ($j = 0; $j < count($notification_views[$i]['translations']); $j++) { 
						NotificationViewLang::create([
							'id' => $notification_views[$i]['main']['id'],
							'lang' => $notification_views[$i]['translations'][$j]['lang'],
							'name' => $notification_views[$i]['translations'][$j]['name']
						]);
					}
				}
			}
		}
	}
}
