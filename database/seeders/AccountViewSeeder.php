<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\AccountViewLang;
use App\Models\AccountView;

class AccountViewSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$account_views = [
			[
				'main' => [
					'id' => 100001,
					'state_id' => 2
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Негізгі шот',
					],[
						'lang' => 'ru',
						'name' => 'Основной баланс',
					],[
						'lang' => 'en',
						'name' => 'Master account'
					]
				]
			]
		];

		if (count($account_views) > 0) {
			for ($i = 0; $i < count($account_views); $i++) { 
				$account_view = AccountView::query()
					->where('id', $account_views[$i]['main']['id'])
					->first();

				if ($account_view) {
					$account_view->update($account_views[$i]['main']);

					for ($j = 0; $j < count($account_views[$i]['translations']); $j++) { 
						$account_view_lang = AccountViewLang::query()
							->where('id', $account_views[$i]['main']['id'])
							->where('lang', $account_views[$i]['translations'][$j]['lang'])
							->first();

						$account_view_lang->update([
							'name' => $account_views[$i]['translations'][$j]['name']
						]);
					}
				} else {
					$account_view = AccountView::create($account_views[$i]['main']);

					for ($j = 0; $j < count($account_views[$i]['translations']); $j++) { 
						AccountViewLang::create([
							'id' => $account_views[$i]['main']['id'],
							'lang' => $account_views[$i]['translations'][$j]['lang'],
							'name' => $account_views[$i]['translations'][$j]['name'],
						]);
					}
				}
			}
		}
	}
}
