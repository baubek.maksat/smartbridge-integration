<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\TypeLang;
use App\Models\Type;

class TypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$types = [
			[  
				'main' => [
					'id' => 1,
					'slug' => 'web'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Веб-сайт'
					],[
						'lang' => 'ru',
						'name' => 'Bеб-сайт'
					],[
						'lang' => 'en',
						'name' => 'Website'
					]
				]
			],[
				'main' => [
					'id' => 2,
					'slug' => 'app'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Мобильді қосымша'
					],[
						'lang' => 'ru',
						'name' => 'Мобильное приложение'
					],[
						'lang' => 'en',
						'name' => 'Mobile app'
					]
				]
			]
		];

		for ($i = 0; $i < count($types); $i++) { 
			$type = Type::query()
				->where('id', $types[$i]['main']['id'])
				->first();

			if ($type) {
				$type->update($types[$i]['main']);

				for ($j = 0; $j < count($types[$i]['translations']); $j++) { 
					$type_lang = TypeLang::query()
						->where('id', $types[$i]['main']['id'])
						->where('lang', $types[$i]['translations'][$j]['lang'])
						->first();

					$type_lang->update([
						'name' => $types[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$type = Type::create($types[$i]['main']);

				for ($j = 0; $j < count($types[$i]['translations']); $j++) { 
					TypeLang::create([
						'id' => $types[$i]['main']['id'],
						'lang' => $types[$i]['translations'][$j]['lang'],
						'name' => $types[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
