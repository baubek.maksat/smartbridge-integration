<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\IdTypeLang;
use App\Models\IdType;

class IdTypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$id_types = [
			[
				'main' => [
					'id' => 101
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Паспорт',
					],[
						'lang' => 'ru',
						'name' => 'Паспорт',
					],[
						'lang' => 'en',
						'name' => 'Passport'
					]
				]
			],[
				'main' => [
					'id' => 102
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Мекендеу қағазы',
					],[
						'lang' => 'ru',
						'name' => 'Вид на жительство',
					],[
						'lang' => 'en',
						'name' => 'Permanent residency'
					]
				]
			],[
				'main' => [
					'id' => 103
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Жеке куәлік',
					],[
						'lang' => 'ru',
						'name' => 'Удостоверение личности',
					],[
						'lang' => 'en',
						'name' => 'Identification'
					]
				]
			],[
				'main' => [
					'id' => 104
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Туу туалы куәлік',
					],[
						'lang' => 'ru',
						'name' => 'Свидетельство о рождении',
					],[
						'lang' => 'en',
						'name' => 'Birth certificate'
					]
				]
			]
		];

		for ($i = 0; $i < count($id_types); $i++) { 
			$id_type = IdType::query()
				->where('id', $id_types[$i]['main']['id'])
				->first();

			if ($id_type) {
				$id_type->update($id_types[$i]['main']);

				for ($j = 0; $j < count($id_types[$i]['translations']); $j++) { 
					$id_type_lang = IdTypeLang::query()
						->where('id', $id_types[$i]['main']['id'])
						->where('lang', $id_types[$i]['translations'][$j]['lang'])
						->first();

					$id_type_lang->update([
						'name' => $id_types[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$id_type = IdType::create($id_types[$i]['main']);

				for ($j = 0; $j < count($id_types[$i]['translations']); $j++) { 
					IdTypeLang::create([
						'id' => $id_types[$i]['main']['id'],
						'lang' => $id_types[$i]['translations'][$j]['lang'],
						'name' => $id_types[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
