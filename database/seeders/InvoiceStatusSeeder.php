<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\InvoiceStatusLang;
use App\Models\InvoiceStatus;

class InvoiceStatusSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$invoice_statuses = [
			[
				'main' => [
					'id' => 1,
					'slug' => 'paid'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Төленген',
					],[
						'lang' => 'ru',
						'name' => 'Оплачен',
					],[
						'lang' => 'en',
						'name' => 'Paid'
					]
				]
			],[
				'main' => [
					'id' => 2,
					'slug' => 'permanent-residency'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Төленген жоқ',
					],[
						'lang' => 'ru',
						'name' => 'Не оплачен',
					],[
						'lang' => 'en',
						'name' => 'Permanent residency'
					]
				]
			],[
				'main' => [
					'id' => 3,
					'slug' => 'canceled'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Бас тартылды',
					],[
						'lang' => 'ru',
						'name' => 'Отменен',
					],[
						'lang' => 'en',
						'name' => 'Canceled'
					]
				]
			]
		];

		for ($i = 0; $i < count($invoice_statuses); $i++) { 
			$invoice_status = InvoiceStatus::query()
				->where('id', $invoice_statuses[$i]['main']['id'])
				->first();

			if ($invoice_status) {
				$invoice_status->update($invoice_statuses[$i]['main']);

				for ($j = 0; $j < count($invoice_statuses[$i]['translations']); $j++) { 
					$invoice_status_lang = InvoiceStatusLang::query()
						->where('id', $invoice_statuses[$i]['main']['id'])
						->where('lang', $invoice_statuses[$i]['translations'][$j]['lang'])
						->first();

					$invoice_status_lang->update([
						'name' => $invoice_statuses[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$invoice_status = InvoiceStatus::create($invoice_statuses[$i]['main']);

				for ($j = 0; $j < count($invoice_statuses[$i]['translations']); $j++) { 
					InvoiceStatusLang::create([
						'id' => $invoice_statuses[$i]['main']['id'],
						'lang' => $invoice_statuses[$i]['translations'][$j]['lang'],
						'name' => $invoice_statuses[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
