<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\StateLang;
use App\Models\State;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            [  
                'main' => [
                    'id' => 1
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Белсенді емес'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Не активный'
                    ],[
                        'lang' => 'en',
                        'name' => 'Not active'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 2
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Белсенді'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Активный'
                    ],[
                        'lang' => 'en',
                        'name' => 'Active'
                    ]
                ]
            ]
        ];

        for ($i = 0; $i < count($states); $i++) { 
            $state = State::query()
                ->where('id', $states[$i]['main']['id'])
                ->first();

            if ($state) {
                $state->update($states[$i]['main']);

                for ($j = 0; $j < count($states[$i]['translations']); $j++) { 
                    $state_lang = StateLang::query()
                        ->where('id', $states[$i]['main']['id'])
                        ->where('lang', $states[$i]['translations'][$j]['lang'])
                        ->first();

                    $state_lang->update([
                        'name' => $states[$i]['translations'][$j]['name']
                    ]);
                }
            } else {
                $state = State::create($states[$i]['main']);

                for ($j = 0; $j < count($states[$i]['translations']); $j++) { 
                    StateLang::create([
                        'id' => $states[$i]['main']['id'],
                        'lang' => $states[$i]['translations'][$j]['lang'],
                        'name' => $states[$i]['translations'][$j]['name']
                    ]);
                }
            }
        }
    }
}
