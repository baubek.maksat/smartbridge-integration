<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SpecialityTypeLang;
use App\Models\SpecialityType;

class SpecialityTypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$speciality_types = [
			[
				'main' => [
					'id' => 101
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Мамандық',
					],[
						'lang' => 'ru',
						'name' => 'Специальность',
					],[
						'lang' => 'en',
						'name' => 'Speciality'
					]
				]
			],[
				'main' => [
					'id' => 102
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Білім беру бағдарламасы',
					],[
						'lang' => 'ru',
						'name' => 'Образовательная программа',
					],[
						'lang' => 'en',
						'name' => 'Educational program'
					]
				]
			]
		];

		for ($i = 0; $i < count($speciality_types); $i++) { 
			$speciality_type = SpecialityType::query()
				->where('id', $speciality_types[$i]['main']['id'])
				->first();

			if ($speciality_type) {
				for ($j = 0; $j < count($speciality_types[$i]['translations']); $j++) { 
					$speciality_type_lang = SpecialityTypeLang::query()
						->where('id', $speciality_types[$i]['main']['id'])
						->where('lang', $speciality_types[$i]['translations'][$j]['lang'])
						->first();

					$speciality_type_lang->update([
						'name' => $speciality_types[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$speciality_type = SpecialityType::create($speciality_types[$i]['main']);

				for ($j = 0; $j < count($speciality_types[$i]['translations']); $j++) { 
					SpecialityTypeLang::create([
						'id' => $speciality_types[$i]['main']['id'],
						'lang' => $speciality_types[$i]['translations'][$j]['lang'],
						'name' => $speciality_types[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
