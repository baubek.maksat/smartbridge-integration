<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\NationalityLang;
use App\Models\Nationality;

class NationalitySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$nationalities = [
			[
				'main' => [
					'id' => 101
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қазақтар',
					],[
						'lang' => 'ru',
						'name' => 'Казахи',
					],[
						'lang' => 'en',
						'name' => 'Kazakhs'
					]
				]
			],[
				'main' => [
					'id' => 102
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Украиндер',
					],[
						'lang' => 'ru',
						'name' => 'Украинцы',
					],[
						'lang' => 'en',
						'name' => 'Ukrainians'
					]
				]
			],[
				'main' => [
					'id' => 103
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Белорустар',
					],[
						'lang' => 'ru',
						'name' => 'Белорусы',
					],[
						'lang' => 'en',
						'name' => 'Belarusians'
					]
				]
			],[
				'main' => [
					'id' => 104
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Өзбектер',
					],[
						'lang' => 'ru',
						'name' => 'Узбеки',
					],[
						'lang' => 'en',
						'name' => 'Uzbeks'
					]
				]
			],[
				'main' => [
					'id' => 105
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Орыстар',
					],[
						'lang' => 'ru',
						'name' => 'Русские',
					],[
						'lang' => 'en',
						'name' => 'Russians'
					]
				]
			],[
				'main' => [
					'id' => 106
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Грузиндер',
					],[
						'lang' => 'ru',
						'name' => 'Грузины',
					],[
						'lang' => 'en',
						'name' => 'Georgians'
					]
				]
			],[
				'main' => [
					'id' => 107
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Әзірбайжандар',
					],[
						'lang' => 'ru',
						'name' => 'Азербайджанцы',
					],[
						'lang' => 'en',
						'name' => 'Azerbaijanians'
					]
				]
			],[
				'main' => [
					'id' => 108
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Литвандар',
					],[
						'lang' => 'ru',
						'name' => 'Литовцы',
					],[
						'lang' => 'en',
						'name' => 'Lithuanians'
					]
				]
			],[
				'main' => [
					'id' => 109
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Молдавандар',
					],[
						'lang' => 'ru',
						'name' => 'Молдаване',
					],[
						'lang' => 'en',
						'name' => 'Moldavians'
					]
				]
			],[
				'main' => [
					'id' => 110
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Латыштар',
					],[
						'lang' => 'ru',
						'name' => 'Латышы',
					],[
						'lang' => 'en',
						'name' => 'Latvians'
					]
				]
			],[
				'main' => [
					'id' => 111
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қырғыздар',
					],[
						'lang' => 'ru',
						'name' => 'Кыргызы',
					],[
						'lang' => 'en',
						'name' => 'Kyrgyz'
					]
				]
			],[
				'main' => [
					'id' => 112
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Тәжіктер',
					],[
						'lang' => 'ru',
						'name' => 'Таджики',
					],[
						'lang' => 'en',
						'name' => 'Tajiks'
					]
				]
			],[
				'main' => [
					'id' => 113
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Армяндар',
					],[
						'lang' => 'ru',
						'name' => 'Армяне',
					],[
						'lang' => 'en',
						'name' => 'Armenians'
					]
				]
			],[
				'main' => [
					'id' => 114
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Түркімендер',
					],[
						'lang' => 'ru',
						'name' => 'Туркмены',
					],[
						'lang' => 'en',
						'name' => 'Turkmens'
					]
				]
			],[
				'main' => [
					'id' => 115
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Эстондар',
					],[
						'lang' => 'ru',
						'name' => 'Эстонцы',
					],[
						'lang' => 'en',
						'name' => 'Estonians'
					]
				]
			],[
				'main' => [
					'id' => 116
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Абхаздар',
					],[
						'lang' => 'ru',
						'name' => 'Абхазы',
					],[
						'lang' => 'en',
						'name' => 'Abkhazians'
					]
				]
			],[
				'main' => [
					'id' => 117
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Балкарлар',
					],[
						'lang' => 'ru',
						'name' => 'Балкарцы',
					],[
						'lang' => 'en',
						'name' => 'Balkars'
					]
				]
			],[
				'main' => [
					'id' => 118
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Башқұрттар',
					],[
						'lang' => 'ru',
						'name' => 'Башкиры',
					],[
						'lang' => 'en',
						'name' => 'Bashkirs'
					]
				]
			],[
				'main' => [
					'id' => 119
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Буряттар',
					],[
						'lang' => 'ru',
						'name' => 'Буряты',
					],[
						'lang' => 'en',
						'name' => 'Buryats'
					]
				]
			],[
				'main' => [
					'id' => 120
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Аварлар',
					],[
						'lang' => 'ru',
						'name' => 'Аварцы',
					],[
						'lang' => 'en',
						'name' => 'Avars'
					]
				]
			],[
				'main' => [
					'id' => 121
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Агулер',
					],[
						'lang' => 'ru',
						'name' => 'Агулы',
					],[
						'lang' => 'en',
						'name' => 'Aguls'
					]
				]
			],[
				'main' => [
					'id' => 122
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Даргиндер',
					],[
						'lang' => 'ru',
						'name' => 'Даргинцы',
					],[
						'lang' => 'en',
						'name' => 'Dargins'
					]
				]
			],[
				'main' => [
					'id' => 123
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Құмықтар',
					],[
							'lang' => 'ru',
							'name' => 'Кумыки',
					],[
						'lang' => 'en',
						'name' => 'Kumyks'
					]
				]
			],[
				'main' => [
					'id' => 124
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Лактар',
					],[
							'lang' => 'ru',
							'name' => 'Лакцы',
					],[
						'lang' => 'en',
						'name' => 'Laks'
					]
				]
			],[
				'main' => [
					'id' => 125
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Лезгиндер',
					],[
							'lang' => 'ru',
							'name' => 'Лезгины',
					],[
						'lang' => 'en',
						'name' => 'Lezgins'
					]
				]
			],[
				'main' => [
					'id' => 126
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Hоғайлар',
					],[
						'lang' => 'ru',
						'name' => 'Hогайцы',
					],[
						'lang' => 'en',
						'name' => 'Nogais'
					]
				]
			],[
				'main' => [
					'id' => 127
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Рутулдар',
					],[
						'lang' => 'ru',
						'name' => 'Рутульцы',
					],[
						'lang' => 'en',
						'name' => 'Rutuls'
					]
				]
			],[
				'main' => [
					'id' => 128
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Табасарандар',
					],[
						'lang' => 'ru',
						'name' => 'Табасараны',
					],[
						'lang' => 'en',
						'name' => 'Tabasarans'
					]
				]
			],[
				'main' => [
					'id' => 129
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Цахурлар',
					],[
						'lang' => 'ru',
						'name' => 'Цахуры',
					],[
						'lang' => 'en',
						'name' => 'Tsakhurs'
					]
				]
			],[
				'main' => [
					'id' => 130
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ингуштер',
					],[
						'lang' => 'ru',
						'name' => 'Ингуши',
					],[
						'lang' => 'en',
						'name' => 'Ingushes'
					]
				]
			],[
				'main' => [
					'id' => 131
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Кабардиндер',
					],[
						'lang' => 'ru',
						'name' => 'Кабардинцы',
					],[
						'lang' => 'en',
						'name' => 'Kabardians'
					]
				]
			],[
				'main' => [
					'id' => 132
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қалмақтар',
					],[
						'lang' => 'ru',
						'name' => 'Калмыки',
					],[
						'lang' => 'en',
						'name' => 'Kalmyks'
					]
				]
			],[
				'main' => [
					'id' => 133
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қарақалпақтар',
					],[
						'lang' => 'ru',
						'name' => 'Каракалпаки',
					],[
						'lang' => 'en',
						'name' => 'Karakalpaks'
					]
				]
			],[
				'main' => [
					'id' => 134
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Карелдер',
					],[
						'lang' => 'ru',
						'name' => 'Карелы',
					],[
						'lang' => 'en',
						'name' => 'Karelians'
					]
				]
			],[
				'main' => [
					'id' => 135
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Комилер',
					],[
						'lang' => 'ru',
						'name' => 'Коми',
					],[
						'lang' => 'en',
						'name' => 'Komi'
					]
				]
			],[
				'main' => [
					'id' => 136
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Марийлер',
					],[
						'lang' => 'ru',
						'name' => 'Марийцы',
					],[
						'lang' => 'en',
						'name' => 'Maris'
					]
				]
			],[
				'main' => [
					'id' => 137
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Мордвалар',
					],[
						'lang' => 'ru',
						'name' => 'Мордва',
					],[
						'lang' => 'en',
						'name' => 'Mordovans'
					]
				]
			],[
				'main' => [
					'id' => 138
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Осетиндер',
					],[
						'lang' => 'ru',
						'name' => 'Осетины',
					],[
						'lang' => 'en',
						'name' => 'Ossetians'
					]
				]
			],[
				'main' => [
					'id' => 139
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Татарлар',
					],[
						'lang' => 'ru',
						'name' => 'Татары',
					],[
						'lang' => 'en',
						'name' => 'Tatars'
					]
				]
			],[
				'main' => [
					'id' => 140
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Тувалар',
					],[
						'lang' => 'ru',
						'name' => 'Тувинцы',
					],[
						'lang' => 'en',
						'name' => 'Tuvans'
					]
				]
			],[
				'main' => [
					'id' => 141
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Удмурттар',
					],[
						'lang' => 'ru',
						'name' => 'Удмурты',
					],[
						'lang' => 'en',
						'name' => 'Udmurts'
					]
				]
			],[
				'main' => [
					'id' => 142
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Шешендер',
					],[
						'lang' => 'ru',
						'name' => 'Чеченцы',
					],[
						'lang' => 'en',
						'name' => 'Chechens'
					]
				]
			],[
				'main' => [
					'id' => 143
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Чуваштар',
					],[
						'lang' => 'ru',
						'name' => 'Чуваши',
					],[
						'lang' => 'en',
						'name' => 'Chuvashes'
					]
				]
			],[
				'main' => [
					'id' => 144
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Саха(Якуттер)',
					],[
						'lang' => 'ru',
						'name' => 'Саха(Якуты)',
					],[
						'lang' => 'en',
						'name' => 'Sakha (Yakuts)'
					]
				]
			],[
				'main' => [
					'id' => 145
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Адыгейлер',
					],[
						'lang' => 'ru',
						'name' => 'Адыгейцы',
					],[
						'lang' => 'en',
						'name' => 'Adygei'
					]
				]
			],[
				'main' => [
					'id' => 146
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Алтайлықтар',
					],[
						'lang' => 'ru',
						'name' => 'Алтайцы',
					],[
						'lang' => 'en',
						'name' => 'Altaians'
					]
				]
			],[
				'main' => [
					'id' => 147
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Еврейлер',
					],[
						'lang' => 'ru',
						'name' => 'Евреи',
					],[
						'lang' => 'en',
						'name' => 'Jews'
					]
				]
			],[
				'main' => [
					'id' => 148
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қарашайлар',
					],[
						'lang' => 'ru',
						'name' => 'Карачаевцы',
					],[
						'lang' => 'en',
						'name' => 'Karachays'
					]
				]
			],[
				'main' => [
					'id' => 149
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Хакастар',
					],[
						'lang' => 'ru',
						'name' => 'Хакасы',
					],[
						'lang' => 'en',
						'name' => 'Khakas'
					]
				]
			],[
				'main' => [
					'id' => 150
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Черкестер',
					],[
						'lang' => 'ru',
						'name' => 'Черкесы',
					],[
						'lang' => 'en',
						'name' => 'Circassians'
					]
				]
			],[
				'main' => [
					'id' => 151
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Коми-пермяктар',
					],[
						'lang' => 'ru',
						'name' => 'Коми-пермяки',
					],[
						'lang' => 'en',
						'name' => 'Komi-perms'
					]
				]
			],[
				'main' => [
					'id' => 152
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ливтер',
					],[
						'lang' => 'ru',
						'name' => 'Ливы',
					],[
						'lang' => 'en',
						'name' => 'Livs'
					]
				]
			],[
				'main' => [
					'id' => 153
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Талыштар',
					],[
						'lang' => 'ru',
						'name' => 'Талышы',
					],[
						'lang' => 'en',
						'name' => 'Talyshs'
					]
				]
			],[
				'main' => [
					'id' => 154
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Долғандар',
					],[
						'lang' => 'ru',
						'name' => 'Долганы',
					],[
						'lang' => 'en',
						'name' => 'Dolgan'
					]
				]
			],[
				'main' => [
					'id' => 155
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Коряктар',
					],[
						'lang' => 'ru',
						'name' => 'Коряки',
					],[
						'lang' => 'en',
						'name' => 'Koryaks'
					]
				]
			],[
				'main' => [
					'id' => 156
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Мансылар',
					],[
						'lang' => 'ru',
						'name' => 'Манси',
					],[
						'lang' => 'en',
						'name' => 'Mansis'
					]
				]
			],[
				'main' => [
					'id' => 157
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Hенцтер',
					],[
						'lang' => 'ru',
						'name' => 'Hенцы',
					],[
						'lang' => 'en',
						'name' => 'Nenets'
					]
				]
			],[
				'main' => [
					'id' => 158
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Хантылар',
					],[
						'lang' => 'ru',
						'name' => 'Ханты',
					],[
						'lang' => 'en',
						'name' => 'Khanty'
					]
				]
			],[
				'main' => [
					'id' => 159
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Чукчалар',
					],[
						'lang' => 'ru',
						'name' => 'Чукчи',
					],[
						'lang' => 'en',
						'name' => 'Chukchi'
					]
				]
			],[
				'main' => [
					'id' => 160
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Эвенкілер',
					],[
						'lang' => 'ru',
						'name' => 'Эвенки',
					],[
						'lang' => 'en',
						'name' => 'Evenks'
					]
				]
			],[
				'main' => [
					'id' => 161
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Алеуттер',
					],[
						'lang' => 'ru',
						'name' => 'Алеуты',
					],[
						'lang' => 'en',
						'name' => 'Aleuts'
					]
				]
			],[
				'main' => [
					'id' => 162
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ительмендер',
					],[
						'lang' => 'ru',
						'name' => 'Ительмены',
					],[
						'lang' => 'en',
						'name' => 'Itelmens'
					]
				]
			],[
				'main' => [
					'id' => 163
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Кеттер',
					],[
						'lang' => 'ru',
						'name' => 'Кеты',
					],[
						'lang' => 'en',
						'name' => 'Kets'
					]
				]
			],[
				'main' => [
					'id' => 164
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Hанайлар',
					],[
						'lang' => 'ru',
						'name' => 'Hанайцы',
					],[
						'lang' => 'en',
						'name' => 'Nanai'
					]
				]
			],[
				'main' => [
					'id' => 165
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Hганасандар',
					],[
						'lang' => 'ru',
						'name' => 'Hганасаны',
					],[
						'lang' => 'en',
						'name' => 'Nganasans'
					]
				]
			],[
				'main' => [
					'id' => 166
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Hегидалдықтар',
					],[
						'lang' => 'ru',
						'name' => 'Hегидальцы',
					],[
						'lang' => 'en',
						'name' => 'Negidals'
					]
				]
			],[
				'main' => [
					'id' => 167
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Hивхтер',
					],[
						'lang' => 'ru',
						'name' => 'Hивхи',
					],[
						'lang' => 'en',
						'name' => 'Nivkhs'
					]
				]
			],[
				'main' => [
					'id' => 168
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ульта(Ороктар)',
					],[
						'lang' => 'ru',
						'name' => 'Ульта(Орки)',
					],[
						'lang' => 'en',
						'name' => 'Ulta (Orks)'
					]
				]
			],[
				'main' => [
					'id' => 169
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Орочтар',
					],[
						'lang' => 'ru',
						'name' => 'Орочи',
					],[
						'lang' => 'en',
						'name' => 'Orochs'
					]
				]
			],[
				'main' => [
					'id' => 170
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Саамдар',
					],[
						'lang' => 'ru',
						'name' => 'Саами',
					],[
						'lang' => 'en',
						'name' => 'Sami'
					]
				]
			],[
				'main' => [
					'id' => 171
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Селькуптер',
					],[
						'lang' => 'ru',
						'name' => 'Селькупы',
					],[
						'lang' => 'en',
						'name' => 'Selkups'
					]
				]
			],[
				'main' => [
					'id' => 172
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Тофалар',
					],[
						'lang' => 'ru',
						'name' => 'Тофалары',
					],[
						'lang' => 'en',
						'name' => 'Tofalars'
					]
				]
			],[
				'main' => [
					'id' => 173
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Удэгейлер',
					],[
						'lang' => 'ru',
						'name' => 'Удэгейцы',
					],[
						'lang' => 'en',
						'name' => 'Udege'
					]
				]
			],[
				'main' => [
					'id' => 174
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ульчалар',
					],[
						'lang' => 'ru',
						'name' => 'Ульчи',
					],[
						'lang' => 'en',
						'name' => 'Ulchi'
					]
				]
			],[
				'main' => [
					'id' => 175
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Чувандар',
					],[
						'lang' => 'ru',
						'name' => 'Чуванцы',
					],[
						'lang' => 'en',
						'name' => 'Chuvans'
					]
				]
			],[
				'main' => [
					'id' => 176
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Эвендер',
					],[
						'lang' => 'ru',
						'name' => 'Эвены',
					],[
						'lang' => 'en',
						'name' => 'Evens'
					]
				]
			],[
				'main' => [
					'id' => 177
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Эндер',
					],[
						'lang' => 'ru',
						'name' => 'Энцы',
					],[
						'lang' => 'en',
						'name' => 'Enets'
					]
				]
			],[
				'main' => [
					'id' => 178
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Эскимостар',
					],[
						'lang' => 'ru',
						'name' => 'Эскимосы',
					],[
						'lang' => 'en',
						'name' => 'Eskimos'
					]
				]
			],[
				'main' => [
					'id' => 179
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Юкагирлер',
					],[
						'lang' => 'ru',
						'name' => 'Юкагиры',
					],[
						'lang' => 'en',
						'name' => 'Yukagirs'
					]
				]
			],[
				'main' => [
					'id' => 180
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Абазиндер',
					],[
						'lang' => 'ru',
						'name' => 'Абазины',
					],[
						'lang' => 'en',
						'name' => 'Abazins'
					]
				]
			],[
				'main' => [
					'id' => 181
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Вепсілер',
					],[
						'lang' => 'ru',
						'name' => 'Вепсы',
					],[
						'lang' => 'en',
						'name' => 'Veps'
					]
				]
			],[
				'main' => [
					'id' => 182
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Гагауздар',
					],[
						'lang' => 'ru',
						'name' => 'Гагаузы',
					],[
						'lang' => 'en',
						'name' => 'Gagauzes'
					]
				]
			],[
				'main' => [
					'id' => 183
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Тау еврейлері',
					],[
						'lang' => 'ru',
						'name' => 'Евреи горские',
					],[
						'lang' => 'en',
						'name' => 'Mountain Jews'
					]
				]
			],[
				'main' => [
					'id' => 184
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Грузин еврейлері',
					],[
						'lang' => 'ru',
						'name' => 'Евреи грузинские',
					],[
						'lang' => 'en',
						'name' => 'Georgian Jews'
					]
				]
			],[
				'main' => [
					'id' => 185
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Орта Азия еврейлері',
					],[
						'lang' => 'ru',
						'name' => 'Евреи среднеазиатские',
					],[
						'lang' => 'en',
						'name' => 'Central Asian Jews'
					]
				]
			],[
				'main' => [
					'id' => 186
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ижорлар',
					],[
						'lang' => 'ru',
						'name' => 'Ижорцы',
					],[
						'lang' => 'en',
						'name' => 'Izhorians'
					]
				]
			],[
				'main' => [
					'id' => 187
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Караимдар',
					],[
						'lang' => 'ru',
						'name' => 'Караимы',
					],[
						'lang' => 'en',
						'name' => 'Karaites'
					]
				]
			],[
				'main' => [
					'id' => 188
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қырымшақтар',
					],[
						'lang' => 'ru',
						'name' => 'Крымчаки',
					],[
						'lang' => 'en',
						'name' => 'Krymchaks'
					]
				]
			],[
				'main' => [
					'id' => 189
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қырым татарлары',
					],[
						'lang' => 'ru',
						'name' => 'Татары крымские',
					],[
						'lang' => 'en',
						'name' => 'Crimean Tatars'
					]
				]
			],[
				'main' => [
					'id' => 190
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Таттар',
					],[
						'lang' => 'ru',
						'name' => 'Таты',
					],[
						'lang' => 'en',
						'name' => 'Tats'
					]
				]
			],[
				'main' => [
					'id' => 191
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Удиндер',
					],[
						'lang' => 'ru',
						'name' => 'Удины',
					],[
						'lang' => 'en',
						'name' => 'Udis'
					]
				]
			],[
				'main' => [
					'id' => 192
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Сығандар',
					],[
						'lang' => 'ru',
						'name' => 'Цыгане',
					],[
						'lang' => 'en',
						'name' => 'Roma'
					]
				]
			],[
				'main' => [
					'id' => 193
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Шорлар',
					],[
						'lang' => 'ru',
						'name' => 'Шорцы',
					],[
						'lang' => 'en',
						'name' => 'Shors'
					]
				]
			],[
				'main' => [
					'id' => 194
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Австриялықтар',
					],[
						'lang' => 'ru',
						'name' => 'Австрийцы',
					],[
						'lang' => 'en',
						'name' => 'Austrians'
					]
				]
			],[
				'main' => [
					'id' => 195
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Албандар',
					],[
						'lang' => 'ru',
						'name' => 'Албанцы',
					],[
						'lang' => 'en',
						'name' => 'Albanians'
					]
				]
			],[
				'main' => [
					'id' => 196
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Америкалықтар',
					],[
						'lang' => 'ru',
						'name' => 'Американцы',
					],[
						'lang' => 'en',
						'name' => 'Americans'
					]
				]
			],[
				'main' => [
					'id' => 197
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ағылшындар',
					],[
						'lang' => 'ru',
						'name' => 'Англичане',
					],[
						'lang' => 'en',
						'name' => 'British'
					]
				]
			],[
				'main' => [
					'id' => 198
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Арабтар',
					],[
						'lang' => 'ru',
						'name' => 'Арабы',
					],[
						'lang' => 'en',
						'name' => 'Arabs'
					]
				]
			],[
				'main' => [
					'id' => 199
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ассирийлер',
					],[
						'lang' => 'ru',
						'name' => 'Ассирийцы',
					],[
						'lang' => 'en',
						'name' => 'Assyrians'
					]
				]
			],[
				'main' => [
					'id' => 200
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ауғандар',
					],[
						'lang' => 'ru',
						'name' => 'Афганцы',
					],[
						'lang' => 'en',
						'name' => 'Afghans'
					]
				]
			],[
				'main' => [
					'id' => 201
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Белуджилер',
					],[
						'lang' => 'ru',
						'name' => 'Белуджи',
					],[
						'lang' => 'en',
						'name' => 'Balochis'
					]
				]
			],[
				'main' => [
					'id' => 202
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Болгарлар',
					],[
						'lang' => 'ru',
						'name' => 'Болгары',
					],[
						'lang' => 'en',
						'name' => 'Bulgarians'
					]
				]
			],[
				'main' => [
					'id' => 203
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Венгрлер',
					],[
						'lang' => 'ru',
						'name' => 'Венгры',
					],[
						'lang' => 'en',
						'name' => 'Hungarians'
					]
				]
			],[
				'main' => [
					'id' => 204
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Вьетнамдар',
					],[
						'lang' => 'ru',
						'name' => 'Вьетнамцы',
					],[
						'lang' => 'en',
						'name' => 'Vietnamese'
					]
				]
			],[
				'main' => [
					'id' => 205
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Голланттар',
					],[
						'lang' => 'ru',
						'name' => 'Голландцы',
					],[
						'lang' => 'en',
						'name' => 'Dutches'
					]
				]
			],[
				'main' => [
					'id' => 206
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Гректер',
					],[
						'lang' => 'ru',
						'name' => 'Греки',
					],[
						'lang' => 'en',
						'name' => 'Greeks'
					]
				]
			],[
				'main' => [
					'id' => 207
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Дүңгендер',
					],[
						'lang' => 'ru',
						'name' => 'Дунгане',
					],[
						'lang' => 'en',
						'name' => 'Dungans'
					]
				]
			],[
				'main' => [
					'id' => 208
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Үндістан мен Пәкістан халықтары',
					],[
						'lang' => 'ru',
						'name' => 'Hароды Индии и Пакистана',
					],[
						'lang' => 'en',
						'name' => 'People of India and Pakistan'
					]
				]
			],[
				'main' => [
					'id' => 209
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Испандар',
					],[
						'lang' => 'ru',
						'name' => 'Испанцы',
					],[
						'lang' => 'en',
						'name' => 'Spaniards'
					]
				]
			],[
				'main' => [
					'id' => 210
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Итальяндар',
					],[
						'lang' => 'ru',
						'name' => 'Итальянцы',
					],[
						'lang' => 'en',
						'name' => 'Italians'
					]
				]
			],[
				'main' => [
					'id' => 211
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қытайлар',
					],[
						'lang' => 'ru',
						'name' => 'Китайцы',
					],[
						'lang' => 'en',
						'name' => 'Chinese'
					]
				]
			],[
				'main' => [
					'id' => 212
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Кәрістер',
					],[
						'lang' => 'ru',
						'name' => 'Корейцы',
					],[
						'lang' => 'en',
						'name' => 'Koreans'
					]
				]
			],[
				'main' => [
					'id' => 213
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Кубиндер',
					],[
						'lang' => 'ru',
						'name' => 'Кубинцы',
					],[
						'lang' => 'en',
						'name' => 'Cubans'
					]
				]
			],[
				'main' => [
					'id' => 214
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Күрттер',
					],[
						'lang' => 'ru',
						'name' => 'Курды',
					],[
						'lang' => 'en',
						'name' => 'Kurds'
					]
				]
			],[
				'main' => [
					'id' => 215
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Hемістер',
					],[
						'lang' => 'ru',
						'name' => 'Hемцы',
					],[
						'lang' => 'en',
						'name' => 'Germans'
					]
				]
			],[
				'main' => [
					'id' => 216
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Парсылар',
					],[
						'lang' => 'ru',
						'name' => 'Персы',
					],[
						'lang' => 'en',
						'name' => 'Persians'
					]
				]
			],[
				'main' => [
					'id' => 217
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Поляктар',
					],[
						'lang' => 'ru',
						'name' => 'Поляки',
					],[
						'lang' => 'en',
						'name' => 'Poles'
					]
				]
			],[
				'main' => [
					'id' => 218
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Румындар',
					],[
						'lang' => 'ru',
						'name' => 'Румыны',
					],[
						'lang' => 'en',
						'name' => 'Romanians'
					]
				]
			],[
				'main' => [
					'id' => 219
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Сербтер',
					],[
						'lang' => 'ru',
						'name' => 'Сербы',
					],[
						'lang' => 'en',
						'name' => 'Serbians'
					]
				]
			],[
				'main' => [
					'id' => 220
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Словактар',
					],[
						'lang' => 'ru',
						'name' => 'Словаки',
					],[
						'lang' => 'en',
						'name' => 'Slovaks'
					]
				]
			],[
				'main' => [
					'id' => 221
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Түріктер',
					],[
						'lang' => 'ru',
						'name' => 'Турки',
					],[
						'lang' => 'en',
						'name' => 'Turks'
					]
				]
			],[
				'main' => [
					'id' => 222
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ұйғырлар',
					],[
						'lang' => 'ru',
						'name' => 'Уйгуры',
					],[
						'lang' => 'en',
						'name' => 'Uyghurs'
					]
				]
			],[
				'main' => [
					'id' => 223
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Финдер',
					],[
						'lang' => 'ru',
						'name' => 'Финны',
					],[
						'lang' => 'en',
						'name' => 'Finns'
					]
				]
			],[
				'main' => [
					'id' => 224
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Француздар',
					],[
						'lang' => 'ru',
						'name' => 'Французы',
					],[
						'lang' => 'en',
						'name' => 'French'
					]
				]
			],[
				'main' => [
					'id' => 225
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Халха-Моңғолдар',
					],[
						'lang' => 'ru',
						'name' => 'Халха-Монголы',
					],[
						'lang' => 'en',
						'name' => ' Khalkha-Mongols'
					]
				]
			],[
				'main' => [
					'id' => 226
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Хорваттар',
					],[
						'lang' => 'ru',
						'name' => 'Хорваты',
					],[
						'lang' => 'en',
						'name' => 'Croats'
					]
				]
			],[
				'main' => [
					'id' => 227
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Чехтар',
					],[
						'lang' => 'ru',
						'name' => 'Чехи',
					],[
						'lang' => 'en',
						'name' => 'Czechs'
					]
				]
			],[
				'main' => [
					'id' => 228
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Жапондар',
					],[
						'lang' => 'ru',
						'name' => 'Японцы',
					],[
						'lang' => 'en',
						'name' => 'Japanese'
					]
				]
			],[
				'main' => [
					'id' => 229
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Датчандықтар',
					],[
						'lang' => 'ru',
						'name' => 'Датчане',
					],[
						'lang' => 'en',
						'name' => 'Danes'
					]
				]
			],[
				'main' => [
					'id' => 230
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Моңғолдар',
					],[
						'lang' => 'ru',
						'name' => 'Монголы',
					],[
						'lang' => 'en',
						'name' => 'Mongols'
					]
				]
			],[
				'main' => [
					'id' => 232
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ирандықтар',
					],[
						'lang' => 'ru',
						'name' => 'Иранцы',
					],[
						'lang' => 'en',
						'name' => 'Iranians'
					]
				]
			],[
				'main' => [
					'id' => 233
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Езидтер',
					],[
						'lang' => 'ru',
						'name' => 'Езиды',
					],[
						'lang' => 'en',
						'name' => 'Yazidis'
					]
				]
			]
		];

		if (count($nationalities) > 0) {
			for ($i = 0; $i < count($nationalities); $i++) { 
				$nationality = Nationality::query()
					->where('id', $nationalities[$i]['main']['id'])
					->first();

				if ($nationality) {
					$nationality->update($nationalities[$i]['main']);

					for ($j = 0; $j < count($nationalities[$i]['translations']); $j++) { 
						$nationality_lang = NationalityLang::query()
							->where('id', $nationalities[$i]['main']['id'])
							->where('lang', $nationalities[$i]['translations'][$j]['lang'])
							->first();

						$nationality_lang->update([
							'name' => $nationalities[$i]['translations'][$j]['name']
						]);
					}
				} else {
					$nationality = Nationality::create($nationalities[$i]['main']);

					for ($j = 0; $j < count($nationalities[$i]['translations']); $j++) { 
						NationalityLang::create([
							'id' => $nationalities[$i]['main']['id'],
							'lang' => $nationalities[$i]['translations'][$j]['lang'],
							'name' => $nationalities[$i]['translations'][$j]['name']
						]);
					}
				}
			}
		}
	}
}
