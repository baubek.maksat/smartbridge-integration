<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\RoleLang;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'main' => [
                    'id' => 101,
                    'slug' => 'system-administrator'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Жүйе әкімшісі'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Админстратор системы'
                    ],[
                        'lang' => 'en',
                        'name' => 'System administrator'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 102,
                    'slug' => 'university-administrator'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Университет әкімшісі',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Админстратор университета'
                    ],[
                        'lang' => 'en',
                        'name' => 'University administrator'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 103,
                    'slug' => 'faculty-administrator'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Факультет әкімшісі'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Админстратор факультета'
                    ],[
                        'lang' => 'en',
                        'name' => 'Faculty administrator'
                    ]
                ]
            ],[ 
                'main' => [
                    'id' => 104,
                    'slug' => 'department-administrator'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Кафедра әкімшісі',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Админстратор кафедры'
                    ],[
                        'lang' => 'en',
                        'name' => 'Department administrator'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 105,
                    'slug' => 'accountant'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Бухгалтер'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Бухгалтер'
                    ],[
                        'lang' => 'en',
                        'name' => 'Accountant'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 106,
                    'slug' => 'adviser'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Эдвайзер',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Эдвайзер',
                    ],[
                        'lang' => 'en',
                        'name' => 'Adviser'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 107,
                    'slug' => 'teacher'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Мұғалім'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Преподаватель'
                    ],[
                        'lang' => 'en',
                        'name' => 'Teacher'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 108,
                    'slug' => 'student'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Студент'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Студент',
                    ],[
                        'lang' => 'en',
                        'name' => 'Student'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 109,
                    'slug' => 'enrollee'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Тіркелуші'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Абитуриент',
                    ],[
                        'lang' => 'en',
                        'name' => 'Enrollee'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 110,
                    'slug' => 'graduate'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Түлек'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Выпускник',
                    ],[
                        'lang' => 'en',
                        'name' => 'Graduate'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 111,
                    'slug' => 'ssc-head'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'СҚКО басшысы'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Начальник ЦОС',
                    ],[
                        'lang' => 'en',
                        'name' => 'SSC head'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 112,
                    'slug' => 'ssc-administrator'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'СҚКО әкімшісі'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Админстратор ЦОС',
                    ],[
                        'lang' => 'en',
                        'name' => 'SSC administrator'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 113,
                    'slug' => 'ssc-employee'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'СҚКО қызметкері'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Сотрудник ЦОС',
                    ],[
                        'lang' => 'en',
                        'name' => 'SSC employee'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 114,
                    'slug' => 'content-manager'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Мазмұн менеджері'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Контент менеджер',
                    ],[
                        'lang' => 'en',
                        'name' => 'Сontent manager'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 999,
                    'slug' => 'no-role'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Рөлсіз'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Без роля',
                    ],[
                        'lang' => 'en',
                        'name' => 'No role'
                    ]
                ]
            ]
        ];

        for ($i = 0; $i < count($roles); $i++) { 
            $role = Role::query()
                ->where('id', $roles[$i]['main']['id'])
                ->first();

            if ($role) {
                $role->update($roles[$i]['main']);

                for ($j = 0; $j < count($roles[$i]['translations']); $j++) { 
                    $role_lang = RoleLang::query()
                        ->where('id', $roles[$i]['main']['id'])
                        ->where('lang', $roles[$i]['translations'][$j]['lang'])
                        ->first();

                    $role_lang->update([
                        'name' => $roles[$i]['translations'][$j]['name']
                    ]);
                }
            } else {
                $role = Role::create($roles[$i]['main']);

                for ($j = 0; $j < count($roles[$i]['translations']); $j++) { 
                    RoleLang::create([
                        'id' => $roles[$i]['main']['id'],
                        'lang' => $roles[$i]['translations'][$j]['lang'],
                        'name' => $roles[$i]['translations'][$j]['name']
                    ]);
                }
            }
        }
    }
}
