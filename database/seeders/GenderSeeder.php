<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\GenderLang;
use App\Models\Gender;

class GenderSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$genders = [
			[
				'main' => [
					'id' => 1
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ер'
					],[
						'lang' => 'ru',
						'name' => 'Мужской'
					],[
						'lang' => 'en',
						'name' => 'Male'
					]
				]
			],[
				'main' => [
					'id' => 2
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Әйел'
					],[
						'lang' => 'ru',
						'name' => 'Женский'
					],[
						'lang' => 'en',
						'name' => 'Female'
					]
				]
			]
		];

		for ($i = 0; $i < count($genders); $i++) { 
			$gender = Gender::query()
				->where('id', $genders[$i]['main']['id'])
				->first();

			if ($gender) {
				$gender->update($genders[$i]['main']);

				for ($j = 0; $j < count($genders[$i]['translations']); $j++) { 
					$gender_lang = GenderLang::query()
						->where('id', $genders[$i]['main']['id'])
						->where('lang', $genders[$i]['translations'][$j]['lang'])
						->first();

					$gender_lang->update([
						'name' => $genders[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$gender = Gender::create($genders[$i]['main']);

				for ($j = 0; $j < count($genders[$i]['translations']); $j++) { 
					GenderLang::create([
						'id' => $genders[$i]['main']['id'],
						'lang' => $genders[$i]['translations'][$j]['lang'],
						'name' => $genders[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
