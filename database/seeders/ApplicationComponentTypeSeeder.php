<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ApplicationComponentTypeLang;
use App\Models\ApplicationComponentType;

class ApplicationComponentTypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$application_component_types = [
			[
				'main' => [
					'id' => 101,
					'slug' => 'input-text',
					'rules' => [
						'required' => [
							0, 1
						],
						'min' => 'int',
						'max' => 'int'
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Жол'
					],[
						'lang' => 'ru',
						'name' => 'Строка'
					],[
						'lang' => 'en',
						'name' => 'String'
					]
				]
			],[
				'main' => [
					'id' => 102,
					'slug' => 'input-number',
					'rules' => [
						'required' => [
							0, 1
						],
						'min' => 'int',
						'max' => 'int'
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Саны',
					],[
						'lang' => 'ru',
						'name' => 'Число'
					],[
						'lang' => 'en',
						'name' => 'Number'
					]
				]
			],[
				'main' => [
					'id' => 103,
					'slug' => 'textarea',
					'rules' => [
						'required' => [
							0, 1
						],
						'min' => 'int',
						'max' => 'int'
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Мәтін өрісі'
					],[
						'lang' => 'ru',
						'name' => 'Текстовой поля'
					],[
						'lang' => 'en',
						'name' => 'Textarea'
					]
				]
			],[ 
				'main' => [
					'id' => 104,
					'slug' => 'select',
					'rules' => [
						'required' => [
							0, 1
						]
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Тізім',
					],[
						'lang' => 'ru',
						'name' => 'Список'
					],[
						'lang' => 'en',
						'name' => 'List'
					]
				]
			],[ 
				'main' => [
					'id' => 105,
					'slug' => 'select-multiple',
					'rules' => [
						'required' => [
							0, 1
						]
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Тізім',
					],[
						'lang' => 'ru',
						'name' => 'Список мультивыбором'
					],[
						'lang' => 'en',
						'name' => 'Multi-select list'
					]
				]
			],[
				'main' => [
					'id' => 106,
					'slug' => 'input-file',
					'rules' => [
						'required' => [
							0, 1
						]
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Файл'
					],[
						'lang' => 'ru',
						'name' => 'Файл'
					],[
						'lang' => 'en',
						'name' => 'File'
					]
				]
			],[
				'main' => [
					'id' => 107,
					'slug' => 'input-files',
					'rules' => [
						'required' => [
							0, 1
						]
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Файлдар'
					],[
						'lang' => 'ru',
						'name' => 'Файлы'
					],[
						'lang' => 'en',
						'name' => 'Files'
					]
				]
			],[
				'main' => [
					'id' => 108,
					'slug' => 'input-date',
					'rules' => [
						'required' => [
							0, 1
						]
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Күні',
					],[
						'lang' => 'ru',
						'name' => 'Дата',
					],[
						'lang' => 'en',
						'name' => 'Date'
					]
				]
			],[
				'main' => [
					'id' => 109,
					'slug' => 'input-time',
					'rules' => [
						'required' => [
							0, 1
						]
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Уақыт'
					],[
						'lang' => 'ru',
						'name' => 'Время'
					],[
						'lang' => 'en',
						'name' => 'Time'
					]
				]
			],[
				'main' => [
					'id' => 110,
					'slug' => 'resource',
					'rules' => [
						'required' => [
							0, 1
						],
						'resources' => [
							'student-subjects',
							'student-teachers',
							'all-specialities',
							'all-study-forms',
							'all-payment-forms',
							'all-study-languages',
							'all-specialties-except-for-the-student\'s-specialty',
							'all-study-languages-except-for-the-student\'s-specialty'
						]
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ресурс'
					],[
						'lang' => 'ru',
						'name' => 'Ресурс',
					],[
						'lang' => 'en',
						'name' => 'Resource'
					]
				]
			],[
				'main' => [
					'id' => 111,
					'slug' => 'resource-multiple',
					'rules' => [
						'required' => [
							0, 1
						],
						'resources' => [
							'student-subjects',
							'student-teachers',
							'all-specialities',
							'all-study-forms',
							'all-payment-forms',
							'all-study-languages',
							'all-specialties-except-for-the-student\'s-specialty',
							'all-study-languages-except-for-the-student\'s-specialty'
						]
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Көп таңдалған ресурс'
					],[
						'lang' => 'ru',
						'name' => 'Ресурс мультивыбором',
					],[
						'lang' => 'en',
						'name' => 'Multi-select resource'
					]
				]
			]
		];

		for ($i = 0; $i < count($application_component_types); $i++) { 
			$application_component_type = ApplicationComponentType::query()
				->where('id', $application_component_types[$i]['main']['id'])
				->first();

			if ($application_component_type) {
				$application_component_type->update($application_component_types[$i]['main']);

				for ($j = 0; $j < count($application_component_types[$i]['translations']); $j++) { 
					$application_component_type_lang = ApplicationComponentTypeLang::query()
						->where('id', $application_component_types[$i]['main']['id'])
						->where('lang', $application_component_types[$i]['translations'][$j]['lang'])
						->first();

					if ($application_component_type_lang) {
						$application_component_type_lang->update([
							'name' => $application_component_types[$i]['translations'][$j]['name']
						]);
					}
				}
			} else {
				$application_component_type = ApplicationComponentType::create($application_component_types[$i]['main']);

				for ($j = 0; $j < count($application_component_types[$i]['translations']); $j++) { 
					ApplicationComponentTypeLang::create([
						'id' => $application_component_types[$i]['main']['id'],
						'lang' => $application_component_types[$i]['translations'][$j]['lang'],
						'name' => $application_component_types[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
