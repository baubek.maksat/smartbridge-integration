<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SeeTypeLang;
use App\Models\SeeType;

class SeeTypeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$see_types = [
			[
				'main' => [
					'id' => 101
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ұлттық бірыңғай тестілеу',
					],[
						'lang' => 'ru',
						'name' => 'Единое национальное тестирование',
					],[
						'lang' => 'en',
						'name' => 'Unified national testing'
					]
				]
			],[
				'main' => [
					'id' => 102
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Талапкерлерді кешенді тестілеу',
					],[
						'lang' => 'ru',
						'name' => 'Комплексное тестирование абитуриентов',
					],[
						'lang' => 'en',
						'name' => 'Complex testing of entrants'
					]
				]
			]
		];

		if (count($see_types) > 0) {
			for ($i = 0; $i < count($see_types); $i++) { 
				$see_type = SeeType::query()
					->where('id', $see_types[$i]['main']['id'])
					->first();

				if ($see_type) {
					$see_type->update($see_types[$i]['main']);

					for ($j = 0; $j < count($see_types[$i]['translations']); $j++) { 
						$see_type_lang = SeeTypeLang::query()
							->where('id', $see_types[$i]['main']['id'])
							->where('lang', $see_types[$i]['translations'][$j]['lang'])
							->first();

						$see_type_lang->update([
							'name' => $see_types[$i]['translations'][$j]['name']
						]);
					}
				} else {
					$see_type = SeeType::create($see_types[$i]['main']);

					for ($j = 0; $j < count($see_types[$i]['translations']); $j++) { 
						SeeTypeLang::create([
							'id' => $see_types[$i]['main']['id'],
							'lang' => $see_types[$i]['translations'][$j]['lang'],
							'name' => $see_types[$i]['translations'][$j]['name']
						]);
					}
				}
			}
		}
	}
}
