<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SecLang;
use App\Models\Sec;

class SecSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $secs = [
            [
                'main' => [
                    'id' => 100000001,
                    'user_id' => 100000001,
                    'school_id' => 100000001,
                    'series' => 'ЖОБ',
                    'number' => '0785427',
                    'date_of_issue' => '2014-05-25',
                    'average_score' => null,
                    'with_distinction' => '0',
                    'gold_mark' => '0'
                ]
            ]
        ];

        if (count($secs) > 0) {
            for ($i = 0; $i < count($secs); $i++) { 
                $sec = Sec::query()
                    ->where('id', $secs[$i]['main']['id'])
                    ->first();

                if ($sec) {
                    $sec ->update($secs[$i]['main']);
                } else {
					$sec = Sec::create($secs[$i]['main']);
				}
            }
        }
    }
}
