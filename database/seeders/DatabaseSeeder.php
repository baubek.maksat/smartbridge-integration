<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //	Ядро

        $this->call([
			LangSeeder::class,
			StateSeeder::class,
			TypeSeeder::class,
			WeekSeeder::class,
			PlatformSeeder::class,
			RoleSeeder::class,
			GenderSeeder::class,
			MaritalStatusSeeder::class,
			MaritalStatusGenderSeeder::class,
			NationalitySeeder::class,
			CountrySeeder::class,
			//	CatoSeeder::class,
			IdTypeSeeder::class,
			IdIssuingAuthoritySeeder::class,
			UserSeeder::class,
			StudyLanguageSeeder::class,
			WorkerPositionSeeder::class,
			NotificationViewSeeder::class,
			IdTypeCountrySeeder::class,
			UserLinkTypeSeeder::class,
			SeeTypeSeeder::class,
			SecSeeder::class
		]);

		//  Центр обслуживания студентов

		$this->call([
			ApplicationStatusSeeder::class,
			ApplicationComponentTypeSeeder::class,
			ApplicationActionSeeder::class,
			//	StudentServiceCenterRoleAccessSeeder::class,
		]);

		//  Финансы

		$this->call([
			AccountViewSeeder::class,
			TransferViewSeeder::class,
			InvoiceStatusSeeder::class,
			TransactionViewSeeder::class,
			ReplenishmentViewSeeder::class
		]);
    }
}
