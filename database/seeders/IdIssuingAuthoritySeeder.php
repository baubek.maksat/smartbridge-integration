<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\IdIssuingAuthorityLang;
use App\Models\IdIssuingAuthority;

class IdIssuingAuthoritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id_issuing_authorities = [
            [
                'main' => [
                    'id' => 101,
                    'country_id' => 316
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Қазақстан Республикасы Ішкі Істер Министрлігі',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Министерство внутренних дел Республики Казахстан',
                    ],[
                        'lang' => 'en',
                        'name' => 'Ministry of Internal Affairs of the Republic of Kazakhstan'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 102,
                    'country_id' => 316
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Қазақстан Республикасы Әділет министрлігі',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Министерство юстиции Республики Казахстан',
                    ],[
                        'lang' => 'en',
                        'name' => 'Ministry of Justice of the Republic of Kazakhstan'
                    ]
                ]
            ]
        ];

        for ($i = 0; $i < count($id_issuing_authorities); $i++) { 
            $id_issuing_authority = IdIssuingAuthority::query()
                ->where('id', $id_issuing_authorities[$i]['main']['id'])
                ->first();

            if ($id_issuing_authority) {
                $id_issuing_authority->update($id_issuing_authorities[$i]['main']);

                for ($j = 0; $j < count($id_issuing_authorities[$i]['translations']); $j++) { 
                    $id_issuing_authority_lang = IdIssuingAuthorityLang::query()
                        ->where('id', $id_issuing_authorities[$i]['main']['id'])
                        ->where('lang', $id_issuing_authorities[$i]['translations'][$j]['lang'])
                        ->first();

                    $id_issuing_authority_lang->update([
                        'name' => $id_issuing_authorities[$i]['translations'][$j]['name']
                    ]);
                }
            } else {
                $id_issuing_authority = IdIssuingAuthority::create([
                    'id' => $id_issuing_authorities[$i]['main']['id'],
                    'country_id' => $id_issuing_authorities[$i]['main']['country_id']
                ]);

                for ($j = 0; $j < count($id_issuing_authorities[$i]['translations']); $j++) { 
                    IdIssuingAuthorityLang::create([
                        'id' => $id_issuing_authorities[$i]['main']['id'],
                        'lang' => $id_issuing_authorities[$i]['translations'][$j]['lang'],
                        'name' => $id_issuing_authorities[$i]['translations'][$j]['name']
                    ]);
                }
            }
        }
    }
}
