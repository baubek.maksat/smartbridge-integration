<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Lang;

class LangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langs = [
            [
                'main' => [
                    'id' => 101,
                    'slug' => 'kz',
                    'name' => 'Қазақша'
                ]
            ],[
                'main' => [
                    'id' => 102,
                    'slug' => 'ru',
                    'name' => 'Русский'
                ]
            ],[
                'main' => [
                    'id' => 103,
                    'slug' => 'en',
                    'name' => 'English'
                ]
            ]
        ];

        if (count($langs) > 0) {
            for ($i = 0; $i < count($langs); $i++) { 
                $lang = Lang::query()
                    ->where('id', $langs[$i]['main']['id'])
                    ->first();

                if ($lang) {
                    $lang->update($langs[$i]['main']);
                } else {
                    $lang = Lang::create($langs[$i]['main']);
                }
            }
        }
    }
}
