<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\WorkerPositionLang;
use App\Models\WorkerPosition;

class WorkerPositionSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$worker_positions = [
			[
				'main' => [
					'id' => 101,
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Президент',
					],[
						'lang' => 'ru',
						'name' => 'Президент',
					],[
						'lang' => 'en',
						'name' => 'President'
					]
				]
			],[
				'main' => [
					'id' => 102,
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Ректор',
					],[
						'lang' => 'ru',
						'name' => 'Ректор',
					],[
						'lang' => 'en',
						'name' => 'Rector'
					]
				]
			],[
				'main' => [
					'id' => 103,
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Провост',
					],[
						'lang' => 'ru',
						'name' => 'Провост',
					],[
						'lang' => 'en',
						'name' => 'Provost'
					]
				]
			],[
				'main' => [
					'id' => 104,
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Директор',
					],[
						'lang' => 'ru',
						'name' => 'Директор',
					],[
						'lang' => 'en',
						'name' => 'Director'
					]
				]
			],[
				'main' => [
					'id' => 105,
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Декан',
					],[
						'lang' => 'ru',
						'name' => 'Декан',
					],[
						'lang' => 'en',
						'name' => 'Dean'
					]
				]
			],[
				'main' => [
					'id' => 106,
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Кафедра меңгерушісі',
					],[
						'lang' => 'ru',
						'name' => 'Заведующий кафедрой',
					],[
						'lang' => 'en',
						'name' => 'Head of department'
					]
				]
			],[
				'main' => [
					'id' => 107,
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Жетекші кеңесші',
					],[
						'lang' => 'ru',
						'name' => 'Ведущий консультант',
					],[
						'lang' => 'en',
						'name' => 'Lead Consultant'
					]
				]
			],[
				'main' => [
					'id' => 108,
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Студенттер жөніндегі бухгалтер',
					],[
						'lang' => 'ru',
						'name' => 'Бухгалтер по работе со студентами',
					],[
						'lang' => 'en',
						'name' => 'Student Accountant'
					]
				]
			]
		];

		if (count($worker_positions) > 0) {
			for ($i = 0; $i < count($worker_positions); $i++) { 
				$worker_position = WorkerPosition::query()
					->where('id', $worker_positions[$i]['main']['id'])
					->first();

				if ($worker_position) {
					for ($j = 0; $j < count($worker_positions[$i]['translations']); $j++) { 
						$worker_position_lang = WorkerPositionLang::query()
							->where('id', $worker_positions[$i]['main']['id'])
							->where('lang', $worker_positions[$i]['translations'][$j]['lang'])
							->first();

						$worker_position_lang->update([
							'name' => $worker_positions[$i]['translations'][$j]['name']
						]);
					}
				} else {
					$worker_position = WorkerPosition::create($worker_positions[$i]['main']);

					for ($j = 0; $j < count($worker_positions[$i]['translations']); $j++) { 
						WorkerPositionLang::create([
							'id' => $worker_positions[$i]['main']['id'],
							'lang' => $worker_positions[$i]['translations'][$j]['lang'],
							'name' => $worker_positions[$i]['translations'][$j]['name']
						]);
					}
				}
			}
		}
	}
}
